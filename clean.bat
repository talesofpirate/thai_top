echo clean # client
del client\system\* /Q
del client\*.log /Q


echo clean # server
del server\*.loc /Q
del server\*.exe /Q
del server\*.pdb /Q
del server\*.dll /Q

echo clean # translation
del translation\*.dll /Q



echo # source
del source\bin  /Q
del source\build /Q
del source\lib\luajit /Q
del source\lib\CaLua /Q
del source\lib\Common /Q
del source\lib\EncLib /Q
del source\lib\ICUHelper /Q
del source\lib\InfoNet /Q
del source\lib\LIBDBC /Q
del source\lib\LightEngine /Q
del source\lib\Status /Q
del source\lib\VimD3DLib /Q
del source\lib\logutil /Q
del source\inc\luajit /Q
del source\lib\Win32\AudioSDL /Q
del source\lib\Win32\Common /Q
del source\lib\Win32\EncLib /Q
del source\lib\Win32\ICUHelper /Q
del source\lib\Win32\InfoNet /Q
del source\lib\Win32\LIBDBC /Q
del source\lib\Win32\logutil /Q
del source\lib\Win32\MindPower3D /Q
del source\lib\Win32\Status /Q
del source\lib\Win32\VimD3DLib /Q