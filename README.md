# Thai Top

# Install Microsoft Sql Server 2019
[Download SQL2019-SSEI-Express](https://l.facebook.com/l.php?u=https%3A%2F%2Fdownload.microsoft.com%2Fdownload%2F7%2Ff%2F8%2F7f8a9c43-8c8a-4f7c-9f92-83c18d96b681%2FSQL2019-SSEI-Expr.exe%3Ffbclid%3DIwAR0wdDAA11_8TebfnYZ7JczXJPs_aAB1qwgAIAWgDWZLYw3WYl7g1kIIo14&h=AT0mhrFsvY9Ge0DMbhtnw6gkgT4kASZMqf1rGOGX9rKORffGlDjAt87J3LJCudZ--EVWfeCWn1aE5NcW2me-8lyQVY_hNu2pyttlP7I72YQdUYo1wu1kwPvV6f1GJdIPXHnQiA)

[Download SQL Server Management Studio (SSMS) 18.9.1](https://aka.ms/ssmsfullsetup)

# Install Visual Studio 2019
Download: https://visualstudio.microsoft.com/downloads/

![vs2019](docs/images/193751621_1427852887563129_8024481025243949712_n.png)
 

# top-recode

Objective is to recode TOP/PKO .lua files in a way the syntax becomes easier to use, or that implements features that can provide more freedom of coding.

This is only intended for testing. Everything here is meant to be used in your own computer (no website for example): including the server and client.

PKO 2.7 mod is used. My default mod is still on alpha and is not supposed to be used yet.

## Goals (ATM):
- Get it working without major bugs. It is.
- Fix warnings.
- Fix some bugs.
- More later.

## WINDOWS:

### How to install:
1. Install Microsoft Visual Studio 2019 Community Edition. Then on Visual Studio Installer get: VC C++ 2019 v142 toolset, C++ Profiling tools, Windows 10 SDK (latest) for Desktop C++, Visual C++ ATL support.
2. Install and configure MSQL of any version liked - recommended https://pko.coffeecup.com/. Don't do anything regarding db accounts (except sa), dbs, ip or more yet - leave that part for later.
3. Install git for windows. Select the cmd.exe options (but no options with added unix tools please). Then clone:
4. (using cmd.exe) cd to the folder where you want the project to be, and type:
git clone "https://gitlab.com/deguix/top-recode"
5. Run with Admin priviledges the "build.bat" in the main folder (symlink creation requires admin priviledges) with the following options if needed:
   5.1. "--mssql2000": Use this only if running SQL 2000. This copies db_2000 files into dbs_in_use folder.
6. (if using SQL 2000) In all .cfg files in server folder, remove any ",1433" you see.
7. Give user full control of the dbs_in_use folder:
   7.1 Right click folder.
   7.2 Click "Properties".
   7.3 Click "Security".
   7.4 Click "Edit".
   7.5 Select "Users" at "Group or user names" listbox.
   7.6 Click the "Allow" checkbow on the "Full control" row.
   7.7 Click "OK".
   7.8 Click "OK".
8. Make sure MSQL is started at this point. Following the account creation part in the MSQL guide, create pko_game and pko_account users (for gameserver and accountserver respectivelly) with the regular password.
9. Following the db attachment in MSQL guide, attach the dbs that are in the dbs_in_use folder - on the attach window, after "opening" the files, assign gameserver db ownership to pko_game and accountserver's to pko_account.
10. Don't follow any ip editting steps or website steps - files are already configured for local use.

### How to update without any other actions (if for some reason build.bat fails and online git files are newer):
1. (using cmd.exe) cd to the project folder, and type:
git pull

### How to update and then compile src files (no symlinks are created here):
1. (using cmd.exe) cd to project folder, and type (if using SQL2000: add --mssql2000 as well):
build.bat --pull

### How to use (assumes build.bat completes successfully once):
1. Start the SQL server (if it isn't by now) and start runall.bat in server folder to start all game server exe's.
2. Run run.bat in client folder to run client.
3. Select the only server available, and put the user name *deguix* and pass *123456* and press Enter. The game should be working at this point.

## LINUX:

Copy or symlink folder from Windows. Unfortunatelly the source can only be compiled on Windows.

## Supplemental - For developers only: How to adapt mod files to use this source:
### Server lua files:
1. Make sure the scripts work on TOP2 server.
2. Copy scripts folder from the resource folder of your mod, and paste in inside this projects resource/scripts folder.
3. Rename it to whatever mod name you would like.
4. Copy initial.lua from pko_2.7 (it's a compability mod) to your recently copied mod folder.
5. Create a "maps" folder in your mod folder.
6. Create a folder for each map with its name in the "maps" folder.
7. Copy each map folder .lua files in your original files to respective map folders (that are in your "map folder).
8. (TO BE CHANGED - TODO: Give each mod independence) Copy the .txt and other files (not .lua) of your map folders to the respective map folders in the resource folder.
9. Replace all "local count = ReadByte( rpk )" with local count = ReadWord( rpk )" in ncpsdk.lua (higher item buy/sale limits).

### Client files (TO BE CHANGED - TODO: Give each mod independence):
1. Lua files have no compatibility yet (and no missions/gems yet).
2. Replace table folder of these files with your own mod's.
3. Replace any other files except lua files and system folder files with your own.

### Publishing:
1. When zipping the files, the symlinks will resolve.
