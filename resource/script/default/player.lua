--TODO: Adapt everything to use char instead of player for anything that isn't a player.

function player_firstusableid()
	--[[
	Regular ID's: Player ID is always the same one after he joins. If he leaves, the ID is freed for the next player that joins.
	Used for everything basically.
	--]]
	for i = 1, players.items, 1 do
		if players[i] == nil then
			return i
		end
	end

	players.items = players.items + 1
	return players.items
end

player_mt = {}

function player_items_report(player)
	local _bag={}
	local _equip={}
	for i=0,GetKbCap(player.role)-1 do
		_bag[i] = GetChaItem(player.role,2,i)
	end
	for i=0, 9 do --max equippable items = 10 (id 1 could be ammo, which is non-existent) TODO: Set to top 2 standards?
		_equip[i] = GetChaItem(player.role,1,i)
	end

	return {bag=_bag,equip=_equip}
end

function player_mt.__index (t,key)
	if 	key == "items_report" then
		rawset(t, key, player_items_report(t))
		--elseif 	key == "ispartyleader" then
		--	rawset(t, key, IsTeamLeader(t.role))
		--also needs command for teammates.
	else
		char_mt.__index (t,key)
	end
	return rawget(t, key)
end

function player_mt.__newindex (t, key, value)
	char_mt.__newindex (t, key, value)
	--player.interacting_with_npc -- for interactions with npc
end

function player_wrap(role)
	player = {}
	player.role = role
	player.id = -1
	for i = 1, players.items, 1 do
		if players[i] ~= nil then -- very important, check if player is nil before checking role, otherwise code silently breaks.
			if players[i].role == role then
				player.id = i
			end
		end
	end

	setmetatable(player, player_mt)
	return player
end

players = {items=0}

--TODO: Implement player functions related to quests.