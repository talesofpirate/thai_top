--[[
Enhancement system:
64 enhancement pools
Item with type 70, lvl (STR) can be used for something to be determined, experience = pool # (energy), durability = max pool # (durability).

Uses ITEMATTR_FORGE in equipment, which can have a number between 0 and 64 (0 = no gemming pool). Can contain unlimited stats.

There can't be more than 1 equipment with the same enhancement pool, and any other ones with same enhancement pool number can't be used.

Attaching enhancement pool number to equipment:
- Buy the enhancement pool from npc.
- Buy number card.
- Double-click the number card, click at enhancement pool.
- Double-click enhancement pool, click the equipment to associate the pool number with it.

Detaching:
- Buy the enhancement pool detacher from npc.
- Double-click enhancement pool, click the equipment to remove the pool number associated with it. It also creates an enhancement pool with that number.

Enhancing:
- Double-click enhancement, click the equipment (it needs to be in inventory).
--]]

gem = {}