dialog={from_char=0,to_player=0,pages={}}

dialog_page_options_mt={}
function dialog_page_options_mt.__index (t, key)
	if key > 8 then
		print("Dialog class: Number of options exceeded 8 (maximum).")
	elseif rawget(t, key) == nil then
		rawset(t, key, {})
	end
	return rawget(t, key)
end

dialog_page_quests_mt={}
function dialog_page_quests_mt.__index (t, key)
	--if key > 8 then
	--    print("Dialog class: Number of quests exceeded 8 (maximum).")
	--end
	return rawget(t, key)
end

dialog_pages_mt={}
function dialog_pages_mt.__index (t, key)
	if key > 32 then
		print("Dialog class: Number of pages exceeded 32 (maximum)")
	elseif rawget(t, key) == nil then
		rawset(t, key, {text="",options={},quests={}})
		setmetatable(t[key].options, dialog_page_options_mt)
		setmetatable(t[key].quests, dialog_page_quests_mt)
	end
	return rawget(t, key)
end

function dialog.new(self, from_char, o)
	o = o or {from_char=from_char,to_player=0,pages={}}
	setmetatable(o, self)
	self.__index = self
	setmetatable(o.pages, dialog_pages_mt)
	return o
end
--TODO: Implement option commands, quests.
function dialog.open(self, pageid, to_player)
	self.to_player = to_player or self.to_player
	local packet = packet:new()
	local count = 0
	for i,v in ipairs(self.pages[pageid].options) do
		if (v.hidden ~= true) then
			count = count + 1
		end
	end
	
	if (count > 0) then
		packet:add(DT_CMD, CMD_MC_FUNCPAGE)
	else
		packet:add(DT_CMD, CMD_MC_TALKPAGE)
	end
	packet:add(DT_4BN, GetCharID(self.from_char.role))
	packet:add(DT_BN, pageid)
	packet:add(DT_STR, self.pages[pageid].text) --self.pages[pageid].text
	if (count > 0) then
		packet:add(DT_BN, count)
		for i = 1, count, 1 do
			if self.pages[pageid].options[i] ~= nil then
				packet:add(DT_STR, self.pages[pageid].options[i][1])
			else
				packet:add(DT_STR, "")
			end
		end
	end
	local ret, scriptid = GetScriptID(self.from_char.role)
	--local ret, misnum = GetNumMission(to_player.role, self.from_char.id) --used by random missions
	local valid_quests = {}
	for n = 1, table.getn(self.pages[pageid].quests), 1 do
		if self.pages[pageid].quests[n] == true then
			--print('before GetMissioninfo at dialog.lua: ',to_player.role, self.from_char.id, n-1)
			local ret, npc_quest_id, state = GetMissionInfo(to_player.role, self.from_char.id, n-1) --ret is returned normally, thus ignore all error messages this function spits out.
			--print(ret, npc_quest_id, state)
			if ret == C_TRUE then
				table.insert(valid_quests, {npcs[scriptid].quests[npc_quest_id][1].name, state})
			end
		end
	end
	packet:add(DT_BN, table.getn(valid_quests))
	for i, quest_info in ipairs(valid_quests) do
		packet:add(DT_STR, quest_info[1])
		packet:add(DT_BN, quest_info[2])
	end
	packet:send(self.to_player)
end
function dialog.close(self)
	local packet = packet:new()
	packet:add(DT_CMD, CMD_MC_CLOSETALK)
	packet:add(DT_4BN, self.from_char.id)
	packet:send(self.to_player)
end