shop_trade={from_char=0,to_player=0,type=0,sections={{},{},{},{}}} --items in each section will have: id,amount,price,level
function shop_trade.new(self, from_char, o) --if type is of exchange: id, amount, id2, amount2, time
	o = o or {from_char=from_char,to_player=0,type=0,sections={{},{},{},{}}}
	setmetatable(o, self)
	self.__index = self
	return o
end
function shop_trade.open(self, type, to_player, cmd)
	self.to_player = to_player or self.to_player
	self.type = type or self.type
	local packet = packet:new()
	if (type == TRADE_EXCHANGE) then --TRADE_EXCHANGE is not used in-game, it's just to make this class simpler.
		cmd = cmd or CMD_MC_BLACKMARKET_EXCHANGEDATA
		packet:add(DT_CMD, cmd)
		packet:add(DT_4BN, GetCharID(self.from_char.role))
		packet:add(DT_BN, math.min(table.getn(self.sections[1]),120)) --number of trade items, max is 120 items | btw, only 1 section
		for n = 1, math.min(table.getn(self.sections[1]),120), 1 do
			if self.sections[i][n] ~= nil then
				packet:add(DT_2BN, self.sections[1][n][1]) --id
				packet:add(DT_2BN, self.sections[1][n][2]) --amount
				packet:add(DT_2BN, self.sections[1][n][3]) --id2
				packet:add(DT_2BN, self.sections[1][n][4]) --amount2
				packet:add(DT_2BN, self.sections[1][n][5]) --time
			else
				packet:add(DT_2BN, 0)
			end
		end

		if cmd == CMD_MC_BLACKMARKET_EXCHANGEUPDATE then
			packet:sync(self.to_player)
		else
			packet:send(self.to_player)
		end
	else
		cmd = cmd or CMD_MC_TRADEPAGE
		packet:add(DT_CMD, cmd)
		packet:add(DT_4BN, GetCharID(self.from_char.role))
		packet:add(DT_BN, type) -- one of the TRADE_* values
		packet:add(DT_4BN, 0) --boatid when TRADE_GOODS is used.
		packet:add(DT_BN, math.min(table.getn(self.sections),4))

		for i = 1, math.min(table.getn(self.sections),4), 1 do
			packet:add(DT_BN, i-1) --type of trade items (each section is for each type of trade items
			packet:add(DT_BN, math.min(table.getn(self.sections[i]),120)) --number of trade items, max is 120 items
			for n = 1, math.min(table.getn(self.sections[i]),120), 1 do
				if self.sections[i][n] ~= nil then
					if type == TRADE_GOODS then
						packet:add(DT_2BN, self.sections[i][n][1]) --id
						packet:add(DT_2BN, self.sections[i][n][2]) --amount
						packet:add(DT_4BN, self.sections[i][n][3]) --price
						packet:add(DT_BN, self.sections[i][n][4]) --level
					else
						packet:add(DT_2BN, self.sections[i][n][1])
					end
				else
					packet:add(DT_2BN, ROLE_INVALID_ID)
					if type == TRADE_GOODS then
						packet:add(DT_4BN, 0)
					end
				end
			end
		end

		if cmd == CMD_MC_TRADE_ALLDATA then
			packet:sync(self.to_player)
		else
			packet:send(self.to_player)
		end
	end
end

function shop_trade.update(self, type, to_player)
	local cmd = CMD_MC_TRADE_ALLDATA
	if (type == TRADE_EXCHANGE) then
		cmd = CMD_MC_BLACKMARKET_EXCHANGEUPDATE
	end
	shop_trade.open(self, type, to_player, cmd)
end

function shop_trade.close(self)
	local packet = packet:new()
	packet:add(DT_CMD, CMD_MC_CLOSETALK)
	packet:add(DT_4BN, GetCharID(self.from_char))
	packet:send(self.to_player)
end

--max text size: 2^10-1. --max option text size: 2^6-1.
--max text size without options: 5317 = 4096+1024+128+64+4+2-1 (risky - buffer overrun somewhere).
--max text size without options: 5120.

--{{"000000000000000000000000000000000000000000000000000000000000006",},{"option2",},{"option3",},{"option4",},{"option5",},{"option6",},{"option7",},{"option8",}}