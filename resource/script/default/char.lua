char_mt = {}

function char_mt.__index (t,key)
	if 	key == "mapname" then
		rawset(t, key, GetChaMapName(t.role))
	elseif 	key == "mobid" then
		rawset(t, key, GetChaID(t.role))
	elseif 	key == "id" then
		rawset(t, key, GetCharID(t.role))
	elseif 	key == "type_id" then
		rawset(t, key, GetChaTypeID(t.role))
	elseif 	key == "mapcopy" then
		rawset(t, key, GetChaMapCopy(t.role))
	elseif 	key == "partyid" then
		rawset(t, key, GetChaTeamID(t.role))
	elseif rawget(t, key) == nil then
		if 	key == "name" then
			rawset(t, key, GetChaDefaultName(t.role))
		elseif 	key == "stats" then
			local stats = {}
			setmetatable(stats, char_stats_mt)
			stats.role = t.role
			rawset(t, key, stats)
		end
	end
	return rawget(t, key)
end

function char_mt.__newindex (t, key, value)
	if key == "name" then
		print("Character Structure: Can't set name of character.")
	else
		rawset(t, key, value)
	end
end

function char_wrap(role)
	char = {}
	char.role = role

	setmetatable(char, char_mt)
	return char
end