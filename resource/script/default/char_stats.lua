attr = {}
attr[1] = {0,0} --���Ըı��б�, ���ı�10��
attr[2] ={0,0} 
attr[3] ={0,0} 
attr[4] ={0,0} 
attr[5] ={0,0}  
attr[6] ={0,0} 
attr[7] ={0,0} 
attr[8] ={0,0} 
attr[9] ={0,0} 
attr[10]={0,0} 

-- TODO (iteminfo edit):
-- Add bullets/arrows to a ring slot - thus remove 1 ring slot for it.
-- Crusaders/Champs/Clerics/SMs will have 1 item here with a stat for the based class.

-- TODO, in order:
-- Stat changes. Done.
-- Adapt old code to new, otherwise rewrite the whole thing.

-- Attribute changes:
-- No own stat increase. Buy stat increasing items (with the change below).
-- LUK added (check LUK stat increasing items).
-- Min/Max Attack merged into simply Attack (not many stat slots are available).
-- Max Attack slot replaced with Magic Attack (many changes are applied to magic attacks thus they are comparable).
-- Replaced Physical Resist with Magical Defense (physical resist is too strong vs melee).
-- Replaced Treasure Rate with real Drop Rate (useless stat).
-- Replaced Life Treasure Drop Rate with Experience Rate (useless stat).
-- Replaced Attack Distance with Critical Resist Rate (game doesn't allow custom attack distances).

-- Maybe by default, best stats should be increased. Then the user should be able to reset them.

-- TODO:
-- Gems don't add glows, what adds glows is the elemental stance or weapon mode...

-- NEW PLAN:
-- Remove classes from attributes menu. Instead, necklaces identify the class each character belongs. That means, whenever changing
-- necks, the person changes class (which includes skills, exp, lvl, stats).

-- Make each "class" reachable by certain actions - Main classes:
-- Berserker (STR): Attacks boss from near with a weapon that is too heavy for him - thus damaging him per second (2h sword).
-- Champion (CON): After luring the boss, attacks using sword and a shield.
-- Assassin (AGI): Attacks with light weapons behind the boss (like dagger).
-- Cleric (SPR): Heals those who are hurt with a healing staff.
-- Hunter (ACC): Attacks from very far with bows.
-- Voyager (LUK): Places buffs in allies for them to perform better in battle. Uses lucky charms only (seen as fists only).

-- Special hybrid classes:
-- (STR, CON): Attacks with 2h and shield only. Great attack and defense, but too much weight which hurts.
-- Crusader (STR, AGI): Attacks using dual-swords. Less damaging than 2h sword (even when using 2 swords), but can lightly stun.
-- Oracle (STR, SPR): Attacks using fists only. Initially they hurt and low dmg, but they rapidly master non-hurtful techniques.
-- (STR, ACC): Using a gun and a sword, provides greater accuracy when close.
-- (STR, LUK): Uses a 2h sword with lucky charms to provide the greatest attack, likewise with the damage on himself.

-- (CON, STR): Attacks with fists and shield.
-- (CON, AGI): Attacks with dagger and shield.
-- (CON, SPR): Attacks with damaging staff and shield.
-- (CON, ACC): Attacks with gun and shield.
-- (CON, LUK): Attacks with lucky charms and shield.

-- (AGI, STR): Attacks with dual daggers.
-- (AGI, CON): Attacks with dagger and shield.
-- (AGI, SPR): Attacks with dagger and a damaging staff.
-- (AGI, ACC): Attacks with dagger and gun.
-- (AGI, LUK): Attacks with dagger and lucky charm.

-- Mage (SPR, STR): Attacks with dual damaging staff. Attacks will also hurt the caster.
-- (SPR, CON): Attacks with healing staff and shield.
-- (SPR, AGI): Attacks with dagger and a healing staff.
-- (SPR, ACC): Attacks with a single damaging staff.
-- Angel (SPR, LUK): Combines healing staff with lucky charms, so that healing will happen unpredictably after cast.

-- Sharpshooter (ACC, STR): Attacks boss from far with a gun. They are less precise, but can be dual-wielded, so more powerful.
-- (ACC, CON): Attacks with bow and shield.
-- (ACC, AGI): Attacks with bow and dagger.
-- (ACC, SPR): Attacks with bow and healing staff.
-- Sniper (ACC, LUK): Attacks with bow and lucky charms. Causes criticals on head shots. Non-crits make way less damage than normal.

-- (LUK, STR): Attacks with dual lucky charms.
-- (LUK, CON): Attacks with lucky charms and shield.
-- (LUK, AGI): Attacks with lucky charms and dagger.
-- (LUK, SPR): Attacks with lucky charms and damaging staff.
-- (LUK, ACC): Attacks with lucky charms and gun.

--[[
-- Class constants
CLASS_NEWBIE		= 0;	-- NO STAT

CLASS_WARRIOR		= 1;	-- STR/Attack/Aspd|Def -- 1
CLASS_HUNTER		= 2;	-- ACC/Hit/Attack|Aspd -- 2
CLASS_THIEF			= 16;	-- AGI/Aspd/Hit|Attack -- 16
CLASS_KNIGHT		= 10;	-- CON/Def/Crit|MAttack -- 10
CLASS_HERBALIST		= 5;	-- SPR/MAttack/Def|Crit -- 5
CLASS_EXPLORER		= 4;	-- LUK/Crit/MAttack|Hit -- 4
CLASS_ARTISAN		= 6;	-- -- 6
CLASS_MERCHANT		= 7;	-- -- 7
CLASS_SAILOR		= 3;	-- -- 3

CLASS_BERSERKER		= 17;	-- STR+STR -- 17 -- Sacrifices himself for his own beliefs.
CLASS_SHARPSHOOTER	= 12;	-- ACC+ACC -- 12 -- Punishes while revealing what others are.
CLASS_ASSASSIN		= 9;	-- AGI+AGI -- 9 -- Stealthly uses enemies' weaknesses to his advantage.
CLASS_CHAMPION		= 8;	-- CON+CON -- 8 -- Carries heavy armor to uphold his strong belief of justice.
CLASS_CLERIC		= 13;	-- SPR+SPR -- 13 -- Always finds ways to make good happen to allies.
CLASS_VOYAGER		= 16;	-- LUK+LUK -- 16 -- Uses trinkets he finds in the world to bend luck his way.
CLASS_BLACKSMITH	= 18;	-- -- 18 -- Makes weapons for all other classes.
CLASS_TAILOR		= 11;	-- -- 11 -- Creates clothing for all other classes.
CLASS_CAPTAIN		= 15;	-- -- 15 -- Creates ships for other classes.

CLASSES_1STCLASS		= {CLASS_WARRIOR,CLASS_HUNTER,CLASS_THIEF,CLASS_KNIGHT,CLASS_HERBALIST,CLASS_EXPLORER,CLASS_ARTISAN,CLASS_MERCHANT,CLASS_SAILOR}
CLASSES_2NDCLASS		= {CLASS_BERSERKER,CLASS_SHARPSHOOTER,CLASS_ASSASSIN,CLASS_CHAMPION,CLASS_CLERIC,CLASS_VOYAGER,CLASS_BLACKSMITH,CLASS_TAILOR,CLASS_CAPTAIN}

CLASS_RATIO_STR_LVL		= 0
CLASS_RATIO_ACC_LVL		= 1
CLASS_RATIO_AGI_LVL		= 2
CLASS_RATIO_CON_LVL		= 3
CLASS_RATIO_SPR_LVL		= 4
CLASS_RATIO_LUK_LVL		= 5
CLASS_RATIO_HP_CON		= 6
CLASS_RATIO_HP_LVL 		= 7
CLASS_RATIO_SP_SPR 		= 8
CLASS_RATIO_SP_LVL		= 9
CLASS_RATIO_ATK_STR		= 10
CLASS_RATIO_ATK_ACC		= 11
CLASS_RATIO_ATK_LUK		= 12
CLASS_RATIO_MATK_SPR	= 13
CLASS_RATIO_MATK_LUK	= 14
CLASS_RATIO_DEF_CON		= 15
CLASS_RATIO_MDEF_LUK	= 16
CLASS_RATIO_HIT_ACC		= 17
CLASS_RATIO_HIT_LUK		= 18
CLASS_RATIO_FLEE_AGI	= 19
CLASS_RATIO_FLEE_LUK	= 20
CLASS_RATIO_DROP_LUK	= 21
CLASS_RATIO_CRT_LUK		= 22
CLASS_RATIO_CRTR_CON	= 23
CLASS_RATIO_CRTR_SPR	= 24
CLASS_RATIO_HREC_BMXHP	= 25
CLASS_RATIO_HREC_CON	= 26
CLASS_RATIO_SREC_BMXSP	= 27
CLASS_RATIO_SREC_SPR	= 28
CLASS_RATIO_ASPD_AGI	= 29
CLASS_RATIO_MSPD_AGI	= 30

--									STATS					HP		SP			ATK						MATK			DEF		MDEF	HIT				FLEE			DROP	CRT		CRTR			HREC			SREC			APSD	MSPD
class_sr = {}
class_sr[CLASS_NEWBIE]={			0,	0,	0,	0,	0,	0,	1,	15,	0.1,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.10,	0.10,	0.5,	0.1,	0.5,	0.1,	0.20,	0.10,	0.10,	0.15,	0.005,	0.125,	0.005,	0.050,	0.9,	0.20}

class_sr[CLASS_WARRIOR]={			1,	0,	0,	0,	0,	0,	2,	20,	0.2,	3,	0.7,	0.0,	0.2,	0.0,	0.0,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.0,	0.010,	0.250,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_HUNTER]={			0,	1,	0,	0,	0,	0,	3,	25,	0.2,	3,	0.0,	0.5,	0.2,	0.0,	0.0,	0.14,	0.14,	0.8,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.0,	0.006,	0.150,	0.010,	0.080,	1.1,	0.21}
class_sr[CLASS_THIEF]={				0,	0,	1,	0,	0,	0,	3,	25,	0.2,	3,	0.0,	0.4,	0.2,	0.0,	0.0,	0.14,	0.14,	0.6,	0.1,	0.8,	0.1,	0.30,	0.20,	0.20,	0.0,	0.006,	0.150,	0.010,	0.080,	1.2,	0.22}
class_sr[CLASS_KNIGHT]={			0,	0,	0,	1,	0,	0,	5,	35,	0.2,	3,	0.5,	0.0,	0.2,	0.0,	0.0,	0.16,	0.13,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.30,	0.0,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_HERBALIST]={			0,	0,	0,	0,	1,	0,	3,	25,	1.0,	5,	0.0,	0.0,	0.0,	0.6,	0.2,	0.13,	0.16,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.0,	0.25,	0.006,	0.150,	0.017,	0.100,	1.0,	0.20}
class_sr[CLASS_EXPLORER]={			0,	0,	0,	0,	0,	1,	3,	25,	0.2,	3,	0.0,	0.0,	0.0,	0.2,	0.4,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.60,	0.30,	0.0,	0.20,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_ARTISAN]={			0,	0,	0,	0,	0,	0,	3,	25,	0.2,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.20,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_MERCHANT]={			0,	0,	0,	0,	0,	0,	3,	25,	0.2,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.20,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}
class_sr[CLASS_SAILOR]={			0,	0,	0,	0,	0,	0,	3,	25,	0.2,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.14,	0.14,	0.6,	0.1,	0.6,	0.1,	0.30,	0.20,	0.20,	0.20,	0.006,	0.150,	0.010,	0.080,	1.0,	0.20}

class_sr[CLASS_BERSERKER]={			2,	0,	0,	0,	0,	0,	3,	25,	0.5,	5,	1.0,	0.0,	0.3,	0.0,	0.0,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.0,	0.015,	0.375,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_SHARPSHOOTER]={		0,	2,	0,	0,	0,	0,	5,	35,	0.5,	5,	0.0,	0.8,	0.3,	0.0,	0.0,	0.18,	0.18,	1.0,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.0,	0.007,	0.175,	0.015,	0.100,	1.3,	0.23}
class_sr[CLASS_ASSASSIN]={			0,	0,	2,	0,	0,	0,	5,	35,	0.5,	5,	0.0,	0.7,	0.3,	0.0,	0.0,	0.14,	0.14,	0.7,	0.1,	1.0,	0.1,	0.45,	0.40,	0.40,	0.0,	0.007,	0.175,	0.015,	0.100,	1.4,	0.25}
class_sr[CLASS_CHAMPION]={			0,	0,	0,	2,	0,	0,	7,	50,	0.5,	5,	0.8,	0.0,	0.3,	0.0,	0.0,	0.20,	0.16,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.0,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_CLERIC]={			0,	0,	0,	0,	2,	0,	5,	35,	1.5,	7,	0.0,	0.0,	0.0,	0.9,	0.3,	0.16,	0.20,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.0,	0.50,	0.007,	0.175,	0.025,	0.120,	1.1,	0.20}
class_sr[CLASS_VOYAGER]={			0,	0,	0,	0,	0,	2,	5,	35,	0.5,	5,	0.0,	0.0,	0.0,	0.3,	0.8,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.80,	0.55,	0.0,	0.40,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_BLACKSMITH]={		0,	0,	0,	0,	0,	0,	5,	35,	0.5,	5,	0.8,	0.8,	0.2,	0.8,	0.2,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.40,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_TAILOR]={			0,	0,	0,	0,	0,	0,	5,	35,	0.5,	5,	0.8,	0.8,	0.2,	0.8,	0.2,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.40,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
class_sr[CLASS_CAPTAIN]={			0,	0,	0,	0,	0,	0,	5,	35,	0.5,	5,	0.8,	0.8,	0.2,	0.8,	0.2,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.40,	0.007,	0.175,	0.015,	0.100,	1.1,	0.20}
--]]

-- Attribute constants
ATTR_COUNT_BASE0    = 0;
ATTR_LV             = ATTR_COUNT_BASE0 + 0; -- ��ɫ�ȼ�
ATTR_HP             = ATTR_COUNT_BASE0 + 1; -- ��ǰHP�����ڴ�ֻ����ʾ�;ã�
ATTR_SP             = ATTR_COUNT_BASE0 + 2; -- ��ǰSP�����ڴ�ֻ����ʾ����
ATTR_TITLE          = ATTR_COUNT_BASE0 + 3; -- ��ɫ�ƺ�
ATTR_CLASS          = ATTR_COUNT_BASE0 + 4; -- ��ɫְҵ
ATTR_FAME           = ATTR_COUNT_BASE0 + 5; -- ����
ATTR_AP             = ATTR_COUNT_BASE0 + 6; -- ���Ե�
ATTR_TP             = ATTR_COUNT_BASE0 + 7; -- ���ܵ�
ATTR_GD             = ATTR_COUNT_BASE0 + 8; -- ��Ǯ
ATTR_SPRI           = ATTR_COUNT_BASE0 + 9; -- �ڵ������ٶ�
ATTR_CHATYPE        = ATTR_COUNT_BASE0 + 10; -- ��ɫ�������ͣ���ң�NPC������ȣ�
ATTR_SAILLV         = ATTR_COUNT_BASE0 + 11; -- �����ȼ�
ATTR_LIFELV         = ATTR_COUNT_BASE0 + 12; -- ���ȼ�
ATTR_LIFETP         = ATTR_COUNT_BASE0 + 13; -- ���ܵ�
-- ���·�Χ�ڵĸ����Ե�˳�򲻿ɸı�
ATTR_CEXP           = ATTR_COUNT_BASE0 + 15; -- ��ǰ���飨���ڴ�ֻ����ʾ�����ȣ�
ATTR_NLEXP          = ATTR_COUNT_BASE0 + 16; -- ��һ�����辭��
ATTR_CLEXP          = ATTR_COUNT_BASE0 + 17; -- ��ǰ�ȼ��ľ���
ATTR_CLEFT_SAILEXP  = ATTR_COUNT_BASE0 + 18; -- ��ǰʣ�ຽ������
ATTR_CSAILEXP       = ATTR_COUNT_BASE0 + 19; -- �ۻ���飨��ǰ�������飩
ATTR_CLV_SAILEXP    = ATTR_COUNT_BASE0 + 20; -- ��ǰ�ȼ���������
ATTR_NLV_SAILEXP    = ATTR_COUNT_BASE0 + 21; -- ��һ�ȼ��ȼ���������
ATTR_CLIFEEXP       = ATTR_COUNT_BASE0 + 22; -- ��ǰ����
ATTR_CLV_LIFEEXP    = ATTR_COUNT_BASE0 + 23; -- ��ǰ�ȼ�����
ATTR_NLV_LIFEEXP    = ATTR_COUNT_BASE0 + 24; -- ��һ�ȼ�����
--

ATTR_COUNT_BASE1    = 25;
ATTR_STR            = ATTR_COUNT_BASE1 + 0; -- ����
ATTR_ACC            = ATTR_COUNT_BASE1 + 1; -- רע
ATTR_AGI            = ATTR_COUNT_BASE1 + 2; -- ����
ATTR_CON            = ATTR_COUNT_BASE1 + 3; -- ����
ATTR_SPR            = ATTR_COUNT_BASE1 + 4; -- ����
ATTR_LUK            = ATTR_COUNT_BASE1 + 5; -- ����
ATTR_MXHP           = ATTR_COUNT_BASE1 + 6; -- ���HP
ATTR_MXSP           = ATTR_COUNT_BASE1 + 7; -- ���SP
ATTR_ATK            = ATTR_COUNT_BASE1 + 8; -- ��С������
ATTR_MATK           = ATTR_COUNT_BASE1 + 9; -- ��󹥻���
ATTR_DEF            = ATTR_COUNT_BASE1 + 10; -- ������
ATTR_HIT            = ATTR_COUNT_BASE1 + 11; -- ������
ATTR_FLEE           = ATTR_COUNT_BASE1 + 12; -- ������
ATTR_DROP           = ATTR_COUNT_BASE1 + 13; -- Ѱ����
ATTR_CRT            = ATTR_COUNT_BASE1 + 14; -- ������
ATTR_HREC           = ATTR_COUNT_BASE1 + 15; -- hp�ָ��ٶ�
ATTR_SREC           = ATTR_COUNT_BASE1 + 16; -- sp�ָ��ٶ�
ATTR_ASPD           = ATTR_COUNT_BASE1 + 17; -- �������
ATTR_CRTR           = ATTR_COUNT_BASE1 + 18; -- ��������
ATTR_MSPD           = ATTR_COUNT_BASE1 + 19; -- �ƶ��ٶ�
ATTR_EXP            = ATTR_COUNT_BASE1 + 20; -- ��Դ�ɼ��ٶ�
ATTR_MDEF           = ATTR_COUNT_BASE1 + 21; -- ����ֿ�
ATTR_BOAT_CRANGE    = ATTR_COUNT_BASE1 + 22; -- vessels of the shells exploded 
ATTR_BOAT_CSPD      = ATTR_COUNT_BASE1 + 23; -- artillery shells flying speed vessels
ATTR_BOAT_PRICE     = ATTR_COUNT_BASE1 + 24; -- the value of vessels 

ATTR_COUNT_BASE2    = 50;
ATTR_BSTR           = ATTR_COUNT_BASE2 + 0; -- ������
ATTR_BACC           = ATTR_COUNT_BASE2 + 1; -- ��רע
ATTR_BAGI           = ATTR_COUNT_BASE2 + 2; -- ������
ATTR_BCON           = ATTR_COUNT_BASE2 + 3; -- ������
ATTR_BSPR           = ATTR_COUNT_BASE2 + 4; -- ����
ATTR_BLUK           = ATTR_COUNT_BASE2 + 5; -- ������
ATTR_BMXHP          = ATTR_COUNT_BASE2 + 6; -- �����HP
ATTR_BMXSP          = ATTR_COUNT_BASE2 + 7; -- �����SP
ATTR_BATK           = ATTR_COUNT_BASE2 + 8; -- ����С������
ATTR_BMATK          = ATTR_COUNT_BASE2 + 9; -- ����󹥻���
ATTR_BDEF           = ATTR_COUNT_BASE2 + 10; -- �������
ATTR_BHIT           = ATTR_COUNT_BASE2 + 11; -- ��������
ATTR_BFLEE          = ATTR_COUNT_BASE2 + 12; -- ��������
ATTR_BDROP          = ATTR_COUNT_BASE2 + 13; -- ��Ѱ����
ATTR_BCRT           = ATTR_COUNT_BASE2 + 14; -- ������
ATTR_BHREC          = ATTR_COUNT_BASE2 + 15; -- ��hp�ָ��ٶ�
ATTR_BSREC          = ATTR_COUNT_BASE2 + 16; -- ��(��ɫsp�ָ��ٶ�)(��ֻΪ����ٶ�)
ATTR_BASPD          = ATTR_COUNT_BASE2 + 17; -- �������
ATTR_BCRTR          = ATTR_COUNT_BASE2 + 18; -- ��������
ATTR_BMSPD          = ATTR_COUNT_BASE2 + 19; -- ���ƶ��ٶ�
ATTR_BEXP           = ATTR_COUNT_BASE2 + 20; -- ����Դ�ɼ��ٶ�
ATTR_BMDEF          = ATTR_COUNT_BASE2 + 21; -- ������ֿ�
ATTR_BOAT_BCRANGE   = ATTR_COUNT_BASE2 + 22; -- basic vessels explosion of shells
ATTR_BOAT_BCSPD     = ATTR_COUNT_BASE2 + 23; -- basic shells flying speed vessels 

ATTR_COUNT_BASE3    = 74;
ATTR_ITEMC_STR      = ATTR_COUNT_BASE3 + 0; -- ��������ϵ��item coefficient���ӳ�
ATTR_ITEMC_AGI      = ATTR_COUNT_BASE3 + 1; -- ���ݵ���ϵ��ӳ�
ATTR_ITEMC_ACC      = ATTR_COUNT_BASE3 + 2; -- רע����ϵ��ӳ�
ATTR_ITEMC_CON      = ATTR_COUNT_BASE3 + 3; -- ���ʵ���ϵ��ӳ�
ATTR_ITEMC_SPR      = ATTR_COUNT_BASE3 + 4; -- ��������ϵ��ӳ�
ATTR_ITEMC_LUK      = ATTR_COUNT_BASE3 + 5; -- ���˵���ϵ��ӳ�
ATTR_ITEMC_ASPD     = ATTR_COUNT_BASE3 + 6; -- �����������ϵ��ӳ�
ATTR_ITEMC_CRTR     = ATTR_COUNT_BASE3 + 7; -- �����������ϵ��ӳ�
ATTR_ITEMC_ATK      = ATTR_COUNT_BASE3 + 8; -- ��С����������ϵ��ӳ�
ATTR_ITEMC_MATK     = ATTR_COUNT_BASE3 + 9; -- ��󹥻�������ϵ��ӳ�
ATTR_ITEMC_DEF      = ATTR_COUNT_BASE3 + 10; -- ����������ϵ��ӳ�
ATTR_ITEMC_MXHP     = ATTR_COUNT_BASE3 + 11; -- ���HP����ϵ��ӳ�
ATTR_ITEMC_MXSP     = ATTR_COUNT_BASE3 + 12; -- ���SP����ϵ��ӳ�
ATTR_ITEMC_FLEE     = ATTR_COUNT_BASE3 + 13; -- �����ʵ���ϵ��ӳ�
ATTR_ITEMC_HIT      = ATTR_COUNT_BASE3 + 14; -- �����ʵ���ϵ��ӳ�
ATTR_ITEMC_CRT      = ATTR_COUNT_BASE3 + 15; -- �����ʵ���ϵ��ӳ�
ATTR_ITEMC_DROP     = ATTR_COUNT_BASE3 + 16; -- Ѱ���ʵ���ϵ��ӳ�
ATTR_ITEMC_HREC     = ATTR_COUNT_BASE3 + 17; -- hp�ָ��ٶȵ���ϵ��ӳ�
ATTR_ITEMC_SREC     = ATTR_COUNT_BASE3 + 18; -- sp�ָ��ٶȵ���ϵ��ӳ�
ATTR_ITEMC_MSPD     = ATTR_COUNT_BASE3 + 19; -- �ƶ��ٶȵ���ϵ��ӳ�
ATTR_ITEMC_EXP      = ATTR_COUNT_BASE3 + 20; -- ��Դ�ɼ��ٶȵ���ϵ��ӳ�
ATTR_ITEMC_MDEF     = ATTR_COUNT_BASE3 + 21; -- ����ֿ�����ϵ��ӳ�

ATTR_COUNT_BASE4    = 96;
ATTR_ITEMV_STR      = ATTR_COUNT_BASE4 + 0; -- ����������ֵ��item value���ӳ�
ATTR_ITEMV_AGI      = ATTR_COUNT_BASE4 + 1; -- ���ݵ�����ֵ�ӳ�
ATTR_ITEMV_ACC      = ATTR_COUNT_BASE4 + 2; -- רע������ֵ�ӳ�
ATTR_ITEMV_CON      = ATTR_COUNT_BASE4 + 3; -- ���ʵ�����ֵ�ӳ�
ATTR_ITEMV_SPR      = ATTR_COUNT_BASE4 + 4; -- ����������ֵ�ӳ�
ATTR_ITEMV_LUK      = ATTR_COUNT_BASE4 + 5; -- ���˵�����ֵ�ӳ�
ATTR_ITEMV_ASPD     = ATTR_COUNT_BASE4 + 6; -- �������������ֵ�ӳ�
ATTR_ITEMV_CRTR     = ATTR_COUNT_BASE4 + 7; -- �������������ֵ�ӳ�
ATTR_ITEMV_ATK      = ATTR_COUNT_BASE4 + 8; -- ��С������������ֵ�ӳ�
ATTR_ITEMV_MATK     = ATTR_COUNT_BASE4 + 9; -- ��󹥻���������ֵ�ӳ�
ATTR_ITEMV_DEF      = ATTR_COUNT_BASE4 + 10; -- ������������ֵ�ӳ�
ATTR_ITEMV_MXHP     = ATTR_COUNT_BASE4 + 11; -- ���HP������ֵ�ӳ�
ATTR_ITEMV_MXSP     = ATTR_COUNT_BASE4 + 12; -- ���SP������ֵ�ӳ�
ATTR_ITEMV_FLEE     = ATTR_COUNT_BASE4 + 13; -- �����ʵ�����ֵ�ӳ�
ATTR_ITEMV_HIT      = ATTR_COUNT_BASE4 + 14; -- �����ʵ�����ֵ�ӳ�
ATTR_ITEMV_CRT      = ATTR_COUNT_BASE4 + 15; -- �����ʵ�����ֵ�ӳ�
ATTR_ITEMV_DROP     = ATTR_COUNT_BASE4 + 16; -- Ѱ���ʵ�����ֵ�ӳ�
ATTR_ITEMV_HREC     = ATTR_COUNT_BASE4 + 17; -- hp�ָ��ٶȵ�����ֵ�ӳ�
ATTR_ITEMV_SREC     = ATTR_COUNT_BASE4 + 18; -- sp�ָ��ٶȵ�����ֵ�ӳ�
ATTR_ITEMV_MSPD     = ATTR_COUNT_BASE4 + 19; -- �ƶ��ٶȵ�����ֵ�ӳ�
ATTR_ITEMV_EXP      = ATTR_COUNT_BASE4 + 20; -- ��Դ�ɼ��ٶȵ�����ֵ�ӳ�
ATTR_ITEMV_MDEF     = ATTR_COUNT_BASE4 + 21; -- ����ֿ�������ֵ�ӳ�

ATTR_COUNT_BASE5    = 118;
ATTR_STATEC_STR     = ATTR_COUNT_BASE5 + 0; -- ����״̬ϵ��state coefficient���ӳ�
ATTR_STATEC_AGI     = ATTR_COUNT_BASE5 + 1; -- ����״̬ϵ��ӳ�
ATTR_STATEC_ACC     = ATTR_COUNT_BASE5 + 2; -- רע״̬ϵ��ӳ�
ATTR_STATEC_CON     = ATTR_COUNT_BASE5 + 3; -- ����״̬ϵ��ӳ�
ATTR_STATEC_SPR     = ATTR_COUNT_BASE5 + 4; -- ����״̬ϵ��ӳ�
ATTR_STATEC_LUK     = ATTR_COUNT_BASE5 + 5; -- ����״̬ϵ��ӳ�
ATTR_STATEC_ASPD    = ATTR_COUNT_BASE5 + 6; -- �������״̬ϵ��ӳ�
ATTR_STATEC_CRTR    = ATTR_COUNT_BASE5 + 7; -- ��������״̬ϵ��ӳ�
ATTR_STATEC_ATK     = ATTR_COUNT_BASE5 + 8; -- ��С������״̬ϵ��ӳ�
ATTR_STATEC_MATK    = ATTR_COUNT_BASE5 + 9; -- ��󹥻���״̬ϵ��ӳ�
ATTR_STATEC_DEF     = ATTR_COUNT_BASE5 + 10; -- ������״̬ϵ��ӳ�
ATTR_STATEC_MXHP    = ATTR_COUNT_BASE5 + 11; -- ���HP״̬ϵ��ӳ�
ATTR_STATEC_MXSP    = ATTR_COUNT_BASE5 + 12; -- ���SP״̬ϵ��ӳ�
ATTR_STATEC_FLEE    = ATTR_COUNT_BASE5 + 13; -- ������״̬ϵ��ӳ�
ATTR_STATEC_HIT     = ATTR_COUNT_BASE5 + 14; -- ������״̬ϵ��ӳ�
ATTR_STATEC_CRT     = ATTR_COUNT_BASE5 + 15; -- ������״̬ϵ��ӳ�
ATTR_STATEC_DROP    = ATTR_COUNT_BASE5 + 16; -- Ѱ����״̬ϵ��ӳ�
ATTR_STATEC_HREC    = ATTR_COUNT_BASE5 + 17; -- hp�ָ��ٶ�״̬ϵ��ӳ�
ATTR_STATEC_SREC    = ATTR_COUNT_BASE5 + 18; -- sp�ָ��ٶ�״̬ϵ��ӳ�
ATTR_STATEC_MSPD    = ATTR_COUNT_BASE5 + 19; -- �ƶ��ٶ�״̬ϵ��ӳ�
ATTR_STATEC_EXP     = ATTR_COUNT_BASE5 + 20; -- ��Դ�ɼ��ٶ�״̬ϵ��ӳ�
ATTR_STATEC_MDEF    = ATTR_COUNT_BASE5 + 21; -- ����ֿ�״̬ϵ��ӳ�

ATTR_COUNT_BASE6    = 140;
ATTR_STATEV_STR     = ATTR_COUNT_BASE6 + 0; -- ����״̬��ֵ��state value���ӳ�
ATTR_STATEV_AGI     = ATTR_COUNT_BASE6 + 1; -- ����״̬��ֵ�ӳ�
ATTR_STATEV_ACC     = ATTR_COUNT_BASE6 + 2; -- רע״̬��ֵ�ӳ�
ATTR_STATEV_CON     = ATTR_COUNT_BASE6 + 3; -- ����״̬��ֵ�ӳ�
ATTR_STATEV_SPR     = ATTR_COUNT_BASE6 + 4; -- ����״̬��ֵ�ӳ�
ATTR_STATEV_LUK     = ATTR_COUNT_BASE6 + 5; -- ����״̬��ֵ�ӳ�
ATTR_STATEV_ASPD    = ATTR_COUNT_BASE6 + 6; -- �������״̬��ֵ�ӳ�
ATTR_STATEV_CRTR    = ATTR_COUNT_BASE6 + 7; -- ��������״̬��ֵ�ӳ�
ATTR_STATEV_ATK     = ATTR_COUNT_BASE6 + 8; -- ��С������״̬��ֵ�ӳ�
ATTR_STATEV_MATK    = ATTR_COUNT_BASE6 + 9; -- ��󹥻���״̬��ֵ�ӳ�
ATTR_STATEV_DEF     = ATTR_COUNT_BASE6 + 10; -- ������״̬��ֵ�ӳ�
ATTR_STATEV_MXHP    = ATTR_COUNT_BASE6 + 11; -- ���HP״̬��ֵ�ӳ�
ATTR_STATEV_MXSP    = ATTR_COUNT_BASE6 + 12; -- ���SP״̬��ֵ�ӳ�
ATTR_STATEV_FLEE    = ATTR_COUNT_BASE6 + 13; -- ������״̬��ֵ�ӳ�
ATTR_STATEV_HIT     = ATTR_COUNT_BASE6 + 14; -- ������״̬��ֵ�ӳ�
ATTR_STATEV_CRT     = ATTR_COUNT_BASE6 + 15; -- ������״̬��ֵ�ӳ�
ATTR_STATEV_DROP    = ATTR_COUNT_BASE6 + 16; -- Ѱ����״̬��ֵ�ӳ�
ATTR_STATEV_HREC    = ATTR_COUNT_BASE6 + 17; -- hp�ָ��ٶ�״̬��ֵ�ӳ�
ATTR_STATEV_SREC    = ATTR_COUNT_BASE6 + 18; -- sp�ָ��ٶ�״̬��ֵ�ӳ�
ATTR_STATEV_MSPD    = ATTR_COUNT_BASE6 + 19; -- �ƶ��ٶ�״̬��ֵ�ӳ�
ATTR_STATEV_EXP     = ATTR_COUNT_BASE6 + 20; -- ��Դ�ɼ��ٶ�״̬��ֵ�ӳ�
ATTR_STATEV_MDEF    = ATTR_COUNT_BASE6 + 21; -- ����ֿ�״̬��ֵ�ӳ�

ATTR_LHAND_ITEMV    = ATTR_COUNT_BASE6 + 22; -- ���ֵ��߼ӳ�

ATTR_COUNT_BASE7        = 163;
ATTR_BOAT_SHIP          = ATTR_COUNT_BASE7 + 0; -- ��ֻID
ATTR_BOAT_HEADER        = ATTR_COUNT_BASE7 + 1; -- ��ͷ����
ATTR_BOAT_BODY          = ATTR_COUNT_BASE7 + 2; -- ��������
ATTR_BOAT_ENGINE        = ATTR_COUNT_BASE7 + 3; -- ����������
ATTR_BOAT_CANNON        = ATTR_COUNT_BASE7 + 4; -- ����������
ATTR_BOAT_PART          = ATTR_COUNT_BASE7 + 5; -- ���������
ATTR_BOAT_DBID          = ATTR_COUNT_BASE7 + 6; -- ������ݿ�洢ID
ATTR_BOAT_DIECOUNT      = ATTR_COUNT_BASE7 + 7; -- �ô���������
ATTR_BOAT_ISDEAD	    = ATTR_COUNT_BASE7 + 8; -- �ô�Ŀǰ�Ƿ�Ϊ����״̬

ATTR_COUNT_BASE8        = 172;
ATTR_BOAT_SKILLC_MNATK  = ATTR_COUNT_BASE8 + 0; -- ��ֻMNATK ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_MXATK  = ATTR_COUNT_BASE8 + 1; -- ��ֻMXATK ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_ADIS   = ATTR_COUNT_BASE8 + 2; -- ��ֻ��������atkrange ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_MSPD   = ATTR_COUNT_BASE8 + 3; -- ��ֻ�ƶ��ٶȼ���ϵ��Ӱ��
ATTR_BOAT_SKILLC_CSPD   = ATTR_COUNT_BASE8 + 4; -- �ڵ������ٶ� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_ASPD   = ATTR_COUNT_BASE8 + 5; -- ��ֻASPD ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_CRANGE = ATTR_COUNT_BASE8 + 6; -- �ڵ���ը��Χ ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_DEF    = ATTR_COUNT_BASE8 + 7; -- ��ֻDEF ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_RESIST = ATTR_COUNT_BASE8 + 8; -- ��ֻRESIST ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_MXUSE  = ATTR_COUNT_BASE8 + 9; -- ��ֻ����;� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_USEREC = ATTR_COUNT_BASE8 + 10; -- ��ֻ�;ûظ��ٶ� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_EXP    = ATTR_COUNT_BASE8 + 11; -- ��ֻ����� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_CPT    = ATTR_COUNT_BASE8 + 12; -- �������� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_SPD    = ATTR_COUNT_BASE8 + 13; -- ���� ����ϵ��Ӱ��
ATTR_BOAT_SKILLC_MXSPLY = ATTR_COUNT_BASE8 + 14; -- ��󲹸�ֵ ����ϵ��Ӱ��

ATTR_COUNT_BASE9        = 187;
ATTR_BOAT_SKILLV_MNATK  = ATTR_COUNT_BASE9 + 0; -- ��ֻMNATK ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_MXATK  = ATTR_COUNT_BASE9 + 1; -- ��ֻMXATK ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_ADIS   = ATTR_COUNT_BASE9 + 2; -- ��ֻ��������atkrange ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_MSPD   = ATTR_COUNT_BASE9 + 3; -- ��ֻ�ƶ��ٶȼ��ܳ���Ӱ��
ATTR_BOAT_SKILLV_CSPD   = ATTR_COUNT_BASE9 + 4; -- �ڵ������ٶ� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_ASPD   = ATTR_COUNT_BASE9 + 5; -- ��ֻASPD ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_CRANGE = ATTR_COUNT_BASE9 + 6; -- �ڵ���ը��Χ ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_DEF    = ATTR_COUNT_BASE9 + 7; -- ��ֻDEF ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_RESIST = ATTR_COUNT_BASE9 + 8; -- ��ֻRESIST ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_MXUSE  = ATTR_COUNT_BASE9 + 9; -- ��ֻ����;� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_USEREC = ATTR_COUNT_BASE9 + 10; -- ��ֻ�;ûظ��ٶ� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_EXP    = ATTR_COUNT_BASE9 + 11; -- ��ֻ����� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_CPT    = ATTR_COUNT_BASE9 + 12; -- �������� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_SPD    = ATTR_COUNT_BASE9 + 13; -- ���� ���ܳ���Ӱ��
ATTR_BOAT_SKILLV_MXSPLY = ATTR_COUNT_BASE9 + 14; -- ��󲹸�ֵ ���ܳ���Ӱ��

ATTR_COUNT_BASE10        = 202;
ATTR_EXTEND0  = ATTR_COUNT_BASE10 + 0;			--��¼ռ����BUFF����
ATTR_EXTEND1  = ATTR_COUNT_BASE10 + 1;			--��¼��BUFF����
ATTR_EXTEND2  = ATTR_COUNT_BASE10 + 2;			--��¼ռ��ʱ�Ǽ���
ATTR_EXTEND3  = ATTR_COUNT_BASE10 + 3;			--��¼�ڱ�����
ATTR_EXTEND4  = ATTR_COUNT_BASE10 + 4;			--��ȡ�ڱ�����ʱ��
ATTR_EXTEND5  = ATTR_COUNT_BASE10 + 5;			--��ȡ˫��ʱ��
ATTR_EXTEND6  = ATTR_COUNT_BASE10 + 6;			--��¼ʣ��˫��ʱ��
ATTR_EXTEND7  = ATTR_COUNT_BASE10 + 7;			--DEBUFF��ʱ��Ϣλ
ATTR_EXTEND8  = ATTR_COUNT_BASE10 + 8;			--�����ճ��������������
ATTR_EXTEND9  = ATTR_COUNT_BASE10 + 9;			--�����ճ�����������ʱ��

ATTR_MAX_NUM                = 213;

--[[
SetChaAttrMax( ATTR_LV		,	120			)	-- ��ɫ�ȼ�
SetChaAttrMax( ATTR_HP		,	2000000000	)	-- ��ǰHP
SetChaAttrMax( ATTR_SP		,	2000000000	)	-- ��ǰSP
SetChaAttrMax( ATTR_CLASS		,	100			)	-- ��ɫְҵ
SetChaAttrMax( ATTR_CEXP		,	6800000000	)	-- ��ǰ����
SetChaAttrMax( ATTR_NLEXP	,	6800000000	)	-- ��һ�����辭��
SetChaAttrMax( ATTR_AP		,	300			)	-- ���Ե�
SetChaAttrMax( ATTR_TP		,	250			)	-- ���ܵ�
SetChaAttrMax( ATTR_GD		,	2000000000	)	-- ��Ǯ
SetChaAttrMax( ATTR_CLEXP	,	6800000000	)	--��ǰ�ȼ��ľ���
SetChaAttrMax( ATTR_MXHP	,	2000000000	)	--���hp
SetChaAttrMax( ATTR_MXSP		,	2000000000	)	--���sp
SetChaAttrMax( ATTR_BSTR		,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BACC		,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BAGI		,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BCON	,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BSPR		,	10000			)	-- ���� (orig was 120)
SetChaAttrMax( ATTR_BLUK		,	10000			)	-- ������ (orig was 120)
SetChaAttrMax( ATTR_BMXHP	,	2000000000	)	-- �����HP
SetChaAttrMax( ATTR_BMXSP	,	2000000000	)	-- �����SP
SetChaAttrMax( ATTR_BATK	,	13000			)	-- ����С������
SetChaAttrMax( ATTR_BMATK	,	13000			)	-- ����󹥻���
SetChaAttrMax( ATTR_BDEF		,	9999			)	-- �������
SetChaAttrMax( ATTR_BHIT		,	9999			)	-- ��������
SetChaAttrMax( ATTR_BFLEE	,	9999			)	-- ��������
SetChaAttrMax( ATTR_BDROP		,	9999			)	-- ��Ѱ����
SetChaAttrMax( ATTR_BCRT		,	9999			)	-- ������
SetChaAttrMax( ATTR_BHREC	,	9999			)	-- ��hp�ָ��ٶ�
SetChaAttrMax( ATTR_BSREC	,	9999			)	-- ��sp�ָ��ٶ�
SetChaAttrMax( ATTR_BASPD	,	9999			)	-- �������
SetChaAttrMax( ATTR_BCRTR	,	9999			)	-- ��������
SetChaAttrMax( ATTR_BMSPD	,	9999			)	-- ���ƶ��ٶ�
SetChaAttrMax( ATTR_BEXP		,	9999			)	-- ����Դ�ɼ��ٶ�
SetChaAttrMax( ATTR_MSPD	,	9999			)	--�ƶ��ٶ�
SetChaAttrMax( ATTR_LHAND_ITEMV	,	9999			)	--���ֵ��߼ӳ�
--]]

--NOTE: Player Param 1: Player timer.

--Cha_timer: Callback function executed every 1 second (internally as 20 FPS).

char_stats_autocalc = true -- Auto stat calculation is enabled so that everytime the stats are manually editted (by gms)
--they get reset right after. If want to disable it for a code section, set it to false, then put it back to true.

char_stats_mt = {}

local char_item_stats_conv = {
	[0]=0,
	[1]=2,
	[2]=1,
	[3]=3,
	[4]=4,
	[5]=5,
	[6]=11,
	[7]=12,
	[8]=8,
	[9]=9,
	[10]=10,
	[11]=14,
	[12]=13,
	[13]=16,
	[14]=15,
	[15]=17,
	[16]=18,
	[17]=6,
	[18]=7,
	[19]=19,
	[20]=20,
	[21]=21
}
local item_char_stats_conv = table.inv_func(char_item_stats_conv)

function char_stats_mt.__index (t,key)
	if key == "role" then
		return rawget(t, key)
	else
		if ((key == ATTR_ASPD) or (key == ATTR_BASPD)) then
			return math.floor(100000/GetChaAttr(t.role,key))
		end
		if (((key >= ATTR_COUNT_BASE3) and (key <= (ATTR_COUNT_BASE3 + 21))) or
		((key >= ATTR_COUNT_BASE5) and (key <= (ATTR_COUNT_BASE5 + 21)))) then
			return GetChaAttr(t.role,key)/1000
		else
			return GetChaAttr(t.role,key)
		end
	end
end

function char_stats_mt.__newindex (t, key, value)
	if key == "role" then
		rawset(t, key, value)
	else
		local k_main = char_stats_getcalcbase(key)
		if (k_main ~= -1) and char_stats_autocalc then
			-- ATTR_COUNT_BASE1 = Base modifiable stats. Calculated as follows: (BASE2 * BASE3 + BASE4 ) * BASE5 + BASE6
			--                    or in another way - (Base * Item Mult + Item Add) * Skill Mult + Skill Add
			if 	(((key >= ATTR_COUNT_BASE3) and (key <= (ATTR_COUNT_BASE3 + 21))) or
			((key >= ATTR_COUNT_BASE5) and (key <= (ATTR_COUNT_BASE5 + 21)))) then
				SetChaAttr(t.role,key,value*1000)
			else
				SetChaAttr(t.role,key,value)
			end
			local v_base = GetChaAttr(t.role,(k_main - ATTR_COUNT_BASE1) + ATTR_COUNT_BASE2)
			local v_imult = GetChaAttr(t.role,char_item_stats_conv[(k_main - ATTR_COUNT_BASE1)] + ATTR_COUNT_BASE3)/1000
			local v_iadd = GetChaAttr(t.role,char_item_stats_conv[(k_main - ATTR_COUNT_BASE1)] + ATTR_COUNT_BASE4)
			local v_smult = GetChaAttr(t.role,char_item_stats_conv[(k_main - ATTR_COUNT_BASE1)] + ATTR_COUNT_BASE5)/1000
			local v_sadd = GetChaAttr(t.role,char_item_stats_conv[(k_main - ATTR_COUNT_BASE1)] + ATTR_COUNT_BASE6)
			local v_main = (v_base*v_imult + v_iadd)*v_smult + v_sadd
			if (k_main == ATTR_ASPD) then
				v_main = math.floor(100000/((v_base*v_imult + v_iadd)*v_smult + v_sadd))
			end
			SetChaAttr(t.role,k_main,v_main)
			-- Thus, anytime someone sets one of the values that give the main value, the main value will be set with
			-- the correct value all the time, without exceptions. Will correct all stat related bugs with 1 blow.
		else
			SetChaAttr(t.role,key,value)
		end
		--Automated stat update
		--TODO: Per stat update (not all stats all at once)
		--AttrRecheck(t.role)
	end
end

function recalculate(self) --stats --namespace pollution but no choice in this case -- dunno why it only works in skills...
	print('recalculate')
	AttrRecheck(self.role)
end

function char_stats_getcalcbase(key)
	if ((key >= ATTR_COUNT_BASE1) and (key <= (ATTR_COUNT_BASE1 + 21))) then -- full stat
		return key -- if a gm tries to set player stats manually, this will step-in, reversing the process automatically.
	elseif ((key >= ATTR_COUNT_BASE2) and (key <= (ATTR_COUNT_BASE2 + 21))) then -- base stat
		return (key - ATTR_COUNT_BASE2) + ATTR_COUNT_BASE1
	elseif ((key >= ATTR_COUNT_BASE3) and (key <= (ATTR_COUNT_BASE3 + 21))) then --
		return item_char_stats_conv[(key - ATTR_COUNT_BASE3)] + ATTR_COUNT_BASE1
	elseif ((key >= ATTR_COUNT_BASE4) and (key <= (ATTR_COUNT_BASE4 + 21))) then
		return item_char_stats_conv[(key - ATTR_COUNT_BASE4)] + ATTR_COUNT_BASE1
	elseif ((key >= ATTR_COUNT_BASE5) and (key <= (ATTR_COUNT_BASE5 + 21))) then
		return item_char_stats_conv[(key - ATTR_COUNT_BASE5)] + ATTR_COUNT_BASE1
	elseif ((key >= ATTR_COUNT_BASE6) and (key <= (ATTR_COUNT_BASE6 + 21))) then
		return item_char_stats_conv[(key - ATTR_COUNT_BASE6)] + ATTR_COUNT_BASE1
	end
	return -1
end

function relearn_skills(role)
	--players[player.id].avoid_passive_skill_calls = true
	
	--BUG: All "use" event of skills are called - thus attack becomes negative...
	local skill_list = { --TODO: Add skill requirements to advanced skill guide in top
	--TODO: Include Rebirth skills
	--TODO: Also include passive weapon skills
		SK_TAUNT,
		SK_ROAR, --SK_TAUNT lvl 10
		SK_CONCENTRATION,
		SK_SWORDMASTERY, --SK_CONCENTRATION lvl 2
		SK_WILLOFSTEEL, --SK_CONCENTRATION lvl 2
		SK_ILLUSIONSLASH, --SK_SWORDMASTERY lvl 3
		SK_BREAKARMOR, --SK_SWORDMASTERY lvl 3
		SK_BERSERK, --SK_ILLUSIONSLASH lvl 4
		SK_TIGERROAR, --SK_BREAKARMOR lvl 4
		SK_DUALSWORDMASTERY, --SK_BERSERK lvl 5
		SK_DEFTNESS, --SK_DUALSWORDMASTERY lvl 2
		SK_BLOODFRENZY, --SK_DUALSWORDMASTERY lvl 2
		SK_POISONDART, --SK_BLOODFRENZY lvl 2
		SK_SHADOWSLASH, --SK_BLOODFRENZY lvl 5
		SK_STEALTH, --SK_DEFTNESS lvl 3
		SK_WINDRIDERSGRACE, --SK_SHADOWSLASH lvl 5
		SK_GREATSWORDMASTERY, --SK_TIGERROAR lvl 5
		SK_STRENGTHEN, --SK_GREATSWORDMASTERY lvl 2
		SK_BLOODBULL, --SK_GREATSWORDMASTERY lvl 3
		SK_PRIMALRAGE, --SK_BLOODBULL lvl 5
		SK_WARRIORSRAGE, --SK_BLOODBULL lvl 5, lvl 125
		SK_HOWL, --SK_MIGHTYSTRIKE lvl 2
		SK_MIGHTYSTRIKE, --SK_STRENGTHEN lvl 3
		
		SK_RANGEMASTERY,
		SK_EAGLESEYE,
		SK_WINDWALK, --SK_RANGEMASTERY lvl 2
		SK_DUALSHOT, --SK_RANGEMASTERY lvl 3
		SK_FIREGUNMASTERY, --SK_RANGEMASTERY lvl 10
		SK_ROUSING, --SK_WINDWALK lvl 4
		SK_FROZENARROW, --SK_DUALSHOT lvl 2
		SK_VENOMARROW, --SK_DUALSHOT lvl 4
		SK_BLUNTINGBOLT, --SK_DUALSHOT lvl 4, lvl 125
		SK_FLAMINGDART, --SK_DUALSHOT lvl 4, lvl 125
		SK_METEORSHOWER, --SK_FROZENARROW lvl 5
		SK_CRIPPLE, --SK_FROZENARROW lvl 5
		SK_MAGMABULLET, --SK_FIREGUNMASTERY lvl 5
		SK_ENFEEBLE, --SK_CRIPPLE lvl 5
		SK_HEADSHOT, --SK_ENFEEBLE lvl 5
		SK_EYEOFPRECISION, --SK_HEADSHOT lvl 4, lvl 125
		SK_DEADEYEBLOOD, --SK_HEADSHOT lvl 4, lvl 125
		
		SK_HEAL,
		SK_SPIRITUALBOLT,
		SK_VIGOR,
		SK_CRYSTALLINEBLESSING,
		SK_INTENSEMAGIC,
		SK_HARDEN, --SK_HEAL lvl 3
		SK_RECOVER, --SK_HARDEN lvl 3
		SK_REVIVAL, --SK_RECOVER lvl 4
		SK_SPIRITUALFIRE, --SK_SPIRITUALBOLT lvl 2
		SK_TEMPESTBOOST, --SK_SPIRITUALFIRE lvl 4
		SK_DIVINEGRACE, --SK_VIGOR lvl 8
		SK_TRUESIGHT, --SK_DIVINEGRACE lvl 2
		SK_TORNADOSWIRL, --SK_TRUESIGHT lvl 2
		SK_CURSEDFIRE, --SK_TRUESIGHT lvl 3
		SK_ANGELICSHIELD, --SK_TORNADOSWIRL lvl 2
		SK_ENERGYSHIELD, --SK_TORNADOSWIRL lvl 3
		SK_HEALINGSPRING, --SK_ANGELICSHIELD lvl 2
		SK_SOULKEEPER, --SK_ENERGYSHIELD lvl 10
		SK_ABYSSMIRE, --SK_CURSEDFIRE lvl 4
		SK_SHADOWINSIGNIA, --SK_CURSEDFIRE lvl 4
		SK_BANEOFWANING, --SK_CURSEDFIRE lvl 5, lvl 125
		SK_CURSEOFWEAKENING, --SK_CURSEDFIRE lvl 10, lvl 125
		SK_SEALOFELDER, --SK_SHADOWINSIGNIA lvl 3
		SK_EXCRUCIO, --SK_SHADOWINSIGNIA lvl 10, lvl 125
		SK_PETRIFYINGPUMMEL, --SK_ABYSSMIRE lvl 8, lvl 125
		
		SK_DILIGENCE,
		SK_LIGHTNINGBOLT, --SK_DILIGENCE lvl 1
		SK_CURRENT, --SK_DILIGENCE lvl 2
		SK_LIGHTNINGCURTAIN, --SK_LIGHTNINGBOLT lvl 8
		SK_CONCHARMOR, --SK_CURRENT lvl 1
		SK_TORNADO, --SK_CURRENT lvl 2
		SK_CONCHRAY, --SK_CONCHARMOR lvl 5
		SK_OMNISIMMUNITY, --SK_CONCHRAY lvl 10, lvl 125
		SK_TAILWIND, --SK_TORNADO lvl 4
		SK_ALGAEENTANGLEMENT, --SK_TORNADO lvl 5
		SK_FOG, -- SK_TAILWIND lvl 2
		SK_WHIRLPOOL, --SK_TAILWIND lvl 4
	}
	
	for i,skill_id in ipairs(skill_list) do
		AddChaSkill(role,skill_id,10,1,0) --TODO: Remove class restriction from skillinfo.
	end
	
	--passive weapon skills
	skill_list = {
		25,
		28,
		29,
		30,
		31,
		32,
		33,
		34,
		35,
		36,
		37,
		38
	}
	
	for i,skill_id in ipairs(skill_list) do
		AddChaSkill(role,skill_id,1,1,0) --TODO: Remove class restriction from skillinfo.
	end
	
	--BUG: Need to call all passive skills of the player with "unuse" event. Attack getting negative for some reason.
	--[[
	for i,v in ipairs(range(1,500)) do --max number of skills
		--player_skills[v] = GetSkillLv(role, v)
		local skill_level = GetSkillLv(role, v)
		if skill_level > 0 then
			skill(PLAYER_SKILL, v, SK_EVENT_USE, GetSkillLv(role, v), role)
		end
	end
	--]]
	
	--players[player.id].avoid_passive_skill_calls = false
	RefreshCha(role)
end

--Char creation function
function CreatCha(role)
	local char = char_wrap(role)
	local player = player_wrap(role)
	local stats = char.stats
	stats[ATTR_AP] = stats[ATTR_AP] + 4
	print('testing-3')
	AttrRecheck(role)
	stats[ATTR_HP] = stats[ATTR_MXHP]
	stats[ATTR_SP] = stats[ATTR_MXSP]
	stats[ATTR_BSTR]=5
	stats[ATTR_BACC]=5
	stats[ATTR_BAGI]=5
	stats[ATTR_BCON]=5
	stats[ATTR_BSPR]=5
	stats[ATTR_BLUK]=5
	print('testing-2')
	AttrRecheck(role)
	
	--Same as init_cha_item.txt file:
	if IsPlayer(role) then
		print('testing-1')
		GiveItem(role, 0, 0289, 1, 0)
		GiveItem(role, 0, 0641, 1, 0)
		GiveItem(role, 0, 0008, 1, 0)
		print('testing0')
		
		--Make it a pk server:
		print('testing1')
		stats[ATTR_LV]=1
		print('testing2')
		stats[ATTR_TP]=0
		print('testing3')
		
		AttrRecheck(role)
		relearn_skills(role)
	end
end

function Shengji_Shuxingchengzhang( role ) --LVL UP
	print('abc')
	local char = char_wrap(role)
	local player = player_wrap(role)
	local stats = char.stats
	
	stats[ATTR_BSTR]=stats[ATTR_BSTR]
	stats[ATTR_BACC]=stats[ATTR_BACC]
	stats[ATTR_BAGI]=stats[ATTR_BAGI]
	stats[ATTR_BCON]=stats[ATTR_BCON]
	stats[ATTR_BSPR]=stats[ATTR_BSPR]
	stats[ATTR_BLUK]=stats[ATTR_BLUK]

	local lv = stats[ATTR_LV]
	local ap_extre
	
	if (math.floor(lv/10)-math.floor((lv-1)/10)) == 1 then 
		ap_extre = 5 
	else
		ap_extre = 1 
	end 

	if lv >= 60 then
		ap_extre = ap_extre + 1 
	end 
	if lv >= 105 then 
		ap_extre = 1
	end

	stats[ATTR_AP]=stats[ATTR_AP]+ap_extre
	
	local tp_extre
	
	if lv > 9 then 
		tp_extre = 1 
	end 

	if lv >= 65 then 
		if (math.floor (lv/5)-math.floor ((lv-1)/5)) == 1 then 
			tp_extre = 2
		else
			tp_extre = 1
		end
	end
	
	stats[ATTR_TP]=stats[ATTR_TP]+tp_extre

	AttrRecheck(role)
end

function ALLExAttrSet ( role )				--���ݽ�ɫ��̬�ֱ�ˢ�µ�ǰ����
	print('allexattrset')
	--[[
	if IsPlayer ( role ) == 0 then				--��ɫΪ����
		ExAttrSet ( role ) 
		return 
	end 
	if ChaIsBoat ( role ) == 0 then			--��ɫ��̬Ϊ����
		AttrRecheck ( role ) 
	else								--��ɫ��̬Ϊ��ֻ
		cha_role = GetMainCha ( role ) 
		ShipAttrRecheck ( cha_role , role ) 
	end 
	--]]
	AttrRecheck ( role ) 
end 

--Stat recheck function
function AttrRecheck(role)
	print('gjhi')
	--[[
	Function called whenever stats are recalculated. The following must apply in order:
	- Base primary stats should not be set here, because player is the one that customizes it.
	- Current primary stats have to be recalculated first. Current secondary stats are based on them.
	- Base secondary stats are set. These don't change with class.
	- Then those are updated with class based stats.
	- In the end, secondary stats are recalculated with the updated base secondary stats.
	--]]

	local char = char_wrap(role)
	local player = player_wrap(role)
	local stats = char.stats
	
	--make main stats recalculate themselves by setting them to something (because main ones have to be recalculated from parts).
	local i=ATTR_COUNT_BASE1
	for i=ATTR_COUNT_BASE1,ATTR_COUNT_BASE1+5 do
		stats[i]=0
	end

	function sr(ratio)
		return class_sr[stats[ATTR_CLASS]][ratio+1]
	end
	function rsr(ratio, relation)
		return class_sr[class_relation(stats[ATTR_CLASS],relation)][ratio+1]
	end

	--Base stats stick after logout, so don't set base values.
	--TODO: Put this stat initialization on EudemonMission (or on a function for that startup).
	--stats[ATTR_BSTR]=5
	--stats[ATTR_BACC]=5
	--stats[ATTR_BAGI]=5
	--stats[ATTR_BCON]=5
	--stats[ATTR_BSPR]=5
	--stats[ATTR_BLUK]=5
	stats[ATTR_BMXHP]=40
	stats[ATTR_BMXSP]=5
	stats[ATTR_BATK]=1
	stats[ATTR_BDEF]=0
	stats[ATTR_BMATK]=1
	stats[ATTR_BMDEF]=0
	stats[ATTR_BHIT]=0
	stats[ATTR_BFLEE]=0
	stats[ATTR_BCRT]=0
	stats[ATTR_BCRTR]=0
	stats[ATTR_BDROP]=100
	stats[ATTR_BHREC]=5 -- was 0
	stats[ATTR_BSREC]=5 -- was 0
	stats[ATTR_BASPD]=100
	stats[ATTR_BMSPD]=450
	stats[ATTR_BEXP]=100

	--put class specific stats here

	--Main stats (additive main stats with level):
	--Disabled because everytime base stats change, this adds tons of stats to player.
	--[[
	if (class_level(stats[ATTR_CLASS]) >= 1) and (stats[ATTR_LV] >= class_req_plvl(stats[ATTR_CLASS])) then
	for main_stat_idx=ATTR_BSTR, ATTR_BLUK do
	stats[main_stat_idx]=stats[main_stat_idx]+
	sr(CLASS_RATIO_STR_LVL+(main_stat_idx-ATTR_BSTR))*(stats[ATTR_LV]-class_req_plvl(stats[ATTR_CLASS]))

	if (class_level(stats[ATTR_CLASS]) == 2) then
	stats[main_stat_idx]=stats[main_stat_idx]+
	rsr(CLASS_RATIO_STR_LVL+(main_stat_idx-ATTR_BSTR),"<")*(class_req_plvl(stats[ATTR_CLASS])-class_req_plvl(class_relation(stats[ATTR_CLASS],"<")))
	end
	end
	end
	--]]

--[[
CLASS_RATIO_STR_LVL		= 0
CLASS_RATIO_ACC_LVL		= 1
CLASS_RATIO_AGI_LVL		= 2
CLASS_RATIO_CON_LVL		= 3
CLASS_RATIO_SPR_LVL		= 4
CLASS_RATIO_LUK_LVL		= 5
CLASS_RATIO_HP_CON		= 6
CLASS_RATIO_HP_LVL 		= 7
CLASS_RATIO_SP_SPR 		= 8
CLASS_RATIO_SP_LVL		= 9
CLASS_RATIO_ATK_STR		= 10
CLASS_RATIO_ATK_ACC		= 11
CLASS_RATIO_ATK_LUK		= 12
CLASS_RATIO_MATK_SPR	= 13
CLASS_RATIO_MATK_LUK	= 14
CLASS_RATIO_DEF_CON		= 15
CLASS_RATIO_MDEF_LUK	= 16
CLASS_RATIO_HIT_ACC		= 17
CLASS_RATIO_HIT_LUK		= 18
CLASS_RATIO_FLEE_AGI	= 19
CLASS_RATIO_FLEE_LUK	= 20
CLASS_RATIO_DROP_LUK	= 21
CLASS_RATIO_CRT_LUK		= 22
CLASS_RATIO_CRTR_CON	= 23
CLASS_RATIO_CRTR_SPR	= 24
CLASS_RATIO_HREC_BMXHP	= 25
CLASS_RATIO_HREC_CON	= 26
CLASS_RATIO_SREC_BMXSP	= 27
CLASS_RATIO_SREC_SPR	= 28
CLASS_RATIO_ASPD_AGI	= 29
CLASS_RATIO_MSPD_AGI	= 30
--]]

--									STATS					HP		SP			ATK						MATK			DEF		MDEF	HIT				FLEE			DROP	CRT		CRTR			HREC			SREC			APSD	MSPD
--	class_sr[CLASS_NEWBIE]={		0,	0,	0,	0,	0,	0,	1,	15,	0.1,	3,	0.3,	0.3,	0.1,	0.3,	0.1,	0.10,	0.10,	0.5,	0.1,	0.5,	0.1,	0.20,	0.10,	0.10,	0.15,	0.005,	0.125,	0.005,	0.050,	0.9,	0.20}
--	class_sr[CLASS_BERSERKER]={		2,	0,	0,	0,	0,	0,	3,	25,	0.5,	5,	1.0,	0.0,	0.3,	0.0,	0.0,	0.18,	0.18,	0.7,	0.1,	0.7,	0.1,	0.45,	0.40,	0.40,	0.0,	0.015,	0.375,	0.015,	0.100,	1.1,	0.20}

	--uses, for example, ATTR_CON instead of ATTR_BCON, because secondary stats are based on what the current CON is, not base CON.
	stats[ATTR_BMXHP]=stats[ATTR_BMXHP]+
	5*stats[ATTR_CON]*3+
	5*(math.floor(stats[ATTR_CON]/5)^2)+
	50*stats[ATTR_LV]

	stats[ATTR_BMXSP]=stats[ATTR_BMXSP]+
	2*stats[ATTR_SPR]*3+
	1*(math.floor(stats[ATTR_SPR]/5)^2)+
	4*stats[ATTR_LV]

	stats[ATTR_BATK]=
	(
	1*stats[ATTR_STR]+
	0.33*stats[ATTR_ACC]+
	0.20*stats[ATTR_LUK]
	)+
	(
	1*(math.floor(stats[ATTR_STR]/5)^2)+
	0.33*(math.floor(stats[ATTR_ACC]/5)^2)+
	0.20*(math.floor(stats[ATTR_LUK]/5)^2)
	)

	stats[ATTR_BDEF]=5+
	0.15*stats[ATTR_CON]*5+
	0.15*(math.floor(stats[ATTR_CON]/5)^2)

	stats[ATTR_BMATK]=
	(
	1*stats[ATTR_SPR]+
	0.33*stats[ATTR_ACC]+
	0.20*stats[ATTR_LUK]
	)+
	(
	1*(math.floor(stats[ATTR_SPR]/5)^2)+
	0.33*(math.floor(stats[ATTR_ACC]/5)^2)+
	0.20*(math.floor(stats[ATTR_LUK]/5)^2)
	)

	stats[ATTR_BMDEF]=5+
	0.15*stats[ATTR_LUK]*5+
	0.15*(math.floor(stats[ATTR_LUK]/5)^2)

	stats[ATTR_BHIT]=5+
	1*stats[ATTR_ACC]+
	stats[ATTR_LV]*2

	stats[ATTR_BFLEE]=5+
	1*stats[ATTR_AGI]+
	stats[ATTR_LV]*2

	stats[ATTR_BCRT]=11+
	0.3*stats[ATTR_LUK]*3

	stats[ATTR_BCRTR]=11+
	0.15*stats[ATTR_CON]*3+
	0.15*stats[ATTR_SPR]*3

	stats[ATTR_BDROP]=100+
	0.10*stats[ATTR_LUK]*3

	stats[ATTR_BEXP]=100

	stats[ATTR_BHREC]=math.max(
	0.0125*stats[ATTR_CON]*3+
	0.0125*stats[ATTR_BMXHP]*2
	,1)

	stats[ATTR_BSREC]=math.max(
	0.01*stats[ATTR_SPR]*3/2+
	0.01*stats[ATTR_BMXSP]/2
	,1)

	stats[ATTR_BASPD]=math.floor(math.min(math.floor(65+
	1.1*stats[ATTR_AGI]
	,300)))

	print('testing0')
	stats[ATTR_BMSPD]=450+
	1*stats[ATTR_AGI]

	--checking fusion:

	--[[
	local cha = TurnToCha ( role )
	local Cha_Num = GetChaTypeID( cha )


	local body = GetChaItem ( role , 1 , 2 )
	local hand = GetChaItem ( role , 1 , 3 )
	local foot = GetChaItem ( role , 1 , 4 )


	local Body_ID = GetItemID ( body )
	local Hand_ID = GetItemID ( hand )
	local Foot_ID = GetItemID ( foot )
	local body_gem_id = GetItemAttr ( body , ITEMATTR_VAL_FUSIONID )
	local hand_gem_id = GetItemAttr ( hand , ITEMATTR_VAL_FUSIONID )
	local foot_gem_id = GetItemAttr ( foot , ITEMATTR_VAL_FUSIONID )
	--]]

	--GetChaItem function description:
	--2nd param: 1 = Body, 2 = Bag, 3 = Temp Bag (probably).
	--print(players[0].role)
	--GiveItemX(role,0,825,1,20)

	--SetItemAttr(GetChaItem(role,1,2),ITEMATTR_VAL_FUSIONID,0))
	--local test = FusionItem(GetChaItem(role,1,2), GetChaItem2(role,2,825))
	--SetItemAttr(GetChaItem(role,1,2),ITEMATTR_VAL_LEVEL,50)
	--SetItemAttr (GetChaItem(role,1,2), ITEMATTR_MAXURE , 25000 )
	--SetItemAttr (GetChaItem(role,1,2), ITEMATTR_URE , 25000 )

	--SetItemAttr(GetChaItem(role,2,1),ITEMATTR_VAL_FUSIONID,5000))
	---SetItemAttr(GetChaItem(role,2,1),ITEMATTR_VAL_LEVEL,50)
	---SetItemAttr (GetChaItem(role,2,1), ITEMATTR_MAXURE , 25000 )
	--SetItemAttr (GetChaItem(role,2,1), ITEMATTR_URE , 25000 )

	--print(GetItemName(GetItemID(GetChaItem(role,1,2))))

	--for i=ITEMATTR_COUNT_BASE0,ITEMATTR_VAL_FUSIONID do
	--	print(GetItemAttr(GetChaItem(role,1,2) , i))
	--SetItemAttr ( GetChaItem(role,1,2) , ITEMATTR_VAL_STR , 1 )
	--end
	--SetItemFinalAttr( GetChaItem(role,1,2) , ITEMATTR_VAL_STR , 1 )

	--SetItemAttr can edit the following (of equips):
	--ENERGY,MAXENERGY,ATK,MATK,DEF,MDEF.
	--All other items can have everything editted.

	--ResetItemFinalAttr(GetChaItem(role,1,2))

	--print(DealAllPlayerInMap())

	--SynChaKitbag(role,13)

	--print(GetItemAttrRange(GetChaItem(role,1,2), ITEMATTR_VAL_MAXENERGY))

	--check if main glove weapon is equipped, then remove any other weapon equipped with it except the left glove gem holder.
	local main_hand_weapon = item_wrap(GetChaItem(role,1,9),player)
	local off_hand_weapon = item_wrap(GetChaItem(role,1,6),player)
	
	print('test2')
	if (off_hand_weapon ~= -1) and (off_hand_weapon.type == 6) then
		if (main_hand_weapon == -1) then
			RemoveChaItem(player.role, 0, 1, 1, 6, 1, 0, 1)
		elseif ((main_hand_weapon ~= 1) and ((main_hand_weapon.type ~= 6) or (main_hand_weapon.id ~= (off_hand_weapon.id-139)))) then
			RemoveChaItem(player.role, 0, 1, 1, 9, 1, 0, 1)
		end
	end
	
	print('test3')
	if (main_hand_weapon ~= -1) and (main_hand_weapon.type == 6) then
		if (off_hand_weapon ~= -1) and ((off_hand_weapon.type ~= 6) or (off_hand_weapon.id ~= (main_hand_weapon.id+139))) then
			RemoveChaItem(player.role, 0, 1, 1, 6, 1, 0, 1)
		end
	end
	print('test4')
	
	-- Skill list: Has to follow requirements, but char already starts with lvl 130, and will have no class or char requirements.

		--SK_EVASION,
--		SK_GREATERHEAL,
--		SK_GREATERRECOVER,
	--SK_REBIRTHMYSTICPOWER,
	
	--make main stats recalculate themselves by setting them to something (because main ones have to be recalculated from parts).
	local i=ATTR_COUNT_BASE1
	for i=ATTR_COUNT_BASE1+6,ATTR_COUNT_BASE1+21 do
		stats[i]=0
	end
end


function Check_Baoliao(ATKER, DEFER, ... ) --[[�ж��Ƿ���,���빥���ߵȼ����ܻ��ߵȼ��������߱����ʡ��ܻ��߱�����]]--
	--local arg = ...
	--LuaPrint("Enter function Check_Baoliao(Atker,Defer,mf_atker,mf_defer) --[[determine if it is drop item]]--".."\n" ) 
	--LG("Drop List", "Enter function Check_Baoliao(Atker,Defer,mf_atker,mf_defer) --[[determine if it is drop item]]--","\n" ) 
	--Atker = TurnToCha ( ATKER ) 
	--Defer = TurnToCha ( DEFER ) 
	--local lv_atker = Lv(Atker)
	--local lv_defer = Lv(Defer)
	--local count = #arg
	print('Check_Baoliao')
	--[[
	local end_items = {}
	for i, chance in ipairs(arg) do
		print(10000/chance)
		if (math_percent_random(10000/chance)) == true then
			table.insert(end_items,i) --used to be table.insertfix (which adds to .n separately)
		end
	end
	SetItemFall(end_items.n,table.unpack(end_items))
	--]]
	--SetItemFall:
	--1st param: amount of items to drop - maximum is the amount of different items the char is defined to drop (which has a maximum of 10).
	--2-infinite amount of parameters (although max ends up being 10 because of 1st parameter's limit):
	--1 = first item in mob drop index to drop, 2 = second... and so forth.
	--[[
	local itemid = 4602
	local amount = 1
	print('abcghy1')
    GiveItem(DEFER,0,itemid,amount,4)
    --- Drop the item ---
	print('abcghy2')
    RemoveChaItem(DEFER, itemid , amount , 2 , -1, 0 , 1  )
	print('abcghy3')
	--]]
	local _char = char_wrap(DEFER)
	print('printing char:'.._char.name)
	
	local i,v
	for i,v in ipairs(tsv.characterinfo[_char.name]['drop_item_ids']) do
		if math_percent_random(100/tsv.characterinfo[_char.name]['drop_item_rates'][i]) then
			print(i,v,'teststdfts ')
			GiveItem(DEFER,0,v,1,4) --amount is 1
			RemoveChaItem(DEFER, v, 1, 2 , -1, 0 , 1)
		end
	end
end

function Check_SpawnResource ( ATKER, DEFER , lv_skill , count , ...) --called whenever a resource is hit
	--local arg = ...
	--item = {}
	--local count = #arg
	
	print('SpawnResource')
	--make it same as killing regular mobs
	--if char_wrap(DEFER).stats[ATTR_HP] <= 0 then
		Check_Baoliao( ATKER, DEFER , ...)
	--end
end

function AskGuildItem(role,Guild_type)
	return 1
end

function DeductGuildItem(role,Guild_type)
end

function AskJoinGuild(role,guild_type) 					-- ������빤���ж�   �������� 0��������1������
	local attr_guild = HasGuild(role)  
	if attr_guild ~= 0 then 
		HelpInfo(role,0,"You are already in a guild.")
		return 0 
	end 
	return 1
end

function GetExp_New(dead , atk) -- Gives "atk" the exp returned. "dead" is the player "atk" killed.
	print('GetExp_New')
	local char = char_wrap(atk)
	local player = player_wrap(atk)
	local stats = char.stats

	local charD = char_wrap(dead)
	local playerD = player_wrap(dead)
	local statsD = charD.stats
	
	local total_gained_exp = statsD[ATTR_CEXP]
	if stats[ATTR_LV] >= 80 then
		total_gained_exp = total_gained_exp / 50
	end
	if stats[ATTR_LV] == 130 then
		total_gained_exp = 0
	end
	
	--print('blargh',stats[ATTR_LV],total_gained_exp, statsD[ATTR_CEXP], stats[ATTR_CEXP], stats[ATTR_NLEXP], stats[ATTR_CLEXP], (stats[ATTR_NLEXP] - stats[ATTR_CLEXP]))
	while total_gained_exp >= (stats[ATTR_NLEXP] - stats[ATTR_CLEXP]) and  stats[ATTR_LV] < 130 do
		stats[ATTR_CEXP] = stats[ATTR_CEXP] + (stats[ATTR_NLEXP] - stats[ATTR_CLEXP])
		total_gained_exp = total_gained_exp - (stats[ATTR_NLEXP] - stats[ATTR_CLEXP])
		--stats[ATTR_LV] = stats[ATTR_LV] + 1 --already gains level automatically
		
		if stats[ATTR_LV] == 80 then
			total_gained_exp = total_gained_exp / 50
		end
		if stats[ATTR_LV] == 130 then
			total_gained_exp = 0
		end
	end
	
	local frac = 0
	total_gained_exp, frac = math.modf(total_gained_exp)
	if (frac > 0) and math_percent_random(frac) then
		total_gained_exp = total_gained_exp + 1
	end
	
	stats[ATTR_CEXP] = stats[ATTR_CEXP] + total_gained_exp
	AttrRecheck(atk)
end

function EightyLv_ExpAdd ( cha , expadd )
	print('EightyLv_ExpAdd')
	local char = char_wrap(cha)
	local player = player_wrap(role)
	local stats = char.stats
	print(expadd)
	stats[ATTR_CEXP] = stats[ATTR_CEXP] + expadd / 50
end

--TODO:
--Check callback functions starting at dofile(GetResPath("script\\calculate\\functions.lua")).

function is_teammate(cha1, cha2) 
    if cha1 == 0 or cha2 == 0 then 
        return 0 
    end 
    if cha1 == cha2 then 
        return 1 
    end
    local ply1 = GetChaPlayer(cha1) 
    local ply2 = GetChaPlayer(cha2) 
    if ply1 ~= 0 and ply2 ~= 0 then 
        if ply1 == ply2 then 
            return 1 
        end 
        local team_id1, team_id2 
        team_id1 = GetChaTeamID(cha1) 
        team_id2 = GetChaTeamID(cha2) 
        if team_id1 ~= 0 and team_id2 ~= 0 and team_id1 == team_id2 then 
            return 1 
        end 
    end 

    return 0 
end 

function is_friend(pDrole, prole) --all skills seem to obey this
	--Map type is actually implemented here (that hard-coded function actually can be applied!)
	--GetChaMapType( cha1 )
	--TODO: Code party/guild PKs.
	if (pDrole == prole) then --makes char itself friendly to itself - seems dumb.
		return 1
	end
	return 0
--[[
--	SystemNotice ( cha1, "transferis_friend" ) 
    local friend_target = 1 
    local Map_type = GetChaMapType( cha1 )
    if CheckChaRole( cha1 ) == 0 and Map_type ~= 2 then			--���cha1Ϊ�����Ҳ��ڹ����ͼ�У���cha2Ϊ����Ϊ�ѷ���cha2Ϊ��Ϊ�з�
	if CheckChaRole( cha2 ) == 0 then							
		return 1 
	else 
		return 0 
	end 

    end
	   
--		SystemNotice ( cha1 , "1" ) 
--		SystemNotice ( cha1 , "Map_type ="..Map_type ) 
--	    if Map_type == 4 then									--�Թ�PK��ͼ�ѷ��ж�
--		local team = is_teammate(cha1, cha2) 
--		if team == 1 then 
--			return 1 
--		end 
--	    else
--		return 0
--	    end
		if Map_type == 1 then 
			if CheckChaRole ( cha1 ) == 1 then 
					if CheckChaRole( cha2 ) == 0 then							
						return 0 
					else 
						return 1 
					end 

			end 
		end 

		if Map_type == 4 then									--�Թ�PK��ͼ�ѷ��ж�
			if Is_NormalMonster (cha1) == 1 then						--�������ͨ��������������ѷ�
				if Is_NormalMonster (cha2) == 1 then 
					return 1 
				end 
			end 
			local team = is_teammate(cha1,cha2) 
			if team == 1 then 
				return 1 
			else 
				local guild_id1, guild_id2 
				guild_id1 = get_cha_guild_id(cha1) 
				guild_id2 = get_cha_guild_id(cha2) 
				if guild_id1 ~= 0 and guild_id2 ~= 0 and guild_id1 == guild_id2 then 
					return 1  
				else
					return 0 
				end
			end
			--local team = is_teammate(cha1,cha2) 
			--		if team == 1 then 
			--			return 1 
			--		else 
			--			return 0 
			--		end 
		end 

--	        SystemNotice ( cha1 , "2" ) 

	    if  Map_type ==3 then									--�Ƕ���ս��ͼ�����Ϊ�ѷ�
--		SystemNotice ( cha1 , "1" ) 
		    local team = is_teammate(cha1, cha2) 
--		    		SystemNotice ( cha1 , "2" ) 

		    if team == 1 then 
--		    		SystemNotice ( cha1 , "3" ) 
		        return 1 
		    else 
			return 0 
		    end
		   
	    end

	    if cha1 == 0 or cha2 == 0 then							--ָ��Ϊ��
	        return 0 
	    end 
--		SystemNotice ( cha1 , "3" ) 
	    if Map_type == 2 then									--����PK��ͼ�ж�
		if Is_NormalMonster (cha1) == 1 then						--�������ͨ��������������ѷ�
			if Is_NormalMonster (cha2) == 1then 
				return 1 
			end 
		end 
		local guild_id1, guild_id2 
		guild_id1 = get_cha_guild_id(cha1) 
		guild_id2 = get_cha_guild_id(cha2) 
		if guild_id1 ~= 0 and guild_id2 ~= 0 and guild_id1 == guild_id2 then 
			return 1 
		else 
			return 0 
		end

	    end 
	    if Map_type == 5 then									--����PK��ͼ�ж�
--		if Is_NormalMonster (cha1) == 1 then						--�������ͨ��������������ѷ�
--			if Is_NormalMonster (cha2) == 1then 
--				return 1 
--			end 
--		end 
		local guild_side_1, guild_side_2 
		guild_side_1 = GetChaSideID(cha1) 
		guild_side_2 = GetChaSideID(cha2) 
		--if guild_side_1 <= 100 and guild_side_1 > 0 and guild_side_2 <= 100 and guild_side_2 > 0 then
		if guild_side_1 == guild_side_2 then
			return 1 
		--elseif
		   --guild_side_1 > 100 and guild_side_1 <= 200 and guild_side_2 > 100 and guild_side_2 <= 200 then
			--return 1
	        else
		        return 0
		end

	    end 
--		SystemNotice ( cha1 , "4" ) 

	    return friend_target 
--]]
 end

--����ʵ��������Ϣ
BASE_ENTITY			= 0		--����ʵ��
RESOURCE_ENTITY	= 1		--��Դʵ��
TRANSIT_ENTITY		= 2		--����ʵ��
BERTH_ENTITY		= 3		--ͣ��ʵ��

--�����¼�ʵ��
function CreateBerthEntity( name, cid, infoid, xpos1, ypos1, dir1, berth, xpos2, ypos2, dir2 )
	local ret, submap = GetCurSubmap()
	if ret ~= LUA_TRUE then
		return
	end
	local ret, e = CreateEventEntity( BERTH_ENTITY, submap, name, cid, infoid, xpos1, ypos1, dir1 )
	if ret ~= LUA_TRUE then
		return
	end
	ret = SetEntityData( e, berth, xpos2, ypos2, dir2 )
	if ret ~= LUA_TRUE then
		return
	end
end

--������Դʵ��
function CreateResourceEntity( name, cid, infoid, xpos, ypos, dir, itemid, count, time )
	local ret, submap = GetCurSubmap()
	if ret ~= LUA_TRUE then
		return
	end
	local ret, e = CreateEventEntity( RESOURCE_ENTITY, submap, name, cid, infoid, xpos, ypos, dir )
	if ret ~= LUA_TRUE then
		return
	end
	ret = SetEntityData( e, itemid, count, time )
	if ret ~= LUA_TRUE then
		return
	end
end

function ShipAttrRecheck ( cha_role , ship_role )
	print('ShipAttrRecheck')
end

function Lifelv_Up ( cha_role )
	print('Lifelv_Up')
end

function Saillv_Up ( cha_role )
	print('Saillv_Up')
end

function BoatLevelUp( character, boat, levelup )
	print('BoatLevelUp')
	return C_TRUE
end

function Ship_Tran (  buyer , boat ) 
	print('Ship_Tran')
end 

function Ship_ExAttrSet ( cha_role , ship_role )									--��ֻ��������
	print('Ship_ExAttrSet')
end

function WGPrizeBegin( role , rightCount)
	print('WGPrizeBegin')
end

function Ship_ExAttrCheck (player_role, ship_role )
	player = player_wrap(player_role)
	print('Ship_ExAttrCheck')

	SetCharaAttr(player.stats[ATTR_JOB], ship_role, ATTR_JOB )											--[[��ֵ��ְֻҵ]]--
	SetCharaAttr(player.stats[ATTR_SPR], ship_role, ATTR_SPR )											--[[��ֵ��ֻ����]]--
	SetCharaAttr(1000, ship_role, ATTR_ATK )									--[[��ֵ����mnatk]]--
	SetCharaAttr(1000, ship_role, ATTR_MATK )									--[[��ֵ����mxatk]]--
	SetCharaAttr(1000, ship_role, ATTR_CRTR )									--[[��ֵ����adis]]--
	SetCharaAttr(1000, ship_role, ATTR_BOAT_CSPD )								--[[��ֵ���մ�ֻ�ڵ������ٶ�]]--
	SetCharaAttr(1000, ship_role, ATTR_ASPD )									--[[��ֵ���մ�ֻ�����ٶ�]]--
	SetCharaAttr(1000, ship_role, ATTR_BOAT_CRANGE )								--[[��ֵ���մ�ֻ�ڵ���ը��Χ]]--
	SetCharaAttr(1000, ship_role, ATTR_DEF )										--[[��ֵ���մ�ֻ����]]--
	SetCharaAttr(1000, ship_role, ATTR_MDEF )									--[[��ֵ���մ�ֻ�ֿ�]]--
	SetCharaAttr(1000, ship_role, ATTR_MXHP )									--[[��ֵ���մ�ֻ����;�]]--
	SetCharaAttr(1000, ship_role, ATTR_HREC )									--[[��ֵ���մ�ֻ�;ûظ��ٶ�]]--
	SetCharaAttr(1000, ship_role, ATTR_SREC )									--[[��ֵ���մ�ֻ���������ٶ�]]--
	SetCharaAttr(1000, ship_role, ATTR_MSPD )									--[[��ֵ���մ�ֻ�ƶ��ٶ�]]--
	SetCharaAttr(1000, ship_role, ATTR_MXSP )									--[[��ֵ���մ�ֻ��󲹸�ֵ]]--
	SetCharaAttr(1000, ship_role , ATTR_FLEE )	
end

BerthPortList = {
	"Argent Harbor",
	"Thundoria Harbor",
	"Laboratory Harbor",
	"Icicle Harbor",
	"Zephyr Harbor",
	"Glacier Harbor",
	"Outlaw Harbor",
	"Harbor of Chill",
	"Canary Harbor",
	"Cupid Harbor",
	"Harbor of Fortune",
	"Mystery Harbor",
	"Spring Harbor",
	"Summer Southern Harbor",
	"Pirate Cove Harbor"
}
BerthPortList[34] = "Phantom Harbor"

function AddBerthPort( id, name )
	BerthPortList[id] = {}
	BerthPortList[id].name = name
end

function GetBerthData( id )
	if id == nil or BerthPortList[id] == nil then
		return "Harbor "..id
	end
	return BerthPortList[id]
end

function CheckExpShare ( ti , atk )
	print('CheckExpShare')
	return 1 
end

function PackBagGoods( character, boat, tp, level )
	print('PackBagGoods')
	return C_TRUE
end

function Resume ( role )
	print('Resume')
end