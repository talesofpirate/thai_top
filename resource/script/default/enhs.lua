player_enhans = {}

function load_enhans()
	myfile = io.open(GetResPath('script/'.. mod .. '/volatile/enhs.lua'),'rb')
    player_enhans = assert(loadstring("return " .. myfile:read()))()
	myfile:close()
end

function save_enhans()
	myfile = io.open(GetResPath('script/'.. mod .. '/volatile/enhs.lua'),'wb')
	myfile:write(mytostring(player_enhans))
	myfile:close()
end

load_enhans()

--TODO: Check forge.lua to see if it's easier to implement things there.