

----??????????

function Read_Table ( Table )
	local role = Table [1]										--???
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local Get_Count = 4
	local ItemReadCount = 0
	local ItemReadNow = 2
	local ItemReadNext = 0
	local ItemBag_Now = 0
	local ItemCount_Now = 0
	local ItemBagCount_New = 0
	local i = 0
	local j = 0

	for i = 0 , Get_Count , 1 do

		if ItemReadNow <= Table.n then
			ItemBagCount [i] = Table [ItemReadNow]
			ItemBagCount_New = ItemBagCount_New + 1
			ItemReadNow = ItemReadNow + 1
			ItemReadNext = ItemReadNow + 2 * ( ItemBagCount [i] - 1 )
			ItemReadCount = ItemReadNow
			if ItemBagCount [i] ~= 0  then
				for j = ItemReadCount , ItemReadNext , 2 do
					ItemBag [ItemBag_Now] = Table [j]
					ItemBag_Now = ItemBag_Now + 1
					ItemCount [ItemCount_Now] = Table [ j+1 ]
					ItemCount_Now = ItemCount_Now + 1
					ItemReadNow = ItemReadNow + 2
				end
			end
		else
			ItemBagCount [i] = 0
		end
	end
	return role , ItemBag , ItemCount , ItemBagCount , ItemBag_Now , ItemCount_Now , ItemBagCount_New
end

--fix this
function process_linked_item_list(baselist)
	local role = baselist[1]
	local player = player_wrap(baselist[1])
	local items = {}
	local temp_item = {}
	--	myprint(baselist)
	for i,param in ipairs(baselist) do
		if (i ~= 1) then
			if ((math.fmod(i,3)) == 0) then -- #2 = item userdata -- param=inventory slot number
				table.insert(items, item_wrap(GetChaItem(role,2,param)))
				print(items[table.getn(items)].name)
				items[table.getn(items)].player = player
				items[table.getn(items)].bagslot = param
			elseif ((math.fmod(i,3)) == 1) then -- #3 = amount -- param=item amount
				items[table.getn(items)].amount = param
			elseif ((math.fmod(i,3)) == 2) and (param == 0) then -- #1 = bagtype, when 0 = nil.
				break
			end
		end
	end
	return player, items
end

function can_unite_item(...)
	local player, items = process_linked_item_list{...}
	if table.getn(items) >= 3 then
		if items[1].id ~= 886 then
			SystemNotice(player.role, "Gem Combining: The top item must be Gem Composition Scroll.")
			return C_FALSE
		end
		--		if items[2].type ~= 49 and items[3].type ~= 49 then
		--			SystemNotice(player.role, "Gem Combining: Top Left/Right items must be gems.")
		--			return C_FALSE
		--		end
		if items[2].type ~= items[3].type then
			SystemNotice(player.role, "Gem Combining: Both gems must be of same kind.")
			return C_FALSE
		end
		--		if items[2].stats[ITEMATTR_VAL_GEMLV] == items[3].stats[ITEMATTR_VAL_GEMLV] and
		--			SystemNotice(player.role, "Gem Combining: Both gems must be of same level.")
		--			return C_FALSE
		--		end
		if items[2].stats[ITEMATTR_VAL_GEMLV] >= 9 and
		items[3].stats[ITEMATTR_VAL_GEMLV] >= 9 then
			SystemNotice(player.role, "Gem Combining: Both gems already are on max levels.")
			return C_FALSE
		end
		if player.stats[ATTR_GD] <= 50000 then
			SystemNotice(player.role, "Gem Combining: Insufficient gold.")
			return C_FALSE
		end
	else
		SystemNotice ( role ,"Gem Combining: Not enough items to combine gems properly.")
		return C_FALSE
	end
	return C_TRUE
end

--WORKING
--??'???
function begin_unite_item (...)
	local arg = ...
	--	Notice("??'???")

	local Check_CanUnite = 0
	--	SystemNotice ( arg[1] , "???ü?????????????")
	Check_CanUnite = can_unite_item_main ( arg )
	if Check_CanUnite == 0 then
		return 0
	end

	local role = 0
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local Get_Count = 4
	local ItemReadCount = 0
	local ItemReadNow = 1
	local ItemReadNext = 0
	local ItemBag_Now = 0
	local ItemCount_Now = 0
	local ItemBagCount_Num = 0

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Now , ItemCount_Now , ItemBagCount_Num = Read_Table ( arg )


	local BagItem1 = ItemBag [0]
	local BagItem2 = ItemBag [1]
	local BagItem3 = ItemBag [2]

	local BagItem1 = arg [3]						--???????????
	local BagItem2 = arg [6]
	local BagItem3 = arg [9]

	local Item1 = GetChaItem ( role , 2 , BagItem1 )			--????????
	local Item2 = GetChaItem ( role , 2 , BagItem2 )
	local Item3 = GetChaItem ( role , 2 , BagItem3 )

	local ItemID1 = GetItemID ( Item1 )					--???????
	local ItemID2 = GetItemID ( Item2 )
	local ItemID3 = GetItemID ( Item3 )

	local ItemType2 = GetItemType ( Item2 )

	local Item2_Lv = Get_StoneLv ( Item2 )					--???????
	local Item3_Lv = Get_StoneLv ( Item3 )

	local i = 0
	local j = 0

	i = RemoveChaItem ( role , ItemID1 , 1 , 2 , BagItem1 , 2 , 1 , 0)		--??????
	j = RemoveChaItem ( role , ItemID3 , 1 , 2 , BagItem3 , 2 , 1 , 0)		--???h?u??

	if i == 0 or j == 0 then
		LG( "Hecheng_BS" , "?????????" )
	end

	Item2_Lv =Item2_Lv + 1

	Set_StoneLv ( Item2 , Item2_Lv )

	local Money_Need = getunite_money_main ( arg )
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	--	Notice ("?j??"..Money_Have.."??ô????")
	--	Notice ("????"..Money_Need.."??ô????")
	Money_Have = Money_Have - Money_Need
	SetCharaAttr ( Money_Have , role , ATTR_GD )
	ALLExAttrSet( role )

	local Sklv = 1
	local StateLv = GetChaStateLv ( role , STATE_HCGLJB )
	local State100 = GetChaStateLv ( role , STATE_HCBYS )	------?????

	Sklv = Sklv + StateLv


	local b = Check_CG_HechengBS ( Item2_Lv , ItemType2 , Sklv )
	if Item2_Lv < 8 and Item3_Lv < 8 then
		if State100 == 11 then
			b = 1
			SystemNotice( role , "???????????????????100%")
		end
	end
	if b == 0 then
		i = RemoveChaItem ( role , ItemID2 , 1 , 2 , BagItem2 , 2 , 1 , 0)		--????
		if i == 0 then
			LG( "Hecheng_BS" , "?????????" )
		end
		local cha_name = GetChaDefaultName ( role )
		LG( "JingLian_ShiBai" , "???"..cha_name.."????????" )
		SystemNotice( role , "???z????????????????????........")

		return 2
	end
	local cha_name = GetChaDefaultName ( role )
	LG( "JingLian_ShiBai" , "???"..cha_name.."????????" )
	return 1
end

function get_item_unite_money (...)
	return 0
end


function can_forge_item(...)
	return C_TRUE
end

StoneEff_Num = 4		StoneAttrType_Num = 4
item_gems_table = {860,861,862,863}

function get_item_gems_table_id(item_id)
	for i=1,StoneEff_Num do
		if (item_gems_table[i] == item_id) then
			return i
		end
	end
	return -1
end

--[[
function begin_forge_item(...)
	local arg = ...
--TODO: Configure forging to support stacks of 511 gems.
local player, items = process_linked_item_list(arg)

if table.getn(items) < 3 then
SystemNotice(player.role, "Forging: Not enough items to forge gem properly.")
return C_FALSE
end
--TODO: Set proper item types to forge (or prevent forging)
--if Check == 0 then
--	SystemNotice( role ,"Item cannot be forged")
--	return 0
--end
if (items[2].type ~= 49) then
SystemNotice(player.role, "Forging: Item in middle slot is of wrong type.")
return C_FALSE
end
if (items[3].type ~= 50) then
SystemNotice(player.role, "Forging: Item in bottom slot is of wrong type.")
return C_FALSE
end

if player.stats[ATTR_GD] <= (items[2].stats[ITEMATTR_VAL_GEMLV] * 10000) then
SystemNotice(player.role, "Forging: Insufficient gold.")
return C_FALSE
end

local gem_lvls_total = 0
for i, gem in ipairs(items[1].gems) do
gem_lvls_total = gem_lvls_total + gem[2]
end
player.stats[ATTR_GD] = player.stats[ATTR_GD] - (gem_lvls_total * 10000)
AttrRecheck(player.role)

--if 	(
--		(RemoveChaItem(player.role, 0, 1, 2, items[2].bagslot, 2, 1, 0) == 0) or --items[2].amount
--		(RemoveChaItem(player.role, 0, 1, 2, items[3].bagslot, 2, 1, 0) == 0) --items[3].amount
--	) then
--	SystemNotice( player.role , "Forging: Inventory might be locked.")
--	return
--end

local gem_id = get_item_gems_table_id(items[2].id)

local forge_gem_slot = 0
local first_empty_slot = 0
for i=1, items[1].gems.slots do
if (items[1].gems[i][1] == gem_id) then -- there's just 1 gem of same type
if (items[1].gems[i][2] >= 9) then
SystemNotice( player.role , "Forging: Maximum level of gem already reached.")
return C_FALSE
end
if (items[1].gems[i][2] >= items[2].stats[ITEMATTR_VAL_GEMLV]) and (items[1].gems[i][2] >= items[3].stats[ITEMATTR_VAL_GEMLV]) then
SystemNotice( player.role , "Forging: Gems are too low leveled to forge properly.")
return C_FALSE
end
forge_gem_slot = i
end
if items[1].gems[i][1] == 0 then
first_empty_slot = i
end
end
if (forge_gem_slot == 0) then
if (first_empty_slot == 0) then
SystemNotice( player.role , "Forging: Not enough space to forge gems.")
return C_FALSE
else
forge_gem_slot = first_empty_slot
end
end

ResetItemFinalAttr(items[1].pointer)
local current_gem_lvl = 0
for i=1, items[1].gems.slots do
if (i == forge_gem_slot) then
items[1].gems[i] = {gem_id,items[1].gems[i][2] + 1}
current_gem_lvl = items[1].gems[i][2]
end
local gem_id_on_slot = items[1].gems[i][1]
for j, stat_pair in ipairs(item_gems_table[gem_id_on_slot][4]) do
AddItemFinalAttr(items[1].pointer, stat_pair[1], stat_pair[2]*items[1].gems[i][2])
end
end

SystemNotice(player.role, "Forging: Successful.")
BickerNotice(player.role, '"'..items[1].name..'" now has "'..items[2].name..'" lvl "'..current_gem_lvl..'" forged in.')
return C_TRUE
end
--]]

function begin_forge_item(...)
	local player, items = process_linked_item_list{...}

	if table.getn(items) < 3 then
		SystemNotice(player.role, "Forging: Not enough items to forge gem properly.")
		return C_FALSE
	end
	if (items[2].type ~= 49) then
		SystemNotice(player.role, "Forging: Item in middle slot is of wrong type.")
		return C_FALSE
	end
	if (items[3].type ~= 50) then
		SystemNotice(player.role, "Forging: Item in bottom slot is of wrong type.")
		return C_FALSE
	end

	local gem_lvls_total = 0
	for i, gem in ipairs(items[1].gems) do
		gem_lvls_total = gem_lvls_total + gem[2]
	end

	if player.stats[ATTR_GD] <= ((gem_lvls_total+1) * 10000) then
		SystemNotice(player.role, "Forging: Insufficient gold.")
		return C_FALSE
	end
	player.stats[ATTR_GD] = player.stats[ATTR_GD] - ((gem_lvls_total+1) * 10000)
	AttrRecheck(player.role)

	local gem_id = get_item_gems_table_id(items[2].id)

	local forge_gem_slot = 0
	local first_empty_slot = 0
	local gems_needed = 0

	--because items[x].amount is never set properly, set it with light_item wrapping.
	items[2].amount = items[2].light.report[1].bag.amount
	items[3].amount = items[3].light.report[1].bag.amount

	for i=1, items[1].gems.slots do
		if (items[1].gems[i][1] == gem_id) then -- there's just 1 gem of same type
			if (items[1].gems[i][2] >= 9) then
				SystemNotice( player.role , "Forging: Maximum level of gem already reached.")
				return C_FALSE
			end
			gems_needed = 2^(items[1].gems[i][2])

			if (items[2].amount < gems_needed) and (items[3].amount < gems_needed) then
				SystemNotice( player.role , 'Forging: Need more "'..gems_needed-items[2].amount..'" "'..items[2].name..'" and more "'..gems_needed-items[3].amount..'" "'..items[3].name..'"."')
				return C_FALSE
			end
			if (items[2].amount < gems_needed) then
				SystemNotice( player.role , 'Forging: Need more "'..gems_needed-items[2].amount..'" "'..items[2].name..'".')
				return C_FALSE
			end
			if (items[3].amount < gems_needed) then
				SystemNotice( player.role , 'Forging: Need more "'..gems_needed-items[3].amount..'" "'..items[3].name..'"."')
				return C_FALSE
			end
			forge_gem_slot = i
		end
		if items[1].gems[i][1] == 0 then
			first_empty_slot = i
		end
	end
	if (forge_gem_slot == 0) then
		if (first_empty_slot == 0) then
			SystemNotice( player.role , "Forging: Not enough space to forge gems.")
			return C_FALSE
		else
			forge_gem_slot = first_empty_slot
			gems_needed = 1
		end
	end

	if 	(
		(RemoveChaItem(player.role, 0, gems_needed, 2, items[2].bagslot, 2, 1, 1) == 0) or
		(RemoveChaItem(player.role, 0, gems_needed, 2, items[3].bagslot, 2, 1, 1) == 0)
	) then
		SystemNotice( player.role , "Forging: Inventory might be locked.")
		return
	end

	local current_gem_lvl = 0
	for i=1, items[1].gems.slots do
		if (i == forge_gem_slot) then
			items[1].gems[i] = {gem_id,items[1].gems[i][2] + 1}
			current_gem_lvl = items[1].gems[i][2]
		end
	end

	SystemNotice(player.role, "Forging: Successful.")
	BickerNotice(player.role, '"'..items[1].name..'" now has "'..items[2].name..'" lvl "'..current_gem_lvl..'" forged in.')
	return C_TRUE
end

--??????????

function get_item_forge_money(...)
	return 0
end

--Socket Making
function can_milling_item (...)
	local player, items = process_linked_item_list{...}
	if table.getn(items) >= 3 then
		if items[2].id ~= 890 then
			SystemNotice(player.role, "Socket Making: The middle item must be Equipment Stabilizer.")
			return C_FALSE
		end
		if items[3].id ~= 891 then
			SystemNotice(player.role, "Socket Making: The bottom item must be Equipment Catalyst.")
			return C_FALSE
		end
		if items[1].gems.slots >= 3 then --max slot number
			SystemNotice(player.role, "Socket Making: Maximum amount of sockets already reached.")
			return C_FALSE
		end
		if items[1].stats[ITEMATTR_URE] < items[1].stats[ITEMATTR_MAXENERGY] then
			SystemNotice(player.role, "Socket Making: Item must have at least 1 durability.")
			return C_FALSE
		end
		if player.stats[ATTR_GD] <= (items[1].gems.slots * 50000) then
			SystemNotice(player.role, "Socket Making: Insufficient gold.")
			return C_FALSE
		end
	else
		SystemNotice ( role ,"Socket Making: Not enough items to make a socket properly.")
		return C_FALSE
	end
	return C_TRUE
end

function get_item_milling_money(...)
	return 0 --Setting price to 0 makes milling faster (no dialog box showing the amount to pay)
end

--??'??h

function begin_milling_item (...)
	if can_milling_item(...) == C_FALSE then
		return C_FALSE
	end

	local player, items = process_linked_item_list{...}
	player.stats[ATTR_GD] = player.stats[ATTR_GD] - (items[1].gems.slots * 50000)
	AttrRecheck(player.role)

	items[1].gems.slots = items[1].gems.slots + 1

	if 	(
	(RemoveChaItem(player.role, 0, 1, 2, items[2].bagslot, 2, 1, 1) == 0) or
	(RemoveChaItem(player.role, 0, 1, 2, items[3].bagslot, 2, 1, 1) == 0)
	) then
		SystemNotice( player.role , "Socket Making: Inventory might be locked.")
		return
	end

	SystemNotice(player.role, "Socket Making: Successful.")
	BickerNotice(player.role, '"'..items[1].name..'" now has "'..items[1].gems.slots..'" gem slot(s).')
	local cha_name = GetChaDefaultName ( role )
	LG( "JingLian_ShiBai" , "Player"..player.name.."Socket Making successful"..items[2].name)
	return 1
end

--Apparel Fusion
function can_fusion_item(...)
	local player, items = process_linked_item_list{...}
	--myprint(items)
	if table.getn(items) >= 3 then
		if items[1].id ~= 453 then
			SystemNotice(player.role, "Apparel Fusion: The top item must be Fusion Scroll.")
			return C_FALSE
		end
		if 	(
		(items[2].stats[ITEMATTR_URE] < items[2].stats[ITEMATTR_MAXENERGY]) or
		(items[3].stats[ITEMATTR_URE] < items[3].stats[ITEMATTR_MAXENERGY])
		) then
			SystemNotice(player.role, "Apparel Fusion: Items must have at least 1 durability.")
			return C_FALSE
		end
		if items[2].type ~= items[3].type then
			SystemNotice(player.role, "Apparel Fusion: Item types mismatch.")
			return C_FALSE
		end
		if CheckFusionItem(items[2].pointer,items[3].pointer) == C_FALSE then
			SystemNotice(player.role, "Apparel Fusion: Equipment type or class requirement doesn't match.")
			return C_FALSE
		end
		if table.getn(items) >= 4 and items[4].id ~= 454 then
			SystemNotice(player.role, "Apparel Fusion: Bottom left item must be Fusion Catalyst.")
			return C_FALSE
		end
		if player.stats[ATTR_GD] <= (items[3].lvl * 1000) then
			SystemNotice(player.role, "Apparel Fusion: Insufficient gold.")
			return C_FALSE
		end
	else
		SystemNotice (player.role, "Apparel Fusion: Not enough items to fuse properly.")
		return C_FALSE
	end
	print('adsdss2')
	return C_TRUE
end

--TODO: Doesn't work with lvl 95 equips (apparently they're bugged on item info... need KOP again).
function begin_fusion_item(...)
	--	Notice("???????")
	--------???????????
	if can_fusion_item(...) == C_FALSE then
		return C_FALSE
	end

	local player, items = process_linked_item_list{...}
	player.stats[ATTR_GD] = player.stats[ATTR_GD] - (items[3].lvl * 1000)
	AttrRecheck(player.role)

	if items[3].stats[ITEMATTR_MAXURE] == 25000 then
		items[2].stats[ITEMATTR_VAL_FUSIONID] = items[3].stats[ITEMATTR_VAL_FUSIONID]
	else
		items[2].stats[ITEMATTR_VAL_FUSIONID] = items[3].id
	end

	if FusionItem(items[2].pointer,items[3].pointer) == C_FALSE then
		SystemNotice( role , "Apparel fusion: Fusion failed (unknown reason).")
		return
	end

	local dest_equip_upgrade_lvl=0
	print('maxure2:'..items[3].stats[ITEMATTR_MAXURE])
	if table.getn(items) >= 4 and items[3].stats[ITEMATTR_MAXURE] >= 25000 then --Modified: made upgrade lvl also reset without catalyst.
		items[2].stats[ITEMATTR_VAL_LEVEL] = items[3].stats[ITEMATTR_VAL_LEVEL]
	else
		items[2].stats[ITEMATTR_VAL_LEVEL] = 10
	end
	items[2].stats[ITEMATTR_MAXURE] = 25000
	items[2].stats[ITEMATTR_URE] = 25000

	if (table.getn(items) < 4) then
		items[2].gems.slots = items[3].gems.slots
		items[2].gems[1] = {0,0}
		items[2].gems[2] = {0,0}
		items[2].gems[3] = {0,0}
		print('testing2')
	else
		items[2].gems = items[3].gems
	end

	if 	(
		(RemoveChaItem(player.role, 0, 1, 2, items[1].bagslot, 2, 1, 0) == 0) or
		(RemoveChaItem(player.role, 0, 1, 2, items[3].bagslot, 2, 1, 0) == 0) or
		(
			(table.getn(items) >= 4) and
			(RemoveChaItem (player.role , 0, 1, 2, items[4].bagslot, 2, 1, 0) == 0)
		)
	) then
	SystemNotice( player.role , "Apparel fusion: Inventory might be locked.")
	return
	end

	SynChaKitbag(player.role,13)

	SystemNotice ( player.role ,"Apparel Fusion: Successful.")
	if (table.getn(items) < 4) then
		BickerNotice(player.role, '"'..items[2].name..'" now has "'..items[3].name..'"'.."'s stats.")
	else
		BickerNotice(player.role, '"'..items[2].name..'" now has "'..items[3].name..'"'.."'s stats and gems.")
	end
	return C_TRUE
end

function get_item_fusion_money(...)
	return 0
end

--Apparel Upgrade
function can_upgrade_item (...)
	local player, items = process_linked_item_list{...}
	if table.getn(items) >= 3 then
		if items[1].id ~= 455 then
			SystemNotice(player.role, "Apparel Upgrade: The top item must be Strengthening Scroll.")
			return C_FALSE
		end
		if items[3].id ~= 456 then
			SystemNotice(player.role, "Apparel Upgrade: The top right item must be Strengthening Crystal.")
			return C_FALSE
		end
		if items[2].stats[ITEMATTR_VAL_FUSIONID] == 0 then
			SystemNotice(player.role, "Apparel Upgrade: Item must have been fused.")
			return C_FALSE
		end
		if items[2].stats[ITEMATTR_VAL_LEVEL] >= 1000 then --max upgrade lvl
			SystemNotice(player.role, "Apparel Upgrade: Maximum upgrade level already reached.")
			return C_FALSE
		end
		if items[2].stats[ITEMATTR_URE] < items[2].stats[ITEMATTR_MAXENERGY] then
			SystemNotice(player.role, "Apparel Upgrade: Item must have at least 1 durability.")
			return C_FALSE
		end
		if player.stats[ATTR_GD] <= (((items[2].stats[ITEMATTR_VAL_LEVEL]+1) ^2) * 10000) then
			SystemNotice(player.role, "Apparel Upgrade: Insufficient gold.")
			return C_FALSE
		end
		--TOP also restricts IDs between 5000 to 6000 for fusion/upgrade.
	else
		SystemNotice ( role ,"Apparel Upgrade: Not enough items to upgrade properly.")
		return C_FALSE
	end
	return C_TRUE
end

function begin_upgrade_item (...)
	if can_upgrade_item(...) == C_FALSE then
		return C_FALSE
	end

	local player, items = process_linked_item_list{...}
	player.stats[ATTR_GD] = player.stats[ATTR_GD] - (((items[2].stats[ITEMATTR_VAL_LEVEL]+1) ^2) * 10000)
	AttrRecheck(player.role)

	SetChaKitbagChange(player.role , 1 ) --lock/unlock inventory?

	items[2].stats[ITEMATTR_VAL_LEVEL] = items[2].stats[ITEMATTR_VAL_LEVEL] + 1

	SynChaKitbag(player.role, 4 ) --no idea what this does.

	if 	(
		(RemoveChaItem(player.role, items[1].pointer, 1, 2, items[1].amount, 2, 1, 1) == 0) or
		(RemoveChaItem(player.role, items[2].pointer, 1, 2, items[2].amount, 2, 1, 1) == 0) or
		(RemoveChaItem(player.role, items[3].pointer, 1, 2, items[3].amount, 2, 1, 1) == 0)
	) then
		SystemNotice( player.role , "Apparel Upgrade: Inventory might be locked.")
		return
	end

	SynChaKitbag(player.role,13)

	SystemNotice(player.role, "Apparel Upgrade: Successful.")
	return C_TRUE
end

function get_item_upgrade_money(...)
	return 0
end



--??????
--????????????????????
function can_jlborn_item(...)
	local arg = ...
	if #arg ~= 12  then
		SystemNotice ( arg[1] , "?????????"..#arg )
		return 0
	end
	local Check = 0
	Check = can_jlborn_item_main ( arg )
	if Check == 1 then
		return 1
	else
		return 0
	end
end

--??????????????????
function can_jlborn_item_main ( Table )
	local role = 0
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Now = 0
	local ItemCount_Now = 0
	local ItemBagCount_Num = 0
	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Now , ItemCount_Now , ItemBagCount_Num = Read_Table ( Table )
	---???????????
	if ItemCount [1] ~= 1 or ItemCount [2] ~= 1 or ItemBagCount [1] ~= 1 or ItemBagCount [2] ~= 1 then
		SystemNotice ( role ,"??????????")
		return 0
	end
	----??????????
	local Item_EMstone = GetChaItem ( role , 2 , ItemBag [0] )	--??h??????
	local Item_JLone = GetChaItem ( role , 2 , ItemBag [1] )	--h?????????????
	local Item_JLother = GetChaItem ( role , 2 , ItemBag [2] )	--??h?????????????
	local Item_JLone_ID = GetItemID ( Item_JLone )   --h?????????ID
	local Item_JLother_ID = GetItemID ( Item_JLother )   --h?????????ID
	---?h??????????????
	local str_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_STR )		 --????
	local con_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_CON )		--????
	local agi_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_AGI )		--??
	local dex_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_DEX )		--????
	local sta_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_STA )		--????
	local URE_JLone = GetItemAttr( Item_JLone ,ITEMATTR_URE )		--????
	local MAXURE_JLone = GetItemAttr( Item_JLone ,ITEMATTR_MAXURE )       --???????
	local lv_JLone = str_JLone + con_JLone + agi_JLone + dex_JLone + sta_JLone  ----h??????j??
	---???h??????????????
	local str_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_STR )       --????
	local con_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_CON )       --????
	local agi_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_AGI )       --??
	local dex_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_DEX )       --????
	local sta_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_STA )       --????
	local URE_JLother = GetItemAttr( Item_JLother ,ITEMATTR_URE )		 --????
	local MAXURE_JLother = GetItemAttr( Item_JLother ,ITEMATTR_MAXURE )       --???????
	local lv_JLother = str_JLother + con_JLother + agi_JLother + dex_JLother + sta_JLother ----??h??????j??
	----?h???????7????
	local Num_JLone = GetItemForgeParam ( Item_JLone , 1 )
	local Part1_JLone = GetNum_Part1 ( Num_JLone )	--Get Num Part 1 ?? Part 7
	local Part2_JLone = GetNum_Part2 ( Num_JLone )
	local Part3_JLone = GetNum_Part3 ( Num_JLone )
	local Part4_JLone = GetNum_Part4 ( Num_JLone )
	local Part5_JLone = GetNum_Part5 ( Num_JLone )
	local Part6_JLone = GetNum_Part6 ( Num_JLone )
	local Part7_JLone= GetNum_Part7 ( Num_JLone )
	----???h???????7????
	local Num_JLother = GetItemForgeParam ( Item_JLother , 1 )
	local Part1_JLother = GetNum_Part1 ( Num_JLother )	--Get Num Part 1 ?? Part 7
	local Part2_JLother = GetNum_Part2 ( Num_JLother )
	local Part3_JLother = GetNum_Part3 ( Num_JLother )
	local Part4_JLother = GetNum_Part4 ( Num_JLother )
	local Part5_JLother = GetNum_Part5 ( Num_JLother )
	local Part6_JLother = GetNum_Part6 ( Num_JLother )
	local Part7_JLother= GetNum_Part7 ( Num_JLother )
	local Item_CanGet = GetChaFreeBagGridNum ( role )
	if Item_CanGet < 2 then
		SystemNotice(role ,"????????????????????????????")
		return 0
	end
	----??h??????
	local  Item_EMstone_ID = GetItemID ( Item_EMstone )
	if Item_EMstone_ID ~= 3918 and Item_EMstone_ID ~= 3919 and Item_EMstone_ID ~= 3920 and Item_EMstone_ID ~= 3921 and Item_EMstone_ID ~= 3922 and Item_EMstone_ID ~= 3924 and Item_EMstone_ID ~= 3925 then
		SystemNotice( role ,"??h???'?ô???")
		return 0
	end
	-----?????????
	if Item_EMstone_ID == 3918 then
		local i1 = CheckBagItem( role, 4530 )			---?????????
		local i2 = CheckBagItem( role,3434 )			----???????????
		if i1 < 10 or i2 < 10 then
			SystemNotice( role ,"?????????h???????")
			return 0
		end
	end

	if Item_EMstone_ID == 3919 then
		local i1 = CheckBagItem( role, 4531 )			---????I?????????
		local i2 = CheckBagItem( role, 3435 )			----???????????
		if i1 < 10 or i2 < 10 then
			SystemNotice( role ,"?????????h???????")
			return 0
		end
	end

	if Item_EMstone_ID == 3920 then
		local i1 = CheckBagItem( role,1196 )			---?????????????
		local i2 = CheckBagItem( role,3436 )			----???????????
		if i1 < 10 or i2 < 10 then
			SystemNotice( role ,"?????????h???????")
			return 0
		end
	end

	if Item_EMstone_ID == 3921 then
		local i1 = CheckBagItem( role, 4533 )			---???ß??
		local i2 = CheckBagItem( role, 3437 )			----???????????
		if i1 < 10 or i2 < 10 then
			SystemNotice( role ,"?????????h???????")
			return 0
		end
	end

	if Item_EMstone_ID == 3922 then
		local i1 = CheckBagItem( role,4537 )			---???
		local i2 = CheckBagItem( role,3444 )			----????
		if i1 < 10 or i2 < 10 then
			SystemNotice( role ,"?????????h???????")
			return 0
		end
	end

	if Item_EMstone_ID == 3924 then
		local i1 = CheckBagItem( role, 4540 )			---??????
		local i2 = CheckBagItem( role, 3443 )			----?????l
		if i1 < 10 or i2 < 10 then
			SystemNotice( role ,"?????????h???????")
			return 0
		end
	end

	if Item_EMstone_ID == 3925 then
		local i1 = CheckBagItem( role, 1253 )			---????I????????????
		local i2 = CheckBagItem( role, 3442 )			----?????
		if i1 < 10 or i2 < 10 then
			SystemNotice( role ,"?????????h???????")
			return 0
		end
	end
	----???????????
	local ItemType_JLone = GetItemType (Item_JLone)
	local ItemType_JLother = GetItemType (Item_JLother)
	if  ItemType_JLone ~=59 or ItemType_JLother ~=59  then
		SystemNotice( role ,"???????????")
		return 0
	end
	------???????????h????
	if ItemBag [1]==ItemBag [2] then
		SystemNotice( role ,"????????????????????????")
		return 0
	end
	----???????????????
	if  Part1_JLone ~=0 or Part1_JLother ~=0  then
		SystemNotice( role ,"?j???????????????")
		return 0
	end
	-----?????????
	if  lv_JLone < 20 or lv_JLother < 20   then
		SystemNotice( role ," ????????20????????")
		return 0
	end
	-----????????????????
	if URE_JLone < MAXURE_JLone or URE_JLone < MAXURE_JLone then
		SystemNotice( role ," ????????k????????£???????????")
		return 0
	end
	----?????????
	local Money_Need = getjlborn_money_main ( Table )
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	if Money_Need > Money_Have then
		SystemNotice( role ,"??????????????")
		return 0
	end
	return 1
end

--??'???????????????
function begin_jlborn_item(...)
	local arg = ...
	-----???????????
	local Check_Canjlborn = 0
	Check_Canjlborn = can_jlborn_item_main ( arg )
	if Check_Canjlborn == 0 then
		return 0
	end
	------??????
	local role = 0
	local ItemBag = {}											--??????????
	local ItemCount = {}											--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Num = 0
	local ItemCount_Num = 0
	local ItemBagCount_Num = 0
	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( arg )
	-----??????????
	local Item_EMstone = GetChaItem ( role , 2 , ItemBag [0] )				--??h??????
	local Item_JLone = GetChaItem ( role , 2 , ItemBag [1] )					--h?????????????
	local Item_JLother = GetChaItem ( role , 2 , ItemBag [2] )				--??h?????????????
	-----?????
	local Money_Need = getjlborn_money_main ( arg )
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	Money_Have = Money_Have - Money_Need
	SetCharaAttr ( Money_Have , role , ATTR_GD )
	ALLExAttrSet( role )
	-----?????
	Check_JLBorn_Item = jlborn_item ( arg )
	if Check_JLBorn_Item == 0  then
		SystemNotice ( role ,"??????????????")
	end
	local cha_name = GetChaDefaultName ( role )
	SystemNotice ( role ,"?????")
	LG( "JLBorn_ShiBai" , "???"..cha_name.."?l???????" )
	return 1
end

--??????????
function get_item_jlborn_money(...)
	local Money = getjlborn_money_main { ... }
	return Money
end

--????????????????
function getjlborn_money_main ( Table )
	local role = 0
	local ItemBag = {}										--??????????????
	local ItemCount = {}										--????????????
	local ItemBagCount = {}									--???????????????
	local ItemBag_Num = 0									--?????????????
	local ItemCount_Num = 0									--??????????????
	local ItemBagCount_Num = 0								--?????????????????
	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( Table )
	---??????????
	local Item_JLone = GetChaItem ( role , 2 , ItemBag [1] )				--h?????????????
	local Item_JLother = GetChaItem ( role , 2 , ItemBag [2] )			--??h?????????????
	---?h??????????????
	local str_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_STR )		  --????
	local con_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_CON )		   --????
	local agi_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_AGI )		  --??
	local dex_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_DEX )		 --????
	local sta_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_STA )		 --????
	local lv_JLone = str_JLone + con_JLone + agi_JLone + dex_JLone + sta_JLone  ----h??????j??
	---???h??????????????
	local str_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_STR )		 --????
	local con_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_CON )		  --????
	local agi_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_AGI )		--??
	local dex_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_DEX )		  --????
	local sta_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_STA )		 --????
	local lv_JLother = str_JLother + con_JLother + agi_JLother + dex_JLother + sta_JLother ----??h??????j??
	local  Money_Need = ( 60 - lv_JLone )*(60 - lv_JLother )*100
	if lv_JLone>60 or lv_JLother>60 then
		Money_Need = 0
	end
	return Money_Need
end
----??????--------------------------------------------------------------------------------------------------------
---??'???
function jlborn_item ( Table )
	local role = 0
	local ItemBag = {}											--??????????
	local ItemCount = {}											--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Num = 0
	local ItemCount_Num = 0
	local ItemBagCount_Num = 0
	local ItemID_Cuihuaji = 0
	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( Table )
	---??????????
	local Item_EMstone = GetChaItem ( role , 2 , ItemBag [0] )				--??h??????
	local Item_JLone = GetChaItem ( role , 2 , ItemBag [1] )					--h?????????????
	local Item_JLother = GetChaItem ( role , 2 , ItemBag [2] )				--??h?????????????
	local  Item_EMstone_ID = GetItemID ( Item_EMstone )					 --??h???ID
	local  Item_JLone_ID = GetItemID ( Item_JLone )						 --h?????????ID
	local  Item_JLother_ID = GetItemID ( Item_JLother )					 --h?????????ID
	---?h??????????????
	local str_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_STR )			  --????
	local con_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_CON )			 ---????
	local agi_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_AGI )			 --????
	local dex_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_DEX )			  --??
	local sta_JLone = GetItemAttr( Item_JLone ,ITEMATTR_VAL_STA )			  --????
	local URE_JLone = GetItemAttr( Item_JLone ,ITEMATTR_URE )			--????
	local MAXURE_JLone = GetItemAttr( Item_JLone ,ITEMATTR_MAXURE )		 --???????
	local lv_JLone = str_JLone + con_JLone + agi_JLone + dex_JLone + sta_JLone	----h??????j??
	---???h??????????????
	local str_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_STR )		--????
	local con_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_CON )		  --????
	local agi_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_AGI )		 --????
	local dex_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_DEX )		 --??
	local sta_JLother = GetItemAttr( Item_JLother ,ITEMATTR_VAL_STA )		 --????
	local URE_JLother = GetItemAttr( Item_JLother ,ITEMATTR_URE )			    --????
	local MAXURE_JLother = GetItemAttr( Item_JLother ,ITEMATTR_MAXURE )	 --???????
	local lv_JLother = str_JLother + con_JLother + agi_JLother + dex_JLother + sta_JLother ----??h??????j??
	----?h???????7????
	local Num_JLone = GetItemForgeParam ( Item_JLone , 1 )
	local Part1_JLone = GetNum_Part1 ( Num_JLone )	--Get Num Part 1 ?? Part 7
	local Part2_JLone = GetNum_Part2 ( Num_JLone )
	local Part3_JLone = GetNum_Part3 ( Num_JLone )
	local Part4_JLone = GetNum_Part4 ( Num_JLone )
	local Part5_JLone = GetNum_Part5 ( Num_JLone )
	local Part6_JLone = GetNum_Part6 ( Num_JLone )
	local Part7_JLone= GetNum_Part7 ( Num_JLone )
	----???h???????7????
	local Num_JLother = GetItemForgeParam ( Item_JLother , 1 )
	local Part1_JLother = GetNum_Part1 ( Num_JLother )	--Get Num Part 1 ?? Part 7
	local Part2_JLother = GetNum_Part2 ( Num_JLother )
	local Part3_JLother = GetNum_Part3 ( Num_JLother )
	local Part4_JLother = GetNum_Part4 ( Num_JLother )
	local Part5_JLother = GetNum_Part5 ( Num_JLother )
	local Part6_JLother = GetNum_Part6 ( Num_JLother )
	local Part7_JLother= GetNum_Part7 ( Num_JLother )
	-----?????¾???????¼???¾?????
	local new_str = math.floor ((str_JLone+str_JLother)*0.125 )
	local new_con = math.floor ((con_JLone+con_JLother)*0.125 )
	local new_agi = math.floor ((agi_JLone+agi_JLother)*0.125 )
	local new_dex = math.floor ((dex_JLone+dex_JLother)*0.125 )
	local new_sta = math.floor ((sta_JLone+sta_JLother)*0.125 )
	local new_lv = new_str + new_con + new_agi + new_dex + new_sta
	local new_MAXENERGY = 240 * ( new_lv + 1 )
	if new_MAXENERGY > 6480 then
		new_MAXENERGY = 6480
	end
	local new_MAXURE = 5000 + 1000*new_lv
	if new_MAXURE > 32000 then
		new_MAXURE = 32000
	end
	if new_MAXURE ==25000  then
		new_MAXURE = 25000+1
	end
	--------??h??????
	if Item_EMstone_ID ==3918 then ---?????h???
		local j1 =TakeItem( role, 0, 4530, 10 )			---?????????
		local j2 = TakeItem( role, 0,3434, 10  )			----???????????
		if j1==0 or j2==0 then
			SystemNotice ( role ,"??????????????")
			return
		end
		local rad = math.random ( 1, 100 )
		local r1 = 0
		local r2 = 0
		if Item_JLone_ID ==680 or Item_JLother_ID ==680 then
			if Item_JLone_ID==Item_JLother_ID then
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=20 and lv_JLone<25 and lv_JLother >=20 and lv_JLother<25 and rad>=88 then---12%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=25 and lv_JLone<35 and lv_JLother >=25 and lv_JLother<35 and rad>=50 then ---50%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=35 and lv_JLother >=35 and rad>=10 then---90%
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			else
				r1,r2 =MakeItem ( role , 231  , 1 , 4 )
			end
		else
			r1,r2 =MakeItem ( role , 231  , 1 , 4 )
		end
		local Item_newJL = GetChaItem ( role , 2 , r2 )			--??¾?????????
		local Item_newJL_ID = GetItemID ( Item_newJL )
		local Num_newJL = GetItemForgeParam ( Item_newJL , 1 )
		local Part1_newJL = GetNum_Part1 ( Num_newJL )	--Get Num Part 1 ?? Part 7
		local Part2_newJL = GetNum_Part2 ( Num_newJL )
		local Part3_newJL = GetNum_Part3 ( Num_newJL )
		local Part4_newJL = GetNum_Part4 ( Num_newJL )
		local Part5_newJL = GetNum_Part5 ( Num_newJL )
		local Part6_newJL = GetNum_Part6 ( Num_newJL )
		local Part7_newJL= GetNum_Part7 ( Num_newJL )
		if lv_JLone>=20 and lv_JLother >=20 then
			Part2_newJL = 6					 ---------???????????
			Part3_newJL = 1						---------???????
		end
		if lv_JLone>=25 and lv_JLother >=25 then
			Part2_newJL = 6					 ---------???????????
			Part3_newJL = 2						---------??????
		end
		if lv_JLone>=35 and lv_JLother >=35 then
			Part2_newJL = 6					 ---------???????????
			Part3_newJL = 3						---------??????
		end
		local rad1 = math.random ( 1, 100 )
		if Part3_newJL==3 then
			GiveItem ( role , 0 , 609  , 1 , 4 )
		end
		if Part3_newJL==2 then
			if rad1 <=95 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 95 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		if Part3_newJL==1 then
			if rad1 <=90 then
				GiveItem ( role , 0 , 239  , 1 , 4 )
			elseif rad1 > 90 and rad1 <=98 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 98 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		Num_newJL = SetNum_Part1 ( Num_newJL , 1 )	 ----------??????
		Num_newJL = SetNum_Part2 ( Num_newJL , Part2_newJL )
		Num_newJL = SetNum_Part3 ( Num_newJL , Part3_newJL )
		Num_newJL = SetNum_Part4 ( Num_newJL , Part4_newJL )
		Num_newJL = SetNum_Part5 ( Num_newJL , Part5_newJL )
		Num_newJL = SetNum_Part6 ( Num_newJL , Part6_newJL )
		Num_newJL = SetNum_Part7 ( Num_newJL , Part7_newJL )
		SetItemForgeParam ( Item_newJL , 1 , Num_newJL )

		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STR , new_str )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_DEX , new_dex )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STA , new_sta )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_AGI , new_agi )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_CON , new_con )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXENERGY , new_MAXENERGY )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXURE , new_MAXURE )
	end
	if Item_EMstone_ID ==3919 then ---?????h???  ???????----???'????
		local j1 = TakeItem( role, 0, 4531, 10 )			---????I?????????
		local j2 = TakeItem( role, 0,3435, 10 )			----???????????
		if j1==0 or j2==0 then
			SystemNotice ( role ,"??????????????")
			return
		end
		local rad = math.random ( 1, 100 )
		local r1 = 0
		local r2 = 0
		if Item_JLone_ID ==680 or Item_JLother_ID ==680 then
			if Item_JLone_ID==Item_JLother_ID then
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif  lv_JLone>=20 and lv_JLone<25 and lv_JLother >=20 and lv_JLother<25 and rad>=88 then---12%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=25 and lv_JLone<35 and lv_JLother >=25 and lv_JLother<35 and rad>=50 then ---50%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=35 and lv_JLother >=35 and rad>=10 then---90%
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			else
				r1,r2 =MakeItem ( role , 233  , 1 , 4 )
			end
		else
			r1,r2 =MakeItem ( role , 233  , 1 , 4 )
		end
		local Item_newJL = GetChaItem ( role , 2 , r2 )			--??¾?????????
		local Num_newJL = GetItemForgeParam ( Item_newJL , 1 )
		local Part1_newJL = GetNum_Part1 ( Num_newJL )	--Get Num Part 1 ?? Part 7
		local Part2_newJL = GetNum_Part2 ( Num_newJL )
		local Part3_newJL = GetNum_Part3 ( Num_newJL )
		local Part4_newJL = GetNum_Part4 ( Num_newJL )
		local Part5_newJL = GetNum_Part5 ( Num_newJL )
		local Part6_newJL = GetNum_Part6 ( Num_newJL )
		local Part7_newJL= GetNum_Part7 ( Num_newJL )
		if lv_JLone>=20 and lv_JLother >=20 then
			Part2_newJL = 7					 ---------???????????
			Part3_newJL = 1					---------???????
		end
		if lv_JLone>=25 and lv_JLother >=25 then
			Part2_newJL = 7					 ---------???????????
			Part3_newJL = 2					---------??????
		end
		if lv_JLone>=35 and lv_JLother >=35 then
			Part2_newJL = 7					 ---------???????????
			Part3_newJL = 3					---------??????
		end
		local rad1 = math.random ( 1, 100 )
		if Part3_newJL==3 then
			GiveItem ( role , 0 , 609  , 1 , 4 )
		end
		if Part3_newJL==2 then
			if rad1 <=95 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 95 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		if Part3_newJL==1 then
			if rad1 <=90 then
				GiveItem ( role , 0 , 239  , 1 , 4 )
			elseif rad1 > 90 and rad1 <=98 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 98 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		Num_newJL = SetNum_Part1 ( Num_newJL , 1 )	----------??????
		Num_newJL = SetNum_Part2 ( Num_newJL , Part2_newJL )
		Num_newJL = SetNum_Part3 ( Num_newJL , Part3_newJL )
		Num_newJL = SetNum_Part4 ( Num_newJL , Part4_newJL )
		Num_newJL = SetNum_Part5 ( Num_newJL , Part5_newJL )
		Num_newJL = SetNum_Part6 ( Num_newJL , Part6_newJL )
		Num_newJL = SetNum_Part7 ( Num_newJL , Part7_newJL )
		SetItemForgeParam ( Item_newJL , 1 , Num_newJL )

		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STR , new_str )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_DEX , new_dex )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STA , new_sta )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_AGI , new_agi )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_CON , new_con )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXENERGY , new_MAXENERGY )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXURE , new_MAXURE )
	end
	if Item_EMstone_ID ==3920 then ---??????h??? ???????----?????????
		local j1 =TakeItem( role, 0, 1196, 10 )			---?????????????
		local j2 = TakeItem( role, 0,3436, 10 )			----???????????
		if j1==0 or j2==0 then
			SystemNotice ( role ,"??????????????")
			return
		end
		local rad = math.random ( 1, 100 )
		local r1 = 0
		local r2 = 0
		if Item_JLone_ID ==680 or Item_JLother_ID ==680 then
			if Item_JLone_ID==Item_JLother_ID then
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif  lv_JLone>=20 and lv_JLone<25 and lv_JLother >=20 and lv_JLother<25 and rad>=88 then---12%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=25 and lv_JLone<35 and lv_JLother >=25 and lv_JLother<35 and rad>=50 then ---50%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=35 and lv_JLother >=35 and rad>=10 then---90%
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			else
				r1,r2 =MakeItem ( role , 232  , 1 , 4 )
			end
		else
			r1,r2 =MakeItem ( role , 232  , 1 , 4 )
		end
		local Item_newJL = GetChaItem ( role , 2 , r2 )			--??¾?????????
		local Num_newJL = GetItemForgeParam ( Item_newJL , 1 )
		local Part1_newJL = GetNum_Part1 ( Num_newJL )	--Get Num Part 1 ?? Part 7
		local Part2_newJL = GetNum_Part2 ( Num_newJL )
		local Part3_newJL = GetNum_Part3 ( Num_newJL )
		local Part4_newJL = GetNum_Part4 ( Num_newJL )
		local Part5_newJL = GetNum_Part5 ( Num_newJL )
		local Part6_newJL = GetNum_Part6 ( Num_newJL )
		local Part7_newJL= GetNum_Part7 ( Num_newJL )
		if lv_JLone>=20 and lv_JLother >=20 then
			Part2_newJL = 8													 ---------???????????
			Part3_newJL = 1													---------???????
		end
		if lv_JLone>=25 and lv_JLother >=25 then
			Part2_newJL = 8													 ---------???????????
			Part3_newJL = 2													---------??????
		end
		if lv_JLone>=35 and lv_JLother >=35 then
			Part2_newJL = 8													 ---------???????????
			Part3_newJL = 3													---------??????
		end
		local rad1 = math.random ( 1, 100 )
		if Part3_newJL==3 then
			GiveItem ( role , 0 , 609  , 1 , 4 )
		end
		if Part3_newJL==2 then
			if rad1 <=95 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 95 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		if Part3_newJL==1 then
			if rad1 <=90 then
				GiveItem ( role , 0 , 239  , 1 , 4 )
			elseif rad1 > 90 and rad1 <=98 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 98 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		Num_newJL = SetNum_Part1 ( Num_newJL , 1 )	----------??????
		Num_newJL = SetNum_Part2 ( Num_newJL , Part2_newJL )
		Num_newJL = SetNum_Part3 ( Num_newJL , Part3_newJL )
		Num_newJL = SetNum_Part4 ( Num_newJL , Part4_newJL )
		Num_newJL = SetNum_Part5 ( Num_newJL , Part5_newJL )
		Num_newJL = SetNum_Part6 ( Num_newJL , Part6_newJL )
		Num_newJL = SetNum_Part7 ( Num_newJL , Part7_newJL )
		SetItemForgeParam ( Item_newJL , 1 , Num_newJL )

		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STR , new_str )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_DEX , new_dex )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STA , new_sta )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_AGI , new_agi )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_CON , new_con )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXENERGY , new_MAXENERGY )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXURE , new_MAXURE )
	end
	if Item_EMstone_ID ==3921 then ---????h??? ??????? ????????
		local j1 =TakeItem( role, 0, 4533, 10 )			---???ß??
		local j2 = TakeItem( role, 0,3437, 10 )			----???????????
		if j1==0 or j2==0 then
			SystemNotice ( role ,"??????????????")
			return
		end
		local rad = math.random ( 1, 100 )
		local r1 = 0
		local r2 = 0
		if Item_JLone_ID ==680 or Item_JLother_ID ==680 then
			if Item_JLone_ID==Item_JLother_ID then
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif  lv_JLone>=20 and lv_JLone<25 and lv_JLother >=20 and lv_JLother<25 and rad>=88 then---12%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=25 and lv_JLone<35 and lv_JLother >=25 and lv_JLother<35 and rad>=50 then ---50%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=35 and lv_JLother >=35 and rad>=10 then---90%
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			else
				r1,r2 =MakeItem ( role , 234  , 1 , 4 )
			end
		else
			r1,r2 =MakeItem ( role , 234  , 1 , 4 )
		end
		local Item_newJL = GetChaItem ( role , 2 , r2 )			--??¾?????????
		local Num_newJL = GetItemForgeParam ( Item_newJL , 1 )
		local Part1_newJL = GetNum_Part1 ( Num_newJL )	--Get Num Part 1 ?? Part 7
		local Part2_newJL = GetNum_Part2 ( Num_newJL )
		local Part3_newJL = GetNum_Part3 ( Num_newJL )
		local Part4_newJL = GetNum_Part4 ( Num_newJL )
		local Part5_newJL = GetNum_Part5 ( Num_newJL )
		local Part6_newJL = GetNum_Part6 ( Num_newJL )
		local Part7_newJL= GetNum_Part7 ( Num_newJL )
		if lv_JLone>=20 and lv_JLother >=20 then
			Part2_newJL = 9													 ---------???????????
			Part3_newJL = 1													---------???????
		end
		if lv_JLone>=25 and lv_JLother >=25 then
			Part2_newJL = 9													 ---------???????????
			Part3_newJL = 2													---------??????
		end
		if lv_JLone>=35 and lv_JLother >=35 then
			Part2_newJL = 9													 ---------???????????
			Part3_newJL = 3													---------??????
		end
		local rad1 = math.random ( 1, 100 )
		if Part3_newJL==3 then
			GiveItem ( role , 0 , 609  , 1 , 4 )
		end
		if Part3_newJL==2 then
			if rad1 <=95 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 95 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		if Part3_newJL==1 then
			if rad1 <=90 then
				GiveItem ( role , 0 , 239  , 1 , 4 )
			elseif rad1 > 90 and rad1 <=98 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 98 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		Num_newJL = SetNum_Part1 ( Num_newJL , 1 ) ----------??????
		Num_newJL = SetNum_Part2 ( Num_newJL , Part2_newJL )
		Num_newJL = SetNum_Part3 ( Num_newJL , Part3_newJL )
		Num_newJL = SetNum_Part4 ( Num_newJL , Part4_newJL )
		Num_newJL = SetNum_Part5 ( Num_newJL , Part5_newJL )
		Num_newJL = SetNum_Part6 ( Num_newJL , Part6_newJL )
		Num_newJL = SetNum_Part7 ( Num_newJL , Part7_newJL )
		SetItemForgeParam ( Item_newJL , 1 , Num_newJL )

		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STR , new_str )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_DEX , new_dex )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STA , new_sta )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_AGI , new_agi )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_CON , new_con )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXENERGY , new_MAXENERGY )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXURE , new_MAXURE )
	end
	if Item_EMstone_ID ==3922 then ---??????h??? ?????? ???????
		local j1 =TakeItem( role, 0,4537, 10 )			---???
		local j2 = TakeItem( role, 0,3444, 10 )			----????
		if j1==0 or j2==0 then
			SystemNotice ( role ,"??????????????")
			return
		end
		local rad = math.random ( 1, 100 )
		local r1 = 0
		local r2 = 0
		if Item_JLone_ID ==680 or Item_JLother_ID ==680 then
			if Item_JLone_ID==Item_JLother_ID then
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif  lv_JLone>=20 and lv_JLone<25 and lv_JLother >=20 and lv_JLother<25 and rad>=88 then---12%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=25 and lv_JLone<35 and lv_JLother >=25 and lv_JLother<35 and rad>=50 then ---50%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=35 and lv_JLother >=35 and rad>=10 then---90%
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			else
				r1,r2 =MakeItem ( role , 235  , 1 , 4 )
			end
		else
			r1,r2 =MakeItem ( role , 235  , 1 , 4 )
		end
		local Item_newJL = GetChaItem ( role , 2 , r2 )			--??¾?????????
		local Num_newJL = GetItemForgeParam ( Item_newJL , 1 )
		local Part1_newJL = GetNum_Part1 ( Num_JLother )	--Get Num Part 1 ?? Part 7
		local Part2_newJL = GetNum_Part2 ( Num_newJL )
		local Part3_newJL = GetNum_Part3 ( Num_newJL )
		local Part4_newJL = GetNum_Part4 ( Num_newJL )
		local Part5_newJL = GetNum_Part5 ( Num_newJL )
		local Part6_newJL = GetNum_Part6 ( Num_newJL )
		local Part7_newJL= GetNum_Part7 ( Num_newJL )
		if lv_JLone>=20 and lv_JLother >=20 then
			Part2_newJL = 10												 ---------??????????
			Part3_newJL = 1													---------???????
		end
		if lv_JLone>=25 and lv_JLother >=25 then
			Part2_newJL = 10												 ---------??????????
			Part3_newJL = 2													---------??????
		end
		if lv_JLone>=35 and lv_JLother >=35 then
			Part2_newJL = 10													 ---------??????????
			Part3_newJL = 3													---------??????
		end
		local rad1 = math.random ( 1, 100 )
		if Part3_newJL==3 then
			GiveItem ( role , 0 , 609  , 1 , 4 )
		end
		if Part3_newJL==2 then
			if rad1 <=95 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 95 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		if Part3_newJL==1 then
			if rad1 <=90 then
				GiveItem ( role , 0 , 239  , 1 , 4 )
			elseif rad1 > 90 and rad1 <=98 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 98 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		Num_newJL = SetNum_Part1 ( Num_newJL , 1 ) ----------??????
		Num_newJL = SetNum_Part2 ( Num_newJL , Part2_newJL )
		Num_newJL = SetNum_Part3 ( Num_newJL , Part3_newJL )
		Num_newJL = SetNum_Part4 ( Num_newJL , Part4_newJL )
		Num_newJL = SetNum_Part5 ( Num_newJL , Part5_newJL )
		Num_newJL = SetNum_Part6 ( Num_newJL , Part6_newJL )
		Num_newJL = SetNum_Part7 ( Num_newJL , Part7_newJL )
		SetItemForgeParam ( Item_newJL , 1 , Num_newJL )

		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STR , new_str )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_DEX , new_dex )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STA , new_sta )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_AGI , new_agi )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_CON , new_con )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXENERGY , new_MAXENERGY )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXURE , new_MAXURE )
	end
	if Item_EMstone_ID ==3924 then ---?????h??? ??????? ????????
		local j1 = TakeItem( role, 0,4540, 10 )			---??????
		local j2 = TakeItem( role, 0,3443, 10 )			----?????l
		if j1==0 or j2==0 then
			SystemNotice ( role ,"??????????????")
			return
		end
		local rad = math.random ( 1, 100 )
		local r1 = 0
		local r2 = 0
		if Item_JLone_ID ==680 or Item_JLother_ID ==680 then
			if Item_JLone_ID==Item_JLother_ID then
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif  lv_JLone>=20 and lv_JLone<25 and lv_JLother >=20 and lv_JLother<25 and rad>=88 then---12%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=25 and lv_JLone<35 and lv_JLother >=25 and lv_JLother<35 and rad>=50 then ---50%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=35 and lv_JLother >=35 and rad>=10 then---90%
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			else
				r1,r2 =MakeItem ( role , 236  , 1 , 4 )
			end
		else
			r1,r2 =MakeItem ( role , 236  , 1 , 4 )
		end
		local Item_newJL = GetChaItem ( role , 2 , r2 )			--??¾?????????
		local Num_newJL = GetItemForgeParam ( Item_newJL , 1 )
		local Part1_newJL = GetNum_Part1 ( Num_newJL )	--Get Num Part 1 ?? Part 7
		local Part2_newJL = GetNum_Part2 ( Num_newJL )
		local Part3_newJL = GetNum_Part3 ( Num_newJL )
		local Part4_newJL = GetNum_Part4 ( Num_newJL )
		local Part5_newJL = GetNum_Part5 ( Num_newJL )
		local Part6_newJL = GetNum_Part6 ( Num_newJL )
		local Part7_newJL= GetNum_Part7 ( Num_newJL )
		if lv_JLone>=20 and lv_JLother >=20 then
			Part2_newJL = 11													 ---------???????????
			Part3_newJL = 1													---------???????
		end
		if lv_JLone>=25 and lv_JLother >=25 then
			Part2_newJL = 11													 ---------???????????
			Part3_newJL = 2													---------??????
		end
		if lv_JLone>=35 and lv_JLother >=35 then
			Part2_newJL = 11													 ---------???????????
			Part3_newJL = 3													---------??????
		end
		local rad1 = math.random ( 1, 100 )
		if Part3_newJL==3 then
			GiveItem ( role , 0 , 609  , 1 , 4 )
		end
		if Part3_newJL==2 then
			if rad1 <=95 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 95 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		if Part3_newJL==1 then
			if rad1 <=90 then
				GiveItem ( role , 0 , 239  , 1 , 4 )
			elseif rad1 > 90 and rad1 <=98 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 98 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		Num_newJL = SetNum_Part1 ( Num_newJL , 1 ) ----------??????
		Num_newJL = SetNum_Part2 ( Num_newJL , Part2_newJL )
		Num_newJL = SetNum_Part3 ( Num_newJL , Part3_newJL )
		Num_newJL = SetNum_Part4 ( Num_newJL , Part4_newJL )
		Num_newJL = SetNum_Part5 ( Num_newJL , Part5_newJL )
		Num_newJL = SetNum_Part6 ( Num_newJL , Part6_newJL )
		Num_newJL = SetNum_Part7 ( Num_newJL , Part7_newJL )
		SetItemForgeParam ( Item_newJL , 1 , Num_newJL )

		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STR , new_str )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_DEX , new_dex )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STA , new_sta )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_AGI , new_agi )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_CON , new_con )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXENERGY , new_MAXENERGY )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXURE , new_MAXURE )
	end
	if Item_EMstone_ID ==3925 then ---?????h??? ?????? ??????
		local j1 = TakeItem( role, 0,1253, 10 )			---????I????????????
		local j2 = TakeItem( role, 0,3442, 10 )			----?????
		if j1==0 or j2==0 then
			SystemNotice ( role ,"??????????????")
			return
		end
		local rad = math.random ( 1, 100 )
		local r1 = 0
		local r2 = 0
		if Item_JLone_ID ==680 or Item_JLother_ID ==680 then
			if Item_JLone_ID==Item_JLother_ID then
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif  lv_JLone>=20 and lv_JLone<25 and lv_JLother >=20 and lv_JLother<25 and rad>=88 then---12%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=25 and lv_JLone<35 and lv_JLother >=25 and lv_JLother<35 and rad>=50 then ---50%?H??????
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			elseif lv_JLone>=35 and lv_JLother >=35 and rad>=10 then---90%
				r1,r2 =MakeItem ( role , 681  , 1 , 4 )
			else
				r1,r2 =MakeItem ( role , 237  , 1 , 4 )
			end
		else
			r1,r2 =MakeItem ( role , 237 , 1 , 4 )
		end
		local Item_newJL = GetChaItem ( role , 2 , r2 )			--??¾?????????
		local Num_newJL = GetItemForgeParam ( Item_newJL , 1 )
		local Part1_newJL = GetNum_Part1 ( Num_newJL )	--Get Num Part 1 ?? Part 7
		local Part2_newJL = GetNum_Part2 ( Num_newJL )
		local Part3_newJL = GetNum_Part3 ( Num_newJL )
		local Part4_newJL = GetNum_Part4 ( Num_newJL )
		local Part5_newJL = GetNum_Part5 ( Num_newJL )
		local Part6_newJL = GetNum_Part6 ( Num_newJL )
		local Part7_newJL= GetNum_Part7 ( Num_newJL )
		if lv_JLone>=20 and lv_JLother >=20 then
			Part2_newJL = 12													 ---------??????????
			Part3_newJL = 1													---------???????
		end
		if lv_JLone>=25 and lv_JLother >=25 then
			Part2_newJL = 12													 ---------??????????
			Part3_newJL = 2													---------??????
		end
		if lv_JLone>=35 and lv_JLother >=35 then
			Part2_newJL = 12													 ---------??????????
			Part3_newJL = 3													---------??????
		end
		local rad1 = math.random ( 1, 100 )
		if Part3_newJL==3 then
			GiveItem ( role , 0 , 609  , 1 , 4 )
		end
		if Part3_newJL==2 then
			if rad1 <=95 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 95 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		if Part3_newJL==1 then
			if rad1 <=90 then
				GiveItem ( role , 0 , 239  , 1 , 4 )
			elseif rad1 > 90 and rad1 <=98 then
				GiveItem ( role , 0 , 608  , 1 , 4 )
			elseif rad1 > 98 and rad1 <=100 then
				GiveItem ( role , 0 , 609  , 1 , 4 )
			end
		end
		Num_newJL = SetNum_Part1 ( Num_newJL , 1 ) ----------??????
		Num_newJL = SetNum_Part2 ( Num_newJL , Part2_newJL )
		Num_newJL = SetNum_Part3 ( Num_newJL , Part3_newJL )
		Num_newJL = SetNum_Part4 ( Num_newJL , Part4_newJL )
		Num_newJL = SetNum_Part5 ( Num_newJL , Part5_newJL )
		Num_newJL = SetNum_Part6 ( Num_newJL , Part6_newJL )
		Num_newJL = SetNum_Part7 ( Num_newJL , Part7_newJL )
		SetItemForgeParam ( Item_newJL , 1 , Num_newJL )

		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STR , new_str )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_DEX , new_dex )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_STA , new_sta )
		SetItemAttr( Item_newJL , ITEMATTR_VAL_AGI , new_agi )
		SetItemAttr ( Item_newJL , ITEMATTR_VAL_CON , new_con )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXENERGY , new_MAXENERGY )
		SetItemAttr ( Item_newJL , ITEMATTR_MAXURE , new_MAXURE )
	end
	--------------???????LG
	local cha_name = GetChaDefaultName ( role )
	--LG( "star_JLZS_lg" ,cha_name,Item_JLone_ID , lv_JLone , str_JLone , con_JLone , agi_JLone , dex_JLone , sta_JLone , Item_JLother_ID , lv_JLother  , str_JLother , con_JLother , agi_JLother , dex_JLother , sta_JLother )
	LG( "star_JLZS_lg" ,cha_name, Item_EMstone_ID , Item_JLone_ID , lv_JLone ,  Item_JLother_ID , lv_JLother , Item_newJL_ID )

	R1 = RemoveChaItem ( role , Item_EMstone_ID , 1 , 2 , ItemBag [0] , 2 , 1 , 0 )		--????h???
	if R1 == 0  then
		SystemNotice( role , "????h??????")
		return
	end
	Elf_Attr_cs ( role , Item_JLone , Item_JLother )
end

--Equipment upgrade
function can_tichun_item(...)
	local arg = ...
	--	Notice ( "???????????")
	if #arg ~= 10 and #arg ~= 14 then
		SystemNotice ( arg[1] , "?????????".. #arg )
		return 0
	end
	local Check = 0
	Check = can_tichun_item_main ( arg )
	if Check == 1 then
		return 1
	else
		return 0
	end
end

--?????????????????
function can_tichun_item_main ( Table )
	--Notice ( "?????????????????")
	local role = 0
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Now = 0
	local ItemCount_Now = 0
	local ItemBagCount_Num = 0
	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Now , ItemCount_Now , ItemBagCount_Num = Read_Table ( Table )
	--------???????????
	if ItemCount [0] ~= 1 or ItemCount [1] ~= 1 or ItemBagCount [0] ~= 1 or ItemBagCount [1] ~= 1 then
		SystemNotice ( role ,"??????????")
		return 0
	end
	--------??????????
	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--????????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????????
	--------?????????
	local  ItemType_mainitem = GetItemType ( Item_mainitem )
	local  ItemType_otheritem = GetItemType ( Item_otheritem )
	--------?????ID
	local ItemID_mainitem = GetItemID ( Item_mainitem )
	local ItemID_otheritem = GetItemID ( Item_otheritem )
	--SystemNotice ( role ,"ItemID_mainitem=="..ItemID_mainitem)
	--SystemNotice ( role ,"ItemID_otheritem=="..ItemID_otheritem)
	--------???????
	local ItemID_mainitem_Lv =  GetItemLv ( Item_mainitem )
	local ItemID_otheritem_Lv =  GetItemLv ( Item_otheritem )
	--SystemNotice ( role ,"ItemID_mainitem_Lv=="..ItemID_mainitem_Lv)
	--SystemNotice ( role ,"ItemID_otheritem_Lv=="..ItemID_otheritem_Lv)
	local ItemID_main=ItemID_mainitem
	local ItemID_other=ItemID_otheritem
	-------???????????????????'ID
	if ItemID_main > 5000 then
		ItemID_main = GetItemAttr( Item_mainitem , ITEMATTR_VAL_FUSIONID )
	end
	-------???????????????????
	local flg=0
	if ItemID_main==825 or ItemID_main==826 or ItemID_main==827  or ItemID_main==828 then---------????????
		if ItemID_other==2403 then
			flg=1
		end
	end
	if ItemID_main==845 or ItemID_main==846 or ItemID_main==847  or ItemID_main==848 then---------????????
		if ItemID_other==2404 then
			flg=1
		end
	end
	if ItemID_main>=2530 and ItemID_main<=2532 then---------70?????BOSS???????
		if ItemID_other==2562 then
			flg=1
		end
	end
	if ItemID_main>=2533 and ItemID_main<=2535 then---------70?????BOSS???????
		if ItemID_other==2563 then
			flg=1
		end
	end
	if ItemID_main>=2536 and ItemID_main<=2538 then---------70?????BOSS???????
		if ItemID_other==2564 then
			flg=1
		end
	end
	if ItemID_main>=2539 and ItemID_main<=2541 then---------70??????BOSS???????
		if ItemID_other==2565 then
			flg=1
		end
	end
	if ItemID_main>=2542 and ItemID_main<=2544 then---------70????BOSS???????
		if ItemID_other==2566 then
			flg=1
		end
	end
	if ItemID_main>=2545 and ItemID_main<=2547 then---------70?????BOSS???????
		if ItemID_other==2567 then
			flg=1
		end
	end
	if flg==0 then
		SystemNotice( role ,"??'????????????????")
		return 0
	end
	--------?????????
	local Money_Need = gettichun_money_main ( Table )
	--SystemNotice(role ,"Money_Need== "..Money_Need)
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	if Money_Need > Money_Have then
		SystemNotice( role ,"?????????????")
		return 0
	end
	--SystemNotice(role ,"?????? ")
	return 1
end

--??'?????????????
function begin_tichun_item(...)
	local arg = ...
	--Notice("??????")
	--------???????????
	local Check_Cantichun = 0
	Check_Cantichun = can_tichun_item_main ( arg )
	if Check_Cantichun == 0 then
		return 0
	end
	--------??????
	local role = 0
	local ItemBag = {}											--??????????
	local ItemCount = {}											--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Num = 0
	local ItemCount_Num = 0
	local ItemBagCount_Num = 0

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( arg )

	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--????????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????????

	--------?????
	local Money_Need = gettichun_money_main ( arg )
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	TakeMoney(role,nil,Money_Need)
	--Money_Have = Money_Have - Money_Need
	--SetCharaAttr ( Money_Have , role , ATTR_GD )
	--ALLExAttrSet( role )

	--------????
	Check_TiChun_Item = tichun_item ( arg )
	if Check_TiChun_Item == 0  then
		SystemNotice ( role ,"?????????????")
	end
	--?????????check_item_final_data ( Item_Waiguan )
	--------Notice("?????")

	return 1
end

--?????????
function get_item_tichun_money(...)
	--Notice("???????")
	local Money = gettichun_money_main { ... }
	return Money
end

--???????????????
function gettichun_money_main ( Table )
	local role = 0
	local ItemBag = {}										--??????????????
	local ItemCount = {}										--????????????
	local ItemBagCount = {}									--???????????????
	local ItemBag_Num = 0									--?????????????
	local ItemCount_Num = 0									--??????????????
	local ItemBagCount_Num = 0								--?????????????????

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( Table )

	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--????????????
	local Item_mainitem_Lv =  GetItemLv ( Item_mainitem )
	local Money_Need = 1000000---------------10W
	--Notice("???????")
	return Money_Need
end

----??????--------------------------------------------------------------------------------------------------------
---??'??
function tichun_item ( Table )
	local role = 0
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Num = 0
	local ItemCount_Num = 0
	local ItemBagCount_Num = 0
	local ItemID_Cuihuaji = 0

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( Table )

	--------??????????
	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--????????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????????
	--------?????????
	local  ItemType_mainitem = GetItemType ( Item_mainitem )
	local  ItemType_otheritem = GetItemType ( Item_otheritem )
	--------?????ID
	local ItemID_mainitem = GetItemID ( Item_mainitem )
	local ItemID_otheritem = GetItemID ( Item_otheritem )
	--SystemNotice ( role ,"ItemID_mainitem=="..ItemID_mainitem)
	--SystemNotice ( role ,"ItemID_otheritem=="..ItemID_otheritem)
	--------???????
	local ItemID_mainitem_Lv =  GetItemLv ( Item_mainitem )
	local ItemID_otheritem_Lv =  GetItemLv ( Item_otheritem )
	--SystemNotice ( role ,"ItemID_mainitem_Lv=="..ItemID_mainitem_Lv)
	--SystemNotice ( role ,"ItemID_otheritem_Lv=="..ItemID_otheritem_Lv)
	local ItemID_main=ItemID_mainitem
	local ItemID_other=ItemID_otheritem
	-------???????????????????'ID
	if ItemID_main > 5000 then
		ItemID_main = GetItemAttr( Item_mainitem , ITEMATTR_VAL_FUSIONID )
	end
	-------????????????
	local Jinglianxinxi = GetItemForgeParam ( Item_mainitem , 1 )
	--local hole_num = Check_HasHole ( Item_mainitem )

	--local flg=0
	local r1 = 0
	local r2 = 0
	-------????????
	local item_energy = GetItemAttr(Item_mainitem,ITEMATTR_ENERGY)
	-------????????
	if ItemID_main==825  then---------??????????
		r1,r2 =MakeItem ( role , 2549  , 1 , 4 )
	end
	if ItemID_main==826  then---------??????????
		r1,r2 =MakeItem ( role , 2550  , 1 , 4 )
	end
	if ItemID_main==827  then---------?????????
		r1,r2 =MakeItem ( role , 2551  , 1 , 4 )
	end
	if ItemID_main==828  then---------?????????
		r1,r2 =MakeItem ( role , 2552  , 1 , 4 )
	end
	-------????????
	local item_qulity=12
	if ItemID_main==845 or ItemID_main==846 or ItemID_main==847  or ItemID_main==848 then---------????????
		if item_energy<1000 then
			item_qulity=2
		elseif item_energy>=1000 and item_energy<2000  then
			item_qulity=12
		elseif item_energy>=2000 and item_energy<3000  then
			item_qulity=13
		elseif item_energy>=3000 and item_energy<4000  then
			item_qulity=14
		elseif item_energy>=4000 and item_energy<5000  then
			item_qulity=15
		elseif item_energy>=5000 and item_energy<6000  then
			item_qulity=16
		elseif item_energy>=6000 and item_energy<7000  then
			item_qulity=17
		elseif item_energy>=7000 and item_energy<8000  then
			item_qulity=18
		elseif item_energy>=8000 and item_energy<9000  then
			item_qulity=19
		elseif item_energy>=9000 and item_energy<10000  then
			item_qulity=20
		end
		ItemID_main=ItemID_main+1522
		r1,r2 =MakeItem ( role , ItemID_main  , 1 , item_qulity )
	end
	---------70BOSS????
	if ItemID_main>=2530 and ItemID_main<=2547 then---------70BOSS????
		ItemID_main=ItemID_main+287
		r1,r2 =MakeItem ( role , ItemID_main  , 1 , 4)
	end
	local R1 = 0
	local R2 = 0
	R1 = RemoveChaItem ( role , ItemID_mainitem , 1 , 2 , ItemBag [0] , 2 , 1 , 1 )	--??????
	R2 = RemoveChaItem ( role , ItemID_otheritem , 1 , 2 , ItemBag [1] , 2 , 1 , 1 )	--??????
	if R1==0 or R2==0 then
		SystemNotice( role , "??????????")
		return
	end
	------------?????????
	local Item_new = GetChaItem ( role , 2 , r2 )
	local Check_SetItemForgeParam = SetItemForgeParam( Item_new , 1 , Jinglianxinxi )
	if Check_SetItemForgeParam == 0 then
		SystemNotice( role , "???þ??????????")
		return
	end
	--------------??LG
	local cha_name = GetChaDefaultName ( role )
	LG( "star_HNBTICHUN_lg" ,cha_name, ItemID_mainitem , ItemID_otheritem , ItemID_main, ItemID_other, item_energy , Jinglianxinxi )
	SynChaKitbag(role,13)
end

--Repair Tool
function can_energy_item(...)
	local arg = ...
	--	Notice ( "??????????????")
	if #arg ~= 10 and #arg ~= 14 then
		SystemNotice ( arg[1] , "?????????".. #arg )
		return 0
	end
	local Check = 0
	Check = can_energy_item_main ( arg )
	if Check == 1 then
		return 1
	else
		return 0
	end
end

--????????????????????
function can_energy_item_main ( Table )
	local role = 0
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Now = 0
	local ItemCount_Now = 0
	local ItemBagCount_Num = 0
	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Now , ItemCount_Now , ItemBagCount_Num = Read_Table ( Table )
	--------???????????
	if ItemCount [0] ~= 1 or ItemCount [1] ~= 1 or ItemBagCount [0] ~= 1 or ItemBagCount [1] ~= 1 then
		SystemNotice ( role ,"??????????")
		return 0
	end
	local Item_CanGet = GetChaFreeBagGridNum ( role )
	if Item_CanGet < 1 then
		SystemNotice(role ,"???????????????1?????")
		UseItemFailed ( role )
		return
	end
	--------??????????
	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--??????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--?????????
	--------?????????
	local  ItemType_mainitem = GetItemType ( Item_mainitem )
	local  ItemType_otheritem = GetItemType ( Item_otheritem )

	--------?????ID
	local ItemID_mainitem = GetItemID ( Item_mainitem )
	local ItemID_otheritem = GetItemID ( Item_otheritem )
	--------???????
	local Item_mainitem_Lv =  GetItemLv ( Item_mainitem )
	-------?????????
	local item_energy = GetItemAttr(Item_mainitem,ITEMATTR_ENERGY) ---??j????
	local item_maxenergy = GetItemAttr(Item_mainitem,ITEMATTR_MAXENERGY) ---???????

	-------??????????
	if ItemType_mainitem~=29 then
		SystemNotice( role ,"?????????????")
		return 0
	end
	-------???????
	if ItemID_otheritem ~= 1022 and ItemID_otheritem ~= 1024 and ItemID_otheritem ~= 6447 then
		SystemNotice( role ,"???????????")
		return 0
	end
	-------???????????????
	if item_energy==item_maxenergy then
		SystemNotice( role ,"??????????d???")
		return 0
	end
	--------?????????
	local Money_Need = get_item_energy_money ( Table )
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	if Money_Need > Money_Have then
		SystemNotice( role ,"???????????????")
		return 0
	end
	--SystemNotice(role ,"?????? ")
	return 1
end

--??'?????????????????????
function begin_energy_item(...)
	local arg = ...
	--Notice("????????")
	--------???????????
	local Check_Canenergy = 0
	Check_Canenergy = can_energy_item_main ( arg )
	if Check_Canenergy == 0 then
		return 0
	end
	--------??????
	local role = 0
	local ItemBag = {}											--??????????
	local ItemCount = {}											--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Num = 0
	local ItemCount_Num = 0
	local ItemBagCount_Num = 0

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( arg )

	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--????????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????????

	--------?????
	local Money_Need = get_item_energy_money ( arg )
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	--Money_Have = Money_Have - Money_Need
	--SetCharaAttr ( Money_Have , role , ATTR_GD )
	--ALLExAttrSet( role )
	TakeMoney(role,nil,Money_Need)

	--------????????
	Check_Energy_Item = energy_item ( arg )
	if Check_Energy_Item == 0  then
		SystemNotice ( role ,"?????????????????")
	end
	-------check_item_final_data ( Item_Waiguan )
	--------Notice("?????????")
	return 1
end

--???????????
function get_item_energy_money(...)
	--Notice("???????")
	local Money = energy_money_main { ... }
	return Money
end

--?????????????????
function energy_money_main ( Table )
	local role = 0
	local ItemBag = {}										--??????????????
	local ItemCount = {}										--????????????
	local ItemBagCount = {}									--???????????????
	local ItemBag_Num = 0									--?????????????
	local ItemCount_Num = 0									--??????????????
	local ItemBagCount_Num = 0								--?????????????????

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( Table )

	--------??????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????????
	--------?????ID
	local ItemID_otheritem = GetItemID ( Item_otheritem )

	if ItemID_otheritem==1022 then
		Money_Need=300
	else
		Money_Need=1000
	end
	--Notice("???????")
	return Money_Need
end

----??????--------------------------------------------------------------------------------------------------------
---??'??????
function energy_item ( Table )
	local role = 0
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Num = 0
	local ItemCount_Num = 0
	local ItemBagCount_Num = 0
	local ItemID_Cuihuaji = 0

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( Table )

	--------??????????
	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--????????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????????
	--------?????????
	local  ItemType_mainitem = GetItemType ( Item_mainitem )
	local  ItemType_otheritem = GetItemType ( Item_otheritem )
	--------?????ID
	local ItemID_mainitem = GetItemID ( Item_mainitem )
	local ItemID_otheritem = GetItemID ( Item_otheritem )
	-------?????????
	local item_energy = GetItemAttr(Item_mainitem,ITEMATTR_ENERGY) ---??j????
	--SystemNotice( role , "??j????=="..item_energy)

	local item_maxenergy = GetItemAttr(Item_mainitem,ITEMATTR_MAXENERGY) ---???????
	--SystemNotice( role , "???????=="..item_maxenergy)

	local energy_differ=0
	local star=math.random(1,20)
	--SystemNotice( role , "star=="..star)
	if ItemID_otheritem==1022 then
		energy_differ=star*50
	else
		energy_differ=1500
	end
	--SystemNotice( role , "energy_differ=="..energy_differ)
	item_energy=item_maxenergy--math.min( item_maxenergy , (item_energy+energy_differ) )
	--SystemNotice( role , "item_energy=="..item_energy)
	SetItemAttr ( Item_mainitem ,ITEMATTR_ENERGY, item_energy )
	--------------???LG
	local cha_name = GetChaDefaultName ( role )
	LG( "star_CHONGDIAN_lg" ,cha_name, ItemID_mainitem , ItemID_otheritem )

	local R1 = 0
	R1 = RemoveChaItem ( role , Item_otheritem , 1 , 2 , ItemBag [1] , 2 , 1 , 0 )		--?????
	if R1 == 0 then
		SystemNotice( role , "??????????")
		return
	end
end


--Extract Gem
function can_getstone_item(...)
	local player, items = process_linked_item_list{...}
	--check if there's enough free space...
	myprint(items)
	if table.getn(items) >= 2 then
		if 	((items[1].stats[ITEMATTR_URE] < items[1].stats[ITEMATTR_MAXENERGY])) then
			SystemNotice(player.role, "Gem Extraction: Equipment must have at least 1 durability.")
			return C_FALSE
		end
		if ((items[2].id ~= 1020) and (items[2].id ~= 5641)) then
			SystemNotice(player.role, "Gem Extraction: Need Pliers or Mystic Vices to extract.")
			return C_FALSE
		end
		
		local j=0
		for i=1, 3 do
			j = j + items[1].gems[i][2]
		end
		if j <= 0 then
			SystemNotice(player.role, "Gem Extraction: Equipment need gems to extract.")
			return C_FALSE
		end
		if player.stats[ATTR_GD] <= (j * 1000) then
			SystemNotice(player.role, "Gem Extraction: Insufficient gold.")
			return C_FALSE
		end
	else
		SystemNotice (player.role, "Gem Extraction: Not enough items to extract properly.")
		return C_FALSE
	end
	return C_TRUE
end

function begin_getstone_item(...)
	local player, items = process_linked_item_list{...}
	
	local j=0
	for i=3,1,-1 do
		if items[1].gems[i][2] > 0 then
			if (RemoveChaItem(player.role, 0, 1, 2, items[2].bagslot, 2, 1, 1) == 0) then
				SystemNotice( player.role , "Gem Extraction: Inventory might be locked.")
				return
			end
		
			local gem_item_id = item_gems_table[items[1].gems[i][1]]
			local gems_needed = 2^(items[1].gems[i][2]-1)
			local gem_light_item = light_item_wrap(gem_item_id)
			
			local string_have = 'have'
			local string_gem_plural = 's'
			if gems_needed == 1 then
				string_have = 'has'
				string_gem_plural = ''
			end
			
			local string_ordinal = '3rd'
			if i == 2 then
				string_ordinal = '2nd'
			elseif i == 1 then
				string_ordinal = '1st'
			end
			
			SystemNotice(player.role, "Gem Extraction: Successful.")
			BickerNotice(player.role, gems_needed..' "'..gem_light_item.name..string_gem_plural..'" '..string_have..' been extracted from "'..items[1].name..'"'.."'s "..string_ordinal..' gem slot.')
			
			GiveItem(player.role,0,gem_item_id,gems_needed,4)
			if (items[1].gems[i][2] - 1) == 0 then
				items[1].gems[i] = {0,0}
			else
				items[1].gems[i] = {items[1].gems[i][1], items[1].gems[i][2] - 1}
			end
			
			return C_TRUE
		end
	end
	return
end

--??????????????
function get_item_getstone_money(...)
	return 0
end

---------------------------------------------------------------------------------------------???k???????????????????u???????---------------------------------------------------
--can_manufacture_item
--begin_manufacture_item
--end_manufacture_item
function can_manufacture_item (...)
	local arg = ...
	--Notice("??'???")
	local ItemBagCount = arg[2]
	--Notice("???????"..ItemBagCount)
	local Length = ItemBagCount+3
	if #arg ~= Length then
		Notice("?????????"..#arg)
		return 0
	end
	local Check = 0
	--	SystemNotice( arg[1] , "???ú??????????")
	Check = can_manufacture_item_main ( arg )
	if Check == 1 then
		return 1
	else
		return 0
	end
end

function can_manufacture_item_main ( Table )
	--Notice ( "????????????")
	local role = 0
	local ItemBag = {}										--??????????
	local ItemBagCount = 0										--???????????

	role , ItemBag , ItemBagCount = Read_manufacture ( Table )
	local Item_CanGet = GetChaFreeBagGridNum ( role )
	if Item_CanGet < 1 then
		SystemNotice(role ,"???????????????1?????")
		UseItemFailed ( role )
		return
	end
	local i = 0
	local Item = {}
	local ItemID = {}
	local ItemType = {}
	for i = 1 , ItemBagCount , 1 do							--??????????????

		--???????(1-????,2-??,3-????,4,5,6-????A,B,C)
		Item[i] = GetChaItem ( role , 2 , ItemBag [i] )			--????????
		ItemID[i] = GetItemID ( Item[i] )						--?????ID
		ItemType[i] = GetItemType ( Item[i] )					 --?????????
	end




	if ItemID[1] ~= 6529 and ItemID[1] ~= 6530 and ItemID[1] ~= 6531 then
		SystemNotice( role ,"??????û??'?þ??????????????g?")

	elseif ItemID[1] ==6529 then

		SystemNotice( role ,"????'?????????????j????????????")
	elseif ItemID[1] ==6530 then

		SystemNotice( role ,"????'?????????????j??????????????")
	elseif ItemID[1] ==6531 then

		SystemNotice( role ,"????'??????????????j??????????????")

	end


	local life_lv = 0
	if ItemID[2]==2300 then ---------??????

		life_lv=GetSkillLv( role , SK_ZHIZAO )	-----????????????
	end
	if ItemID[2]==2301 then ---------??????

		life_lv=GetSkillLv( role , SK_ZHUZAO )	-----????????????
	end
	if ItemID[2]==2302 then ---------?????

		life_lv=GetSkillLv( role , SK_PENGREN )	-----????????????
	end

	if ItemID[3]~=1067 and ItemID[3]~=1068 and ItemID[3]~=1069 then---------?????????
		SystemNotice( role ,"??'?ù???")
		return 0
	end
	if ItemID[3]==1067 or ItemID[3]==1068 or ItemID[3]==1069 or ItemID[3]==1070 then---------???????
		local Gj_ure=GetItemAttr ( Item[3] , ITEMATTR_URE )
		if Gj_ure<=0 then
			Gj_ure=0
			SystemNotice( role ,"????h??????????????????????????????????¯????????")
			return 0
		end
		if ItemID[3]==1068 and  ItemID[2]~=2300 then-------????????
			SystemNotice( role ,"?????????????")
			return 0
		end
		if ItemID[3]==1069 and  ItemID[2]~=2301 then-------????????
			SystemNotice( role ,"?????????????")
			return 0
		end
		if ItemID[3]==1067 and ItemID[2]~=2302 then-------???????
			SystemNotice( role ,"?????????????")
			return 0
		end

	end

	--???????????????
	if ItemType[2] ~= 69 then
		SystemNotice( role ,"????????ü???????????")
		return 0
	end


	--???????????????????ID??????1??????2??????3???????????????????????g??????
	local paper_lv = GetItemAttr(Item[2], ITEMATTR_URE )--??????
	if life_lv<paper_lv then
		SystemNotice( role ,"??????????????j???????")
		return 0
	end
	local paper_id1=GetItemAttr(Item[2], ITEMATTR_VAL_STR )--???????1ID
	--SystemNotice( role ,"???????1ID=="..paper_id1)

	local paper_id2=GetItemAttr(Item[2], ITEMATTR_VAL_CON )--???????2ID
	--SystemNotice( role ,"???????2ID=="..paper_id2)

	local paper_id3=GetItemAttr(Item[2], ITEMATTR_VAL_DEX )--???????3ID
	--SystemNotice( role ,"???????3ID=="..paper_id3)

	if ItemID[4]~=paper_id1 or  ItemID[5]~=paper_id2 or  ItemID[6]~=paper_id3 then
		SystemNotice( role ,"??????????????y??????????????????")
		return 0
	end
	--------------??????????????????????
	local Num_paper = GetItemForgeParam ( Item[2] , 1 )---------?????????????????????
	Num_paper = TansferNum ( Num_paper )
	local Part1_paper = GetNum_Part1 ( Num_paper )	--Get Num Part 1 ?? Part 7
	local Part2_paper = GetNum_Part2 ( Num_paper )	--???????1????
	--SystemNotice( role ,"???????1????=="..Part2_paper)

	local Part3_paper = GetNum_Part3 ( Num_paper )
	local Part4_paper = GetNum_Part4 ( Num_paper )	--???????2????
	--SystemNotice( role ,"???????2????=="..Part4_paper)

	local Part5_paper = GetNum_Part5 ( Num_paper )
	local Part6_paper = GetNum_Part6 ( Num_paper )	--???????3????
	--SystemNotice( role ,"???????3????=="..Part6_paper)

	local Part7_paper = GetNum_Part7 ( Num_paper )
	local i1 = CheckBagItem( role, ItemID[4] )			----
	local i2 = CheckBagItem( role, ItemID[5] )			----
	local i3 = CheckBagItem( role, ItemID[6] )			----
	--SystemNotice( role ,"???????3????i1=="..i1)
	--SystemNotice( role ,"???????3????i2=="..i2)
	--SystemNotice( role ,"???????3????i2=="..i2)
	if i1 < Part2_paper or i2 < Part4_paper or i3 < Part6_paper  then
		SystemNotice( role ,"?????????????????????")
		return 0
	end
	local paper_num=GetItemAttr(Item[2], ITEMATTR_VAL_STA )--??'?ô???
	--SystemNotice( role ,"??'?ô???=="..paper_num)
	if paper_num <= 0 then
		SystemNotice( role ,"??????????????????")
		return 0
	end
	local a1 = CheckBagItem( role, 855 )			---???????
	local a1_num=GetItemAttr(Item[2], ITEMATTR_MAXURE )--???????????
	if a1< a1_num then
		SystemNotice( role ,"?????????????????????????????")
		return 0
	end
	--SystemNotice( role ,"??")
	return 1
end

function Read_manufacture ( Table )

	local role = Table [1]										--???
	local ItemBagCount = Table [2]								--???????????
	local ItemBag = {}										--??????????
	--	local ItemCount = {}										--????????
	local i = 0
	--Notice( " Read_manufacture_ItemBagCount=="..ItemBagCount)
	if ItemBagCount==0 then
		return role , ItemBag , ItemBagCount
	end
	for i = 1 , ItemBagCount , 1 do
		local ReadNow = i + 2
		ItemBag [i] = Table [ReadNow]
		--Notice( " Read_manufacture_ItemBag"..i.."=="..ItemBag [i])
		--ItemCount [i] = Table [ReadNow+1]
	end

	return role , ItemBag , ItemBagCount
end


function begin_manufacture_item (...)
	local arg = ...
	--Notice(  "????")

	local role = 0
	local ItemBag = {}											--??????????
	--local ItemCount = {}											--????????
	local ItemBagCount = 0										--???????????

	role , ItemBag , ItemBagCount = Read_manufacture ( arg )

	local Check1 = can_manufacture_item_main ( arg )
	if Check1 ~= 1 then
		return 0
	end

	local i = 0
	local j = 0

	local Item = {}
	local ItemID = {}
	local ItemType = {}
	for j = 1 , ItemBagCount , 1 do
		Item[j] = GetChaItem ( role , 2 , ItemBag [j] )			--????????
		ItemID[j] = GetItemID ( Item[j] )						--???????
		ItemType[j] = GetItemType ( Item[j] )					--?????????
	end
	local Gj_lv= 0-------------------??????
	if ItemID[3]==1068 then---------???????
		Gj_lv=GetItemAttr ( Item[3] , ITEMATTR_VAL_STR )
	end
	local life_lv=GetSkillLv( role , SK_ZHIZAO )	-----????????????

	local paper_lv = GetItemAttr(Item[2], ITEMATTR_URE )--??????

	local paper_energy = GetItemAttr(Item[2], ITEMATTR_MAXENERGY )-100--????????????
	local star_good=30   --????????
	if ItemID[1]==6529 then
		star_good=(math.min(life_lv,paper_lv)*0.03+Gj_lv*0.05+(100-paper_energy*10)*0.01)*100
	elseif ItemID[1]==6530 then
		star_good=(math.min(life_lv,paper_lv)*0.03+Gj_lv*0.05+(100-paper_energy*10)*0.01)*100+5
	elseif ItemID[1]==6531 then
		star_good=(math.min(life_lv,paper_lv)*0.03+Gj_lv*0.05+(100-paper_energy*10)*0.01)*100+10
	else star_good=(math.min(life_lv,paper_lv)*0.03+Gj_lv*0.05+(100-paper_energy*10)*0.01)*100-10
	end


	local star_radom = math.random ( 1, 100 )

	local eleven=2
	local a1 = star_radom+7
	local a2 = star_radom+14
	local a3 = star_radom+21
	local a4  = star_radom+28
	local a5  = star_radom+35
	local a6  = star_radom+42
	local a7  = star_radom+49
	local a8  = star_radom+56
	local a9  = star_radom+63

	if star_good<star_radom then
		eleven=1
	elseif star_good>=98 then
		eleven=11
	elseif star_good>=a9 then
		eleven=10
	elseif star_good>=a8 then
		eleven=9
	elseif star_good>=a7 then
		eleven=8
	elseif star_good>=a6 then
		eleven=7
	elseif star_good>=a5 then
		eleven=6
	elseif star_good>=a4 then
		eleven=5
	elseif star_good>=a3 then
		eleven=4
	elseif star_good>=a2 then
		eleven=3
	elseif star_good>=a1 then
		eleven=2
	end
	local star_begin=3*(1+paper_lv)
	local star_end=5*(1+paper_lv)
	local star=math.random ( star_begin , star_end )
	if star>64 then
		star=64
	end
	local run_time = star

	return 2,run_time,eleven
end
function begin_manufacture1_item (...)
	local arg = ...
	--Notice(  "????")
	local role = 0
	local ItemBag = {}											--??????????
	--local ItemCount = {}											--????????
	local ItemBagCount = 0										--???????????

	role , ItemBag , ItemBagCount = Read_manufacture ( arg )

	local Check1 = can_manufacture_item_main ( arg )
	if Check1 ~= 1 then
		return 0
	end

	local i = 0
	local j = 0

	local Item = {}
	local ItemID = {}
	local ItemType = {}
	--Notice( " ????_ItemBagCount=="..ItemBagCount)
	for j = 1 , ItemBagCount , 1 do
		Item[j] = GetChaItem ( role , 2 , ItemBag [j] )			--????????
		ItemID[j] = GetItemID ( Item[j] )						--???????
		ItemType[j] = GetItemType ( Item[j] )					--?????????
		--Notice( " ????_ItemID["..j.."]=="..ItemID[j])
		--Notice( " ????_ItemType["..j.."]=="..ItemType[j])
	end

	local paper_lv = GetItemAttr(Item[2], ITEMATTR_URE )--??????
	--Notice(  "paper_lv"..paper_lv)

	local star_begin=3*(1+paper_lv)
	local star_end=5*(1+paper_lv)
	local star=math.random ( star_begin , star_end )
	if star>64 then
		star=64
	end
	local run_time = star
	local WORD1 =math.random ( 1, 6 )
	local WORD2 =math.random ( 1, 6 )
	local WORD3 =math.random ( 1, 6 )
	local str =""..WORD1..","..WORD2..","..WORD3
	--Notice(  "????_str=="..str)
	return 2,run_time,str
end
function begin_manufacture2_item (...)
	local arg = ...
	--Notice(  "???")
	local role = 0
	local ItemBag = {}											--??????????
	--????????
	local ItemBagCount = 0										--???????????

	role , ItemBag , ItemBagCount = Read_manufacture ( arg )

	local Check1 = can_manufacture_item_main ( arg )
	if Check1 ~= 1 then
		return 0
	end

	local i = 0
	local j = 0

	local Item = {}
	local ItemID = {}
	local ItemType = {}
	for j = 1 , ItemBagCount , 1 do
		Item[j] = GetChaItem ( role , 2 , ItemBag [j] )			--????????
		ItemID[j] = GetItemID ( Item[j] )						--???????
		ItemType[j] = GetItemType ( Item[j] )					--?????????
	end

	local paper_lv = GetItemAttr(Item[2], ITEMATTR_URE )--??????
	--Notice(  "paper_lv"..paper_lv)

	local star_begin=3*(1+paper_lv)
	local star_end=4*(1+paper_lv)
	local star=math.random ( star_begin , star_end )
	if star>64 then
		star=64
	end

	local run_time = star   --??????????

	local star_ok = 12
	--SystemNotice( role , "run_time==="..run_time)

	return 2,run_time,star_ok
end
function begin_manufacture3_item (...)
	local arg = ...
	--Notice(  "???")
	local role = 0
	local ItemBag = {}											--??????????
	--local ItemCount = {}											--????????
	local ItemBagCount = 0										--???????????

	role , ItemBag , ItemBagCount = Read_manufacture ( arg )

	local Check1 = can_fenjie_item_main ( arg )
	if Check1 ~= 1 then
		return 0
	end

	local i = 0
	local j = 0

	local Item = {}
	local ItemID = {}
	local ItemType = {}
	for j = 1 , ItemBagCount , 1 do
		Item[j] = GetChaItem ( role , 2 , ItemBag [j] )			--????????
		ItemID[j] = GetItemID ( Item[j] )						--???????
		ItemType[j] = GetItemType ( Item[j] )					--?????????
	end
	local Item_Lv =  GetItemLv ( Item[3] )
	if ItemID[3]>=5000 then
		local tmd_rad=math.random ( 1 , 10 )
		if tmd_rad==1 then
			Item_Lv=80
		elseif tmd_rad==2 then
			Item_Lv=70
		elseif tmd_rad==3 then
			Item_Lv=60
		elseif tmd_rad==4 then
			Item_Lv=50
		elseif tmd_rad==5 then
			Item_Lv=40
		elseif tmd_rad==6 then
			Item_Lv=30
		elseif tmd_rad==7 then
			Item_Lv=20
		else
			Item_Lv=10
		end
	end
	local base_rad=0
	base_rad=math.max((80-math.max(Item_Lv,10))*0.01,0.15)
	------------??????ÿ????h??+10%?????
	-- local Num_JL = GetItemForgeParam ( Item[1] , 1 )
	-- Num_JL = TansferNum ( Num_JL )
	-- local Part1_JL = GetNum_Part1 ( Num_JL )	--Get Num Part 1 ?? Part 7
	-- local Part2_JL = GetNum_Part2 ( Num_JL )
	-- local Part3_JL = GetNum_Part3 ( Num_JL )
	-- local Part4_JL = GetNum_Part4 ( Num_JL )
	-- local Part5_JL = GetNum_Part5 ( Num_JL )
	-- local Part6_JL = GetNum_Part6 ( Num_JL )
	-- local Part7_JL = GetNum_Part7 ( Num_JL )
	-- local JL_jineng=0
	-- local JL_jineng_lv=0
	-- if Part2_JL==16 then
	-- JL_jineng=Part2_JL
	-- JL_jineng_lv=Part3_JL
	-- elseif Part4_JL==16 then
	-- JL_jineng=Part4_JL
	-- JL_jineng_lv=Part5_JL
	-- elseif Part6_JL==16 then
	-- JL_jineng=Part6_JL
	-- JL_jineng_lv=Part7_JL
	-- end
	local JL_jineng_lv=3
	--------------??????ÿ????h??+1%?????
	local Gj_lv=0
	if  ItemID[2]==1070 then---------???????
		Gj_lv=GetItemAttr ( Item[2] , ITEMATTR_VAL_STR )
	end
	--------------??????ÿ????h??+5%?????
	local life_lv = 0
	life_lv=GetSkillLv( role , SK_FENJIE )	-----???????????
	--------------??????10%
	local run_time = math.random ( 4, 8 )
	local word_test = math.floor((JL_jineng_lv*0.05+life_lv*0.02+Gj_lv*0.03+base_rad)*100000)
	if word_test>99999 then
		word_test=99999
	end
	local word_radom = math.random ( 10000, 99999 )
	local str =""..word_test..","..word_radom
	--Notice( "str=="..str)

	return 2,run_time,str
end
function end_manufacture_item (...)
	local arg = ...
	--Notice(  "end_manufacture_item")
	local role = 0
	local ItemBag = {}											--??????????
	local ItemBagCount = 0										--???????????

	role , ItemBag , ItemBagCount = Read_manufacture ( arg )
	--Notice( "?????????arg[#arg]==".. arg[#arg])
	--Notice( "?????????#arg==".. #arg)

	local i = 0
	local j = 0

	local star_check=0
	--SystemNotice( role , "end_manufacture_item_star_check=="..star_check)
	star_check=arg[#arg]
	--SystemNotice( role , "?????????star_check=="..star_check)
	local Item = {}
	local ItemID = {}
	local ItemType = {}
	local check = {}
	for j = 1 , ItemBagCount , 1 do
		Item[j] = GetChaItem ( role , 2 , ItemBag [j] )			--????????
		ItemID[j] = GetItemID ( Item[j] )						--???????
		ItemType[j] = GetItemType ( Item[j] )					--?????????
	end
	local paper_id1=GetItemAttr(Item[2], ITEMATTR_VAL_STR )--???????1ID
	local paper_id2=GetItemAttr(Item[2], ITEMATTR_VAL_CON )--???????2ID
	local paper_id3=GetItemAttr(Item[2], ITEMATTR_VAL_DEX )--???????3ID
	local Num_paper = GetItemForgeParam ( Item[2] , 1 )---------?????????????????????
	Num_paper = TansferNum ( Num_paper )
	local Part2_paper = GetNum_Part2 ( Num_paper )	--???????1????
	local Part4_paper = GetNum_Part4 ( Num_paper )	--???????2????
	local Part6_paper = GetNum_Part6 ( Num_paper )	--???????3????
	local life_lv = 0-----------------?????????
	local Gj_lv= 0-------------------??????
	local paper_lv = GetItemAttr(Item[2], ITEMATTR_URE )--??????
	local num_x=1
	local star_num_qulity=4
	if ItemID[2]==2300 then ---------??????
		life_lv=GetSkillLv( role , SK_ZHIZAO )	-----????????????
		if star_check==1 then
			num_x=0
		elseif star_check==2 or star_check==3 or star_check==4 then
			num_x=1
		elseif star_check==5 or star_check==6 or star_check==7 then
			num_x=2
		elseif star_check==8 or star_check==9 or star_check==10 then
			num_x=3
		elseif star_check==11 then
			num_x=4
		end
	end
	if ItemID[2]==2301 then ---------??????
		life_lv=GetSkillLv( role , SK_ZHUZAO )	-----????????????
	end
	if ItemID[2]==2302 then ---------?????
		life_lv=GetSkillLv( role , SK_PENGREN )	-----????????????
		local differ_check=math.abs(star_check-75)
		--Notice(  "<<<<<<star_check>>>>>>=="..star_check)
		--Notice(  "<<<<<differ_check>>>>>=="..differ_check)

		--Notice(  "ItemID=="..ItemID[1])
		if ItemID[1] ==6529 then
			if differ_check==0 then
				num_x=4
			elseif differ_check==1 then
				num_x=3
			elseif differ_check==2  then
				num_x=2
			elseif differ_check>=3 and differ_check<=6 and star_check<=77 then
				num_x=1
			elseif differ_check>=7 and differ_check<=25 and star_check<=77 then
				num_x=1
			else
				num_x=0
			end
		elseif ItemID[1] ==6530 then
			if differ_check==0 then
				num_x=5
			elseif differ_check==1 then
				num_x=4
			elseif differ_check==2  then
				num_x=3
			elseif differ_check>=3 and differ_check<=6 and star_check<=77 then
				num_x=2
			elseif differ_check>=7 and differ_check<=25 and star_check<=77 then
				num_x=1
			else
				num_x=0
			end
		elseif ItemID[1] ==6531 then
			if differ_check==0 then
				num_x=5
			elseif differ_check==1 then
				num_x=4
			elseif differ_check==2  then
				num_x=3
			elseif differ_check>=3 and differ_check<=6 and star_check<=77 then
				num_x=2
			elseif differ_check>=7 and differ_check<=25 and star_check<=77 then
				num_x=2
			else
				num_x=0
			end
		else
			if differ_check==0 then
				num_x=2
			elseif differ_check==1 then
				num_x=2
			elseif differ_check==2  then
				num_x=1
			elseif differ_check>=3 and differ_check<=6 and star_check<=77 then
				num_x=1
			elseif differ_check>=7 and differ_check<=25 and star_check<=77 then
				num_x=1
			else
				num_x=0
			end
		end
	end
	if ItemID[3]==1067 or ItemID[3]==1068 or ItemID[3]==1069 or ItemID[3]==1070 then---------???????
		Gj_lv=GetItemAttr ( Item[3] , ITEMATTR_VAL_STR )
	end

	--??????????
	local i1 = 0
	local i2 = 0
	local i3 = 0

	local s1 = 0
	local s2 = 0
	local s3 = 0
	i1 =TakeItem( role, 0, paper_id1, Part2_paper)	--???
	i2 =TakeItem( role, 0, paper_id2, Part4_paper)	--???
	i3 =TakeItem( role, 0, paper_id3, Part6_paper)	--???

	if ItemID[1]==6529 then    		  --h??????
		s1 =TakeItem( role, 0, 6529, 1)
	elseif  ItemID[1]==6530 then      --h??????
		s2 =TakeItem( role, 0, 6530, 1)
	elseif ItemID[1]==6531 then      --h??????
		s3 =TakeItem( role, 0, 6531, 1)
	else
		SystemNotice ( role ,"d'?þ??????????ó???'?g?")
	end


	if i1 == 0 or  i2 == 0 or  i3 == 0 then
		LG( "Hecheng_BS" , "?????????" )
	end
	local a1_num=GetItemAttr(Item[2], ITEMATTR_MAXURE )--???????????

	local a1=TakeItem( role, 0, 855, a1_num )
	if a1==0  then
		SystemNotice ( role ,"???????????")
		return
	end
	--------?????????????ID
	local new_num = GetItemAttr(Item[2], ITEMATTR_VAL_AGI )--????????ID

	-- local  aa=new_num

	if ItemID[2]==2300 then ---------??????
		if new_num==1067 or  new_num==1068 or new_num==1069 or new_num==1070 or  new_num==2236 then
			num_x=1
		end
	end
	local paper_energy = GetItemAttr(Item[2], ITEMATTR_MAXENERGY )-100--????????????
	local star_good=0

	if ItemID[1]==6529 then
		star_good=(math.min(life_lv,paper_lv)*0.03+Gj_lv*0.05+(100-paper_energy*10)*0.01)*100
	elseif ItemID[1]==6530 then
		star_good=(math.min(life_lv,paper_lv)*0.03+Gj_lv*0.05+(100-paper_energy*10)*0.01)*100+5
	elseif ItemID[1]==6531 then
		star_good=(math.min(life_lv,paper_lv)*0.03+Gj_lv*0.05+(100-paper_energy*10)*0.01)*100+10
	else star_good=(math.min(life_lv,paper_lv)*0.03+Gj_lv*0.05+(100-paper_energy*10)*0.01)*100-10
	end
	--Notice(  "new_num=="..new_num)
	--Notice ("???star_good=="..star_good)


	local xianshi = {}   	 ---?????????
	xianshi[1] =1136
	xianshi[2] =2720
	xianshi[6] =1138
	xianshi[7] =2722
	xianshi[11]=1137
	xianshi[12]=2721
	xianshi[16]=1080  			--???
	xianshi[17]=4022			--?????????
	xianshi[18]=1082     	 	--????
	xianshi[19]=4022     		--?????????
	xianshi[20]=4023			--???????
	xianshi[21]=2426    		 --??????
	xianshi[24]=1083            --????
	xianshi[25]=1087			--é?
	xianshi[26]=1871			--???????
	xianshi[27]=4025			--????????
	xianshi[28]=4024			--???????

	local i=0
	for i =1,28,1 do
		if new_num ==xianshi[i] then
			star_good =star_good*0.5
			--Notice ("?????????????½?h??star_good=="..star_good)
		end

	end

	---------??????---------------------
	local star_radom = math.random ( 1, 100 )
	--SystemNotice( role ,",star_radom=="..star_radom)
	local m1 = -1
	local m2 = -1
	if ItemID[2]==2300 then
		if star_check>=2 then
			star_good=100
			star_radom=1
		else
			star_good=1
			star_radom=100
		end
	end
	--SystemNotice( role ,"star_check=="..star_check..",?????star_good=="..star_good..",????star_radom=="..star_radom..",????????????num_x=="..num_x)
	--SystemNotice( role , "??????j??????????????????????????????")
	local star_check_chenggong=0
	if star_check~=0 and star_good >star_radom and num_x~=0 then
		star_check_chenggong=1
		--SystemNotice( role , "star_check=="..star_check..",star_good=="..star_good..",star_radom=="..star_radom..",num_x=="..num_x)
		m1, m2 = MakeItem ( role , new_num  , num_x , star_num_qulity )
		local Itemfinal = GetChaItem ( role , 2 , m2 )
		if ItemID[2]==2301 and CheckItem_CanJinglian(Itemfinal)==1 then ---------???????õ???????
			local Itemfinal_energy=GetItemAttr ( Itemfinal ,ITEMATTR_ENERGY) --------????????
			local itemfinal_maxenergy =GetItemAttrRange(new_num , ITEMATTR_MAXENERGY , 1 )-------????????? ---?????????????????
			local itemfinal_minenergy = GetItemAttrRange(new_num ,ITEMATTR_MAXENERGY , 0 )--------????????? ---??????????????
			--SystemNotice( role , "Itemfinal_energy=="..Itemfinal_energy)
			--SystemNotice( role , "itemfinal_maxenergy=="..itemfinal_maxenergy)
			--SystemNotice( role , "itemfinal_minenergy=="..itemfinal_minenergy)
			if paper_energy>7 then
				paper_energy=7
			end
			if itemfinal_maxenergy~=itemfinal_minenergy then ---------??BOSS?????????
				Itemfinal_energy=math.fmod(Itemfinal_energy,1000)+paper_energy*1000
				SetItemAttr ( Itemfinal ,ITEMATTR_MAXENERGY , Itemfinal_energy)
				SetItemAttr ( Itemfinal ,ITEMATTR_ENERGY , Itemfinal_energy)
			end
		end
		local item_final_ID=GetItemID(Itemfinal)
		--SystemNotice( role , "item_final_ID="..item_final_ID)
		if item_final_ID==1067 or item_final_ID==1068 or item_final_ID==1069 or item_final_ID==1070 then
			SetItemAttr(Itemfinal, ITEMATTR_VAL_STR ,1)--??????????ù?????
			SetItemAttr(Itemfinal, ITEMATTR_MAXENERGY ,10000)--???ù??????????
			SetItemAttr(Itemfinal, ITEMATTR_ENERGY ,1)--???ù????j????
		end
		if item_final_ID==2236 then
			SetItemAttr(Itemfinal, ITEMATTR_VAL_STR ,paper_lv)--????????????????????
		end
	else
		SystemNotice( role , "????????????????????????,???????????,??????????????")
	end
	--??????????
	local paper_num=GetItemAttr(Item[2], ITEMATTR_VAL_STA )--??'?ô???
	paper_num=paper_num-1

	SetItemAttr(Item[2], ITEMATTR_VAL_STA , paper_num )

	local Gj_ure=0
	if ItemID[3]==1067 or ItemID[3]==1068 or ItemID[3]==1069 or ItemID[3]==1070 then---------???????
		Gj_ure=GetItemAttr ( Item[3] , ITEMATTR_URE )
		local star_gjlv_num=GetItemAttr ( Item[3] , ITEMATTR_VAL_STR )
		Gj_ure=Gj_ure-50*star_gjlv_num
		if Gj_ure<=0 then
			Gj_ure=0
		end




		local star_lv_num = GetItemAttr( Item[3] ,ITEMATTR_ENERGY )       --???????????????¼???????
		if star_check_chenggong==1 then-------???????????????????
			star_lv_num=star_lv_num+paper_lv
		else						--------????????1
			star_lv_num=star_lv_num+1
		end
		if star_lv_num>=10000 then
			star_lv_num=10000
		end
		SystemNotice( role , "????j?j??"..star_lv_num.."????")
		if star_lv_num>=star_gjlv_num*star_gjlv_num*100 then --------??????????
			star_gjlv_num=star_gjlv_num+1
			SetItemAttr ( Item[3] , ITEMATTR_VAL_STR ,star_gjlv_num)----------????????
			SystemNotice( role , "???????L???????????")
			star_lv_num=0
		end
		SetItemAttr (  Item[3] , ITEMATTR_ENERGY , star_lv_num )
		SetItemAttr ( Item[3] , ITEMATTR_URE ,Gj_ure)
	end
	-----------------------------------------------------LG---------------------------------------------------------------------------------------------------------------------
	local cha_name = GetChaDefaultName ( role )
	LG( "star_SHENGHUO_lg" ,cha_name, star_check , ItemID[2] , paper_lv , paper_id1 , Part2_paper ,  paper_id2 , Part4_paper ,  paper_id3 , Part6_paper , ItemID[3] , Gj_lv ,  life_lv )
	SynChaKitbag(role,13)
	--check_item_final_data(Item[2])--------??????????
	--check_item_final_data(Item[3])--------????????????
	return m2
end
function can_fenjie_item (...)
	local arg = ...
	--Notice("??'???")
	local ItemBagCount = arg[2]
	--Notice("???????"..ItemBagCount)
	local Length = ItemBagCount+3
	if #arg ~= Length then
		Notice("?????????".. #arg)
		return 0
	end
	local Check = 0
	--	SystemNotice( arg[1] , "???÷??????????")
	Check = can_fenjie_item_main ( arg )
	if Check == 1 then
		return 1
	else
		return 0
	end
	--Notice("??'??????????")
end


function can_fenjie_item_main ( Table )
	--Notice ( "????????????")
	local role = 0
	local ItemBag = {}									--??????????
	local ItemBagCount = 0								--???????????

	role , ItemBag , ItemBagCount = Read_manufacture ( Table )
	--Notice( "????????????ItemBag [1]=="..ItemBag [1])
	--Notice( "????????????ItemBag [2]=="..ItemBag [2])
	--Notice( "????????????ItemBag [3]=="..ItemBag [3])
	--Notice( "????????????ItemBag [4]=="..ItemBag [4])
	role , ItemBag , ItemBagCount = Read_manufacture ( Table )
	local Item_CanGet = GetChaFreeBagGridNum ( role )
	if Item_CanGet < 1 then
		SystemNotice(role ,"???????????????1?????")
		UseItemFailed ( role )
		return
	end
	local i = 0
	local Item = {}
	local ItemID = {}
	local ItemType = {}
	for i = 1 , ItemBagCount , 1 do							--??????????????
		--if ItemBag[i] == 0  then
		--	SystemNotice( role , "?????????")
		--	return 0
		--end
		--???????(1-????,2-????,3-???,4-?????)
		Item[i] = GetChaItem ( role , 2 , ItemBag [i] )			--????????
		ItemID[i] = GetItemID ( Item[i] )						--?????ID
		ItemType[i] = GetItemType ( Item[i] )					--?????????
		--Notice( " ???_ItemID["..i.."]=="..ItemID[i])
		--Notice( " ???_ItemType["..i.."]=="..ItemType[i])
	end
	local type_check=0
	type_check=CheckItem_CanJinglian ( Item[3] )
	if type_check==0 then
		SystemNotice( role ,"???????????????????")
		return 0
	end


	local JL_jineng=0
	local JL_jineng_lv=0
	local life_lv = 0
	life_lv=GetSkillLv( role , SK_FENJIE )	-----???????????

	--SystemNotice( role ,"??? ??????????????????aaa")

	if  ItemID[2]~=1070 then---------???????
		SystemNotice( role ,"??'???????????")
		return 0
	else
		local Gj_ure=GetItemAttr ( Item[2] , ITEMATTR_URE )
		if Gj_ure<=0 then
			Gj_ure=0
			SystemNotice( role ,"??????h???????????'????")
			return 0
		end
		local Gj_lv=GetItemAttr ( Item[2] , ITEMATTR_VAL_STR )

	end

	if ItemID[4] ~=2625 and ItemID[4] ~=2630 and ItemID[4] ~=2634 and ItemID[4] ~=2635 and ItemID[4] ~=2636 and ItemID[4] ~=2637 and ItemID[4] ~=2638 and ItemID[4] ~=2639  then	---?????
		SystemNotice( role ,"???????????")
		return 0
	end

	--Notice ( "???????????????")

	return 1
end

function end_fenjie_item (...)
	local arg = ...
	--Notice(  "end_fenjie_item")
	local role = 0
	local ItemBag = {}											--??????????
	--local ItemCount = {}											--????????
	local ItemBagCount = 0										--???????????

	role , ItemBag , ItemBagCount = Read_manufacture ( arg )

	local i = 0
	local j = 0


	local star_check=0
	--Notice( "star_check=="..star_check)
	star_check=arg[#arg]
	--Notice( "???_star_check=="..star_check)
	--SystemNotice(role ,"???_star_check=="..star_check)

	local Item = {}
	local ItemID = {}
	local ItemType = {}
	--local check = {}
	for j = 1 , ItemBagCount , 1 do
		Item[j] = GetChaItem ( role , 2 , ItemBag [j] )			--????????
		ItemID[j] = GetItemID ( Item[j] )						--???????
		ItemType[j] = GetItemType ( Item[j] )					--?????????
	end
	local Item_Lv =  GetItemLv ( Item[3] )
	--Notice( "??????aaaaaaaaaaaaa")


	--------------??????ÿ????h??+1%?????
	local Gj_lv=0
	if  ItemID[2]==1070 then---------???????
		Gj_lv=GetItemAttr ( Item[2] , ITEMATTR_VAL_STR )
	end
	--------------??????????ÿ????h??+5%?????
	local life_lv = 0
	life_lv=GetSkillLv( role , SK_FENJIE )	-----???????????
	local num_new =0
	--???????????
	if ItemID[1]==6529 then
		num_new =math.min(math.max(1,math.floor( (1*0.1+life_lv*0.05+Gj_lv*0.05 )*10)),10)
		SystemNotice(role ,"????'?????????????????????????")
	elseif 	ItemID[1]==6530 then
		num_new =math.min(math.max(1,math.floor( (2*0.1+life_lv*0.05+Gj_lv*0.05 )*10)),10)
		SystemNotice(role ,"????'??????????????????????????")
	elseif 	ItemID[1]==6531 then
		num_new =math.min(math.max(1,math.floor( (3*0.1+life_lv*0.05+Gj_lv*0.05 )*10)),10)
		SystemNotice(role ,"????'???????????????????????")
	else
		num_new =math.min(math.max(1,math.floor( (life_lv*0.05+Gj_lv*0.05 )*10)),10)

	end
	--Notice( "num_new"..num_new)
	local i1 = 0
	local i2 = 0
	i1= RemoveChaItem ( role , ItemID[3] , 1 , 2 , ItemBag [3] , 2 , 1 , 1)		--???
	i2= RemoveChaItem ( role , ItemID[4] , 1 , 2 , ItemBag [4] , 2 , 1 , 1)		--???
	if i1 == 0 or  i2 == 0 then
		LG( "Hecheng_BS" , "?????????" )
	end
	local new_num=1346
	if ItemID[4] ==2625 then	---??J????
		local rad1=0
		if Item_Lv>=80 then
			rad1=math.random ( 131, 143 )
		elseif Item_Lv>=70 then
			rad1=math.random ( 116, 130 )
		elseif Item_Lv>=60 then
			rad1=math.random ( 94, 115 )
		elseif Item_Lv>=50 then
			rad1=math.random ( 72, 93 )
		elseif Item_Lv>=40 then
			rad1=math.random ( 49, 71 )
		elseif Item_Lv>=30 then
			rad1=math.random ( 27, 48 )
		elseif Item_Lv>=20 then
			rad1=math.random ( 13, 26 )
		else
			rad1=math.random ( 1, 12 )
		end
		new_num=STONE1_ID[rad1]
	elseif ItemID[4] ==2630	 then ---??J????
		local rad1=0
		if Item_Lv>=80 then
			rad1=math.random ( 150, 151 )
		elseif Item_Lv>=70 then
			rad1=math.random ( 145, 149 )
		elseif Item_Lv>=60 then
			rad1=math.random ( 119, 144 )
		elseif Item_Lv>=50 then
			rad1=math.random ( 101, 118 )
		elseif Item_Lv>=40 then
			rad1=math.random ( 76, 100 )
		elseif Item_Lv>=30 then
			rad1=math.random ( 41, 75 )
		elseif Item_Lv>=20 then
			rad1=math.random ( 25, 40 )
		else
			rad1=math.random ( 1, 24 )
		end
		new_num=FOOD_ID[rad1]
	elseif ItemID[4] ==2634	 then ---????????
		local rad1=0
		if Item_Lv>=80 then
			rad1=math.random ( 336, 346 )
		elseif Item_Lv>=70 then
			rad1=math.random ( 249, 335 )
		elseif Item_Lv>=60 then
			rad1=math.random ( 191, 248 )
		elseif Item_Lv>=50 then
			rad1=math.random ( 127, 190 )
		elseif Item_Lv>=40 then
			rad1=math.random ( 89, 126 )
		elseif Item_Lv>=30 then
			rad1=math.random ( 57, 88 )
		elseif Item_Lv>=20 then
			rad1=math.random ( 19, 56 )
		else
			rad1=math.random ( 1, 18 )
		end
		new_num=SPECIL_ID[rad1]
	elseif ItemID[4] ==2635	 then ---????????
		local rad1=0
		if Item_Lv>=80 then
			rad1=math.random ( 171, 176 )
		elseif Item_Lv>=70 then
			rad1=math.random ( 155, 170 )
		elseif Item_Lv>=60 then
			rad1=math.random ( 133, 154 )
		elseif Item_Lv>=50 then
			rad1=math.random ( 109, 132 )
		elseif Item_Lv>=40 then
			rad1=math.random ( 88, 108 )
		elseif Item_Lv>=30 then
			rad1=math.random ( 56, 87 )
		elseif Item_Lv>=20 then
			rad1=math.random ( 38, 55 )
		else
			rad1=math.random ( 1, 37 )
		end
		new_num=BONE_ID[rad1]
	elseif ItemID[4] ==2636	 then ---???????
		local rad1=0
		if Item_Lv>=70 then
			rad1=math.random ( 57, 72 )
		elseif Item_Lv>=60 then
			rad1=math.random ( 57, 69 )
		elseif Item_Lv>=50 then
			rad1=math.random ( 57, 63 )
		elseif Item_Lv>=30 then
			rad1=math.random ( 57, 61 )
		elseif Item_Lv>=20 then
			rad1=math.random ( 37, 56 )
		else
			rad1=math.random ( 1, 36 )
		end
		new_num=TREE_ID[rad1]
	elseif ItemID[4] ==2637 then ---ë??????
		local rad1=0
		if Item_Lv>=70 then
			rad1=math.random ( 77, 97 )
		elseif Item_Lv>=60 then
			rad1=math.random ( 77, 93 )
		elseif Item_Lv>=50 then
			rad1=math.random ( 63, 81 )
		elseif Item_Lv>=40 then
			rad1=math.random ( 48, 62 )
		elseif Item_Lv>=30 then
			rad1=math.random ( 30, 47 )
		elseif Item_Lv>=20 then
			rad1=math.random ( 20, 29 )
		else
			rad1=math.random ( 1, 19 )
		end
		new_num=SKIP_ID[rad1]
	elseif ItemID[4] ==2638 then ---???????
		local rad1=0
		if Item_Lv>=50 then
			rad1=math.random ( 33, 37 )
		elseif Item_Lv>=40 then
			rad1=math.random ( 28, 32 )
		elseif Item_Lv>=30 then
			rad1=math.random ( 19, 27 )
		elseif Item_Lv>=20 then
			rad1=math.random ( 12, 18 )
		else
			rad1=math.random ( 1, 11 )
		end
		new_num=WATER_ID[rad1]
	end
	if ItemID[1]==6529 then    		  --h??????
		s1 =TakeItem( role, 0, 6529, 1)
	elseif  ItemID[1]==6530 then      --h??????
		s2 =TakeItem( role, 0, 6530, 1)
	elseif ItemID[1]==6531 then      --h??????
		s3 =TakeItem( role, 0, 6531, 1)
	else
		SystemNotice(role ,"????'?þ????????????????????'?þ???????????")
	end
	--Notice( "new_num======"..new_num)
	--Notice( "num_new======"..num_new)

	--------????????????
	--if star_check~=0  and num_new~=0 then
	GiveItem ( role , 0 , new_num  , num_new , 4  )
	--else
	-- 	SystemNotice( role , "????????????????,???????????,??????????????")
	--end
	--???????;?
	if ItemID[2]==1070 then---------???????
		local Gj_ure=GetItemAttr ( Item[2] , ITEMATTR_URE )
		local star_gjlv_num=GetItemAttr ( Item[2] , ITEMATTR_VAL_STR )
		Gj_ure=Gj_ure-50*star_gjlv_num
		if Gj_ure<=0 then
			Gj_ure=0
		end
		--if Gj_ure==0 then
		--	local k1 = 0
		--	k1= RemoveChaItem ( role , ItemID[2] , 1 , 2 , ItemBag [2] , 2 , 1 , 1)		--???
		--	SystemNotice( role , "?????????????????????????")
		--	if k1 == 0 then
		--		LG( "FENJIE_BS" , "?????????" )
		--	end
		--end
		local star_lv_num = GetItemAttr( Item[2] ,ITEMATTR_ENERGY )       --???????????????¼???????
		star_lv_num=star_lv_num+1-------???????????????????
		if star_lv_num>=10000 then
			star_lv_num=10000
		end
		SystemNotice( role , "????j?j??"..star_lv_num.."????")
		local star_gjlv_num=GetItemAttr ( Item[2] , ITEMATTR_VAL_STR )
		if star_lv_num>=star_gjlv_num*star_gjlv_num*100 then --------??????????
			star_gjlv_num=star_gjlv_num+1
			SetItemAttr ( Item[2] , ITEMATTR_VAL_STR ,star_gjlv_num)----------????????
			SystemNotice( role , "???????L???????????")
			star_lv_num=0
		end
		SetItemAttr (  Item[2] , ITEMATTR_ENERGY , star_lv_num )
		SetItemAttr ( Item[2] , ITEMATTR_URE ,Gj_ure)
	end
	--------------LG
	local cha_name = GetChaDefaultName ( role )
	LG( "star_FENJIE_lg" ,cha_name, star_check, ItemID[2] , ItemID[3] , ItemID[4] , Gj_lv ,  life_lv )
	--check_item_final_data(Item[3])--------????????????
	SynChaKitbag(role,13)
	--Notice(  "end_fenjie_item-------????")
	return 1
end
--???????
--????????????????????????????
function can_shtool_item(...)
	local arg = ...
	--	Notice ( "????????????????")
	if #arg ~= 10 and #arg ~= 14 then
		SystemNotice ( arg[1] , "?????????".. #arg )
		return 0
	end
	local Check = 0
	Check = can_shtool_item_main ( arg )
	if Check == 1 then
		return 1
	else
		return 0
	end
end

--??????????????????????
function can_shtool_item_main ( Table )
	local role = 0
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Now = 0
	local ItemCount_Now = 0
	local ItemBagCount_Num = 0
	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Now , ItemCount_Now , ItemBagCount_Num = Read_Table ( Table )
	--------???????????
	if ItemCount [0] ~= 1 or ItemCount [1] ~= 1 or ItemBagCount [0] ~= 1 or ItemBagCount [1] ~= 1 then
		SystemNotice ( role ,"??????????")
		return 0
	end
	local Item_CanGet = GetChaFreeBagGridNum ( role )
	if Item_CanGet < 1 then
		SystemNotice(role ,"???????????????1?????")
		UseItemFailed ( role )
		return
	end
	--------??????????
	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--?????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????
	--------?????????
	local  ItemType_mainitem = GetItemType ( Item_mainitem )
	local  ItemType_otheritem = GetItemType ( Item_otheritem )

	--------?????ID
	local ItemID_mainitem = GetItemID ( Item_mainitem )
	local ItemID_otheritem = GetItemID ( Item_otheritem )
	--------??????
	local Item_mainitem_Lv =  GetItemAttr ( Item_mainitem, ITEMATTR_VAL_STR )
	--------???????
	local Item_otheritem_Lv =  GetItemAttr ( Item_otheritem , ITEMATTR_VAL_STR)
	-------?????;?
	local item_shtool_ure = GetItemAttr(Item_mainitem,ITEMATTR_URE) ---??j?;?
	local item_shtool_maxure = GetItemAttr(Item_mainitem,ITEMATTR_MAXURE) ---????;?
	-------??????????
	if ItemType_mainitem~=70 then
		SystemNotice( role ,"???????????????????????????????????????????????")
		return 0
	end
	if ItemID_mainitem~=1067 and ItemID_mainitem~=1068 and ItemID_mainitem~=1069 and ItemID_mainitem~=1070 then
		SystemNotice( role ,"???????????????????????????????????????????????")
		return 0
	end
	-------?????????
	if ItemType_otheritem ~= 70 or ItemID_otheritem~=2236 then
		SystemNotice( role ,"??'??????????????")
		return 0
	end
	-------????????????;?h??
	if item_shtool_ure>=item_shtool_maxure then
		SystemNotice( role ,"??????dh??")
		return 0
	end
	-------??????????????????????
	if Item_mainitem_Lv>Item_otheritem_Lv then
		SystemNotice( role ,"???????j?????????????????j??")
		return 0
	end
	--------?????????
	local Money_Need = get_item_shtool_money ( Table )
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	if Money_Need > Money_Have then
		SystemNotice( role ,"??????????????????")
		return 0
	end
	--SystemNotice(role ,"?????? ")
	return 1
end

--??'???????????????????????
function begin_shtool_item(...)
	local arg = ...
	--Notice("???????????")
	--------???????????
	local Check_Canshtool = 0
	Check_Canshtool = can_shtool_item_main ( arg )
	if Check_Canshtool == 0 then
		return 0
	end
	--------??????
	local role = 0
	local ItemBag = {}											--??????????
	local ItemCount = {}											--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Num = 0
	local ItemCount_Num = 0
	local ItemBagCount_Num = 0

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( arg )

	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--????????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????????

	--------?????
	local Money_Need = get_item_shtool_money ( arg )
	local Money_Have = GetChaAttr ( role , ATTR_GD )
	--Money_Have = Money_Have - Money_Need
	--SetCharaAttr ( Money_Have , role , ATTR_GD )
	--ALLExAttrSet( role )
	TakeMoney(role,nil,Money_Need)

	--------?????????
	Check_shtool_Item = shtool_item ( arg )
	if Check_shtool_Item == 0  then
		SystemNotice ( role ,"???????????????????????")
	end
	-------check_item_final_data ( Item_Waiguan )
	--------Notice("??????????")
	return 1
end

--??????????????
function get_item_shtool_money(...)
	--Notice("???????")
	local Money = shtool_money_main { ... }
	return Money
end

--????????????????????
function shtool_money_main ( Table )
	local role = 0
	local ItemBag = {}										--??????????????
	local ItemCount = {}										--????????????
	local ItemBagCount = {}									--???????????????
	local ItemBag_Num = 0									--?????????????
	local ItemCount_Num = 0									--??????????????
	local ItemBagCount_Num = 0								--?????????????????

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( Table )

	--------??????????
	--local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--?????????
	--------?????ID
	-- local Item_mainitem_Lv = GetItemAttr ( Item_mainitem, ITEMATTR_VAL_STR )
	local Money_Need=200--*Item_mainitem_Lv*Item_mainitem_Lv
	--Notice("???????")
	return Money_Need
end

----??????--------------------------------------------------------------------------------------------------------		elseif funclist[id].func == PlayEffect then
------------------------------------------------------------------------------------------------		ChaPlayEffect( npc, funclist[id].p1 )
---??'???????
function shtool_item ( Table )
	local role = 0
	local ItemBag = {}										--??????????
	local ItemCount = {}										--????????
	local ItemBagCount = {}										--???????????
	local ItemBag_Num = 0
	local ItemCount_Num = 0
	local ItemBagCount_Num = 0
	local ItemID_Cuihuaji = 0

	role , ItemBag , ItemCount , ItemBagCount , ItemBag_Num , ItemCount_Num , ItemBagCount_Num = Read_Table ( Table )

	--------??????????
	local Item_mainitem = GetChaItem ( role , 2 , ItemBag [0] )	--????????????
	local Item_otheritem = GetChaItem ( role , 2 , ItemBag [1] )	--??????????????
	--------?????????
	local  ItemType_mainitem = GetItemType ( Item_mainitem )
	local  ItemType_otheritem = GetItemType ( Item_otheritem )
	--------?????ID
	local ItemID_mainitem = GetItemID ( Item_mainitem )
	local ItemID_otheritem = GetItemID ( Item_otheritem )
	-------?????;?
	local item_shtool_ure = GetItemAttr(Item_mainitem,ITEMATTR_URE) ---??j?;?
	local item_shtool_maxure = GetItemAttr(Item_mainitem,ITEMATTR_MAXURE) ---????;?

	SetItemAttr ( Item_mainitem ,ITEMATTR_URE, item_shtool_maxure )
	--------------???LG
	local cha_name = GetChaDefaultName ( role )
	LG( "star_xiuguo_lg" ,cha_name, ItemID_mainitem , ItemID_otheritem )

	local R1 = 0
	R1 = RemoveChaItem ( role , Item_otheritem , 1 , 2 , ItemBag [1] , 2 , 1 , 0 )		--???????
	if R1 == 0 then
		SystemNotice( role , "??????????")
		return
	end
	SynChaKitbag(role,13)
end

-- Item repair
function can_repair_item(npc_role, player_role , item_handle)
	--local re_type = IsPlayer ( role_repair )
	local npc = player_wrap(npc_role)
	local player = player_wrap(player_role)
	local item = item_wrap(item_handle)

	if item.stats[ITEMATTR_MAXURE] <= 2500 then
		SystemNotice(player_role, "Equipment Repair: Unrepairable." )
		return C_FALSE
	end
	if item.stats[ITEMATTR_MAXURE] == item.stats[ITEMATTR_URE] then
		SystemNotice(player_role, "Equipment Repair: Durability already at maximum.")
		return C_FALSE
	end
	if player.stats[ATTR_GD] <= get_item_repair_money(player_role, item_handle) then
		SystemNotice(player_role, "Equipment Repair: Insufficient gold.")
		return C_FALSE
	end
	return C_TRUE
end

function get_item_repair_money(player_role, item_handle)
	local item = item_wrap(item_handle)
	return math.max((math.floor(item.stats[ITEMATTR_MAXURE]/50)-math.floor(item.stats[ITEMATTR_URE]/50)*math.floor((item.lvl/10)^1.7)-1),1)
end

function begin_repair_item (npc_role, player_role, item_handle)
	local npc = player_wrap(npc_role)
	local player = player_wrap(player_role)
	local item = item_wrap(item_handle)

	if can_repair_item(npc_role, player_role, item_handle) == C_FALSE then
		return C_FALSE
	end
	player.stats[ATTR_GD] = player.stats[ATTR_GD] - get_item_repair_money(player_role, item_handle)
	AttrRecheck(player.role)
	item.stats[ITEMATTR_URE] = item.stats[ITEMATTR_MAXURE]

	SystemNotice(player_role ,"Equipment Repair: Successful." )
	return 1
end
