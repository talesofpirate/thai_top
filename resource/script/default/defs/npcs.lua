function npc_init(npc, scriptid, player, state)
	if state == NPC_INIT then
		--npcs[scriptid].proc = npc_tradeonly_proc
		npcs[scriptid].dialog = dialog:new(npc.char)
		npcs[scriptid].trade = shop_trade:new(npc.char)
		
		npcs[scriptid].proc = npc_custominit_proc
		npcs[scriptid].on_init = function(npc, scriptid, player, cmd, p1, p2)
			SystemNotice(player.role,'#43   This npc doesn\'t communicate.')
		end
	end
	
	--shorthand functions
	function n() --npc
		return npcs[scriptid]
	end
	
	function d() --npc dialog
		return npcs[scriptid].dialog
	end
	
	function p(page) --page table
		return npcs[scriptid].dialog.pages[page]
	end
	
	function t() --npc trade
		return npcs[scriptid].trade
	end
	
	function jtp(page) --jumptopage
		npcs[scriptid].dialog:open(page)
	end

			--some built-in top functions return many values...
			function HasItem_S(p,item_amounts)
				for i,v in ipairs(item_amounts) do
					local ret = HasItem(p.role,v[1],v[2])
					if ret == C_FALSE then
						return false
					end
				end
				return true
			end
			
			function HasMoney_S(p,gold)
				local result = HasMoney(p.role,gold,gold)
				if result == C_TRUE then
					return true
				end
				return false
			end
			
			function HasLeaveBagGrid_S(p,bagslots)
				local result = HasLeaveBagGrid(p.role,bagslots)
				if result == C_TRUE then
					return true
				end
				return false
			end
			
			function TakeItem_L(p,item_amounts)
				for i,v in ipairs(item_amounts) do
					local ret = TakeItem(p.role,v[1],v[2])
					if ret == C_FALSE then
						return false
					end
				end
				return true
			end
			
			function HasNoGuild_S(p)
				local result = NoGuild(p.role)
				if result == C_TRUE then
					return true
				end
				return false
			end
	if npc.name == 'Forbei' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			n().quests = {quests[2]}
			p(1).quests = {true} --max 55 missions per npc.
			p(1).text = 'Want to know what\'s 1+1? It\'s equals 2! Then 1+2... Wait, don\'t go!'
		end
		if state == NPC_UPDATE then
			p(1).options = {
				--myprint(tsv_read(GetResPath('iteminfo.txt')))
				{"Repair",					OpenRepair,					{player.role,npc.char.role}}
			}
		end
	elseif npc.name == 'Blacksmith - Goldie' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			n().quests = {quests[1], quests[2], quests[3]}
			p(1).quests = {true, true, true} --max 55 missions per npc.
			p(1).text = 'I forge stuff'
		end
		print('state', state)
		--myprint(npc) --can't myprint in NPC_UPDATE state, remember that - because of some handle I guess.
		
		function reincarnate(p)
			ps = p.stats
			if ps[ATTR_LV] == 120 then
				ClearFightSkill(p.role)
				ps[ATTR_LV] = 1
				ps[ATTR_CEXP] = 0
				ps[ATTR_NLEXP] = 1
				ps[ATTR_CLEXP] = 1
				ps[ATTR_SAILLV] = ps[ATTR_SAILLV] + 1
				ps[ATTR_TP] = 0
				ps[ATTR_AP] = 4 + 5 * ps[ATTR_SAILLV]
				ps[ATTR_BSTR]=5
				ps[ATTR_BACC]=5
				ps[ATTR_BAGI]=5
				ps[ATTR_BCON]=5
				ps[ATTR_BSPR]=5
				ps[ATTR_BLUK]=5
				AttrRecheck(p.role)
				RefreshCha(p.role) --Refreshes client.
				
				function l_rs(skill_ids) -- learn_reincarnationskill
				myprint(skill_ids)
					for i,skill_id in ipairs(skill_ids) do
						print(i, skill_id)
						if (((ps[ATTR_SAILLV]-1) <= 122) and (AddChaSkill(p.role,tsv.skillinfo[skill_id]['id'],ps[ATTR_SAILLV]-1,1,1) ~= 0)) then
							print('learning successful')
						end
					end
				end

				l_rs({
					"Newbie Mighty Strike",
					"Ethereal Strike",
					"Beast Legion Smash",
					"Devil's Curse",
					"Holy Judgment",
					"Super Consciousness",
					"Red Thunder Cannon"})
			end
		end
        
		if state == NPC_UPDATE then
			p(1).options = {
				--myprint(tsv_read(GetResPath('iteminfo.txt')))
				{"Repair",					OpenRepair,					{player.role,npc.char.role}},

				{"Forge",					OpenForge,					{player.role,npc.char.role}},
				{"Extract Gem",				OpenGetStone,				{player.role,npc.char.role}},
				{"Make a Socket",			OpenMilling,				{player.role,npc.char.role}},

				{"Apparel Fusion",			OpenFusion,					{player.role,npc.char.role}},
				{"Apparel Upgrade",			OpenUpgrade, 				{player.role,npc.char.role}},

				{"Special Set Upgrade",		OpenItemTiChun,				{player.role,npc.char.role}}
				--{"Reincarnate",				reincarnate,				{player}}
			}
        end
		
        print('mytest2')
--[[			p(1).options = {
				{"Bank",					OpenBank,					{player.role,npc.char.role}},
				{"Repair",					OpenRepair,					{player.role,npc.char.role}},
				{"Forge",					OpenForge,					{player.role,npc.char.role}},
				{"Fusion",					OpenMilling,				{player.role,npc.char.role}},
				{"Apparel Fusion",			OpenFusion,					{player.role,npc.char.role}},
				{"Apparel Upgrade",			OpenUpgrade, 				{player.role,npc.char.role}},
				{"Combine",					OpenUnite,   				{player.role,npc.char.role}},
				{"->",						jtp, 						{2}}
			}
			
			p(2).options = {
				{"Special Set Upgrade",		OpenItemTiChun,				{player.role,npc.char.role}},
				{"Coral Recharge",			OpenItemEnergy,				{player.role,npc.char.role}},
				{"Extract Gem",				OpenGetStone,				{player.role,npc.char.role}},
				{"Repair Lifeskill Book",	OpenItemFix,				{player.role,npc.char.role}},
				{"Fairies Marriage",		OpenEidolonMetempsychosis,	{player.role,npc.char.role}},
				{"View Top 3 Guilds",		ListChallenge,				{player.role,npc.char.role}},
				{"Reincarnate",				reincarnate,				{player}},
				{"<-",						jtp,						{1}}
			}
--]]
	elseif npc.name == "Clothing Department" then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
			t().sections = {{
					{2835},{2817},{2818},{2819},
					{5056},{5041},{5045},{5049},
					{5069},{5057},{5061},{5065},
					{5204},{5195},{5196},{5197}, --Royal set is bugged (dunno why)
					{0000},{5264},{5265},{5266},
					{5287},{5288},{5289},{5290},
					{0000},{5303},{5304},{5305},
					{5315},{5316},{5317},{5318},
					{5352},{5341},{5342},{5343},
					{5356},{5357},{5358},{5359},
					{5372},{5373},{5374},{5375},
					{5404},{5405},{0000},{5406},
					{5456},{5457},{5458},{5459},
					{5472},{0000},{0000},{0000},
					{5476},{0000},{0000},{0000},
					{5480},{0000},{0000},{0000},
					{5484},{0000},{0000},{0000},
					{5497},{5488},{5489},{5490},
					{5510},{5501},{5502},{5503},
					{5521},{5522},{5523},{5524},
					{5537},{5538},{5539},{5540},
					{5553},{5554},{5555},{5556},
					{5577},{5578},{5579},{5580},
					{5581},{5582},{5583},{5584},
					{5651},{5652},{5653},{5654},
					{5661},{5662},{5663},{5664},
					{5677},{5678},{5679},{5680},
					{5726},{5727},{5728},{5729},
					{5742},{5743},{5744},{5745},
					{5945},{5946},{5947},{5948}
				},{
					{5965},{5966},{5967},{5968},
					{6153},{6144},{6145},{6146},
					{6104},{6105},{6106},{6107},
				},{
					{0453},{0454},{0000},{0000},
					{0455},{0456},{0000},{0000},
					{0890},{0891},{0000},{0000},
					{1020},{0000},{0000},{0000},
					{0860},{0861},{0862},{0863},
					{0885},{0000},{0000},{0000}
				},{}
			}
		end
		if state == NPC_UPDATE then
			p(1).options = {
				{"Trade",					t().open,  					{t(),TRADE_SALE}},
				{"Repair",					OpenRepair,					{player.role,npc.char.role}},

				{"Forge",					OpenForge,					{player.role,npc.char.role}},
				{"Extract Gem",				OpenGetStone,				{player.role,npc.char.role}},
				{"Make a Socket",			OpenMilling,				{player.role,npc.char.role}},

				{"Apparel Fusion",			OpenFusion,					{player.role,npc.char.role}},
				{"Apparel Upgrade",			OpenUpgrade, 				{player.role,npc.char.role}}
			}
		end
	elseif npc.name == 'WuGang' then --Kidswear
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			npcs[scriptid].trade.sections = {{
					{2196},{0359},{0535},{0711},
					{2197},{0360},{0536},{0712},
					{2198},{0361},{0537},{0713},
					{2210},{0388},{0564},{0740},
					{2213},{0391},{0567},{0743},
					{2215},{0393},{0569},{0745},
					{2204},{0382},{0558},{0734},
					{2220},{0407},{0597},{0757},
					{2221},{0408},{0598},{0758},
					{0000},{0791},{0812},{0814},

					{2199},{0362},{0538},{0714},
					{2207},{0385},{0561},{0737},
					{2200},{0363},{0539},{0715},
					{2201},{0364},{0540},{0716},
					{2218},{0403},{0593},{0753},
					{2219},{0404},{0594},{0754},
					{0000},{0797},{0811},{0813},

					{2188},{0351},{0527},{0703},
					{2189},{0352},{0528},{0704},
					{2187},{0350},{0526},{0702},
					{2190},{0353},{0529},{0705},
					{2192},{0355},{0531},{0707},
					{2194},{0357},{0533},{0709},
					{2195},{0358},{0534},{0710},
					{2222},{0412},{0601},{0761},
					{2223},{0413},{0602},{0824},
					{0000},{0805},{0815},{0877},
					
					{5037},{5016},{5024},{5032},
					{5038},{5020},{5028},{5036},
					{5130},{5131},{5132},{5133}
				},{
					{5134},{5135},{5136},{5137},
					{5138},{5139},{5140},{5141},
					{5053},{5054},{5055},{5348},
					{5178},{5175},{5176},{5177},
					{5182},{5179},{5180},{5181},
					{5186},{5183},{5184},{5185},
					{5190},{5187},{5188},{5189},
					{5194},{5191},{5192},{5193},
					{5252},{5253},{5254},{5255},
					{5256},{5257},{5258},{5259},
					{5260},{5261},{5262},{5263},
					{5400},{5401},{5402},{5403},
					{5428},{5429},{5430},{5431},
					{0000},{0000},{5439},{0000},
					{0000},{0000},{5443},{0000},
					{0000},{0000},{5447},{0000},
					{5858},{5859},{5860},{5861},
					{5862},{5863},{5864},{5865}
				},{
				},{}
			}
		end
	elseif npc.name == 'Newbie Guide - Senna' then --Ladieswear
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					--Normal armor
					{0000},{0365},{0541},{0717},
					{0000},{0366},{0542},{0718},
					{0000},{0368},{0544},{0720},
					{0000},{0370},{0546},{0722},
					{0000},{0371},{0547},{0723},
					{0000},{0379},{0555},{0731},
					{0000},{0394},{0570},{0746},
					{0000},{0405},{0595},{0755},
					{0000},{0406},{0596},{0756},
					--Unseal armor
					{0000},{0791},{0812},{0814},

					{0000},{0367},{0553},{0719},
					{0000},{0369},{0545},{0721},
					{0000},{0376},{0552},{0728},
					{0000},{0377},{0553},{0729},
					{0000},{0401},{0591},{0751},
					{0000},{0402},{0592},{0752},
					{0000},{0797},{0811},{0813},
					
					{0000},{5015},{5023},{5031},
					{0000},{5019},{5027},{5035},
					{5142},{5143},{5144},{5145},
					{5146},{5147},{5148},{5149},
					{5150},{5151},{5152},{5153},
					{0000},{5172},{5173},{5174},
					{5344},{5520},{0000},{0000},
					{5219},{0000},{0000},{0000},
					{5244},{5245},{5246},{5247},
					{5248},{5249},{5250},{5251},
					{5396},{5397},{5398},{5399},
					{0000},{5416},{5417},{5418},
					{0000},{5426},{5438},{5427},
					{5970},{5971},{5972},{5973},
					{5986},{5987},{5988},{5989},
					{6293},{6276},{6275},{6277}
				},{
					{0000},{0000},{5442},{0000},
					{0000},{0000},{5446},{0000}
				},{
				},{}
			}
		end
	elseif npc.name == 'Magician Chiatan' then --Menswear
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					--Normal armor (copied section - swordsman)
					{0000},{0290},{0466},{0642},
					{0000},{0291},{0467},{0643},
					{0000},{0293},{0469},{0645},
					{0000},{0295},{0471},{0647},
					{0000},{0299},{0475},{0651},
					{0000},{0395},{0587},{0747},
					{0000},{0396},{0588},{0748},
					{0000},{0768},{0807},{0808},

					{0000},{0305},{0481},{0657},
					{0000},{0306},{0482},{0658},
					{0000},{0307},{0483},{0659},
					{0000},{0310},{0486},{0662},
					{0000},{0312},{0488},{0664},
					{0000},{0316},{0492},{0668},
					{0000},{0317},{0493},{0669},
					{0000},{0399},{0589},{0749},
					{0000},{0400},{0590},{0750},
					{0000},{6144},{6145},{6146},
					{0000},{0779},{0809},{0810},

					{0000},{0335},{0511},{0687},
					{0000},{0337},{0513},{0689},
					{0000},{0339},{0515},{0691},
					{0000},{0341},{0517},{0693},
					{0000},{0345},{0521},{0697},
					{0000},{0343},{0519},{0695},
					{0000},{0344},{0520},{0696},
					{0000},{0409},{0599},{0759},
					{0000},{0411},{0600},{0760},
					{0000},{0805},{0815},{0877},
					
					{0000},{5013},{5021},{5029}
				},{
					{0000},{5017},{5025},{5033},
					{5118},{5119},{5120},{5121},
					{5122},{0000},{0000},{0000},
					{0000},{5123},{5124},{5125},
					{5126},{0000},{0000},{0000},
					{0000},{5127},{5128},{5129},
					{0000},{5154},{5155},{5156},
					{0000},{5157},{5158},{5159},
					{0000},{5160},{5161},{5162},
					{5220},{0000},{0000},{0000},
					{0000},{5221},{5222},{5223},
					{0000},{5224},{5225},{5226},
					{0000},{5227},{5228},{5229},
					{5388},{5389},{5390},{5391},
					{0000},{5422},{5432},{5423},
					{0000},{0000},{5436},{0000},
					{0000},{0000},{5440},{0000},
					{0000},{0000},{5444},{0000},
					{5569},{5570},{5571},{5572},
					{0000},{5992},{5993},{5994},
					{5974},{5975},{5976},{5977},
					{0000},{6263},{6264},{6265},
					{6293},{6281},{6282},{6283}
				},{
				},{}
			}
		end
	elseif npc.name == "Mysterious Augur" then --Big'N'Tall
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					{0000},{0228},{0000},{0000},
					{0000},{0230},{0000},{0000},
					{0000},{5039},{0000},{0000},
					{0000},{5040},{0000},{0000},
					{0000},{0397},{0603},{0829},
					{0000},{0398},{0604},{0830},
					{0000},{5014},{5022},{5030},
					{0000},{5018},{5026},{5034},
					{5106},{0000},{0000},{0000},
					{0000},{5107},{5108},{5109},
					{5110},{5111},{5112},{5113},
					{5114},{0000},{0000},{0000},
					{0000},{5115},{5116},{5117},
					{0000},{5163},{5164},{5165},
					{5218},{0000},{0000},{0000},
					{5230},{5231},{5232},{5233},
					{5234},{5235},{5236},{5237},
					{0000},{5238},{5239},{5240},
					{5392},{5393},{5394},{5395},
					{0000},{5514},{5515},{5516}
				},{
				},{
				},{}
			}
		end
	elseif npc.name == "Apparels - Sharp Things" then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					{0013},{0014},{0015},{0020},
					{3802},{3803},{0016},{0021},
					{1387},{3804},{0017},{0018},
					{0115},{0116},{0019},{6133},
					{6141},{2333},{4275},{6101},
					{5003},{5009},{5071},{5209},
					{5214},{5277},{5450}
				},{
					{0001},{0010},{1395},{0002},
					{0011},{3798},{0003},{1397},
					{3799},{1398},{3800},{1399},
					{3801},{0006},{0007},{1394},
					{0114},{0113},{6128},{6136},
					{0775},{2331},{4274},{6096},
					{5001},{5007},{5002},{5008},
					{5208},{5213},{5276},{5278},
					{5283},{5284},{5285},{5449},
					{5990},{5991}
				},{
					{0121},{0122},{0123},{0124},
					{0125},{0126},{0127},{6129},
					{6137},{4282},{6092}
				},{}
			}
		end
	elseif npc.name == "Apparels - Blunt Things" then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					{0025},{0032},{1403},{0026},
					{0033},{1404},{0027},{0034},
					{1405},{0028},{0029},{0030},
					{0117},{0118},{6130},{2339},
					{4278},{5448},{6098}
				},{
					{0037},{0038},{0039},{1413},
					{3807},{0040},{1414},{3808},
					{0041},{0119},{1411},{0120},
					{0043},{6139},{0783},{2337},
					{4277},{6099},{5005},{5011},
					{5072},{5210},{5215},{5279},
					{5281},{5286}
				},{
				},{}
			}
		end
	elseif npc.name == "Apparels - Floaty Things" then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					{0097},{0104},{1434},{1469},
					{0098},{0105},{1435},{1470},
					{0099},{0106},{1436},{1471},
					{0100},{4301},{1440},{3809},
					{0103},{4302},{1476},{3811},
					{4303},{1477},{3812},{4198},
					{0109},{0110},{1472},{3813},
					{3810},{3814},{0108},{1438},
					{3815},{1439},{1474},{0101},
					{0102},{4204},{0111},{0112},
					{6134},{6142},{0794},{0787},
					{2341},{2343},{4279},{4280},
					{6102},{5006},{5012},{5073},
					{5211},{5216},{5280},{5451},
					{5452}
				},{
					{0904},{0905},{0906},{0141},
					{0935},{0936},{0142},{0937}
				},{
				},{}
			}
		end
	elseif npc.name == "Apparels - Vertical Enhancers" then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
				},{
				},{
				},{}
			}
		end
	elseif npc.name == "Accessories Department" then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					{0073},{0080},{1422},{1450},
					{0074},{0081},{1423},{1451},
					{0075},{0082},{1424},{3816},
					{0076},{1425},{3817},{3818},
					{0077},{0084},{1426},{3819},
					{0078},{0079},{1421},{0150},
					{0151},{6132},{6140},{0801},
					{2335},{4276},{6100},{0498},
					{5004},{5010},{5074},{5212},
					{5217},{5282},{5453},{5454},
					{5455}
				},{
				},{
				},{}
			}
		end
	elseif npc.name == 'Banker - Monica' then
		if state == NPC_INIT then
			n().proc = npc_custominit_proc
			n().on_init = function(npc, scriptid, player, cmd, p1, p2)
				OpenBank(player.role,npc.char.role)
			end
		end
	elseif npc.name == "Blacksmith's Apprentice" then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
				},{
				},{
					{0453},{0454},{0000},{0000},
					{0455},{0456},{0000},{0000},
					{0890},{0891},{0000},{0000},
					{1020},{0000},{0000},{0000},
					{0860},{0861},{0862},{0863},
					{0885},{0000},{0000},{0000}
				},{}
			}
		end
	elseif npc.name == 'Harbor Operator - Shirley' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).options = {
				{"Hairstyle",				OpenHair,					{player.role,npc.char.role}},
				{"(Cur Boat) Repair",		RepairBoat,					{player.role}}, --ship role
				{"(Cur Boat) Supply",		SupplyBoat,					{player.role}}, --ship role
				{"(Boat) Make Guppy",		CreateBoat,					{player.role,1,1}}, --1 = guppy, 3=dock number
				{"(Boat) Make ?",			CreateBoat,					{player.role,2,1}}, --1 = guppy, 3=dock number
				{"->",						jtp,						{2}}
			}
		--TriggerAction( 1, LuanchBerthList,  )
			p(2).options = {
				{"(Boat) Trade",			BoatBerthList,				{player.role,npc.char.id,0,1,0,0,0}},
				{"(Boat) Launch",			BoatBerthList,				{player.role,npc.char.id,1,1,2260,2829,177}},
				{"(Boat) Repair",			BoatBerthList,				{player.role,npc.char.id,3,1,0,0,0}},
				{"(Boat) Salvage",			BoatBerthList,				{player.role,npc.char.id,4,1,0,0,0}},
				{"(Boat) Supply",			BoatBerthList,				{player.role,npc.char.id,5,1,0,0,0}},
				{"(Boat) Upgrade",			BoatBerthList,				{player.role,npc.char.id,6,1,0,0,0}},
				{"<-",						jtp,						{1}}
			}
		end
	elseif npc.name == 'Physican - Ditto' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "test"
			
			function resetme (role)
				local player_skills = {}
				--[[
				for i,v in ipairs(range(1,500)) do --max number of skills
					--player_skills[v] = GetSkillLv(role, v)
					local skill_level = GetSkillLv(role, v)
					if skill_level > 0 then
						skill(PLAYER_SKILL, v, SK_EVENT_UNUSE, GetSkillLv(role, v), role)
					end
				end
				--]]
				ClearFightSkill(role)
				RefreshCha ( role )
				--BUG: Need to call all passive skills of the player with "unuse" event. Attack getting negative for some reason.
			end
			
			function relearn_main_skills(role)
				local skill_list = { --TODO: Add skill requirements to advanced skill guide in top
				--TODO: Include Rebirth skills
				--TODO: Also include passive weapon skills
					"Taunt",
					"Roar", --SK_TAUNT lvl 10
					"Concentration",
					"Sword Mastery", --SK_CONCENTRATION lvl 2
					"Will of Steel", --SK_CONCENTRATION lvl 2
					"Illusion Slash", --SK_SWORDMASTERY lvl 3
					"Break Armor", --SK_SWORDMASTERY lvl 3
					"Berserk", --SK_ILLUSIONSLASH lvl 4
					"Tiger Roar", --SK_BREAKARMOR lvl 4
					"Dual Sword Mastery", --SK_BERSERK lvl 5
					"Deftness", --SK_DUALSWORDMASTERY lvl 2
					"Blood Frenzy", --SK_DUALSWORDMASTERY lvl 2
					"Poison Dart", --SK_BLOODFRENZY lvl 2
					"Shadow Slash", --SK_BLOODFRENZY lvl 5
					"Stealth", --SK_DEFTNESS lvl 3
					"Windrider's Grace", --SK_SHADOWSLASH lvl 5
					"Greatsword Mastery", --SK_TIGERROAR lvl 5
					"Strengthen", --SK_GREATSWORDMASTERY lvl 2
					"Bloodbull", --SK_GREATSWORDMASTERY lvl 3
					"Primal Rage", --SK_BLOODBULL lvl 5
					"Warrior's Rage", --SK_BLOODBULL lvl 5, lvl 125
					"Mighty Strike", --SK_STRENGTHEN lvl 3
					"Howl", --SK_MIGHTYSTRIKE lvl 2
					
					"Range Mastery",
					"Eagle's Eye",
					"Windwalk", --SK_RANGEMASTERY lvl 2
					"Dual Shot", --SK_RANGEMASTERY lvl 3
					"Firegun Mastery", --SK_RANGEMASTERY lvl 10
					"Rousing", --SK_WINDWALK lvl 4
					"Frozen Arrow", --SK_DUALSHOT lvl 2
					"Venom Arrow", --SK_DUALSHOT lvl 4
					"Blunting Bolt", --SK_DUALSHOT lvl 4, lvl 125
					"Flaming Dart", --SK_DUALSHOT lvl 4, lvl 125
					"Meteor Shower", --SK_FROZENARROW lvl 5
					"Cripple", --SK_FROZENARROW lvl 5
					"Magma Bullet", --SK_FIREGUNMASTERY lvl 5
					"Enfeeble", --SK_CRIPPLE lvl 5
					"Headshot", --SK_ENFEEBLE lvl 5
					"Eye of Precision", --SK_HEADSHOT lvl 4, lvl 125
					"Deadeye Blood", --SK_HEADSHOT lvl 4, lvl 125
					
					"Heal",
					"Spiritual Bolt",
					"Vigor",
					"Crystalline Blessing",
					"Intense Magic",
					"Harden", --SK_HEAL lvl 3
					"Recover", --SK_HARDEN lvl 3
					"Revival", --SK_RECOVER lvl 4
					"Spiritual Fire", --SK_SPIRITUALBOLT lvl 2
					"Tempest Boost", --SK_SPIRITUALFIRE lvl 4
					"Divine Grace", --SK_VIGOR lvl 8
					"True Sight", --SK_DIVINEGRACE lvl 2
					"Tornado Swirl", --SK_TRUESIGHT lvl 2
					"Cursed Fire", --SK_TRUESIGHT lvl 3
					"Angelic Shield", --SK_TORNADOSWIRL lvl 2
					"Energy Shield", --SK_TORNADOSWIRL lvl 3
					"Healing Spring", --SK_ANGELICSHIELD lvl 2
					"Soulkeeper", --SK_ENERGYSHIELD lvl 10
					"Abyss Mire", --SK_CURSEDFIRE lvl 4
					"Shadow Insignia", --SK_CURSEDFIRE lvl 4
					"Bane of Waning", --SK_CURSEDFIRE lvl 5, lvl 125
					"Curse of Weakening", --SK_CURSEDFIRE lvl 10, lvl 125
					"Seal of Elder", --SK_SHADOWINSIGNIA lvl 3
					"Excrucio", --SK_SHADOWINSIGNIA lvl 10, lvl 125
					"Petrifying Pummel", --SK_ABYSSMIRE lvl 8, lvl 125
					
					"Diligence",
					"Lightning Bolt", --SK_DILIGENCE lvl 1
					"Current", --SK_DILIGENCE lvl 2
					"Lightning Curtain", --SK_LIGHTNINGBOLT lvl 8
					"Conch Armor", --SK_CURRENT lvl 1
					"Tornado", --SK_CURRENT lvl 2
					"Conch Ray", --SK_CONCHARMOR lvl 5
					"Omnis Immunity", --SK_CONCHRAY lvl 10, lvl 125
					"Tail Wind", --SK_TORNADO lvl 4
					"Algae Entanglement", --SK_TORNADO lvl 5
					"Fog", -- SK_TAILWIND lvl 2
					"Whirlpool", --SK_TAILWIND lvl 4
				}
				
				for i,skill_id in ipairs(skill_list) do
					AddChaSkill(role,tsv.skillinfo[skill_id]['id'],10,1,0) --TODO: Remove class restriction from skillinfo.
				end
			end
			
			function relearn_weapon_skills(role)
				--passive weapon skills

				local skill_list = {
					"Bare Hand",
					"Sword",
					"Greatsword",
					"Bow",
					"Firegun",
					"Blade",
					"Boxing Gloves",
					"Dagger",
					"Short Staff",
					"Hammer",
					"Dual Weapon",
					"Woodcutting",
					"Mining"
				}
				
				for i,skill_id in ipairs(skill_list) do
					AddChaSkill(role,tsv.skillinfo[skill_id]['id'],1,1,0)
				end
			end
			
			function relearn_new_main_skills(role)
				local skill_list = {
					"The Warrior's Rage",
					"The Windrider's Grace",
					"Blunting Bolt",
					"Flaming Dart",
					"Eye of Precision",
					"Deadeye Blood",
					"Excrucio",
					"Petrifying Pummel",
					"Curse of Weakening",
					"Bane of Waning",
					"Omnis Immunity"
				}
				
				for i,skill_id in ipairs(skill_list) do
					AddChaSkill(role,tsv.skillinfo[skill_id]['id'],10,1,0) --TODO: Remove class restriction from skillinfo.
				end
				
				local skill_list = {
					"The Warrior's Rage",
					"The Windrider's Grace",
					"Soulkeeper"
				}
				
				for i,skill_id in ipairs(skill_list) do
					AddChaSkill(role,tsv.skillinfo[skill_id]['id'],1,1,0) --TODO: Remove class restriction from skillinfo.
				end
			end

			function relearn_unreleased_skills(role) --only learning those with effects right now - also most of these are hidden
				local skill_list = {
					"Eagle's Eye",
					"Angel Blessing",
					"Cure",
					"Frost Shield",
					"Traversing",
					"Inferno Blast",
					"Evasion",
					"Beast Strength",
					"Recuperate",
					"Shield Mastery",
					"Ranger",
					"Hunter Disguise",
					"Backstab",
					"Numb",
					"Hunter Strike",
					"Astro Strike",
					"Barbaric Crush",
					"Parry",
					"Dispersion Bullet",
					"Penetrating Bullet",
					"Greater Heal",
					"Greater Recover",
					"Counterguard",
					--ship skils - they work, but Bombardment replaces the auto attack of any other weapon skill.
--					"Cannon Mastery",
--					"Toughen Wood",
--					"Sail Mastery",
--					"Reinforce Ship",
--					"Oil Tank Upgrade",
--					"Bombardment"
				}
				
				for i,skill_id in ipairs(skill_list) do
					print(tsv.skillinfo[skill_id]['id'])
					AddChaSkill(role,tsv.skillinfo[skill_id]['id'],1,1,0) --TODO: Remove class restriction from skillinfo.
				end
			end
			
			function increase_luck(p)
				local ps = p.stats
				if ps[ATTR_AP] > 0 then
					ps[ATTR_BLUK] = ps[ATTR_BLUK] + 1
					ps[ATTR_AP] = ps[ATTR_AP] - 1
					jtp(1)
					AttrRecheck(p.role)
					RefreshCha(p.role) --Refreshes client.
				end
			end
			
			p(1).options = {
				{"Increase luck by 1 (1 AP)",	increase_luck, {player}},
				{"Reset skills",	resetme, {player.role}},
				{"(Re)learn main skills (maxed)",	relearn_main_skills, {player.role}},
				{"(Re)learn weapon skills",	relearn_weapon_skills, {player.role}},
				{"(Re)learn new main skills (bugged)",	relearn_new_main_skills, {player.role}},
				{"(Re)learn unreleased skills",	relearn_unreleased_skills, {player.role}}
			}
		end
	elseif npc.name == 'Superstar PK Admin' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Saint Arena Admin:\"I am proud that I did not leave anyone in the sky \""

			p(1).options = {
				{"Go to Argent City!",	GoTo, {player.role, 2231, 2788, "garner"}},
				{"Go to Superstar PK Snr!",	GoTo, {player.role, 77, 47, "hell2"}},
				{"Go to Superstar PK Jr!",		GoTo, {player.role, 74, 59, "hell4"}}
			}
		end
	elseif npc.name == 'Nurse - Gina' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "Gina: Hello! I am Nurse Gina. Look for me if you are sick or injured!"
			p(1).options = {
				{"Full Heal",	function () player.stats[ATTR_HP] = player.stats[ATTR_MAXHP] end , {}}
			}
		end
	elseif npc.name == 'Grocery - Jimberry' then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
				},{
				},{
					{3225},{3226},{3294},{3295},
					{3296},{2679},{2689},{2699},
					{2709},{1611},{1682},{1842},
					{1612},{1710},{1693},{4716},
					{1716},{1711},{3384},{3932},
					{1619},{2396},{1729},{4459},
					{1697},{1730},{1712},{1734},
					{1621},{1703},{2440},{2634},
					{2635},{2636},{2637},{2638},
					{2639}
				},{}
			}
		end
				--{"Take me somewhere I can level",	MoveCity,		{player.role,"levelme"}}
	elseif npc.name == 'Fairy Merchant Tingle' then
		if state == NPC_INIT then
			n().proc = npc_stddialog_proc
		end
		if state == NPC_UPDATE then
			p(1).text = "My Name is Tingle. I love collecting all things Fairy. If you give me fairy items, I will give you special gifts, Kaloooompa!!"
			p(1).options = {
				{"Books for Fairies",	jtp, {2}},
				{"Energy",				jtp, {3}},
				{"Metal",				jtp, {4}},
				{"Skill Books 1-5",		jtp, {5}},
				{"Skill Books 6-10",	jtp, {6}}
			}

			function mt(take_table, give_table, page)
			--Trigger: Give item to player, then go back to a page.
				if HasItem(player.role, take_table[1], take_table[2]) == C_TRUE then
					TakeItem(player.role, 0, take_table[1], take_table[2])
					GiveItem(player.role, 0, give_table[1], give_table[2], 4) --quality set to 4 (in terms of non-equips, it makes no difference).
					d():open(page,player)
				else
					d():open(17,player)
				end
			end

			p(2).text = "Did you know pets love to read? This is my collection of Favourite Fairy Books. Easy Books are worth 50 Fairy Coins, Medium ones are 50 Elven Signets."
			p(2).options = {
				{"Novice Pet Manufacturing", 	mt, {{855,	50},{1055, 1},2}},
				{"Standard Pet Manufacturing", 	mt, {{2588,	50},{1056, 1},2}},
				{"Novice Pet Crafting", 		mt, {{855,	50},{1058, 1},2}},
				{"Standard Pet Crafting", 		mt, {{2588,	50},{1059, 1},2}},
				{"Novice Pet Analyze", 			mt, {{855,	50},{1061, 1},2}},
				{"Standard Pet Analyze", 		mt, {{2588,	50},{1062, 1},2}},
				{"Novice Pet Cooking", 			mt, {{855,	50},{1064, 1},2}},
				{"Standard Pet Cooking", 		mt, {{2588,	50},{1065, 1},2}}
			}

			p(3).text = "I have long tracked the source of fairy magic. So far my discoveries have lead me to these chunks of Energy. I just don't know what they do."
			p(3).options = {
				{"Fairy Coin x30 - Compressed Energy I", 			mt, {{855,	30},{2617, 1},3}},
				{"Fairy Coin x50 - Compressed Energy II", 			mt, {{855,	50},{2619, 1},3}},
				{"Elven Signet x10 - Compressed Energy III", 		mt, {{2588,	10},{2622, 1},3}},
				{"Elven Royal Signet x1 - Compressed Energy IV", 	mt, {{2589,	1},	{2624, 1},3}}
			}
			
			p(4).text = "I have long tracked the source of fairy magic. So far my discoveries have lead me to these chunks of Rare Metals. I just don't know what they do."
			p(4).options = {
				{"Fairy Coin x30 - Solid Metal", 				mt, {{855,	30},{2641, 1},4}},
				{"Fairy Coin x50 -  Pressurised Metal", 		mt, {{855,	50},{2640, 1},4}},
				{"Elven Signet x10 -  Support Metal", 			mt, {{2588,	10},{2642, 1},4}},
				{"Elven Signet x10 -  Tenacious Metal", 		mt, {{2588,	10},{2643, 1},4}},
				{"Elven Signet x25 -  Waterproof Metal", 		mt, {{2588,	25},{2644, 1},4}},
				{"Elven Signet x25 -  Insulating Metal", 		mt, {{2588,	25},{2649, 1},4}}
			}
			
			p(5).text = "I have also written books on how to enjoy activities with your fairies. Each volume has 4 different activities."
			p(5).options = {
				{"Fairy Coin x2 - Volume 1", 			jtp, {7}},
				{"Fairy Coin x10 - Volume 2", 			jtp, {8}},
				{"Fairy Coin x50 - Volume 3", 			jtp, {9}},
				{"Fairy Coin x150 - Volume 4", 			jtp, {10}},
				{"Elven Signet x5 - Volume 5", 			jtp, {11}}
			}
			
			p(6).text = "I have also written books on how to enjoy activities with your fairies. Each volume has 4 different activities."
			p(6).options = {
				{"Elven Signet x25 - Volume 6", 		jtp, {12}},
				{"Elven Signet x50 - Volume 7", 		jtp, {13}},
				{"Elven Royal Signet x2 - Volume 8", 	jtp, {14}},
				{"Elven Royal Signet x5 - Volume 9", 	jtp, {15}},
				{"Elven Royal Signet x10 - Volume 10", 	jtp, {16}}
			}
			
			p(7).text = "I have also written books on how to enjoy activities with your fairies. I can give you volume 1 for 2 fairy coins."
			p(7).options = {
				{"Level 1 Manufacturing Guide", mt, {{855,2},{2679,1},7}},
				{"Level 1 Cooking Guide", 		mt, {{855,2},{2689,1},7}},
				{"Level 1 Crafting Guide", 		mt, {{855,2},{2699,1},7}},
				{"Level 1 Analyse Guide", 		mt, {{855,2},{2709,1},7}}
			}
			
			p(8).text = "I have also written books on how to enjoy activities with your fairies. I can give you volume 2 for 10 fairy coins."
			p(8).options = {
				{"Level 2 Manufacturing Guide", mt, {{855,10},{2680,1},8}},
				{"Level 2 Cooking Guide", 		mt, {{855,10},{2690,1},8}},
				{"Level 2 Crafting Guide", 		mt, {{855,10},{2700,1},8}},
				{"Level 2 Analyse Guide", 		mt, {{855,10},{2710,1},8}}
			}
			
			p(9).text = "I have also written books on how to enjoy activities with your fairies. I can give you volume 3 for 50 fairy coins."
			p(9).options = {
				{"Level 3 Manufacturing Guide", mt, {{855,50},{2681,1},9}},
				{"Level 3 Cooking Guide", 		mt, {{855,50},{2691,1},9}},
				{"Level 3 Crafting Guide", 		mt, {{855,50},{2701,1},9}},
				{"Level 3 Analyse Guide", 		mt, {{855,50},{2711,1},9}}
			}
			
			p(10).text = "I have also written books on how to enjoy activities with your fairies. I can give you volume 4 for 150 fairy coins."
			p(10).options = {
				{"Level 4 Manufacturing Guide", mt, {{855,150},{2682,1},10}},
				{"Level 4 Cooking Guide", 		mt, {{855,150},{2692,1},10}},
				{"Level 4 Crafting Guide", 		mt, {{855,150},{2702,1},10}},
				{"Level 4 Analyse Guide", 		mt, {{855,150},{2712,1},10}}
			}
			
			p(11).text = "Oooh, I knew you would like those books. Special release - Volume 5+ now available for Elven signets. Lvl 5 is 5."
			p(11).options = {
				{"Level 5 Manufacturing Guide", mt, {{2588,5},{2683,1},11}},
				{"Level 5 Cooking Guide", 		mt, {{2588,5},{2693,1},11}},
				{"Level 5 Crafting Guide", 		mt, {{2588,5},{2703,1},11}},
				{"Level 5 Analyse Guide", 		mt, {{2588,5},{2713,1},11}}
			}
	
			p(12).text = "Oooh, I knew you would like those books. Special release - Volume 6 is now 25 elven signets."
			p(12).options = {
				{"Level 6 Manufacturing Guide", mt, {{2588,25},{2684,1},12}},
				{"Level 6 Cooking Guide", 		mt, {{2588,25},{2694,1},12}},
				{"Level 6 Crafting Guide", 		mt, {{2588,25},{2704,1},12}},
				{"Level 6 Analyse Guide", 		mt, {{2588,25},{2714,1},12}}
			}
	
			p(13).text = "Oooh, I knew you would like those books. Special release - Volume 7 is now 50 elven signets."
			p(13).options = {
				{"Level 7 Manufacturing Guide", mt, {{2588,50},{2685,1},13}},
				{"Level 7 Cooking Guide", 		mt, {{2588,50},{2695,1},13}},
				{"Level 7 Crafting Guide", 		mt, {{2588,50},{2705,1},13}},
				{"Level 7 Analyse Guide", 		mt, {{2588,50},{2715,1},13}}
			}
	
			p(14).text = "These are my latest and most special books. Special release - Volume 8+ now avaiable for Elven Royal signets. Lvl 8 is 2."
			p(14).options = {
				{"Level 8 Manufacturing Guide", mt, {{2589,2},{2686,1},14}},
				{"Level 8 Cooking Guide", 		mt, {{2589,2},{2696,1},14}},
				{"Level 8 Crafting Guide", 		mt, {{2589,2},{2706,1},14}},
				{"Level 8 Analyse Guide", 		mt, {{2589,2},{2716,1},14}}
			}
			
			p(15).text = "These are my latest and most special books. Special release - Volume 9 is now 5 elven royal signets."
			p(15).options = {
				{"Level 9 Manufacturing Guide", mt, {{2589,5},{2687,1},15}},
				{"Level 9 Cooking Guide", 		mt, {{2589,5},{2697,1},15}},
				{"Level 9 Crafting Guide", 		mt, {{2589,5},{2707,1},15}},
				{"Level 9 Analyse Guide", 		mt, {{2589,5},{2717,1},15}}
			}
			
			p(16).text = "These are my latest and most special books. Final special release - Volume 10 is now 10 elven royal signets."
			p(16).options = {
				{"Level 10 Manufacturing Guide", 	mt, {{2589,10},{2688,1},16}},
				{"Level 10 Cooking Guide", 			mt, {{2589,10},{2698,1},16}},
				{"Level 10 Crafting Guide", 		mt, {{2589,10},{2708,1},16}},
				{"Level 10 Analyse Guide", 			mt, {{2589,10},{2718,1},16}}
			}
			
			p(17).text = "You don't have enough Fairy Items? aww Tingle is sad."
		end
	elseif npc.name == 'Animal Nutritionist' then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					{0183},{0184},{0185},{0186},
					{0187},{0188},{0189},{0190},
					{0191},{0199},{0262},{1015},
					{0680}
				},{
					{0243},{0244},{0246},{0247},
					{0249},{0250},{0252},{0253},
					{0259},{0260},{1055},{1056},
					{1058},{1059},{1061},{1062},
					{1064},{1065}
				},{
					{0222},{0276},{5721},{5722},
					{0223},{0277},{5723},{5724},
					{0224},{0278},{5725},{5726},
					{0225},{0279},{5727},{5728},
					{0226},{0280},{5729},{5730},
					{0227},{5718},{5719},{5720},
					{0000},{0578}
				},{} }
		end
	elseif npc.name == 'Tailor - Bebe' then
		if state == NPC_INIT then
			n().proc = npc_tradeonly_proc
			t().sections = {{
					{0453},{0454},{3883},{3074},
					{0886},{0000},{3884},{3075},
					{0455},{0456},{0000},{0000},
					{0890},{0891},{0000},{0000},
					{1020},{0000},{0000},{0000}
				},{
					--Skill books
					{3297},{3164},{3160},{3161},
					{3165},{3238},{3170},{3293},
					{3298},{3168},{3162},{3166},
					{3167},{3172},{3173},{8061},
					--Skill books
					{3174},{3163},{3176},{3180},
					{3179},{3177},{8062},

					--Skill books
					{3187},{3189},{3188},{3190},
					{3239},{3197},{3193},{3241},
					{3192},{3198},{3202},{3203},
					{3204},{8710},{8711},{8712},
					{8729},

					--Skill books
					{3206},{3208},{3205},{3242},
					{3210},{3207},{3211},{3215},
					{3212},{3209},{3216},{3223},
					{3217},{3224},{3301},{8064},
					--Skill books
					{3220},{3222},{3219},{3218},
					{3300},{8475},{8476},{8477},
					{8478},

					--Skill books
					{3227},{3231},{3228},{3229},
					{3230},{3232},{3233},{3234},
					{3235},{3236},{3237},{8065}
				},{
					--Ammunition
					{0135},{2434},{0149},{2448},
					--Ammunition
					{1022},{1024},{0000},{0000},
				},{} }
		end
	end
--]]
end