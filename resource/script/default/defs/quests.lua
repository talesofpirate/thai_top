function quest_init(id, player, char)
	--Initialization action has player and char == nil. Please check if player is nil before using it.
	--Cancel action has char == nil. Please check if char is nil before using it.
	local q = quests[id] -- no "random" quests yet
	if id == 1 then
		q[1].name = 'Normal quest test'
		q[1].type = QUEST_TYPE_NORMAL
		q[1].actions[QUEST_STATE_START].text = 'This quest only appears after you get a Ticket to Argent. Testing if quest is initialized/cancelled/ended/viewed properly. Also testing for conditonals and actions. 2 short swords are being given out once you accept this. They are going to be automatically removed if mission is cancelled. The reward gives 1 ticket to argent. Also testing other indicators.'
		q[1].actions[QUEST_STATE_END].text = 'This is the end of the quest. You are going to be rewarded for patiently testing this.'
		q[1].actions[QUEST_STATE_ONGOING].text = 'This text appears on the on going log. To be able to finish or cancel the quest, you need the 2 short swords that were given to you. If you want more details, abandon the quest and start it again.'
		q[1].actions[QUEST_STATE_LOG].text = 'This text appears on quest log. You cannot abandon this if you do not have the 2 short swords that were given to you. If you want more details, abandon the quest and start it again.'

		q[1].indicators.needs[1] = {type=MIS_NEED_ITEM,1,2 }
		q[1].indicators.prizes[1] = {type=MIS_PRIZE_MONEY, 0, 1 }
		q[1].indicators.prizes[2] = {type=MIS_PRIZE_ITEM, 4602, 1}
		q[1].indicators.prizes[3] = {type=MIS_PRIZE_FAME, 0, 1}
		q[1].indicators.prizes[4] = {type=MIS_PRIZE_CESS, 0, 1}

		if ((player ~= nil) and (char ~= nil)) then
			q[1].actions[QUEST_STATE_START].conditions[1]={HasItem,{player.role, 4602, 1} } --GetMissionIndex gives error if selected quest doesn't exist (if thes conditions are not met) - made error msg blank on en_US.res
			q[1].actions[QUEST_STATE_START].actions[1]={GiveItem,{player.role, char.role, 1, 2, 1} }
			q[1].actions[QUEST_STATE_END].conditions[1]={HasItem,{player.role, 1, 2} }
			q[1].actions[QUEST_STATE_END].actions[1]={TakeItem,{player.role, char.id, 1, 2}}
			q[1].actions[QUEST_STATE_END].actions[2]={GiveItem,{player.role, char.role, 4602, 1, 1} }
			q[1].actions[QUEST_STATE_LOG].conditions[1]={HasItem,{player.role, 1, 2} }
			q[1].actions[QUEST_STATE_LOG].actions[1]={TakeItem,{player.role, char.id, 1, 2}}
		end
	end
	if id == 2 then
		q[1].name = 'Multiple char normal quest test'
		q[1].type = QUEST_TYPE_NORMAL
		q[1].actions[QUEST_STATE_START].text = 'starttext'
		q[1].actions[QUEST_STATE_END].text = 'endtext'
		q[1].actions[QUEST_STATE_ONGOING].text = 'pendingtext'
		q[1].actions[QUEST_STATE_LOG].text = 'logtext'

		if char ~= nil then
			if char.name == 'Blacksmith - Goldie' then
				q[1].show = ACCEPT_SHOW
			elseif char.name == 'Forbei' then
				q[1].show = COMPLETE_SHOW
				q[1].actions[QUEST_STATE_ONGOING].text = 'pendingtext2'
			end
		end

		q[1].indicators.needs[1] = {type=MIS_NEED_ITEM,1,10 }
		q[1].indicators.prizes[1] = {type=MIS_PRIZE_MONEY, 100, 1 }
		q[1].indicators.prizes[2] = {type=MIS_PRIZE_ITEM, 1, 10}
		q[1].indicators.prizes[3] = {type=MIS_PRIZE_FAME, 10, 1}
		q[1].indicators.prizes[4] = {type=MIS_PRIZE_CESS, 10, 1}

		if ((player ~= nil) and (char ~= nil)) then
			--q.actions[QUEST_STATE_START].actions[1]={GiveItem,{player.role, char.role, 1, 1, 4} }
			q[1].actions[QUEST_STATE_END].conditions[1]={HasItem,{player.role, 1, 10} }
			--q[1].actions[QUEST_STATE_END].actions[1]={TakeItem,{player.role, char.id, 1, 10}}
		end
	end
	if id == 3 then
		q[1].name = 'Global quest'
		q[1].type = QUEST_TYPE_GLOBAL
		q[1].actions[QUEST_STATE_START].text = 'starttext'
		q[1].actions[QUEST_STATE_END].text = 'endtext'
		q[1].actions[QUEST_STATE_ONGOING].text = 'pendingtext'
		q[1].actions[QUEST_STATE_LOG].text = 'logtext'

		q[1].indicators.needs[1] = {type=MIS_NEED_ITEM,1,10 }
		q[1].indicators.prizes[1] = {type=MIS_PRIZE_MONEY, 100, 1 }
		q[1].indicators.prizes[2] = {type=MIS_PRIZE_ITEM, 1, 10}
		q[1].indicators.prizes[3] = {type=MIS_PRIZE_FAME, 10, 1}
		q[1].indicators.prizes[4] = {type=MIS_PRIZE_CESS, 10, 1}

		if ((player ~= nil) and (char ~= nil)) then
			--q.actions[QUEST_STATE_START].actions[1]={GiveItem,{player.role, char.role, 1, 1, 4} }
			q[1].actions[QUEST_STATE_END].conditions[1]={HasItem,{player.role, 1, 10} }
			--q[1].actions[QUEST_STATE_END].actions[1]={TakeItem,{player.role, char.id, 1, 10}}
		end
	end
end