--TOFIX: Skills that can affect self should use the func_end, not func_start. It's probably due to skillinfo.txt syntax (somewhat different).

--BeatBack(s.p.role, s.pD.role, 500) --pushes enemy backwards 500 distance, maximum is around 2000 (not completely sure)
--BeatBack(s.p.role, s.pD.role, -300) --pushes enemy back and forth - funny as hell.

--[[TODO: Make all skills use the following damage effect:
--s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))

Meaning, all skills are based on the amount of attacks the player would have done:
- an attack depends on the defense of the enemy player (the lower the better).
- also depends on the attack speed of player attacking - it's based on 100 aspd (the higher the better - 200 aspd = 2x damage).

--TODO: These changes for automatic skills:
-They become life skills (Is Not Life Skill? to 0).
-Use an icon like "land.tga" or "ship.tga" (icon column). 
-Make books for them.
-on characterinfo, on character skills column, use -1 (no skills). That column tells which skills at lvl 1 player learns when created.

- any passive skill which requires the use of a weapon that doesn't use any cooldown script will use the player's attack speed.

- AddState is bugged in this gameserver - the skill id can only be a byte (max of 255) and it wraps around.
-WORKAROUND: Delete a couple of effects not used.

--TODO: Add critical attack to skill damage.

--Also need to fix a bug in the soulkeeper skill - the revival screen closes the previous soulkeeper use revival screen.

--SKILLEFF limit is 255 = (2^16)-1, it wraps around...

--TODO: need a better way than to use functions here...
--]]

local w = {} --alternative to with statement in python
local si,se,ii = tsv.skillinfo, tsv.skilleff, tsv.iteminfo

-- Player Skills
-------------------

--28-36


--[[
--Melee
for i,v in ipairs({25,26,28,29,32,33,34,35,37,38,150}) do
skills[PLAYER_SKILL][v][SK_EVENT_END] = function(s)
local dmg = math.max(0, (s.ps[ATTR_ATK]+math.random(-1*(s.ps[ATTR_ATK]/10),s.ps[ATTR_ATK]/10))-s.pDs[ATTR_DEF])
if s.ps[ATTR_CRT] >= math.random(0,100) then
dmg = dmg * 2
SkillCrt(s.pDs.role)
end
s.pDs[ATTR_HP] = s.pDs[ATTR_HP] - dmg
end
end

--Ranged
for i,v in ipairs({30,31,42,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,151,152,153,154,253}) do
skills[PLAYER_SKILL][v][SK_EVENT_END] = function(s)
local dmg = math.max(0, (s.ps[ATTR_ATK]+math.random(-1*(s.ps[ATTR_ATK]/10),s.ps[ATTR_ATK]/10))-s.pDs[ATTR_DEF])
if s.ps[ATTR_CRT] >= math.random(0,100) then
dmg = dmg * 2
SkillCrt(s.pDs.role)
end
s.pDs[ATTR_HP] = s.pDs[ATTR_HP] - dmg
end
end

--]]

--Melee
function melee_skill(s)
	print(s.pD.name)
	local right_weapon_type = GetItemType(GetChaItem(s.p.role, 1, 6))
	local left_weapon_type = GetItemType(GetChaItem(s.p.role, 1, 9))
	local item_type = GetItemType(GetChaItem(s.p.role, 1, 7))
	--[[
	if ((right_weapon_type == 3) and (item_type == 12)) or ((left_weapon_type == 4) and (item_type == 13)) then
		RemoveChaItem(s.p.role, 0, 1, 0, 7, 2, 1, 1)
	elseif ((right_weapon_type == 3) or (left_weapon_type == 4)) then
		BickerNotice(s.p.role,'Not enough ammunition to deal damage.')
		return C_TRUE
	end
--]]

	local dmg = math.max(1, (s.ps[ATTR_ATK]+math.random(-1*(s.ps[ATTR_ATK]/10),s.ps[ATTR_ATK]/10))-s.pDs[ATTR_DEF])
	--local dmg = math.max(0, (s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
	if s.ps[ATTR_CRT] >= math.random(0,100) then
		dmg = dmg * 2 * s.ps[ATTR_CRT]
		SkillCrt(s.pDs.role)
	end
	s.pDs[ATTR_HP] = s.pDs[ATTR_HP] - dmg
	print(s.pDs[ATTR_MSPD])
end

si["Firegun"]["func_miss"] =	function(s)
	local right_weapon_type = GetItemType(GetChaItem(s.p.role, 1, 6))
	local left_weapon_type = GetItemType(GetChaItem(s.p.role, 1, 9))
	local item_type = GetItemType(GetChaItem(s.p.role, 1, 7))
--[[
	if ((right_weapon_type == 3) and (item_type == 12)) or ((left_weapon_type == 4) and (item_type == 13)) then
		RemoveChaItem(s.p.role, 0, 1, 0, 7, 2, 1, 1)
	elseif ((right_weapon_type == 3) or (left_weapon_type == 4)) then
		BickerNotice(s.p.role,'Not enough ammunition to deal damage.')
		return C_TRUE
	end
--]]
end

for i,v in ipairs({"Bare Hand","Dual Implosion","Sword","Greatsword","Bow","Firegun","Blade","Boxing Gloves","Short Staff","Hammer","Dual Weapon"}) do --{25,28,29,30,31,32,33,35,37,38}
	si[v]["func_end"] = melee_skill
end


si["Dagger"]["func_end"] =	function(s)
	print('dagger skill')
	local dmg = math.max(1, --leave minimum at 1 to make sure player knows its hitting.
		math.max(0,(((s.ps[ATTR_ATK]/2)+math.random(-1*(s.ps[ATTR_ATK]/20),s.ps[ATTR_ATK]/20))-s.pDs[ATTR_DEF]))+
		math.max(0,(((s.ps[ATTR_MATK]/2)+math.random(-1*(s.ps[ATTR_MATK]/20),s.ps[ATTR_MATK]/20))-s.pDs[ATTR_MDEF]))
	)
	print('damage:'..dmg..', enemy hp old:'..s.pDs[ATTR_HP])
	if s.ps[ATTR_CRT] >= math.random(0,100) then
		dmg = dmg * 2 * (1 + s.ps[ATTR_CRT]/100)
		SkillCrt(s.pDs.role)
	end
	s.pDs[ATTR_HP] = s.pDs[ATTR_HP] - dmg --TODO: stats possibly not updating while just setting stats. When getting them, they show up fine.
	print('damage final:'..dmg..', enemy hp new:'..s.pDs[ATTR_HP])
	print(s.p.name)
	--local sk_lightningbolt_lvl = GetChaStateLv(s.p.role, SK_LIGHTNINGBOLT) --always gives 0 for some reason.
	--print(sk_lightningbolt_lvl)
	--if (sk_lightningbolt_lvl >= 1) then
		AddState(s.p.role,s.pD.role,se["Numb"]['id'],10,3)
	--end
end

si["Cannon Shot"]["func_aoe_range"] =	function(s) --used by ships
	SetSkillRange(4,400)
end


--made spiritual bolt into cleric's/sm's default attack.
--made voy's default attack a lightning melee.

--Charged skills (made for 1 hit killing):
--Charged Coray Ray, Charged Spiritual Bolt, Illusion Slash, Dual Shot, Headshot.

--
--34	Dagger	1	-1,10;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,7;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	-1,-1,0	0	0	0	-1	-1,-1;-2,-2;-2,-2	1	1	3	100	4	1	0	0	0	0	0	0	0	0	0	0	Skill_34_7	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	1	7,8,9,0,0,0,0,0,0,0	0	-1	-1	0	0	-1	0,0	0,0	-1	-1	-1	0	0	134	2	131	0	0	0	0	1	0,0	0	0	0
--36	Short Staff	1	-1,10;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,9;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	-1,-1,0	0	0	0	-1	-1,-1;-2,-2;-2,-2	1	1	3	900	4	1	0	0	0	0	0	0	0	0	0	0	Skill_36_7	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	2	1	7,8,9,0,0,0,0,0,0,0	0	128	-1	0	0	-1	0,0	0,0	0	6	-1	1015	4000	-1	2	131	0	0	0	0	1	0,0	0	0	0

--214	Lightning Bolt	1	4,10;16,10;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	7,1,29	2	1	0	-1	210,1;-2,-2;-2,-2	1	1	1	800	4	1	0	0	0	0	0	Skill_214_2	0	Skill_214_4	0	0	Skill_214_7	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Skill_214_10	1	1	12,0,0,0,0,0,0,0,0,0	-1	102	2	269	0	0	0,0	0,0	0	-1	-1	0	0	103	-1	270	0	0	0	s0214.tga	0	0,0	Equip Thunder Coral to strike target with lightning	Damage is determined by skill level and Spirit. Requires Thunder Coral to be equiped	Consumes 27 SP at Level 1. Increases by 2 SP per skill level
--216	Conch Ray	1	16,10;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	6,1,29	2	1	0	-1	212,5;-2,-2;-2,-2	1	1	1	600	4	2	0	0	1	0	0	Skill_216_2	0	Skill_216_4	Skill_216_5	0	Skill_216_7	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Skill_216_10	1	1	13,0,0,0,0,0,0,0,0,0	0	106	6	265	0	0	0,0	0,0	0	-1	-1	0	0	107	2	267	0	0	0	s0216.tga	0	0,0	Uses Strike Coral to damage targets in a straight line	Damage is determined by skill level and Spirit. Requires Strike Coral to be equipped	Consumes 23 SP at Level 1. Increases by 3 SP per skill level
--99	Spiritual Bolt	1	5,10;13,10;14,10;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,9;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	1,-1;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2;-2,-2	-1,-1,0	2	1	0	-1	-1,-1;-2,-2;-2,-2	1	1	1	800	4	1	0	0	0	0	0	Skill_99_2	0	0	0	0	Skill_99_7	0	0	0	0	0	0	0	0	0	0	0	0	0	0	Skill_99_10	2	1	11,0,0,0,0,0,0,0,0,0	-1	92	-1	190	0	-1	0,0	0,0	0	9	1	2003	1200	93	2	0	-2	0	0	s0099.tga	0	0,0	Use spiritual powers to damage target, ignoring defense	Deal magical damage and ignore Physical Defense. Damage determined by skill level and spirit difference between caster and targ	Consumes 32 SP at Level 1. Increases by 2 SP per skill level

--Magic
for i,v in ipairs({"Short Staff"}) do
	si[v]["func_end"] = function(s)
		local dmg = math.max(1, (s.ps[ATTR_MATK]+math.random(-1*(s.ps[ATTR_MATK]/10),s.ps[ATTR_MATK]/10))-s.pDs[ATTR_MDEF])
		if s.ps[ATTR_CRT] >= math.random(0,100) then
			dmg = dmg * 2
			SkillCrt(s.pDs.role)
		end
		s.pDs[ATTR_HP] = s.pDs[ATTR_HP] - dmg
	end
end

local w,y
local rps = function (w) si[w]['func_unuse'] = si[w]['func_use'] end --reflexive_passive_skill
local rpse = function (y) se[y]['func_end'] = se[y]['func_start'] end --reflexive_passive_skill_effect
w = si["Sword Mastery"]
w['func_use'] =	function(s) -- this "s" or rather "self" refers to the "skills" table directly.
	s.ps[ATTR_STATEV_ATK] = s.ps[ATTR_STATEV_ATK] + (s.passive_switch * (4*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Will of Steel"]
y = se["Will of Steel"]
w['func_cooldown'] =	function(s) return 15000 end -- in milliseconds
w['func_sp'] =	function(s) return 15 end
w['func_start'] =	function(s) --functions with target area 1 (just self) use func_start.
	AddState(s.p.role,s.p.role,se["Will of Steel"]['id'],s.lvl,15)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] + (s.passive_switch * (3*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Strengthen"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_MXHP] = s.ps[ATTR_STATEV_MXHP] + (s.passive_switch * (20*s.lvl + 3*s.ps[ATTR_CON]))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Deftness"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_FLEE] = s.ps[ATTR_STATEV_FLEE] + (s.passive_switch * (3*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Concentration"]
w['func_use'] = 	function(s)
	s.ps[ATTR_STATEV_HIT] = s.ps[ATTR_STATEV_HIT] + (s.passive_switch * (s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Greatsword Mastery"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_ATK] = s.ps[ATTR_STATEV_ATK] + (s.passive_switch * (7*s.lvl))
	--chaotic balance
	--s.ps[ATTR_STATEC_HIT] = s.ps[ATTR_STATEC_HIT] + (s.passive_switch * (0.02*GetSkillLv(s.p.role,SK_SWORDMASTERY)))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Blood Bull"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEC_MXHP] = s.ps[ATTR_STATEC_MXHP] + (s.passive_switch * (0.1+0.02*s.lvl))
	s.ps[ATTR_STATEC_DEF] = s.ps[ATTR_STATEC_DEF] + (s.passive_switch * (0.1+0.02*s.lvl))
	--chaotic balance
	--s.ps[ATTR_STATEV_HREC] = s.ps[ATTR_STATEV_HREC] + (s.passive_switch * (10*GetSkillLv(s.p.role,SK_WILLOFSTEEL)))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Blood Frenzy"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] + (s.passive_switch * (0.1+0.01*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Range Mastery"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_ATK] = s.ps[ATTR_STATEV_ATK] + (s.passive_switch * (2*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Windwalk"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (0.02*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Firegun Mastery"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_ATK] = s.ps[ATTR_STATEV_ATK] + (s.passive_switch * (8*s.lvl)) --max is 10, min is 6, so use 8 as average.
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Vigor"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_MXSP] = s.ps[ATTR_STATEV_MXSP] + (s.passive_switch * (40*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Divine Grace"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_SREC] = s.ps[ATTR_STATEV_SREC] + (s.passive_switch * (1*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Illusion Slash"]
w['func_cooldown'] =	function(s) return 2000+(300*s.lvl) end -- in milliseconds
w['func_sp'] =	function(s) return 20+(3*s.lvl) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Mighty Strike"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl + 8 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((1.2 + (0.05 * s.lvl))*s.ps[ATTR_ATK])-s.pDs[ATTR_DEF]))
end


w = si["Primal Rage"]
w['func_cooldown'] =	function(s) return 60000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl + 8 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((3 + (0.5 * s.lvl))*s.ps[ATTR_ATK])-s.pDs[ATTR_DEF]))
	--bd set effect
	--25% of damage * 5.
end


w = si["Berserk"]
y = se["Berserk"]
w['func_cooldown'] =	function(s) return 35000 end -- in milliseconds
w['func_sp'] =	function(s) return 15 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Berserk"]['id'],s.lvl,20)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] + (s.passive_switch * (0.2+(0.015*s.lvl)))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Shadow Slash"]
w['func_cooldown'] =	function(s) return 30000 end -- in milliseconds
w['func_sp'] =	function(s) return 3*s.lvl+20 end
w['func_end'] =	function(s)
	local state_time = math.floor(0.5*s.lvl)+3
	local angle_difference = math.abs(GetObjDire(s.p.role) - GetObjDire(s.pD.role))
	if (angle_difference < 90) or (angle_difference > 180) then
		state_time = state_time * 2
	end
	if s.pDs[ATTR_HP] >= 100000 then -- if hp is boss-like.
		state_time = (state_time / 2) + 1
		--More specific boss calcs here.
	end
	--bd set effect
	--50% of stun time * 2.

	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((1 + (0.1 * s.lvl))*s.ps[ATTR_ATK])-s.pDs[ATTR_DEF]))
	AddState(s.p.role,s.pD.role,se["Blackout"]['id'],s.lvl,state_time) --this effect is not handled script-wise
end


w = si["Poison Dart"]
y = se["Poisoned"]
w['func_cooldown'] =	function(s) return 20000 end -- in milliseconds
w['func_sp'] =	function(s) return 20 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Poisoned"]['id'],s.lvl,4*s.lvl+5)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - (10 + (2 * s.lvl)))
	--chaotic effect
	--s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 320)
end
y['func_end'] = y['func_start']


w = si["Eagle's Eye"]
y = se["Eagle's Eye"]
w['func_cooldown'] =	function(s) return 120000 end -- in milliseconds
w['func_sp'] =	function(s) return 10 end
w['func_start'] =	function(s)
	AddState(s.p.role,s.p.role,se["Eagle's Eye"]['id'],s.lvl,10*s.lvl+20)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_HIT] = s.ps[ATTR_STATEV_HIT] + (s.passive_switch * (3*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Dual Shot"]
w['func_cooldown'] =	function(s) return 2000+(300*s.lvl) end -- in milliseconds
w['func_sp'] =	function(s) return 20+(3*s.lvl) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end


w = si["Frozen Arrow"]
y = se["Frozen Arrow"]
w['func_cooldown'] =	function(s) return 15000 end
w['func_sp'] =	function(s) return 15 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((1.2*s.ps[ATTR_ATK])-s.pDs[ATTR_DEF]))
	AddState(s.p.role,s.pD.role,se["Frozen Arrow"]['id'],s.lvl,5)
	--Special use by a boss (state s.lvl is 10).
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (-1 * (0.2+0.03*s.lvl)))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Cripple"]
y = se["Cripple"]
w['func_cooldown'] =	function(s) return (-200*s.lvl)+8000 end
w['func_sp'] =	function(s) return 0.5*s.lvl+10 end
w['func_end'] =	function(s)
	local state_time = 5+math.floor(0.5*s.lvl)
	if s.pDs[ATTR_HP] >= 100000 then
		state_time = math.floor(state_time/3)+1
	end
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((1+(0.05*s.lvl))*s.ps[ATTR_ATK])-s.pDs[ATTR_DEF]))
	--More specific boss calcs here.
	AddState(s.p.role,s.pD.role,se["Cripple"]['id'],s.lvl,state_time)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_FLEE] = s.ps[ATTR_STATEC_FLEE] + (s.passive_switch * (-0.2))
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (-1 * (0.5+0.025*s.lvl)))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Enfeeble"]
y = se["Enfeeble"]
w['func_cooldown'] =	function(s) return 15000 end
w['func_sp'] =	function(s) return s.lvl+25 end
w['func_end'] =	function(s)
	local state_time = 5+(0.5*s.lvl)
	if s.pDs[ATTR_HP] >= 100000 then
		state_time = 3 + math.floor(0.3*s.lvl)
	end
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (100+10*s.lvl))

	--More specific boss calcs here.

	AddState(s.p.role,s.pD.role,se["Enfeeble"]['id'],s.lvl,state_time)
	AddState(s.p.role,s.pD.role,se["Skill Forbidden"]['id'],s.lvl,state_time)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ATK] = s.ps[ATTR_STATEC_ATK] + (s.passive_switch * (-0.2))
	s.ps[ATTR_STATEC_MATK] = s.ps[ATTR_STATEC_MATK] + (s.passive_switch * (-0.2))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Headshot"]
w['func_cooldown'] =	function(s) return 2000+(300*s.lvl) end -- in milliseconds
w['func_sp'] =	function(s) return 20+(3*s.lvl) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end


w = si["Heal"]
w['func_cooldown'] =	function(s) return -300*s.lvl+7000 end
w['func_sp'] =	function(s) return 4*s.lvl+30 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.min(s.pDs[ATTR_HP] + ((5+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_MATK]-s.pDs[ATTR_MDEF]),s.pDs[ATTR_MXHP])
	--god and dark god set effects, 25% of 2x heal.
	--1/8 of it goes to exs.p.
	--adds hate equivalent to heal.
end


w = si["Recover"]
w['func_cooldown'] =	function(s) return 3000 end
w['func_sp'] =	function(s) return 20 end
w['func_end'] =	function(s)
--Rem_State_Unnormal(DEFER) -- Remove a set of skill effects
end


w = si["Spiritual Bolt"]
w['func_cooldown'] =	function(s) return 2000+(300*s.lvl) end -- in milliseconds
w['func_sp'] =	function(s) return 20+(3*s.lvl) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_MATK]-s.pDs[ATTR_MDEF]))
end


w = si["Spiritual Fire"]
y = se["Spiritual Fire"]
w['func_cooldown'] =	function(s) return 3000 end
w['func_sp'] =	function(s) return 3*s.lvl+45 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Spiritual Fire"]['id'],s.lvl,20*s.lvl+180)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ATK] = s.ps[ATTR_STATEC_ATK] + (s.passive_switch * (0.01*s.lvl+0.1))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Tempest Boost"]
y = se["Tempest Boost"]
w['func_cooldown'] =	function(s) return 3000 end
w['func_sp'] =	function(s) return 4*s.lvl+40 end
w['func_end'] =	function(s)
	--boss effect here
	AddState(s.p.role,s.pD.role,se["Tempest Boost"]['id'],s.lvl,18*s.lvl+180)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] + (s.passive_switch * (0.01*s.lvl+0.05))
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (0.01*s.lvl+0.05))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Tornado Swirl"]
y = se["Tornado Swirl"]
w['func_cooldown'] =	function(s) return 5000 end
w['func_sp'] =	function(s) return 20 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Tornado Swirl"]['id'],s.lvl,3*s.lvl+30)
	--chaotic effect (so dumb, 99.9% of the time is the same as original).
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_CRT] = s.ps[ATTR_STATEV_CRT] + (s.passive_switch * (s.lvl+5))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Angelic Shield"]
y = se["Angelic Shield"]
w['func_cooldown'] =	function(s) return 5000 end
w['func_sp'] =	function(s) return 20 end
w['func_end'] =	function(s)
	--boss effect here
	AddState(s.p.role,s.pD.role,se["Angelic Shield"]['id'],s.lvl,3*s.lvl+30)
	--chaotic effect (so dumb, 90% of the time is the same as original, and if it does, just 5 seconds extra tops).
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_DEF] = s.ps[ATTR_STATEC_DEF] + (s.passive_switch * (0.03*s.lvl))
	s.ps[ATTR_STATEC_MDEF] = s.ps[ATTR_STATEC_MDEF] + (s.passive_switch * (0.03*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Seal of Elder"]
w['func_cooldown'] =	function(s) return 20000 end
w['func_sp'] =	function(s) return 2*s.lvl+30 end
w['func_end'] =	function(s)
	local state_time = math.floor(0.5*s.lvl)+10
	if (s.pDs[ATTR_HP] >= 100000) then
		if math_percent_random(0.8) then
			state_time =math.floor(0.3*s.lvl)+5
		else
			return --fail
		end
	end
	AddState(s.p.role,s.pD.role,se["Skill Forbidden"]['id'],s.lvl,state_time)
	--chaotic effect (replace the 10 in statetime for mathmax(10,con/15)).
	--BD set effect: 70% of 1.5*statetime
	--God or dark god set effect: 25% of 2*statetime
	--boss effects hereend
end


w = si["Shadow Insignia"]
w['func_cooldown'] =	function(s) return 30000 end
w['func_sp'] =	function(s) return 3*s.lvl+30 end
w['func_end'] =	function(s)
	local state_time = s.lvl+5
	if (s.pDs[ATTR_HP] >= 100000) then
		if math_percent_random(0.7) then
			if (s.pDs[ATTR_HP] >= 1000000) then
				state_time=4
			else
				state_time=9
			end
		else
			return --fail
		end
	end
	AddState(s.p.role,s.pD.role,se["Attack forbidden"]['id'],s.lvl,state_time)
	--chaotic effect (replace the 5 in statetime for math.max(5,con/30)).
	--BD set effect: 70% of 1.5*statetime
	--boss effects here end
end


w = si["Energy Shield"]
y = se["Energy Shield"]
w['func_cooldown'] =	function(s) return 1000 end
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	local state_time = -1
	AddState(s.p.role,s.pD.role,se["Energy Shield"]['id'],s.lvl,state_time)
	--Skill effect calculation is somewhere else.
	--boss effects here
end

w = si["Howl"]
w['func_cooldown'] =	function(s) return 5000 end
w['func_sp'] =	function(s) return math.floor(-0.5*s.lvl)+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,20*s.lvl+300) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((0.05*s.lvl+1)*s.ps[ATTR_ATK])-s.pDs[ATTR_DEF]))
end


w = si["Dual Sword Mastery"]
w['func_use'] =	function(s)
	s.ps[ATTR_LHAND_ITEMV] = s.ps[ATTR_LHAND_ITEMV] + 8*s.lvl
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Meteor Shower"]
w['func_cooldown'] =	function(s) return 25000 end
w['func_sp'] =	function(s) return 2*s.lvl+26 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,400) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((0.1*s.lvl+0.5)*s.ps[ATTR_ATK])-s.pDs[ATTR_DEF]))
end

w = si["Magma Bullet"]
y = se["Burn"]
w['func_cooldown'] = 	function(s) print('Magma Bullet func_cooldown') return 15000 end
w['func_sp'] = 	function(s) print('Magma Bullet func_sp') return 2*s.lvl+15 end
w['func_aoe_range'] =	function(s) print('Magma Bullet func_aoe_range') SetSkillRange(4,250) end
w['func_aoe_effect'] =	function(s) print('Magma Bullet func_aoe_effect') SetRangeState(se["Burn"]['id'],s.lvl,10) end
y['func_start'] =	function(s)
	print('Magma Bullet effect func_start/end')
	print('Magma Bullet effect GetAreaStateLevel', GetAreaStateLevel(s.p.role,se["Burn"]['id']))
	if (GetAreaStateLevel(s.p.role,se["Burn"]['id']) > 1) then -- this = 0 when TRANS event happens.
		s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - (3*s.lvl+30))
	else
		s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 10)
	end
end
y['func_end'] =	y['func_start']
y['func_transition'] =	function(s) print('Magma Bullet effect func_transition') return 10 end -- TRANS event takes 1 second to start when defender is in area. Return is the dmg done to player.

w = si["True Sight"]
y = se["True Sight"]
w['func_cooldown'] = 	function(s) return 5000 end
w['func_sp'] = 	function(s) return 3*s.lvl+10 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,20*s.lvl+600) end
w['func_aoe_effect'] =	function(s) SetRangeState(se["True Sight"]['id'],s.lvl,9*s.lvl+90) end
y['func_transition'] =	function(s) return 1 end


w = si["Cursed Fire"]
y = se["Cursed Fire"]
w['func_cooldown'] = 	function(s) return 30000 end
w['func_sp'] = 	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,300) end
w['func_aoe_effect'] =	function(s)
	SetRangeState(se["Cursed Fire"]['id'],s.lvl,s.lvl+5)
	--chaotic effect - on state_time replace 5 with math.floor(CON/30).
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_DEF] = s.ps[ATTR_STATEC_DEF] + (s.passive_switch * -1*(0.02*s.lvl+0.1))
	s.ps[ATTR_STATEC_MDEF] = s.ps[ATTR_STATEC_MDEF] + (s.passive_switch * -1*(0.02*s.lvl+0.1)) --added reduce magic def
	AttrRecheck(s.p.role)
end
y['func_transition'] =	function(s) return 10 end
y['func_end'] = y['func_start']


w = si["Abyss Mire"]
y = se["Abyss Mire"]
w['func_cooldown'] = 	function(s) return 30000 end
w['func_sp'] = 	function(s) return 5*s.lvl+50 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,500) end
w['func_aoe_effect'] =	function(s)
	--boss effect here
	SetRangeState(se["Abyss Mire"]['id'],s.lvl,2*s.lvl+20)
end
y['func_end'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * -1*(0.015*s.lvl+0.20))
	AttrRecheck(s.p.role)
end
y['func_transition'] =	function(s) return 3 end
y['func_end'] = y['func_start']


w = si["Healing Spring"]
y = se["Healing Spring"]
w['func_cooldown'] = 	function(s) return 30000 end
w['func_sp'] = 	function(s) return 2*s.lvl+30 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,40*s.lvl+400) end
w['func_aoe_effect'] =	function(s) SetRangeState(se["Healing Spring"]['id'],s.lvl,2*s.lvl+15) end
y['func_end'] =	function(s)
	s.ps[ATTR_HP] = math.min(s.ps[ATTR_HP] + (3*(15*s.lvl+50)),s.ps[ATTR_MXHP])
	--could change activation speed from 3 to 1 in skilleff.txt, but that could be too much spammy.
	--changed it to heal 3x more.
	--god/dark god set effects: 25% of double heal.
end
y['func_transition']=	function(s) return 3 end


w = si["Stealth"]
y = se["Conceal"]
w['func_cooldown'] =	function(s) return 30000 end
w['func_sp'] =	function(s) return 10 end
w['func_end'] =	function(s)
	--chaotic effect
	--AddState(s.p.role,s.pD.role,se["Conceal"]['id'],s.lvl,5*s.lvl+s.ps[ATTR_AGI]/4+20)
	AddState(s.p.role,s.pD.role,se["Conceal"]['id'],s.lvl,10*s.lvl+20)
end
y['func_start'] =	function(s)
	s.ps[ATTR_SP] = math.max(0,s.ps[ATTR_SP] - math.floor(0.5*s.lvl+10))
	--note: activation interval changed from 5 to 1 second - skilleff.txt.
	--to make it go along faster hp/sp recovery.
	if (s.ps[ATTR_SP] == 0) then
		RemoveState (s.p.role,s.id)
	end
end

w = si["Tiger Roar"]
y = se["Tiger Roar"]
w['func_cooldown'] = 	function(s) return 20000 end
w['func_sp'] = 	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,200) end
w['func_end'] =	function(s) 
	AddState(s.p.role,s.pD.role,se["Tiger Roar"]['id'],s.lvl,15)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_ATK] = s.ps[ATTR_STATEV_ATK] + (s.passive_switch * -1*(3*s.lvl))
	s.ps[ATTR_STATEV_MATK] = s.ps[ATTR_STATEV_MATK] + (s.passive_switch * -1*(3*s.lvl))
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * -1*(0.015*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Diligence"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_SREC] = s.ps[ATTR_STATEV_SREC] + (s.passive_switch * (s.lvl+1))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Current"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (0.01*s.lvl+0.05))
	s.ps[ATTR_BOAT_SKILLC_MSPD] = s.ps[ATTR_BOAT_SKILLC_MSPD] + (s.passive_switch * (0.01*s.lvl+0.05))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Conch Armor"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] + (s.passive_switch * (8*s.lvl))
	s.ps[ATTR_BOAT_SKILLV_DEF] = s.ps[ATTR_BOAT_SKILLV_DEF] + (s.passive_switch * (8*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Tornado"]
y = se["Tornado"]
w['func_cooldown'] =	function(s) return 12000 end
w['func_sp'] =	function(s) return 2*s.lvl+25 end
--w['func_energy'] = 	function(s) return math.floor(0.25*s.lvl+1) end
w['func_end'] =	function(s)
	local state_time = math.floor(0.5*s.lvl+3)
	--boss effect here
	--[[
	if (s.pDs[ATTR_HP] >= 100000) then -- changed from mxhp to hp
		return --fail
	end
	if (s.pDs[ATTR_HP] >= 50000) then
		if (math_percentage_random(0.2) == false) then
			return
		end
		state_time = math.floor(state_time/2)+1
	end
	--]]
	AddState(s.p.role,s.pD.role,se["Tornado"]['id'],s.lvl,state_time) --empty effect
	--AddState(s.p.role,s.pD.role,se["Crystalline Blessing"]['id'],s.lvl,state_time)
end
y['func_start'] =	function(s) end --null function
y['func_end'] = y['func_start']

w = si["Lightning Bolt"]
y = se["Numb"]
w['func_cooldown'] =	function(s) return 2000+(300*s.lvl) end -- in milliseconds
w['func_sp'] =	function(s) return 20+(3*s.lvl) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_MATK]-s.pDs[ATTR_MDEF]))
	AddState(s.p.role,s.pD.role,se["Numb"]['id'],s.lvl,3)
end	--chaotic effect s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((((3+s.lvl*0.05)*s.ps[ATTR_MATK])**(1/2))-s.pDs[ATTR_MDEF]))
	--bd set effect: 80% of 1.5 * damage.
	--god/dark god set effects: 25% of triple damage.
	--	local dmg = math.floor ( 80 + sklv*10 +sta_atk * 6 ) + 3 * Lv
	--	hpdmg = math.floor (( 10 + sta_atker * 2 ) * ( 1 + sklv * 0.25 ) * ( 1 +  lv_dif * 0.025 ))
	--boss effect here
	--affected also by intense magic, magic fairy skill, rebirth mystic power.

	--also, new idea -> make energy comsumption variable with amount of energy spent.
	--this can only be done itemwise.
	--make a lightning bolt charge skill, with a special item. Consuming this item and all the energy in the coral will burst on the oponent.
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] + (s.passive_switch * (-0.03*s.lvl+0.1)) --changed skill so that those effects rly get stronger with s.lvl us.p.
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (-0.03*s.lvl+0.2)) --becoming like Cripple, except for voyagers as alternative to Tornado.
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Algae Entanglement"]
y = se["Algae Entanglement"]
w['func_cooldown'] =	function(s) return 12000 end
w['func_sp'] =	function(s) return s.lvl+20 end
w['func_end'] =	function(s)
	local state_time = math.floor(0.5*s.lvl+6)
	--bd set effect: 80% of 2 * duration.
	if (s.pDs[ATTR_HP] >= 1000000) then --changed all max hp mention for bosses to hp so that bosses can be easier to kill in the end.
		if math_percent_random(0.5) then
			return --fail
		end
	end
	AddState(s.p.role,s.pD.role,se["Algae Entanglement"]['id'],s.lvl,state_time) --s.b.role (for boats)
end
y['func_start'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (2*s.lvl+10))
end
y['func_end'] = y['func_start']


w = si["Conch Ray"]
w['func_cooldown'] = 	function(s) return -500*s.lvl+7000 end
w['func_sp'] = 	function(s) return 3*s.lvl+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(1,30*s.lvl+500,10*s.lvl+100) end
--w['func_energy'] =	function(s) return math.floor(0.5*s.lvl+3) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((1+s.lvl*0.25)*s.ps[ATTR_MATK])-s.pDs[ATTR_MDEF])) --same attack power as Spiritual Bolt
	--chaotic effect AddState(s.p.role,s.pD.role,SKEFF_STUN,s.lvl,s.lvl/4) -- stuns target
	--affected also by intense magic, magic fairy skill, rebirth mystic power.
end


w = si["Tail Wind"]
y = se["Tail Wind"]
w['func_cooldown'] = 	function(s) return 10000 end
w['func_sp'] = 	function(s) return 3*s.lvl+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,1000) end
--w['func_energy'] =	function(s) return math.floor(0.5*s.lvl+3) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Tail Wind"]['id'],s.lvl,10*s.lvl+150)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (0.01*s.lvl+0.05))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Whirlpool"]
y = se["Whirlpool"]
w['func_cooldown'] = 	function(s) return 10000 end
w['func_sp'] = 	function(s) return s.lvl+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,300) end
w['func_aoe_effect'] =	function(s)
	SetRangeState(se["Whirlpool"]['id'],s.lvl,s.lvl+20)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (-1)*(0.02*s.lvl+0.1))
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] + (s.passive_switch * (-1)*(0.01*s.lvl+0.5))
	AttrRecheck(s.p.role)
end
y['func_transition'] =	function(s) return 1 end
y['func_end'] = y['func_start']


w = si["Fog"]
y = se["Fog"]
w['func_cooldown'] = 	function(s) return 10000 end
w['func_sp'] = 	function(s) return s.lvl+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,50*s.lvl+300) end
w['func_aoe_effect']=	function(s)
	SetRangeState(se["Fog"]['id'],s.lvl,20)
end
--w['func_energy'] =	function(s) return math.floor(0.5*s.lvl+3) end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ATK] = s.ps[ATTR_STATEC_ATK] + (s.passive_switch * (-1)*(0.01*s.lvl+0.05))
	s.ps[ATTR_STATEC_MATK] = s.ps[ATTR_STATEC_MATK] + (s.passive_switch * (-1)*(0.01*s.lvl+0.05))
	AttrRecheck(s.p.role)
end
y['func_transition']=	function(s) return 1 end
y['func_end'] = y['func_start']


w = si["Lightning Curtain"]
y = se["Lightning Curtain"]
w['func_cooldown'] = 	function(s) return 10000 end
w['func_sp'] = 	function(s) return s.lvl+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,300) end
w['func_aoe_effect']=	function(s)
	SetRangeState(se["Lightning Curtain"]['id'],s.lvl,s.lvl+15)
end
--w['func_energy'] =	function(s) return math.floor(0.5*s.lvl+3) end
y['func_start'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (20*s.lvl+160))
end
y['func_transition']=	function(s) return 1 end


w = si["Break Armor"]
y = se["Break Armor"]
w['func_cooldown'] =	function(s) return 25000 end
w['func_sp'] =	function(s) return 25 end
w['func_end'] =	function(s)
	--chaos effect: state_time = math.min(15,s.ps[ATTR_AGI]/10)
	AddState(s.p.role,s.pD.role,se["Break Armor"]['id'],s.lvl,15)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] + (s.passive_switch * (-4*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Rousing"]
y = se["Rousing"]
w['func_cooldown'] =	function(s) return 25000 end
w['func_sp'] =	function(s) return 25 end
w['func_start'] =	function(s)
	--boss effect here
	AddState(s.p.role,s.p.role,se["Rousing"]['id'],s.lvl,20)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_ASPD] = s.ps[ATTR_STATEV_ASPD] + (s.passive_switch * (s.lvl+10))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Venom Arrow"]
y = se["Dart"]
w['func_cooldown'] =	function(s) return 25000 end
w['func_sp'] =	function(s) return 20 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Dart"]['id'],s.lvl,s.lvl+11)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - (2*s.lvl+10))
end


w = si["Harden"]
y = se["Harden"]
w['func_cooldown'] =	function(s) return 3000 end
w['func_sp'] =	function(s) return 4*s.lvl+40 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Harden"]['id'],s.lvl,180)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] + (s.passive_switch * (4*s.lvl+10))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Taunt"]
y = se["Taunt"]
w['func_cooldown'] =	function(s) return 5000 end
w['func_sp'] =	function(s) return 10 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Taunt"]['id'],s.lvl,s.lvl+10)
	--AddHate (s.pD.role,s.p.role,maxhp)
end
y['func_start'] =	function(s)
--ADD/END have this chaotic effect: -0.02*s.lvl defense for target
--RemoveHate?
end
y['func_end'] = y['func_start']


w = si["Roar"]
w['func_cooldown'] =	function(s) return -200*s.lvl+4000 end
w['func_sp'] =	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,20*s.lvl+200) end
w['func_end'] =	function(s)
	--chaotic effect: call taunt skill effect with (s.lvl + taunts.lvl/10) as taunt skill s.lvl.
	AddState(s.p.role,s.pD.role,se["Roar"]['id'],s.lvl,3) --uses "Taunt" from last section
	--AddHate (s.pD.role,s.p.role,maxhp) --taunt skill effect will remove hate.
end


w = si["Crystalline Blessing"]
y = se["Kiss of Frost"]
w['func_cooldown'] =	function(s) return 3000 end
w['func_sp'] =	function(s) return 4*s.lvl+40 end
w['func_end'] =	function(s)
	local icy_crystal_item_id = 3463
	local chill_crystal_item_id = 6465

	if (
	(CheckBagItem(s.p.role,icy_crystal_item_id) and DelBagItem(s.p.role,icy_crystal_item_id,1)) or
	(CheckBagItem(s.p.role,chill_crystal_item_id) and DelBagItem(s.p.role,chill_crystal_item_id,1)) --TODO: chill crystal should receive 2*state_time
	) then
		--chaotic effect: state_time = math.max(8,math.floor(sta_atker/15))+sklv*2
		AddState(s.p.role,s.pD.role,se["Kiss of Frost"]['id'],s.lvl,2*s.lvl+8) --effect functions are empty
	end
	--AddHate (s.pD.role,s.p.role,maxhp)
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']


w = si["Intense Magic"]
y = se["Intense Magic"]
w['func_cooldown'] =	function(s) return 3000 end
w['func_sp'] =	function(s) return 4*s.lvl+40 end
w['func_end'] =	function(s)
	local magical_clover_item_id = 3462
	local magical_clover_2_item_id = 6464

	--5% chance per s.lvl of not consuming clover
	if (
	(CheckBagItem(s.p.role,magical_clover_item_id) and DelBagItem(s.p.role,magical_clover_item_id,1)) or
	(CheckBagItem(s.p.role,magical_clover_2_item_id) and DelBagItem(s.p.role,magical_clover_2_item_id,1)) --TODO: magical clover 2 should receive 2*state_time
	) then
		--chaotic effect: state_time = math.max(8,math.floor(sta_atker/15))+sklv*2
		AddState(s.p.role,s.pD.role,se["Intense Magic"]['id'],s.lvl,90*s.lvl+90) --effect functions are empty
	end
	--AddHate (s.pD.role,s.p.role,maxhp)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_MATK] = s.ps[ATTR_STATEV_MATK] + (s.passive_switch * (30*s.lvl))
	s.ps[ATTR_STATEC_MATK] = s.ps[ATTR_STATEC_MATK] + (s.passive_switch * (0.02*s.lvl+1.4))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Fairy body"]
w['func_cooldown'] =	function(s) return 180000 end
w['func_sp'] =	function(s) return 20 end
w['func_start'] =	function(s)
--Skill_JLFT_BEGIN
--TODO: Implement this skill (part of item effects)
end
w['func_end'] =	function(s)
end
for i,y in ipairs({"Fairy Possession A","Fairy Possession B","Fairy Possession C","Fairy Possession D","Fairy Possession E","Fairy Possession F","Fairy Possession G","Fairy Possession H"}) do
	se[y]['func_start'] =	function(s)
	end
	se[y]['func_end'] = se[y]['func_start']
end

w = si["Self Destruct"]
w['func_cooldown'] =	function(s) return 180000 end
w['func_sp'] =	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,1500) end
w['func_start'] =	function(s)
--Skill_jlzb_Begin
--TODO: Implement this skill (part of item effects)
end
w['func_end'] =	function(s)
end

for i,w in ipairs({"Skill - Defecate","Skill - Undergarment","Skill - Garment","Skill - Coin Shower","Skill - Fool","Skill - Snooty","Skill - Trickster","Skill - Dumb"}) do
	si[w]['func_cooldown'] =	function(s) return 10000 end
	si[w]['func_sp'] =	function(s) return 20 end
end
si["Skill - Defecate"]['func_end'] =	function(s) AddState(s.p.role,s.pD.role,se["Skill - Defecate"]['id'],s.lvl,60) end
si["Skill - Undergarment"]['func_end'] =	function(s) AddState(s.p.role,s.pD.role,se["Skill - Undergarment"]['id'],s.lvl,60) end
si["Skill - Garment"]['func_end'] =	function(s) AddState(s.p.role,s.pD.role,se["Skill - Garment"]['id'],s.lvl,60) end
si["Skill - Coin Shower"]['func_end'] =	function(s) AddState(s.p.role,s.pD.role,se["Skill - Coin Shower"]['id'],s.lvl,60) end
si["Skill - Fool"]['func_end'] =	function(s) AddState(s.p.role,s.pD.role,se["Skill - Fool"]['id'],s.lvl,60) end
si["Skill - Snooty"]['func_end'] =	function(s) AddState(s.p.role,s.pD.role,se["Skill - Snooty"]['id'],s.lvl,60) end
si["Skill - Trickster"]['func_end'] =	function(s) AddState(s.p.role,s.pD.role,se["Skill - Trickster"]['id'],s.lvl,60) end
si["Skill - Dumb"]['func_end'] =	function(s) AddState(s.p.role,s.pD.role,se["Skill - Dumb"]['id'],s.lvl,60) end



w = si["Ethereal Slash"]
w['func_cooldown'] =	function(s) return 30000 end
w['func_sp'] =	function(s) return 125 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,800) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (750 * s.lvl))
end


w = si["Super Consciousness"] --word case matters (wrong case might crash game server!)
y = se["Super Consciousness"]
w['func_cooldown'] = 	function(s) return 30000 end
w['func_sp'] = 	function(s) return 160 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,800) end
w['func_aoe_effect']=	function(s)
	SetRangeState(se["Super Consciousness"]['id'],s.lvl,15)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - (50*s.lvl+25))
end


w = si["Beast Legion Smash"]
w['func_cooldown'] =	function(s) return 30000 end
w['func_sp'] =	function(s) return 125 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,800) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (750 * s.lvl))
end


w = si["Red Thunder Cannon"]
w['func_cooldown'] =	function(s) return 30000 end
w['func_sp'] =	function(s) return 105 end
w['func_aoe_range'] =	function(s) SetSkillRange(1,3*s.lvl+900,s.lvl+250) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (750 * s.lvl))
end


w = si["Devil Curse"]
y = se["Devil Curse"]
w['func_cooldown'] =	function(s) return 30000 end
w['func_sp'] =	function(s) return 155 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,5*s.lvl+550) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (500 * s.lvl))
	AddState(p.role,s.pD.role,se["Devil Curse"]['id'],s.lvl,25)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - (30*s.lvl+4))
end


w = si["Holy Judgement"]
w['func_cooldown'] =	function(s) return 30000 end
w['func_sp'] =	function(s) return 20*s.lvl+120 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,1000) end
w['func_end'] =	function(s)
	--damage becomes 0 at region 2: IsChaInRegion( ATKER, 2 ) dmg = 0
	if (is_friend(ATKER, DEFER) == 0) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (500*s.lvl+50))
	else
		s.pDs[ATTR_HP] = math.min(s.pDs[ATTR_HP] + (500*s.lvl+20),s.pDs[ATTR_MXHP])
	end
end


w = si["Rebirth Mystic Power"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEC_ATK] = s.ps[ATTR_STATEC_ATK] + (s.passive_switch * (0.005*s.lvl+0.05))
	s.ps[ATTR_STATEC_MATK] = s.ps[ATTR_STATEC_MATK] + (s.passive_switch * (0.005*s.lvl+0.05))
	s.ps[ATTR_STATEC_DEF] = s.ps[ATTR_STATEC_DEF] + (s.passive_switch * (0.005*s.lvl+0.05))
	s.ps[ATTR_STATEC_MDEF] = s.ps[ATTR_STATEC_MDEF] + (s.passive_switch * (0.005*s.lvl+0.05))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Newbie Mighty Strike"]
w['func_cooldown'] =	function(s) return 5000 end
w['func_sp'] =	function(s) return 7 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (75 + 15 * s.lvl))
end

w = si["Sit"]
w['func_use'] =	function(s)
	s.ps[ATTR_STATEC_HREC] = s.ps[ATTR_STATEC_HREC] + s.passive_switch * 15
	s.ps[ATTR_STATEV_HREC] = s.ps[ATTR_STATEV_HREC] + s.passive_switch * 5
	s.ps[ATTR_STATEC_SREC] = s.ps[ATTR_STATEC_SREC] + s.passive_switch * 5
	s.ps[ATTR_STATEV_SREC] = s.ps[ATTR_STATEV_SREC] + s.passive_switch * 5
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["The Warrior's Rage"]
y = se["The Warrior's Rage"] -- considered as id 0 in skilleffectinfo sigh --TODO: fix it in game source.
w['func_cooldown'] =	function(s) return 60000 end -- in milliseconds
w['func_sp'] =	function(s) return 200 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["The Warrior's Rage"]['id'],s.lvl,60)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 600)
	s.ps[ATTR_STATEC_ATK] = s.ps[ATTR_STATEC_ATK] + (s.passive_switch * 0.4)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] - (s.passive_switch * 3000)
	s.ps[ATTR_STATEV_MDEF] = s.ps[ATTR_STATEV_MDEF] - (s.passive_switch * 30)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["The Windrider's Grace"]
y = se["The Windrider's Grace"]
w['func_cooldown'] =	function(s) return 60000 end -- in milliseconds
w['func_sp'] =	function(s) return 100 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["The Windrider's Grace"]['id'],s.lvl,60)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_HIT] = s.ps[ATTR_STATEV_HIT] + (s.passive_switch * 100)
	s.ps[ATTR_STATEV_FLEE] = s.ps[ATTR_STATEV_FLEE] + (s.passive_switch * 100)
	s.ps[ATTR_STATEV_CRT] = s.ps[ATTR_STATEV_CRT] + (s.passive_switch * 50)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] - (s.passive_switch * 300)
	s.ps[ATTR_STATEV_MDEF] = s.ps[ATTR_STATEV_MDEF] - (s.passive_switch * 30)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Blunting Bolt"]
w['func_cooldown'] =	function(s) return 30000 end -- in milliseconds
w['func_sp'] =	function(s) return 2 * s.lvl + 30 end
w['func_end'] =	function(s)
	s.pDs[ATTR_SP] = math.max(0,((1-(0.04 * s.lvl)) * s.pDs[ATTR_SP]))
end


w = si["Flaming Dart"]
w['func_cooldown'] =	function(s) return 7000 end -- in milliseconds
w['func_sp'] =	function(s) return 3 * s.lvl + 70 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (200 * s.lvl))
end


w = si["Eye of Precision"]
y = se["Eye of Precision"]
w['func_cooldown'] =	function(s) return 30000 end -- in milliseconds
w['func_sp'] =	function(s) return 100 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Eye of Precision"]['id'],s.lvl,math.floor((0.3 * s.lvl) + 3))
	AddState(s.p.role,s.p.role,se["Skill Forbidden"]['id'],s.lvl,math.floor((0.3 * s.lvl) + 3))
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ATK] = s.ps[ATTR_STATEC_ATK] + s.passive_switch
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

y = se["Deadeye Blood"] --using y here
w['func_cooldown'] =	w['func_cooldown']
w['func_sp'] =	w['func_sp']
w['func_end'] =	w['func_end']

-- Soul Keeper skill is skipped (more difficult to implement - it's based on item effect).

w = si["Excrucio"]
y = se["Excrucio"]
w['func_cooldown'] =	function(s) return 62000 - (2000*s.lvl) end -- in milliseconds
w['func_sp'] =	function(s) return 50*s.lvl+50 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,50*s.lvl) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Excrucio"]['id'],s.lvl,60) --TODO: Fix effect not working
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - (15*s.lvl + 100))
end


w = si["Petrifying Pummel"]
w['func_cooldown'] =	function(s) return 29000 + (1000*s.lvl) end -- in milliseconds
w['func_sp'] =	function(s) return (20*s.lvl)+100 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,50*s.lvl) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - math.min((0.03 * s.lvl * s.pDs[ATTR_HP]),30000))
	AddState(s.p.role,s.pD.role,se["Blackout"]['id'],s.lvl,math.floor(0.5*s.lvl + 0.5))
end


w = si["Curse of Weakening"]
y = se["Curse of Weakening"]
w['func_cooldown'] =	function(s) return 60000 end -- in milliseconds
w['func_sp'] =	function(s) return 100*s.lvl+900 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,50*s.lvl) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Curse of Weakening"]['id'],s.lvl,4*s.lvl)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_MDEF] = s.ps[ATTR_STATEV_MDEF] - (s.passive_switch * ((3*s.lvl)+5))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Bane of Waning"]
y = se["Bane of Waning"]
w['func_cooldown'] =	function(s) return 60000 end -- in milliseconds
w['func_sp'] =	function(s) return 50*s.lvl+1000 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,10*s.lvl+50) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Bane of Waning"]['id'],s.lvl,s.lvl+6)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ATK] = s.ps[ATTR_STATEC_ATK] - (s.passive_switch * 0.03)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']


w = si["Omnis Immunity"] --fix the extra space
w['func_cooldown'] =	function(s) return 180000 end -- in milliseconds
w['func_sp'] =	function(s) return 200 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Invincible Potion"]['id'],s.lvl,s.lvl) --get this from blessed pot's effect
end


w = si["Revival"]
w['func_cooldown'] =	function(s) return 30000-(s.lvl*1500) end -- in milliseconds
w['func_sp'] =	function(s) return 50 end
w['func_end'] =	function(s)
	if s.ps[ATTR_HP] <= 0 then
		SetRelive(s.p.role,s.pD.role,s.lvl, "Accept revival request from: \n\n"..s.pD.name)
	else
		return 1
	end
end


w = si["Soulkeeper"]
w['func_cooldown'] =	function(s) return 1800000 end -- (30*60*1000) in milliseconds
w['func_sp'] =	function(s) return 200 end
w['func_end'] =	function(s)
	SetRelive(s.p.role,s.pD.role,s.lvl, "Accept your own revival request?\nTip: Hide this until you are dead.")
end

--IT_IMMUNITY = 1860

--w = ii[IT_IMMUNITY]['func_use'] = 	function(s)
--end

-- Not used player skills
----------------------------

w = si["Tempest Slash"] --not implemented in TOP
w['func_use'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']
w = si["Gun Research"] --not implemented in TOP
w['func_use'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Angel Blessing"] --Gives target +3% HP Recovery per skill level
y = se["Angel Blessing"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Angel Blessing"]['id'],s.lvl,(2*s.lvl)+5)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_HREC] = s.ps[ATTR_STATEC_HREC] + (s.passive_switch * (0.03 * s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Parlay Hit"] --not implemented in TOP
w['func_use'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Phantom Slash"] --not implemented in TOP
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
end

w = si["Cure"] --Removes poison (useless compared to "Recover")
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl end
w['func_end'] =	function(s)
	RemoveState(s.p.role,  se["Poisoned"]['id']) 
end

w = si["Blind"] --Not implemented in TOP
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
end

w = si["Protective Shield"] --not implemented in TOP
w['func_use'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Frost Shield"] --Adds defence
y = se["Frost Shield"] --Adds defence
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Frost Shield"]['id'],s.lvl,(2*s.lvl)+5)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] + (s.passive_switch * ((2*s.lvl)+5))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Refined Gunpowder"] --not implemented in TOP
w['func_use'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Attack Weakness"] --not implemented in TOP
w['func_use'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

--[[
w = "Angel Blessing 2" --not implemented in TOP (-.- another Angel Blessing version)
w['func_use'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']
--]]

w = si["Benediction"] --not implemented in TOP
w['func_use'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Inferno Wings"] --not implemented in TOP
y = se["Inferno Wings"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Inferno Wings"]['id'],s.lvl,(2*s.lvl)+10)
end
y['func_start'] =	function(s)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Holy Beam"] --not implemented in TOP
y = se["Holy Beam"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Holy Beam"]['id'],s.lvl,(2*s.lvl)+10)
end
y['func_start'] =	function(s)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Holy Heraldry"] --not implemented in TOP
y = se["Holy Heraldry"]
w['func_cooldown'] =	function(s) return 0 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Ray of Luck"] --not implemented in TOP
y = se["Ray of Luck"]
w['func_cooldown'] =	function(s) return 0 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Traversing"] --Gives player +100 movement speed.
y = se["Traversing"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Traversing"]['id'],s.lvl,(2*s.lvl)+3)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_MSPD] = s.ps[ATTR_STATEV_MSPD] + (s.passive_switch * ((10*s.lvl)+100))
end
y['func_end'] = y['func_start']

-- These seem to have been intended to be used, but were never used.

w = si["Inferno Blast"] --Damage skill - Effect is similar to "Dual Shot", so made it damage exactly like it.
w['func_cooldown'] =	function(s) return 15000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl+15 end
w['func_aoe_range'] =	function(s) SetSkillRange(2,(20*s.lvl)+600,(3*s.lvl)+90) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Evasion"] --a bit stronger than "Deftness"
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_FLEE] = s.ps[ATTR_STATEV_FLEE] + (s.passive_switch * (4*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Beast Strength"] --a bit stronger than "Strengthen" (added a CON component to complement the change to "Strengthen").
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_MXHP] = s.ps[ATTR_STATEV_MXHP] + (s.passive_switch * (50*s.lvl + 3*s.ps[ATTR_CON]))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Recuperate"] --HP Recovery skill
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_HREC] = s.ps[ATTR_STATEV_HREC] + (s.passive_switch * s.lvl)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Shield Mastery"] --Gives defense while wearing shield.
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] + (s.passive_switch * (3 * s.lvl))
	s.ps[ATTR_STATEV_MDEF] = s.ps[ATTR_STATEV_MDEF] + (s.passive_switch * (3 * s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Ranger"] --same as "Deftness", except for SS.
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_FLEE] = s.ps[ATTR_STATEV_FLEE] + (s.passive_switch * (3*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Hunter Disguise"] --weaker than "Deftness", except for Hunter.
w['func_use'] =	function(s)
	s.ps[ATTR_STATEV_FLEE] = s.ps[ATTR_STATEV_FLEE] + (s.passive_switch * (2*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Backstab"] --Recoded to do equivalent to 8 attacks if behind enemy, otherwise, 2.
w['func_cooldown'] =	function(s) return 60000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+15 end
w['func_end'] =	function(s)
	local angle_difference = math.abs(GetObjDire(s.p.role) - GetObjDire(s.pD.role))
	local multiplier = 0
	if (angle_difference < 90) or (angle_difference > 180) then
		multiplier = 0.6
	end
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(multiplier*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Numb"] --Decreases attack speed and movement speed of target.
y = se["Numb"]
w['func_cooldown'] =	function(s) return 20000 end -- in milliseconds
w['func_sp'] =	function(s) return 10 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Numb"]['id'],s.lvl,5)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] + (s.passive_switch * (0.03 * s.lvl + 0.1))
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (0.03 * s.lvl + 0.2))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Hunter Strike"] --Recoded to do equivalent to 3 attacks
w['func_cooldown'] =	function(s) return 30000 end -- in milliseconds
w['func_sp'] =	function(s) return 10 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((2+(0.1*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Astro Strike"] --Pushes enemy back
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return 2*s.lvl+20 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((1.5+(0.1*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
	BeatBack (s.p.role, s.pD.role, 30*s.lvl+300) 
end

w = si["Barbaric Crush"] --AOE Attack that does 1.5 attacks damage.
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl+10 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(10*s.lvl)+200) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((1.5+(0.1*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Shield of Thorns"] --used a permanent skill effect that was removed
y['func_start'] =	function(s)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Parry"] --Stuns enemies in an area for 5 seconds and pushes them.
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return 2*s.lvl+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(2,(10*s.lvl)+400,(4*s.lvl)+100) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Blackout"]['id'],s.lvl,5)
end

w = si["Dispersion Bullet"] --AOE damage skill - need aiming
w['func_cooldown'] =	function(s) return 15000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl+15 end
w['func_aoe_range'] =	function(s) SetSkillRange(2,(20*s.lvl)+600,(3*s.lvl)+90) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((1.2+(0.15*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Penetrating Bullet"] --AOE damage skill - need very careful aiming
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl+10 end
w['func_aoe_range'] =	function(s) SetSkillRange(2,(50*s.lvl)+1500,50) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((1+(0.2*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Greater Heal"] --AOE "Heal"
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return s.lvl+30 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(30*s.lvl)+300) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.min(s.pDs[ATTR_HP] + ((2+(0.15*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_MATK]-s.pDs[ATTR_MDEF]),s.pDs[ATTR_MXHP])
end

w = si["Greater Recover"] --AOE "Recover"
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return math.floor(0.5*s.lvl)+15 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(30*s.lvl)+300) end
w['func_end'] =	function(s)
	--Rem_State_Unnormal(DEFER) -- Remove a set of skill effects
end

w = si["Counterguard"] --Reduces hit rate of enemies in area.
y = se["Counterguard"]
w['func_cooldown'] = 	function(s) return 20000 end -- in milliseconds
w['func_sp'] = 	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,300) end
w['func_aoe_effect']=	function(s) SetRangeState(se["Counterguard"]['id'],s.lvl,5) end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_HIT] = s.ps[ATTR_STATEC_HIT] + (s.passive_switch * (-0.02*s.lvl))
end
y['func_end'] = y['func_start']

--Totem's Worship --not even defined.
--Gunpowder Research --not even defined.

w = si["Cannon Mastery"] --increases ship attack by 25 per level - cannot be implemented.
w['func_use'] =	function(s)
	s.ps[ATTR_BOAT_SKILLV_MNATK] = s.ps[ATTR_BOAT_SKILLV_MNATK] + (s.passive_switch * (25*s.lvl))
	s.ps[ATTR_BOAT_SKILLV_MXATK] = s.ps[ATTR_BOAT_SKILLV_MXATK] + (s.passive_switch * (25*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Toughen Wood"] --increases ship defense by 12 per level.
w['func_use'] =	function(s)
	s.ps[ATTR_BOAT_SKILLV_DEF] = s.ps[ATTR_BOAT_SKILLV_DEF] + (s.passive_switch * (8*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Sail Mastery"] --increases ship attack speed by 10% and 2% per skill level.
w['func_use'] =	function(s)
	s.ps[ATTR_BOAT_SKILLC_ASPD] = s.ps[ATTR_BOAT_SKILLC_ASPD] + (s.passive_switch * (0.02*s.lvl)+0.1)
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Reinforce Ship"] --increases ship fuel capacity by 400 per skill level.
w['func_use'] =	function(s)
	s.ps[ATTR_BOAT_SKILLV_MXUSE] = s.ps[ATTR_BOAT_SKILLV_MXUSE] + (s.passive_switch * (400*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']

w = si["Oil Tank Upgrade"] --removed in TOP, but it should be like this below:
w['func_use'] =	function(s)
	s.ps[ATTR_BOAT_SKILLV_MXSPLY] = s.ps[ATTR_BOAT_SKILLV_MXSPLY] + (s.passive_switch * (400*s.lvl))
	AttrRecheck(s.p.role)
end
w['func_unuse'] = w['func_use']


w = si["Bombardment"] --High damage, but fades with distance.
w['func_aoe_range'] =	function(s) SetSkillRange(4,400) end
w['func_end'] =	function(s)
	local x1,y1 = GetSkillPos(s.p.role)
	local x2,y2 = GetChaPos(s.pD.role)
	
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((2+(0.3*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF])*
		math.max(0,math.min(1,(1-(math.pow( math.pow(x1-x2, 2) + math.pow(y1-y2, 2), 0.5)/1000)))) --also based on distance from target
	))
end

-- Boss Skills
--------------

w = si["Death Shriek"]
y = se["Death Shriek"]
w['func_cooldown'] =	function(s) return 6000 end -- in milliseconds
w['func_sp'] =	function(s) return 50 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Death Shriek"]['id'],s.lvl,30)
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Blood Fury"]
y = se["Blood Fury"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 20 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Blood Fury"]['id'],s.lvl,60)
end
y['func_start'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]-300)
end

w = si["Lair"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 40 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Lair"]['id'],s.lvl,60)
end
y = se["Lair"]
y['func_start'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]-(3*s.ps[ATTR_ATK]))
end

w = si["Earthquake"]
y = se["Earthquake"]
w['func_cooldown'] = 	function(s) return 3000 end -- in milliseconds
w['func_sp'] = 	function(s) return 5*s.lvl+50 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,1000) end
w['func_aoe_effect']=	function(s) SetRangeState(se["Earthquake"]['id'],s.lvl,20) end
y['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Earthquake"]['id'],s.lvl,20)
	AddState(s.p.role,s.pD.role,se["Blackout"]['id'],s.lvl,10)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * (-0.3))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Colossus Smash"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 20 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]-(20*s.ps[ATTR_ATK]))
end

w = si["Kiss of Frost"]
y = se["Kiss of Frost"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 50 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Kiss of Frost"]['id'],s.lvl,60)
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Tempest Blade"]
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,1000) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]-(2*s.ps[ATTR_ATK]))
end

w = si["Replicate"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 10 end
w['func_end'] =	function(s)
end

w = si["Alga Ambush"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,300) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Algae Entanglement"]['id'],10,30)
end

w = si["Jellyfish Electric Bolt"]
w['func_cooldown'] = 	function(s) return 1000 end -- in milliseconds
w['func_sp'] = 	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,400) end
w['func_aoe_effect']=	function(s) SetRangeState(se["Lightning Curtain"]['id'],10,25) end

w = si["Squid Whirlstrike"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return math.floor(0.5*s.lvl)+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,400) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]-(3*s.ps[ATTR_ATK]))
end

w = si["Shark Attack"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+30 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,400) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]/2)
end

w = si["Polliwog Self Explode"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return math.floor(0.5*s.lvl)+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,400) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]-(3*s.ps[ATTR_ATK]))
end

y = si["Primal Skill"]
w['func_cooldown'] =	function(s) return 2500 end -- in milliseconds

y = si["Primal Rage_"]
w['func_cooldown'] =	function(s) return 6000 end -- in milliseconds

w = si["Water Mine Explosion"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(20*s.lvl)+1200) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]-1500)
	s.ps[ATTR_HP] = -1
end

w = si["Corpse Attack"]
y = se["Corpse Poison"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 5 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((1+(0.2*s.lvl))*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
	AddState(s.p.role,s.pD.role,se["Corpse Poison"]['id'],1,30)
end
y['func_start'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP]-(100*s.lvl))
end

w = si["Corpse Range Attack"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+3 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (math.max(30, 150 - s.pDs[ATTR_CON]) * 2.8))
end

w = si["Fox Taoist Sorcery"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+30 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (math.max(30, 150 - s.pDs[ATTR_CON]) * 4))
end

w = si["Fox Spirit Sorcery"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+10 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (math.max(30, 150 - s.pDs[ATTR_CON]) * 3.5))
end

w = si["Fascinate"]
y = se["Fascinate"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+50 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Fascinate"]['id'],s.lvl,s.lvl+6)
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Fox Immortal Sorcery"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+10 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (math.max(50, 150 - s.pDs[ATTR_CON]) * 10))
end

w = si["Fox Immortal Area Spell"]
w['func_cooldown'] =	function(s) return (-500*s.lvl)+7000 end -- in milliseconds
w['func_sp'] =	function(s) return (3*s.lvl)+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,100) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 150 - (s.pDs[ATTR_CON]/2)) * 5)+300))
end

w = si["Ritual"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+10 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (math.max(50, 150 - s.pDs[ATTR_CON]) * 5))
end

w = si["Sachem's Witchcraft"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+10 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (math.max(50, 150 - s.pDs[ATTR_CON]) * 5))
	AddState(s.p.role,s.pD.role,se["Corpse Poison"]['id'],4,15)
end

w = si["Whirling Hook"]
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return (math.floor(-0.5*s.lvl))+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,300) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.min(s.pDs[ATTR_HP] + (s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]),s.pDs[ATTR_MXHP])
end

w = si["Dog Howl"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return (5*s.lvl)+50 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,1000) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 200)
	AddState(s.p.role,s.pD.role,se["Blackout"]['id'],5,10)
end


w = si["Corpse Venom"]
w['func_cooldown'] =	function(s) return 20000 end -- in milliseconds
w['func_sp'] =	function(s) return 20 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 300)
	AddState(s.p.role,s.pD.role,se["Corpse Poison"]['id'],3,60)
end

w = si["Icy Dragon Strike"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 15 end
w['func_aoe_range'] =	function(s) SetSkillRange(2,800,120) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (1.5*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Cyborg Blockade"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return (3*s.lvl)+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(1,500,200) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 150)
end

w = si["Crab Binding"]
y = se["Crab Binding"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+50 end
w['func_end'] =	function(s) 
	if GetChaStateLv(s.pD.role, se["Spring Town Status"]['id']) ~= 3 then
		AddState(s.p.role,s.pD.role,se["Crab Binding"]['id'],s.lvl,15)
	end
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Curse of Kelpie"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+50 end
w['func_end'] =	function(s) --if StateLv ~= 2 then
	if GetChaStateLv(s.pD.role, se["Spring Town Status"]['id']) ~= 2 then
		AddState(s.p.role,s.pD.role,se["Cursed Fire"]['id'],10,180)
	end
end

w = si["Frost Breath"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return (2*s.lvl)+50 end
w['func_end'] =	function(s) --if StateLv ~= 1 then
	if GetChaStateLv(s.pD.role, se["Spring Town Status"]['id']) ~= 1 then
		AddState(s.p.role,s.pD.role,se["Frozen Arrow"]['id'],5,180)
	end
end

w = si["Dance of Icy Dragon"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,300) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Frozen Arrow"]['id'],10,15)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 500)
end

w = si["Black Dragon Terror"]
y = se["Black Dragon Terror"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 200 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Black Dragon Terror"]['id'],s.lvl,30)
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Black Dragon Roar"]
y = se["Black Dragon Roar"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return math.floor(-0.5*s.lvl)+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,500) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Black Dragon Roar"]['id'],s.lvl,120)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ATK] = s.ps[ATTR_STATEC_ATK] - (s.passive_switch * 0.5)
	s.ps[ATTR_STATEC_MATK] = s.ps[ATTR_STATEC_MATK] - (s.passive_switch * 0.5)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Black Dragon Flight"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return math.floor(-0.5*s.lvl)+20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,3000) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (math.max(100, 150 - s.pDs[ATTR_CON]) * 15))
end

w = si["Black Dragon Breath"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 15 end
w['func_aoe_range'] =	function(s) SetSkillRange(2,600,120) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (1.5*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Resurrect"]
w['func_cooldown'] =	function(s) return (-300*s.lvl)+7000 end -- in milliseconds
w['func_sp'] =	function(s) return (4*s.lvl)+30 end
w['func_end'] =	function(s)
	s.ps[ATTR_HP] = math.min(s.ps[ATTR_HP] + 50000)
end

w = si["Black Dragon Roar 2"]
y = se["Black Dragon Roar 2"]
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,5000) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (0.8*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
	AddState(s.p.role,s.pD.role,se["Black Dragon Roar 2"]['id'],s.lvl,10)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] - (s.passive_switch * 0.3)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] - (s.passive_switch * 1)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Black Dragon Lightning Bolt"]
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 150 - s.pDs[ATTR_CON]) * 10) + 1000))
	AddState(s.p.role,s.pD.role,se["Black Dragon Terror"]['id'],4,3)
end

w = si["Black Dragon Illusion Slash"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (2*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Black Dragon Fireball"]
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 150 - s.pDs[ATTR_CON]) * 20) + 2500))
end

w = si["Black Dragon Summon"]
w['func_cooldown'] =	function(s) return 1200000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,1000) end
w['func_end'] =	function(s)
	local x, y = GetChaPos(s.p.role)
	local pos = {{x,y+700},{x+700,y-700},{x-700,y-700}}
	SetChaLifeTime(CreateCha(791, pos[1][1], pos[1][2], 145, 50), 900000)
	SetChaLifeTime(CreateCha(793, pos[2][1], pos[2][2], 145, 50), 900000)
	SetChaLifeTime(CreateCha(794, pos[3][1], pos[3][2], 145, 50), 900000)
end

w = si["Deathsoul Magma Bullet"]
y = se["Deathsoul Magma Bullet"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (2*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
	AddState(s.p.role,s.pD.role,se["Deathsoul Magma Bullet"]['id'],s.lvl,6)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 60)
end

w = si["Deathsoul Acceleration"]
y = se["Deathsoul Acceleration"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Deathsoul Acceleration"]['id'],4,6)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * 1)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Firegun Attack"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (1.5*s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Deathsoul Roquet"]
y = se["Deathsoul Roquet"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 150 - s.pDs[ATTR_CON]) * 8) + 400))
	AddState(s.p.role,s.pD.role,se["Deathsoul Roquet"]['id'],4,3)
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Laser Beam"]
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - math.min(2000, (0.05*s.pDs[ATTR_HP])+500))
end

w = si["Deathsoul Poison Dart"]
y = se["Deathsoul Poison Dart"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Deathsoul Poison Dart"]['id'],10)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 160)
end

w = si["Deathsoul Summon"]
w['func_cooldown'] =	function(s) return 500000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,1000) end
w['func_end'] =	function(s)
	local x, y = GetChaPos(s.p.role)
	local pos = {{x+200,y+200},{x+200,y-200},{x-200,y+200},{x-200,y-200}}
	for i,v in range(1,4) do
		SetChaLifeTime(CreateCha(799, pos[i][1], pos[i][2], 145, 50), 900000)
	end
end

w = si["Ranged Blast"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 150 - s.pDs[ATTR_CON]) * 20) + 1000))
end

w = si["Cloud Attack"]
w['func_cooldown'] = 	function(s) return 2000 end -- in milliseconds
w['func_sp'] = 	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,300) end
w['func_aoe_effect']=	function(s)
	SetRangeState(se["Fog"]['id'],s.lvl,20)
end

w = si["Encumbered Skeleton"]
y = se["Encumbered Skeleton"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Encumbered Skeleton"]['id'],s.lvl,6)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 80)
end

w = si["Mytho Teleportation"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	local x, y = GetChaPos(s.pD.role)
	GoTo(s.p.role, math.floor(x/100),math.floor(y/100), GetChaMapName(s.pD.role))
end

w = si["Skeletar Shielding"]
y = se["Skeletar Shielding"]
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Skeletar Shielding"]['id'],s.lvl,10)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_DEF] = s.ps[ATTR_STATEC_DEF] - (s.passive_switch * 0.8)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Evil Attack"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 150 - s.pDs[ATTR_CON]) * 5) + 400))
end

w = si["Deathsoul Slice"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Roar of Deathsoul"]
y = se["Roar of Deathsoul"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Roar of Deathsoul"]['id'],s.lvl,10)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_ATK] = s.ps[ATTR_STATEV_ATK] + (s.passive_switch * 800)
	s.ps[ATTR_STATEV_MATK] = s.ps[ATTR_STATEV_MATK] + (s.passive_switch * 800)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Deathsoul Taunt"]
y = se["Deathsoul Taunt"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,2000) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Deathsoul Taunt"]['id'],s.lvl,10)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_HIT] = s.ps[ATTR_STATEV_HIT] - (s.passive_switch * 30)
	s.ps[ATTR_STATEV_FLEE] = s.ps[ATTR_STATEV_FLEE] - (s.passive_switch * 10)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Cursed Blood"]
y = se["Moonlight Recovery"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,2000) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Moonlight Recovery"]['id'],s.lvl,10)
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 100)
end

w = si["Moonlight Recovery"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.ps[ATTR_HP] = math.min(s.ps[ATTR_HP] + (0.2*(s.ps[ATTR_MXHP]-s.ps[ATTR_HP])),s.ps[ATTR_MXHP])
end

w = si["Deathsoul Water Arrow"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF])+800))
end

w = si["Deathsoul Whirlpool"]
y = se["Deathsoul Whirlpool"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,1000) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Deathsoul Whirlpool"]['id'],s.lvl,10)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] - (s.passive_switch * 0.3)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] - (s.passive_switch * 0.5)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Deathsoul Lightning Curtain"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,500) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Lightning Curtain"]['id'],7,15)
end

w = si["Bomb Throwing"]
w['func_cooldown'] =	function(s) return 4000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,500) end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 100 - s.pDs[ATTR_CON]) * 8) + 500))
end

w = si["Spiritual Dart"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 100 - s.pDs[ATTR_CON]) * 8) + 500))
end

w = si["Physical Dart"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (s.ps[ATTR_ASPD]/100)*(s.ps[ATTR_ATK]-s.pDs[ATTR_DEF]))
end

w = si["Shoe Barrage"]
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 25)
end

w = si["Illusion Slash Illusion"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.lvl = 10
	si["Illusion Slash"]['func_end'](s)
end

w = si["Conch Ray Illusion"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s)
	s.lvl = 8
	si["Conch Ray"]['func_aoe_range'](s)
end
w['func_end'] =	function(s)
	s.lvl = 8
	si["Conch Ray"]['func_end'](s)
end

w = si["Headshot Illusion"]
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.lvl = 10
	si["Headshot"]['func_end'](s)
end

w = si["Spiritual Bolt Illusion"]
w['func_cooldown'] =	function(s) return 5000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.lvl = 10
	si["Spiritual Bolt"]['func_end'](s)
end

w = si["Grenade"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - math.min(1000*s.lvl,4*((30*s.lvl)+(((0.008*s.lvl)+0.05)*s.pDs[ATTR_HP])+320)-1200))
	end
	--delete item
end

w = si["Flash Bomb"]
y = se["Flash Bomb"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,800) end
w['func_end'] =	function(s)
	if ((KitbagLock(s.p.role,0) ~= C_FALSE) and (s.pDs[ATTR_HP] < 1000000)) then
		AddState(s.p.role,s.pD.role,se["Flash Bomb"]['id'],s.lvl,s.lvl)
	end
	--delete item
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Radiation"]
y = se["Radiation"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(100*s.lvl)+800) end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - math.min(500*s.lvl,2*((30*s.lvl)+(((0.008*s.lvl)+0.05)*s.pDs[ATTR_HP])+320)-600))
		AddState(s.p.role,s.pD.role,se["Radiation"]['id'],s.lvl,(2*s.lvl)+15)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 20)
end
y['func_end'] = y['func_start']

w = si["Soul Detector"]
w['func_cooldown'] = 	function(s) return 1500 end -- in milliseconds
w['func_sp'] = 	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,(20*s.lvl)+600) end
w['func_aoe_effect']=	function(s)
	--delete item
	SetRangeState(se["True Sight"]['id'],s.lvl,(9*s.lvl)+40)
end
w['func_end'] =	function(s)
end

w = si["Ship Accelerator"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Deathsoul Acceleration"]['id'],s.lvl,(20*s.lvl)+30)
	end
	--delete item
end

w = si["Ship Atomizer"]
y = se["Ship Atomizer"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Ship Atomizer"]['id'],s.lvl,(2*s.lvl)+3)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] + (s.passive_switch * 3)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Ship Penetrator"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((0.5*s.lvl)+1)*450))
		AddState(s.p.role,s.pD.role,se["Break Armor"]['id'],s.lvl,s.lvl+10)
	end
	--delete item
end

w = si["Ship Impaler"]
y = se["Impaler"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((0.5*s.lvl)+1)*450))
		AddState(s.p.role,s.pD.role,se["Impaler"]['id'],s.lvl,s.lvl+10)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_ATK] = s.ps[ATTR_STATEV_ATK] - (s.passive_switch * 30)
	s.ps[ATTR_STATEV_MATK] = s.ps[ATTR_STATEV_MATK] - (s.passive_switch * 30)
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Ship Flamer"]
y = se["Ship Flamer"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - (((0.5*s.lvl)+1)*150))
		AddState(s.p.role,s.pD.role,se["Ship Flamer"]['id'],s.lvl,(2*s.lvl)+4)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 150)
end

w = si["Little Snow Ball"]
y = se["Barbecue"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if ((KitbagLock(s.p.role,0) ~= C_FALSE) and (math.random(1,3) == 1)) then
		AddState(s.p.role,s.pD.role,se["Barbecue"]['id'],10,2)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(5,s.ps[ATTR_HP] - 1)
end

w = si["Carrion Ball Lv1"]
y = se["Carrion Ball"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Carrion Ball"]['id'],s.lvl,(4*s.lvl)+5)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEV_DEF] = s.ps[ATTR_STATEV_DEF] - (s.passive_switch * (0.03*s.lvl))
	s.ps[ATTR_STATEV_MDEF] = s.ps[ATTR_STATEV_MDEF] - (s.passive_switch * (0.03*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Noise Polluter Lv1"]
y = se["Noise Polluter"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(40*s.lvl)+550) end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Noise Polluter"]['id'],s.lvl,(2*s.lvl)+15)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_SP] = math.max(0,s.ps[ATTR_SP] - 15)
end

w = si["Earthquake Generator Lv1"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(40*s.lvl)+550) end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Earthquake Generator"]['id'],s.lvl,(2*s.lvl)+10)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] - (s.passive_switch * ((0.1*s.lvl)+0.2))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Chain Bullet Lv1"]
y = se["Chain Bullet"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Chain Bullet"]['id'],s.lvl,(20*s.lvl)+30)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] - (s.passive_switch * ((0.09*s.lvl)+0.3))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Mirage Generator Lv1"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Mirage Generator"]['id'],s.lvl,(3*s.lvl)+2)
	end
	--delete item
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Stealth Ship Lv1"]
y = se["Stealth Ship"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Stealth Ship"]['id'],s.lvl,(15*s.lvl)+5)
	end
	--delete item
end
y['func_start'] =	function(s)
end
y['func_end'] = y['func_start']

w = si["Radar Lv1"]
y = se["Radar"]
w['func_cooldown'] = 	function(s) return 3000 end -- in milliseconds
w['func_sp'] = 	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(3,(300*s.lvl)+50) end
w['func_aoe_effect']=	function(s)
	SetRangeState(se["Radar"]['id'],s.lvl,(55*s.lvl)+20)
end
w['func_end'] =	function(s)
end
y['func_start'] =	function(s)
end
y['func_transition'] =	function(s) return 1 end
y['func_end'] = y['func_start']

w = si["Hull Repair Lv1"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.ps[ATTR_HP] = math.min(s.ps[ATTR_HP] + ((1.5*s.lvl)+550),s.ps[ATTR_MXHP])
	end
	--delete item
end

w = si["Food Generation Lv1"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.ps[ATTR_SP] = math.min(s.ps[ATTR_SP] + 650*s.lvl,s.ps[ATTR_MXSP])
	end
	--delete item
end

w = si["Carrion Bullet Lv1"]
y = se["Carrion Bullet"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Carrion Bullet"]['id'],s.lvl,(8*s.lvl)+2)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_SP] = math.max(0,s.ps[ATTR_SP] - 80)
end

w = si["Illusion Generator Lv1"]
w['func_cooldown'] =	function(s) return 3000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		--TODO: remove stealth (not defined in TOP).
	end
	--delete item
end

w = si["Exploding Lamb Lv1"]
y = se["Burning Lamb"]
w['func_cooldown'] =	function(s) return 6000 end -- in milliseconds
w['func_sp'] =	function(s) return 20 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(50*s.lvl)+550) end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 20*s.lvl)
		s.ps[ATTR_HP] = 4*s.lvl
		AddState(s.p.role,s.p.role,se["Burning Lamb"]['id'],s.lvl,(s.lvl)+3)
		AddState(s.p.role,s.pD.role,se["Burning Lamb"]['id'],s.lvl,(s.lvl)+3)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 2)
end
y['func_transition']=	function(s) return 1 end

w = si["Water Mine Lv1"]
y = se["Water Mine burning"]
w['func_cooldown'] =	function(s) return 6000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,(50*s.lvl)+650) end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 20*s.lvl)
		s.ps[ATTR_HP] = 4*s.lvl
		AddState(s.p.role,s.p.role,se["Water Mine burning"]['id'],s.lvl,(s.lvl)+3)
		AddState(s.p.role,s.pD.role,se["Water Mine burning"]['id'],s.lvl,(s.lvl)+3)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_HP] = math.max(0,s.ps[ATTR_HP] - 2)
end
y['func_transition']=	function(s) return 1 end

w = si["Blessed Potion"]
y = se["Invincible Potion"]
w['func_cooldown'] =	function(s) return 20000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		AddState(s.p.role,s.pD.role,se["Invincible Potion"]['id'],10,5)
	end
	--delete item
end
y['func_start'] =	function(s)
end

w = si["Drag Tower"]
y = se["Drag Tower"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s) 
	AddState(s.p.role,s.pD.role,se["Drag Tower"]['id'],1,3)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] - (s.passive_switch * (0.5*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

w = si["Devil Tower"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 800*s.lvl)
	s.pDs[ATTR_SP] = math.max(0,s.pDs[ATTR_SP] - 50*s.lvl)
end

w = si["Serpent Attack"]
w['func_cooldown'] =	function(s) return 1000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 2000*s.lvl)
end

w = si["Pirate Wave"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(1,1000,200) end
w['func_end'] =	function(s)
	local def_guild_id = GetChaGuildID(s.pD.role)
	if def_guild_id > 100 and def_guild_id <= 200 then
		return 0
	else
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 1950)
	end
end

w = si["Navy Wave"]
w['func_cooldown'] =	function(s) return 2000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(1,1000,200) end
w['func_end'] =	function(s)
	local def_guild_id = GetChaGuildID(s.pD.role)
	if def_guild_id > 0 and def_guild_id <= 100 then
		return 0
	else
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 1950)
	end
end

w = si["Spirit Lash"]
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - ((math.max(50, 150 - s.pDs[ATTR_CON]) * 10) + 300))
end

w = si["Sacred Candle"]
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	GiveItem(s.p.role,0,tsv.iteminfo["Lighted Sacred Candle"]['id'],1,41) 
end

w = si["Goddess Statue"]
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	local i = item_wrap(GetChaItem2(s.p.role, 2, tsv.iteminfo["Sacred Candle"]['id']))
	if i.s[ITEMATTR_ENERGY]<999 then
		i.s[ITEMATTR_ENERGY] = i.s[ITEMATTR_ENERGY] + 1
		RefreshCha(s.p.role)
	end
end

w = si["Frozen Ring"]
y = se["Frozen Ring"]
w['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_aoe_range'] =	function(s) SetSkillRange(4,500) end
w['func_end'] =	function(s)
	AddState(s.p.role,s.pD.role,se["Frozen Ring"]['id'],1,5)
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] - (s.passive_switch * (0.6))
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] - (s.passive_switch * (0.5*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']

--[[
w = si["Healing potion that restores health over time"]
y = se["Healing potion that restores health over time"]
['func_cooldown'] =	function(s) return 10000 end -- in milliseconds
['func_sp'] =	function(s) return 0 end
['func_aoe_range'] =	function(s) SetSkillRange(4,500) end
['func_end'] =	function(s)
	if (KitbagLock(s.p.role,0) ~= C_FALSE) then
		s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 20*s.lvl)
		s.ps[ATTR_HP] = 4*s.lvl
		AddState(s.p.role,s.p.role,se["Water Mine"]['id'],s.lvl,(s.lvl)+3)
		AddState(s.p.role,s.pD.role,se["Water Mine"]['id'],s.lvl,(s.lvl)+3)
	end
	--delete item
end
y['func_start'] =	function(s)
	s.ps[ATTR_STATEC_ASPD] = s.ps[ATTR_STATEC_ASPD] - (s.passive_switch * (0.6))
	s.ps[ATTR_STATEC_MSPD] = s.ps[ATTR_STATEC_MSPD] - (s.passive_switch * (0.5*s.lvl))
	AttrRecheck(s.p.role)
end
y['func_end'] = y['func_start']
--]]


--TODO: Continue here.

-- Life skills
--------------

w = si["Woodcutting"]
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 1)
end

w = si["Mining"]
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 1)
end

w = si["Fishing"]
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 1)
end

w = si["Salvage"]
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	s.pDs[ATTR_HP] = math.max(0,s.pDs[ATTR_HP] - 1)
end

w = si["Repair"]
w['func_cooldown'] =	function(s) return 1500 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
	if CheckBagItem(s.p.role, tsv.iteminfo["Repair Wood"]['id']) > 0 then
		DelBagItem(s.p.role, tsv.iteminfo["Repair Wood"]['id'],1)
		s.pDs[ATTR_HP] = math.min(s.pDs[ATTR_HP] + ((20*s.lvl)+200),s.pDs[ATTR_MXHP])
	end
end

w = si["Cooking"] --check forge.lua
w['func_cooldown'] =	function(s) return 0 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
end

w = si["Manufacturing"] --check forge.lua
w['func_cooldown'] =	function(s) return 0 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
end

w = si["Crafting"] --check forge.lua
w['func_cooldown'] =	function(s) return 0 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
end

w = si["Analyze"] --check forge.lua
w['func_cooldown'] =	function(s) return 0 end -- in milliseconds
w['func_sp'] =	function(s) return 0 end
w['func_end'] =	function(s)
end

-- Non-player skills
--------------------
w = si["Ink Blast"]
w['func_cooldown'] =	function(s) return 2500 end

for i,w in ipairs({"Saliva of Lizard","Needle of Stramonium","Stone of Treant","Panda's Bamboo Shoot","Bolt of Terra Elder","Bolt of Naiad","Oyster","Bubble Clam","Baby Icy Dragon","Icy Dragoon","Shadow Quiver","Mist of Snow Lady","Fireball of Chieftain","Werewolf Quiver","Fireball of Giant Slug","Icecube of King Penguin","Dart of Ninja Mole","Heart of Mermaid","Relic Protector","Wave of King Tortoise","Bubble of Water Dancer","Mud of Mudman","Hand of Stramonium","Guardian Angel","Soul of Goddess","Mist of Snow Queen","Lump of Mud King"}) do --138-154
	si[w]['func_cooldown'] =	function(s) return 2000 end
	si[w]['func_end'] =	melee_skill
end

w = si["Cannon Strike"]
w['func_end'] =	melee_skill

-- Item Skills
-----------------

function itemskill_learnplayerskillbook(s, skill_id)
	print('itemskill_learnplayerskillbook called')
	if (GetSkillLv(s.p.role, skill_id) ~= 0) or (AddChaSkill(s.p.role,skill_id,1,1,1) == 0) then
		UseItemFailed(s.p.role)
		return
	end
end

IT_ENHPOOL = 88
IT_ENHPOOL_EXTRACT = 89
IT_ENHPOOL_CREATE = 3302

--cannot be dialog page - it crashes game.
--[[
ii[88]['func_use'] = 	function(s)
	BickerNotice('Check System Messages')
	SystemNotice(s.p.role, 'STR:'.. 	s.is[ITEMATTR_VAL_STR])
	SystemNotice(s.p.role, 'ACC:'.. 	s.is[ITEMATTR_VAL_ACC])
	SystemNotice(s.p.role, 'AGI:'.. 	s.is[ITEMATTR_VAL_AGI])
	SystemNotice(s.p.role, 'CON:'.. 	s.is[ITEMATTR_VAL_CON])
	SystemNotice(s.p.role, 'SPR:'.. 	s.is[ITEMATTR_VAL_SPR])
	SystemNotice(s.p.role, 'LUK:'.. 	s.is[ITEMATTR_VAL_LUK])
	SystemNotice(s.p.role, 'ASPD:'.. 	s.is[ITEMATTR_VAL_ASPD])
	SystemNotice(s.p.role, 'CRTR:'.. 	s.is[ITEMATTR_VAL_CRTR])
	SystemNotice(s.p.role, 'ATK:'.. 	s.is[ITEMATTR_VAL_ATK])
	SystemNotice(s.p.role, 'MATK:'.. 	s.is[ITEMATTR_VAL_MATK])
	SystemNotice(s.p.role, 'MXHP:'.. 	s.is[ITEMATTR_VAL_MXHP])
	SystemNotice(s.p.role, 'MXSP:'.. 	s.is[ITEMATTR_VAL_MXSP])
	SystemNotice(s.p.role, 'FLEE:'.. 	s.is[ITEMATTR_VAL_FLEE])
	SystemNotice(s.p.role, 'HIT:'.. 	s.is[ITEMATTR_VAL_HIT])
	SystemNotice(s.p.role, 'CRT:'.. 	s.is[ITEMATTR_VAL_CRT])
	SystemNotice(s.p.role, 'DROP:'.. 	s.is[ITEMATTR_VAL_DROP])
	SystemNotice(s.p.role, 'HREC:'.. 	s.is[ITEMATTR_VAL_HREC])
	SystemNotice(s.p.role, 'SREC:'.. 	s.is[ITEMATTR_VAL_SREC])
	SystemNotice(s.p.role, 'MSPD:'.. 	s.is[ITEMATTR_VAL_MSPD])
	SystemNotice(s.p.role, 'EXP:'.. 	s.is[ITEMATTR_VAL_EXP])
	SystemNotice(s.p.role, 'MDEF:'.. 	s.is[ITEMATTR_VAL_MDEF])
end

IT_ENHPOOL_ATTR_STR = 3303
IT_ENHPOOL_ATTR_ACC = 3304
IT_ENHPOOL_ATTR_AGI = 3305
IT_ENHPOOL_ATTR_CON = 3306
IT_ENHPOOL_ATTR_SPR = 3307
IT_ENHPOOL_ATTR_LUK = 3308
IT_ENHPOOL_ATTR_MXHP = 3309
IT_ENHPOOL_ATTR_MXSP = 3310
IT_ENHPOOL_ATTR_ATK = 3311
IT_ENHPOOL_ATTR_DEF = 3312
IT_ENHPOOL_ATTR_MATK = 3313
IT_ENHPOOL_ATTR_MDEF = 3314
IT_ENHPOOL_ATTR_HIT = 3315
IT_ENHPOOL_ATTR_FLEE = 3316
IT_ENHPOOL_ATTR_CRT = 3317
IT_ENHPOOL_ATTR_CRTR = 3318
IT_ENHPOOL_ATTR_HPREC = 3319
IT_ENHPOOL_ATTR_SPREC = 3320
IT_ENHPOOL_ATTR_ASPD = 3321
IT_ENHPOOL_ATTR_MSPD = 3322
IT_ENHPOOL_ATTR_EXP = 3323
IT_ENHPOOL_ATTR_DROP = 3324

ii["Enhancement Pool Extractor"]['func_use'] = 	function(s)
	slots = light_item_wrap(89,s.p).report[1].bag.slots
end

ii["Enhancement Pool Stat Checker"]['func_use'] = 	function(s)
	myitem = item_wrap(GetChaItem(player.role,2,light_item_wrap(88,s.p).report[1].bag.slots[1]))
end
ii["Accuracy Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_ACC] = s.ps[ATTR_ACC] + 1 end
ii["Agility Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_AGI] = s.ps[ATTR_AGI] + 1 end
ii["Constitution Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_CON] = s.ps[ATTR_CON] + 1 end
ii["Spirit Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_SPR] = s.ps[ATTR_SPR] + 1 end
ii["Luck Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_LUK] = s.ps[ATTR_LUK] + 1 end
ii["Maximum Hit Points Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_MXHP] = s.ps[ATTR_MXHP] + 1 end
ii["Maximum Spirit Points Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_MXSP] = s.ps[ATTR_MXSP] + 1 end
ii["Physical Attack Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_ATK] = s.ps[ATTR_ATK] + 1 end
ii["Physical Defense Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_DEF] = s.ps[ATTR_DEF] + 1 end
ii["Magical Attack Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_MATK] = s.ps[ATTR_MATK] + 1 end
ii["Magical Defense Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_MDEF] = s.ps[ATTR_MDEF] + 1 end
ii["Hit Rate Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_HIT] = s.ps[ATTR_HIT] + 1 end
ii["Dodge Rate Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_FLEE] = s.ps[ATTR_FLEE] + 1 end
ii["Critical Rate Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_CRT] = s.ps[ATTR_CRT] + 1 end
ii["Critical Resistance Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_CRTR] = s.ps[ATTR_CRTR] + 1 end
ii["Hit Points Recovery Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_HPREC] = s.ps[ATTR_HPREC] + 1 end
ii["Spirit Points Recovery Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_SPREC] = s.ps[ATTR_SPREC] + 1 end
ii["Attack Speed Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_ASPD] = s.ps[ATTR_ASPD] + 1 end
ii["Movement Speed Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_MSPD] = s.ps[ATTR_MSPD] + 1 end
ii["Experience Rate Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_EXP] = s.ps[ATTR_EXP] + 1 end
ii["Drop Rate Equipment Enhancement"]['func_use'] = 	function(s) s.ps[ATTR_DROP] = s.ps[ATTR_DROP] + 1 end
--]]

-- New developments: Item Skills
--[[
NOTE:
The use item script removes the item only AFTER the function ends.
The strategy to fix this is to remove the item before and re-add it at the end (in the skills function).

--]]

function failure_system_notice(s,string)
	SystemNotice(s.p.role, string)
	return true
end

function not_enough_required_inventory_space(s,max) --item_info s needed
	if GetChaFreeBagGridNum(s.p.role) < max then
		return failure_system_notice(s, 'This item requires at least '..tostring(max)..' free slot(s) in the inventory.')
	end
	return false
end

ii['Chest of Cabal']['func_use'] = function(s)
	--CHANGED: if event has ended, that note should be shown to all, not just lvl 60+
	SystemNotice(s.p.role,GetResString("CALCULATE_ITEMEFFECT_LUA_000253"))
end

--ii['Crusade Salute','func_use'] = kw.nilf -- null function - same as just commenting this line...

--ii['Victory Treasure Chest']['func_use'] = function(s) -- Not available on TOP2
--	--CHANGED: using generic inv space function
--	if not_enough_required_inventory_space(s,1) then return true end
--	
--	local a = math.random (1,100)
--	GiveItem(s.p.role,0,lfunc.case(
--		{a >= 1 and a <= 50,ii['Gem of Soul']['id']},
--		{a > 50 and a <= 80,ii['Angelic Dice']['id']},
--		{true,ii['Refining Gem']['id']}),1,4)
--end

ii['New Term Gift Package']['func_use'] = function(s)
	local gift_list_per_char = {
		{'Street Love Hat-Lance',	'Street Love Robe-Lance',	'Street Love Gloves-Lance',	'Street Love Shoes-Lance'},
		{'Busker\'s Hat-Carsise',	'Busker\'s Robe-Carsise',	'Busker\'s Gloves-Carsise',	'Busker\'s Boots-Carsise'},
		{'Fantasy Hat-Phyllis',		'Fantasy Robe-Phyllis',		'Fantasy Gloves-Phyllis',	'Fantasy Boots-Phyllis',},
		{'Hunny Hat-Ami',		'Hunny Robe-Ami',		'Hunny Gloves-Ami',		'Hunny Boots-Ami',}
	}
	
	--TODO: Use char name instead of id.
	--TODO: Check difference between GetChaTypeID, GetCharID, and GetChaID.
	
	--CHANGED: using generic inv space function
	--CHANGED: inv space required depends on the size of the gift list per char
	if not_enough_required_inventory_space(s,t_len(gift_list_per_char[s.p.type_id])) then return true end
	
	for i,v in ipairs(gift_list_per_char[s.p.type_id]) do
		GiveItem(s.p.role,0,ii[v]['id'],1,4)
	end
end

function food_crab(s,lvl,time)
	local role = s.p.role
	if s.ps[ATTR_LV]>86 then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000310"))
	end
	
	--CHANGED: s.p is automatically the player in question, not the boat - remove all boat checks
	if UseAexpItem(role,s.i.pointer,lvl,time)==0 then
		return true
	end
	SystemNotice(role,GetResString("CALCULATE_ITEMEFFECT_LUA_000311"))
end

ii['Hairy Crab']['func_use'] = function(s)
	if food_crab(s,7,120) then
		return true
	end
	SystemNotice(s.p.role,GetResString("CALCULATE_ITEMEFFECT_LUA_000311"))
end

ii['Steamed Crab']['func_use'] = function(s)
	if food_crab(s,10,120) then
		return true
	end
	SystemNotice(s.p.role,GetResString("CALCULATE_ITEMEFFECT_LUA_000312"))
end

ii['Crab Fry']['func_use'] = function(s)
	--CHANGED: gives only 1000 instead of 1020 on lvls+ 80.
	s.ps[ATTR_CEXP] = s.ps[ATTR_CEXP] + 1000
end

--[[
TODO: Make exp go with a uniform function instead of a table. Also make 1 exp in CEXP = 20 or something that makes up
for the 50 exp after lvl 80 (that was invented as a stop gap - there would be a need for extended maintenance - changing
every single player's CEXP with new adjusted ones). That will simplify exp calculation - which is very important in
a server. Players could kill many monsters all at once, which means function needs to be realy fast to keep up.

Calculating it now: 260 (rounded up from 259). Need plans to make weaker mobs give more exp at lower levels.

TODO: Make exp automatically implement the lvl 80+ convertion.

For now: consider ATTR_ EXP adjusts it automatically.

TODO: Adjusting ATTR_LV should adjust the current/next lvl exp. Adjusting it by a decimal number should
adjust current exp by percent. Might need another ATTR just for that.

For now: consider ATTR_LV adjusts it automatically.

Strategy with chance lists:
- Put most likely targets first.
- Use a lfunc.case list.
- Keep it in a single list.
- Use percentages to see the chances easier.

--]]

ii['King Crab']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000044")
	local l = s.ps[ATTR_LV] -- not for setting!
	if l >= 86 then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000073"))
	end
	
	s.ps[ATTR_LV] = lfunc.case(
		{(l > 1) and (l < 10),   l + 5},
		{(l >= 10) and (l < 30), l + 3},
		{(l >= 30) and (l < 60), l + 1},
		{(l >= 60) and (l < 76), l + (1/2)},
		{(l >= 76) and (l < 86), l + (1/3)}
	)
end


ii['Illusionary Certificate']['func_use'] = function(s)
	--CHANGED: Automatically uses player pointer, not boat. Message not necessary. Others will only list comment for message deleted.
	--if GetCtrlBoat(s.p.role) ~= nil then --cannot be boat
	--	return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000378"))
	--end
	
	if(s.ps[ATTR_LV]>=20 and s.ps[ATTR_LV]<=54) then
		s.ps[ATTR_LV]=55
		Notice(GetResString("CALCULATE_ITEMEFFECT_LUA_000349") ..cha_name..GetResString("CALCULATE_ITEMEFFECT_LUA_000380")..lv ..GetResString("CALCULATE_ITEMEFFECT_LUA_000379"))
	else
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000381"))
	end
end

ii['Bear Chest']['func_use'] = function(s)
	--unknown
end

function random_from_table(_table)
	local total = 0
	for i,v in _table do
		total = total + v[1]
	end
	
	local random = math.random(1,total)
	for i,v in _table do
		if v[1] <= random then
			return _table[i]
		end
	end
	
	--it failed (nil)
end

ii['God-shaking Treasure Chest']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000356")
	if GetChaFreeBagGridNum(s.p.role) < 1 then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000357"))
	end

	local item_name = random_from_table{
		{10,	'Draco'},
		{7,	'Demon Bane Rod'},
		{7,	'Visceral'},
		{4,	'Staff of Wonders'},
		{4,	'Crag'},
		{4,	'Rainbow'},
		{4,	'Meteor Pearl'},
		{3.5,	'Blue Color Gem'},
		{3.5,	'Red Color Gem'}
	}

	GiveItem(s.p.role, 0, ii[item_name]['id'], 1, 94)
	Notice(GetResString("CALCULATE_ITEMEFFECT_LUA_000349")..GetChaDefaultName(s.p.role)..GetResString("CALCULATE_ITEMEFFECT_LUA_000358")..item_name)
end

ii['Overlord Chest']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000359")
	if not_enough_required_inventory_space(s,1) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000361"))
	end
	
	--TODO: Adjust ATTR_JOB to index starting at 1 instead of 0.
	--For now: Assume it's adjusted automatically.
	
	local gifts_per_char = {
		[tsv.classes['Champion']['id']] = 'Colossus',
		[tsv.classes['Crusader']['id']] = 'Drakan',
		[tsv.classes['Sharpshooter']['id']] = random_from_table{
			{1,	'Twilight'},
			{1,	'Blitz Thunderbolt'},
		},
		[tsv.classes['Cleric']['id']] = 'Revered Staff',
		[tsv.classes['Seal Master']['id']] = 'Crimson Rod',
		[tsv.classes['Voyager']['id']] = 'Riven Soul'
	}
	
	--add 1 to ATTR_JOB temporarily
	local item_name = _te(gifts_per_char[s.ps[ATTR_JOB]+1],nil)
	if item_name == nil then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000360"))
	end	
	GiveItem(s.p.role,0,ii[item_name]['id'],1,4)
end

ii['Doubtable Treasure Map']['func_use'] = function(s)
	Notice(s.p.role..GetResString("CALCULATE_ITEMEFFECT_LUA_000355"))
end

ii['Superman Chest']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000382")
	if not_enough_required_inventory_space(s,1) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000383"))
	end
	if s.ps[ATTR_LV]>=70 then 
		GiveItem(s.p.role,0,ii['Superman Certificate']['id'],1,4)
		Notice(GetResString("CALCULATE_ITEMEFFECT_LUA_000349")..GetChaDefaultName(s.p.role)..GetResString("CALCULATE_ITEMEFFECT_LUA_000384"))
	else
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000385"))
	end
end

ii['Experience Potion Package']['func_use'] = function(s)
	if not_enough_required_inventory_space(s,1) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000377"))
	end
	local role = s.p.role
	local r1 = 0
	local r2 = 0
	r1,r2 =MakeItem(role,ii['Experience Flask']['id'],1,4)
	item_wrap(GetChaItem(role,2,r2)).stats[ITEMATTR_VAL_PARAM1] = 0
	SynChaKitbag(role,13)
end

ii['Chest of Kylin']['func_use'] = function(s)
	local role = s.p.role -- it's already a player pointer
	local gifts = {'Kylin Armor', 'Kylin Gloves', 'Kylin Boots'}
	local extra_gifts_per_char = {{},{},{},{'Kylin Cap'}}
	
	--CHANGED/BUGFIX: Made function require Ami's to have 4 slots instead of 3.
	if not_enough_required_inventory_space(s,3+t_len(extra_gift_per_char[s.p.type_id])) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000052"))
	end

	--TODO: Make the functions only give the items if the item has the model for that char
	for v in extra_gifts_per_char[s.p.type_id] do
		GiveItem(role,0,ii[v]['id'],1,0)
	end
	for v in gifts do
		GiveItem(role,0,ii[v]['id'],1,0)
	end
end

--CHANGED: (Relic Armor Boxes) it really should give the items even to chars that can't use it (they are tradeable)
function simple_item_gift_box(gifts,quality)
	if not_enough_required_inventory_space(s,t_len(gifts)) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000053"))
	end
	--Class check failure message was here: CALCULATE_ITEMEFFECT_LUA_000054
	for v in gifts do
		GiveItem(s.p.role,0,v,1,quality)
	end
end

--CHANGED: (Relic Armor Boxes) it really should give the items even to chars that can't use it (they are tradeable)
function item_gift_box_per_char(gifts_per_char,quality)
	if not_enough_required_inventory_space(s,t_len(gifts_per_char[s.p.type_id])) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000053"))
	end
	if t_len(gifts_per_char[p.type_id]) == 0 then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000054"))
	end
	
	for v in gifts_per_char[p.type_id] do
		GiveItem(s.p.role,0,v,1,quality)
	end
end

ii['Relic Armor Box (Night)']['func_use'] = function(s)
	local gifts = {'Armor of Night', 'Gloves of Night', 'Boots of Night'}
	return item_gift_box_per_char({[1] = gifts, [3] = gifts},95)
end

ii['Relic Armor Box (Hercules)']['func_use'] = function(s)
	return item_gift_box_per_char({[2] = {'Armor of Hercules', 'Gauntlets of Hercules', 'Greaves of Hercules'}},95)
end

ii['Relic Armor Box (Progeny)']['func_use'] = function(s)
	local gifts = {'Vest of Marksmanship', 'Gloves of Marksmanship', 'Boots of Marksmanship'}
	return item_gift_box_per_char({[1] = gifts, [3] = gifts},95)
end

ii['Relic Armor Box (Wise/Panda)']['func_use'] = function(s)
	return item_gift_box_per_char({
		[3] = {'Robe of the Wise', 'Gloves of the Wise', 'Boots of the Wise'},
		[4] = {'Happy Panda Cap', 'Happy Panda Costume', 'Happy Panda Gloves', 'Happy Panda Shoes'}
	},95)
end

ii['Relic Armor Box (Fairy/Gold Fish)']['func_use'] = function(s)
	return item_gift_box_per_char({
		[3] = {'Fairy Robe', 'Fairy Gloves', 'Fairy Shoes'},
		[4] = {'Gold Fish Cap', 'Gold Fish Costume', 'Gold Fish Muffs', 'Gold Fish Shoes'}
	},95)
end

ii['Relic Armor Box (Fairy/Gold Fish)']['func_use'] = function(s)
	local gifts_1_3 = {'Tidal Robe', 'Tidal Gloves', 'Tidal Shoes'}
	return item_gift_box_per_char({
		[1] = gifts_1_3,
		[3] = gifts_1_3,
		[4] = {'Dragon Cap', 'Dragon Costume', 'Dragon Muffs', 'Dragon Shoes'}
	},95)
end

ii['Relic Weapon Box (Wonders)']['func_use'] = function(s)
	return simple_item_gift_box({'Staff of Wonders'},95)
end

ii['Relic Weapon Box (Demon Bane)']['func_use'] = function(s)
	return simple_item_gift_box({'Demon Bane Rod'},95)
end

ii['Relic Weapon Box (Draco)']['func_use'] = function(s)
	return simple_item_gift_box({'Draco'},95)
end

ii['Relic Weapon Box (Crag)']['func_use'] = function(s)
	return simple_item_gift_box({'Crag'},95)
end

ii['Relic Weapon Box (Rainbow)']['func_use'] = function(s)
	return simple_item_gift_box({'Rainbow'},95)
end

ii['Relic Weapon Box (Meteor)']['func_use'] = function(s)
	return simple_item_gift_box({'Meteor Pearl'},95)
end

ii['Relic Weapon Box (Visceral)']['func_use'] = function(s)
	return simple_item_gift_box({'Visceral'},95)
end

local fairy = {}

fairy.max_level = 63

function fairy.level(s)
	--CHANGED: Added LUK.
	return s.iDs[ITEMATTR_VAL_STR]+s.iDs[ITEMATTR_VAL_AGI]+s.iDs[ITEMATTR_VAL_CON]+s.iDs[ITEMATTR_VAL_ACC]+s.iDs[ITEMATTR_VAL_SPR]+s.iDs[ITEMATTR_VAL_LUK]
end

function fairy.is_maxed(s)
	return fairy.level(s) >= fairy.max_level
end

function fairy.level_up(s, fairy_fruit_stat_type_increase, fairy_fruit_level_increase)
	if fairy.is_maxed(s) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000080")..fairy.max_level..GetResString("CALCULATE_ITEMEFFECT_LUA_000079"))
	end
	
	if  (GetItemType(s.i) == tsv.itemtype['Fairy fruit']['id']) and
		(GetItemType(s.iD) == tsv.itemtype['Fairy']['id']) then
		if s.iDs[ITEMATTR_ENERGY] < s.iDs[ITEMATTR_MAXENERGY] then
			return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000082"))
		end
		
		--Leveling up fairy
		
		--implementing ElfLVUP function here
		--ElfLVUP(s.p.role,Item_Traget,fairy_fruit_stat_type_increase,1,0)

		--CHANGES:
		---Removing the fairy upgrade failure chance (which would make fairy lose 1/2 of that level exp).
		---Removing choice to fail completely (losing all exp for that level)
		---TODO: Make process much longer instead - adjust MAXENERGY.
		
		AddItemEffect(s.p.role,s.i.pointer,0) --make fairy disappear for now
		
		SystemNotice(s.p.role, GetResString("CALCULATE_FUNCTIONS_LUA_000129"))
		
		s.iDs[fairy_fruit_stat_type_increase] = s.iDs[fairy_fruit_stat_type_increase] + fairy_fruit_level_increase
		s.iDs[ITEMATTR_MAXENERGY] = math.min(240 * fairy.level(s),6480) --increase level up experience needed - fairy level here is automatically updated
		s.iDs[ITEMATTR_MAXURE] = math.min(s.iDs[ITEMATTR_MAXURE] + 1000*lvupType,32000) --increase maximum durability of fairy
		
		ResetItemFinalAttr(s.iD.pointer) --update fairy stats
		AddItemEffect(s.p.role,s.iD.pointer,1) --make fairy reappear
			--failure messages were here:
			--Failure if fruit was "false": CALCULATE_FUNCTIONS_LUA_000130.
			--Failure if upgrade chance failed: CALCULATE_FUNCTIONS_LUA_000131 (it also reduced current exp to half).
		s.iDs[ITEMATTR_ENERGY] = 0 --set current exp to 0
	end
	--TODO: make max energy vary by chance.
end

ii['Snow Dragon Fruit']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000044")
	fairy.level_up(s, ITEMATTR_VAL_STR, 1)
end

ii['Icespire Plum']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000044")
	fairy.level_up(s, ITEMATTR_VAL_AGI, 1)
end

ii['Zephyr Fish Floss']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000044")
	fairy.level_up(s, ITEMATTR_VAL_ACC, 1)
end

ii['Argent Mango']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000044")
	fairy.level_up(s, ITEMATTR_VAL_CON, 1)
end

ii['Shaitan Biscuit']['func_use'] = function(s)
	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000044")
	fairy.level_up(s, ITEMATTR_VAL_SPR, 1)
end

--ii['Unknown LUK fruit']['func_use'] = function(s)
--	--Boat check failure message was here: GetResString("CALCULATE_ITEMEFFECT_LUA_000044")
--	fairy.level_up(s, ITEMATTR_VAL_LUK, 1)
--end

function fairy.ration(s, Num)
	--TODO: Make breaking/unbreaking item process automatic.
	if fairy.is_maxed(s) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000080")..fairy.max_level..GetResString("CALCULATE_ITEMEFFECT_LUA_000079"))
	end
	
	if  (GetItemType(s.i) == tsv.itemtype['Fairy fruit']['id']) and
		(GetItemType(s.iD) == tsv.itemtype['Fairy']['id']) then
		if s.iDs[ITEMATTR_URE] < s.iDs[ITEMATTR_MAXURE] then
			if s.iDs[ITEMATTR_URE] == 0 then --CHANGED: from 49 -- if it's in "broken" state, make it unbroken.
				SetChaKbItemValid2(s.p.role, s.iD.pointer, 1, 0) --make item useable maybe... but dunno what that 0 is.
			end
			s.iDs[ITEMATTR_URE] = math.min(math.max(0,s.iDs[ITEMATTR_URE]+2500),s.iDs[ITEMATTR_MAXURE])
   	 	else
			return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000081"))
		end
	end
end

ii['Fairy Ration']['func_use'] = function(s)
	return fairy.ration(s, 2500)
end

ii['Open Sesame']['func_use'] = function(s)
	if not_enough_required_inventory_space(s,1) then
		return failure_system_notice(s,GetResString("CALCULATE_ITEMEFFECT_LUA_000118"))
	end
	
	local r1,r2 =MakeItem(s.p.role, ii['Fairy of Life']['id'],1,4) --CHANGED C1 = 183
	
	--TODO: Continue here.
	local Item_newJL = GetChaItem (s.p.role, 2, r2)			--取新精灵道具指针
	local Item_newJLID = GetItemID ( Item_newJL )			--取新精灵道具指针


	local str_JLone = GetItemAttr( Item_newJL ,ITEMATTR_VAL_STR )       --力量
	local con_JLone = GetItemAttr( Item_newJL ,ITEMATTR_VAL_CON )       --体质
	local agi_JLone = GetItemAttr( Item_newJL ,ITEMATTR_VAL_AGI )       --专注
	local dex_JLone = GetItemAttr( Item_newJL ,ITEMATTR_VAL_DEX )       --敏捷
	local sta_JLone = GetItemAttr( Item_newJL ,ITEMATTR_VAL_STA )       --精神

	local Num_JL = GetItemForgeParam ( Item_newJL , 1 )
	Num_JL = TansferNum ( Num_JL )
	local Part1_JL = GetNum_Part1 ( Num_JL )	--Get Num Part 1 到 Part 7
	local Part2_JL = GetNum_Part2 ( Num_JL )	
	local Part3_JL = GetNum_Part3 ( Num_JL )
	local Part4_JL = GetNum_Part4 ( Num_JL )
	local Part5_JL = GetNum_Part5 ( Num_JL )
	local Part6_JL = GetNum_Part6 ( Num_JL )
	local Part7_JL = GetNum_Part7 ( Num_JL )
	if Item_newJLID==231 or Item_newJLID==232 or Item_newJLID==233 or Item_newJLID==234 or Item_newJLID==235  or Item_newJLID==236  or Item_newJLID==237  or Item_newJLID==681 then
		Part1_JL=1
		Num_JL = SetNum_Part1 ( Num_JL , 1 ) ----------二转标记
		SetItemForgeParam ( Item_newJL , 1 , Num_JL )
	end
	str_JLone	= N1
	con_JLone = N2
	agi_JLone	= N3
	dex_JLone = N4
	sta_JLone	= N5
	local new_lv=N1+N2+N3+N4+N5
	local new_MAXENERGY = 240 * ( new_lv + 1 )
	if new_MAXENERGY > 6480 then
		new_MAXENERGY = 6480
	end
	local new_MAXURE = 5000 + 1000*new_lv
	if new_MAXURE > 32000 then
		new_MAXURE = 32000
	end
	SetItemAttr ( Item_newJL ,ITEMATTR_VAL_STR , str_JLone )
	SetItemAttr ( Item_newJL ,ITEMATTR_VAL_CON , con_JLone )
	SetItemAttr ( Item_newJL ,ITEMATTR_VAL_AGI , agi_JLone )
	SetItemAttr ( Item_newJL ,ITEMATTR_VAL_DEX , dex_JLone)
	SetItemAttr ( Item_newJL ,ITEMATTR_VAL_STA , sta_JLone)
	SetItemAttr ( Item_newJL , ITEMATTR_MAXENERGY , new_MAXENERGY ) 	
	SetItemAttr ( Item_newJL , ITEMATTR_MAXURE , new_MAXURE )
	SetItemAttr ( Item_newJL , ITEMATTR_ENERGY , new_MAXENERGY ) 	
	SetItemAttr ( Item_newJL , ITEMATTR_URE , new_MAXURE )
	local cha_name = GetChaDefaultName ( role )
	--LG( "star_JLZS_lg" ,cha_name,Item_JLone_ID , lv_JLone , str_JLone , con_JLone , agi_JLone , dex_JLone , sta_JLone , Item_JLother_ID , lv_JLother  , str_JLother , con_JLother , agi_JLother , dex_JLother , sta_JLother )
	LG( "star_CJBOX" ,cha_name, C1 , N1 , N2 ,  N3 , N4 , N5 )
end