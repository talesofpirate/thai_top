packet_mt = {}
packet = {pointer=0}

function packet.new(self, pointer, o)
	o = o or {}
	if pointer == nil then
		pointer = GetPacket()
	end
	o.pointer = pointer
	setmetatable(o, self)
	self.__index = self
	return o
end

function packet.add(self, datatype, data) --might be best if it wasn't in 1 function for performance,
	--but the way it's implemented is horrible
	if (datatype == DT_CMD) then
		WriteCmd(self.pointer, data)
	end
	if (datatype == DT_BN) then --why rename into 1BN? Because Intel's wording for bytes is horrible,
		--true data have diverse lengths, so no naming is needed for those over 1, except for its size.
		WriteByte(self.pointer, data)
	end
	if (datatype == DT_2BN) then
		WriteWord(self.pointer, data)
	end
	if (datatype == DT_4BN) then
		WriteDword(self.pointer, data)
	end
	if (datatype == DT_STR) then
		WriteString(self.pointer, data)
	end
end

function packet.rem(self, datatype) --remember, it works from last to first order.
	if (datatype == DT_CMD) then
		return ReadCmd(self.pointer)
	end
	if (datatype == DT_BN) then
		return ReadByte(self.pointer)
	end
	if (datatype == DT_2BN) then
		return ReadWord(self.pointer)
	end
	if (datatype == DT_4BN) then
		return ReadDword(self.pointer)
	end
	if (datatype == DT_STR) then
		return ReadString(self.pointer)
	end
end

function packet.send(self, player)
	SendPacket(player.role, self.pointer)
end

function packet.syn(self, player)
	SynPacket(player.role, self.pointer)
end