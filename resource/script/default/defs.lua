print('Loading constants') -- also a couple of function shortcuts

oo = lfunc.ocompose
r = lfunc.reiterate
a = lfunc.apply
nop = function(x) return x end
c = lfunc.curry
m = lfunc.map

--[[
TRUE and FALSE are different in C and lua (thus made mention on next constants):
-TRUE:      C=any value but 0.  LUA=any value but nil.
-FALSE:     C=0.                LUA=nil.
--]]
LUA_TRUE 			= 1
LUA_FALSE			= 0
LUA_ERROR			= -1
LUA_NULL			= 0

C_TRUE 			= 1
C_FALSE			= 0
C_ERROR			= -1
C_NULL			= 0

--ï¿½ï¿½ï¿½ï¿½ÏµÍ³ï¿½Å±ï¿½
TE_MAPINIT		= 0			--ï¿½ï¿½ï¿½ï¿½
TE_NPC				= 1			--npcÐ¯ï¿½ï¿½
TE_KILL				= 2			--ï¿½Ý»ï¿½ï¿½ï¿½ï¿½
TE_GAMETIME		= 3			--ï¿½ï¿½Ï·Ê±ï¿½ï¿½
TE_CHAT			= 4			--ï¿½ï¿½ï¿½ï¿½Ø¼ï¿½ï¿½ï¿½
TE_GETITEM		= 5			--Ê°È¡ï¿½ï¿½Æ·
TE_EQUIPITEM	= 6			--×°ï¿½ï¿½ï¿½ï¿½Æ·
TE_GOTO_MAP    = 7			--ï¿½ï¿½ï¿½ï¿½Ä¿ï¿½ï¿½ï¿½	ï¿½ï¿½ï¿½ï¿½É«ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½æ´¢ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½1ï¿½ï¿½ï¿½ï¿½Í¼IDï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½2ï¿½ï¿½ï¿½ï¿½Í¼ï¿½ï¿½ï¿½ï¿½radom ï¿½Ê£ï¿½ï¿½ï¿½ï¿½ï¿½3ï¿½ï¿½ï¿½ï¿½Í¼ï¿½ï¿½ï¿½xï¿½ï¿½ï¿½ï¿½ï¿½ï¿½4, ï¿½ï¿½Í¼ï¿½ï¿½ï¿½y
TE_LEVELUP      	= 8			--ï¿½ï¿½			ï¿½ï¿½ï¿½ï¿½É«ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½æ´¢ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½1ï¿½ï¿½ï¿½Ç·ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ø±Õ£ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ø±Õ£ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½2ï¿½ï¿½ï¿½Ç·ï¿½Ã¿ï¿½ï¿½ï¿½È¼ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½3ï¿½ï¿½Ö¸ï¿½ï¿½ï¿½È¼ï¿½ï¿½ï¿½ï¿½ï¿½

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ê±ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
TT_CYCLETIME	= 0			--Ê±ï¿½ï¿½Ñ­ï¿½ï¿½
TT_MULTITIME	= 1			--Ñ­ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½nï¿½ï¿½

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ð¯ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
TR_MAXNUM_CONDITIONS		= 12
TR_MAXNUM_ACTIONS			= 12

--npcï¿½ï¿½ï¿½ï¿½×´Ì¬ï¿½ï¿½Ï¢ï¿½ï¿½ï¿½ï¿½
MIS_ACCEPT       = 1			--ï¿½Ð·ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ô½Óµï¿½ï¿½ï¿½ï¿½ï¿½
MIS_DELIVERY     = 2			--ï¿½ï¿½ï¿½ï¿½É¿É½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_PENDING		= 4			--ï¿½ï¿½ï¿½ï¿½É¿É½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_IGNORE		= 8			--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ô½Óµï¿½ï¿½ï¿½ï¿½ï¿½

--ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½Ò³ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_PREV			= 0			--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½ï¿½ï¿½ï¿½Ò»Ò³ï¿½ï¿½Ï¢
MIS_NEXT			= 1			--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½ï¿½ï¿½ï¿½Ò»Ò³ï¿½ï¿½Ï¢
MIS_PREV_END	= 2			--ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½Ã»ï¿½ï¿½ï¿½ï¿½Ò»Ò³ï¿½ï¿½Ï¢
MIS_NEXT_END	= 3			--ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½Ã»ï¿½ï¿½ï¿½ï¿½Ò»Ò³ï¿½ï¿½Ï¢
MIS_SEL				= 4			--ï¿½ï¿½ï¿½ï¿½Ñ¡ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½ï¿½ï¿½Ä¿
MIS_TALK			= 5			--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ô»ï¿½ï¿½ï¿½Ï¢
MIS_BTNACCEPT	= 6			--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_BTNDELIVERY= 7			--ï¿½ï¿½ï¿½ó½»¸ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_BTNPENDING	= 8			--Î´ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ó£¨¿Í»ï¿½ï¿½Ë½ï¿½Ö¹ï¿½ï¿½Å¥ï¿½ï¿½
MIS_LOG			= 9			--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ö¾ï¿½ï¿½Ï¢

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢ï¿½ï¿½ï¿½ï¿½
--ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Í¶ï¿½ï¿½ï¿½
MIS_NEED_ITEM		= 0		--ï¿½ï¿½Òªï¿½ï¿½È¡ï¿½ï¿½Æ·
MIS_NEED_KILL		= 1		--ï¿½ï¿½Òªï¿½Ý»ï¿½ï¿½ï¿½ï¿½
MIS_NEED_SEND		= 2        --ï¿½ï¿½Òªï¿½Í¸ï¿½Ä³ï¿½ï¿½
MIS_NEED_CONVOY	= 3 		--ï¿½ï¿½Òªï¿½ï¿½ï¿½Íµï¿½Ä³ï¿½ï¿½
MIS_NEED_EXPLORE = 4		--ï¿½ï¿½ÒªÌ½ï¿½ï¿½Ä³ï¿½ï¿½
MIS_NEED_DESP		= 5		--ï¿½ï¿½ï¿½Ö±ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ä¿ï¿½ï¿½

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Í¶ï¿½ï¿½ï¿½
MIS_PRIZE_ITEM		= 0		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Æ·
MIS_PRIZE_MONEY	= 1		--ï¿½ï¿½ï¿½ï¿½ï¿½Ç®
MIS_PRIZE_FAME		= 2		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_PRIZE_CESS		= 3		--ï¿½ï¿½ï¿½ï¿½Ã³ï¿½ï¿½Ë°ï¿½ï¿½
MIS_PRIZE_PETEXP  = 4		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¾­ï¿½ï¿½Öµ

--ï¿½ï¿½É«Ö°Òµï¿½ï¿½ï¿½ï¿½
MIS_NOVICE			= 0		--ï¿½ï¿½ï¿½ï¿½
MIS_FENCER			= 1		--ï¿½ï¿½Ê¿
MIS_HUNTER			= 2		--ï¿½ï¿½ï¿½ï¿½
MIS_EXPERIENCED	= 2		--Ë®ï¿½ï¿½
MIS_RISKER			= 4		--Ã°ï¿½ï¿½ï¿½ï¿½
MIS_DOCTOR			= 5		--Ò½ï¿½ï¿½(ï¿½ï¿½Ô¸Ê¹)
MIS_TECHNICIAN		= 6		--ï¿½ï¿½Ê¦
MIS_TRADER			= 7		--ï¿½ï¿½ï¿½ï¿½
MIS_LARGE_FENCER  = 8		--ï¿½Þ½ï¿½Ê¿
MIS_TWO_FENCER    = 9		--Ë«ï¿½ï¿½Ê¿
MIS_SHIELD_FENCER = 10		--ï¿½ï¿½ï¿½ï¿½Ê¿
MIS_WILD_ANIMAL_TRAINER = 11 --Ñ±ï¿½ï¿½Ê¦
MIS_GUNMAN			= 12      --ï¿½Ñ»ï¿½ï¿½ï¿½
MIS_CLERGY			= 13		--Ê¥Ö°ï¿½ï¿½
MIS_SEALER			= 14		--ï¿½ï¿½Ó¡Ê¦
MIS_SHIPMASTER	= 15		--ï¿½ï¿½ï¿½ï¿½
MIS_VOYAGE			= 16		--ï¿½ï¿½ï¿½ï¿½Ê¿
MIS_ARRIVISTE		= 17		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_ENGINEER			= 18		--ï¿½ï¿½ï¿½ï¿½Ê¦

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_GUILD_NAVY		= 0		--ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_GUILD_PIRATE   = 1		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ë·ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢Ð­ï¿½é¶¨ï¿½ï¿½
CMD_MC_BASE				= 500
CMD_MC_ROLEBASE 			= CMD_MC_BASE + 300
CMD_MC_TALKPAGE 			= CMD_MC_ROLEBASE + 1 --
CMD_MC_FUNCPAGE 		= CMD_MC_ROLEBASE + 2 --
CMD_MC_CLOSETALK 		= CMD_MC_ROLEBASE + 3 --
CMD_MC_HELPINFO			= CMD_MC_ROLEBASE + 4 --
CMD_MC_TRADEPAGE		= CMD_MC_ROLEBASE + 5 --
CMD_MC_TRADERESULT	= CMD_MC_ROLEBASE + 6
CMD_MC_TRADE_DATA		= CMD_MC_ROLEBASE + 7 --
CMD_MC_TRADE_ALLDATA	= CMD_MC_ROLEBASE + 8 --

CMD_MC_MISSION			= CMD_MC_ROLEBASE + 22 --
CMD_MC_MISSIONLIST		= CMD_MC_ROLEBASE + 23
CMD_MC_MISSIONTALK		= CMD_MC_ROLEBASE + 24
CMD_MC_MISPAGE			= CMD_MC_ROLEBASE + 27 --
CMD_MC_MISLOG				= CMD_MC_ROLEBASE + 28
CMD_MC_MISLOGINFO		= CMD_MC_ROLEBASE + 29 --
CMD_MC_BEGIN_ITEM_FORGE   = CMD_MC_ROLEBASE + 35
CMD_MC_BEGIN_ITEM_UNITE   = CMD_MC_ROLEBASE + 36

CMD_MC_CREATEBOAT		= CMD_MC_ROLEBASE + 38
CMD_MC_UPDATEBOAT		= CMD_MC_ROLEBASE + 39
CMD_MC_UPDATEBOAT_PART	= CMD_MC_ROLEBASE + 40
CMD_MC_BERTH_LIST		= CMD_MC_ROLEBASE + 41
CMD_MC_BOAT_LIST		= CMD_MC_ROLEBASE + 42
CMD_MC_BOAT_ADD			= CMD_MC_ROLEBASE + 43
CMD_MC_BOAT_CLEAR		= CMD_MC_ROLEBASE + 44
CMD_MC_BOATINFO			= CMD_MC_ROLEBASE + 45
CMD_MC_BOAT_BAGLIST	= CMD_MC_ROLEBASE + 46

CMD_MC_TALK             = CMD_MC_BASE + 1

--ï¿½Í»ï¿½ï¿½Ë·ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢Ð­ï¿½é¶¨ï¿½ï¿½
CMD_CM_BASE				= 0
CMD_CM_ROLEBASE 			= CMD_CM_BASE + 300
CMD_CM_REQUESTTALK 	= CMD_CM_ROLEBASE + 1
CMD_CM_TALKPAGE			= CMD_CM_ROLEBASE + 2 --
CMD_CM_FUNCITEM 		= CMD_CM_ROLEBASE + 3 --
CMD_CM_REQESTTRADE    = CMD_CM_ROLEBASE + 8
CMD_CM_TRADEITEM		= CMD_CM_ROLEBASE + 9 --
CMD_CM_REQUESTAGENCY= CMD_CM_ROLEBASE + 10
CMD_CM_MISSION			= CMD_CM_ROLEBASE + 22 --
CMD_CM_MISSIONLIST		= CMD_CM_ROLEBASE + 23
CMD_CM_MISSIONTALK		= CMD_CM_ROLEBASE + 24
CMD_CM_MISLOG				= CMD_CM_ROLEBASE + 25

CMD_MC_BLACKMARKET_EXCHANGEDATA	= CMD_MC_BASE + 71 --
CMD_MC_BLACKMARKET_EXCHANGEUPDATE = CMD_MC_BASE + 73 --
CMD_MC_BLACKMARKET_TRADEUPDATE = CMD_MC_BASE + 74 --
CMD_MC_EXCHANGEDATA = CMD_MC_BASE + 75 --
CMD_CM_BLACKMARKET_EXCHANGE_REQ = CMD_CM_BASE + 51 --


--ï¿½Ô»ï¿½Ò³ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢
ROLE_FIRSTPAGE				= 0		-- ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Î¶Ô»ï¿½ï¿½ï¿½Ï¢
ROLE_CLOSEPAGE			= -1		-- ï¿½ï¿½ï¿½ï¿½Ø±Õ¶Ô»ï¿½Ò³ï¿½ï¿½ï¿½ï¿½

--ï¿½ï¿½ï¿½×ºï¿½ï¿½ï¿½Ï¢ï¿½ï¿½ï¿½ï¿½
ROLE_MAXNUM_TRADEITEM = 60	--Ã¿ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ò³ï¿½ï¿½ï¿½Ú·ï¿½ï¿½ï¿½Æ·ï¿½ï¿½ï¿½ï¿½
ROLE_INVALID_ID				= -1		--ï¿½ï¿½Ð§ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Æ·ï¿½ï¿½ï¿½Í¶ï¿½ï¿½ï¿½
WEAPON 						= 0		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
DEFENCE 						= 1 		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
OTHER    						= 2		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
SYNTHESIS 					= 3		--ï¿½Ï³ï¿½ï¿½ï¿½Æ·ï¿½ï¿½ï¿½ï¿½

TRADE_SALE					= 0       --ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Æ·ï¿½ï¿½ï¿½ï¿½
TRADE_BUY						= 1       --ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Æ·ï¿½ï¿½ï¿½ï¿½
TRADE_GOODS				= 2 		--ï¿½ï¿½ï¿½×´ï¿½ï¿½Õ»ï¿½ï¿½ï¿½
TRADE_EXCHANGE = 3 -- just to merge exchange functionality in 1 class.

ROLE_TRADE_SALE				= 0	--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Æ·
ROLE_TRADE_BUY				= 1	--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Æ·
ROLE_TRADE_SALE_GOODS 	= 2	--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Û»ï¿½ï¿½ï¿½ï¿½ï¿½Æ·
ROLE_TRADE_BUY_GOODS		= 3	--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Æ·
ROLE_TRADE_SELECT_BOAT   = 4   --Ñ¡ï¿½ï¿½ï¿½×µÄ´ï¿½Ö»

--ï¿½ï¿½Ö»ï¿½ï¿½Ê¾ï¿½Ð±ï¿½ï¿½ï¿½ï¿½ï¿½
BERTH_TRADE_LIST				= 0	-- ï¿½ï¿½ï¿½×´ï¿½Ö»ï¿½Ð±ï¿½
BERTH_LUANCH_LIST			= 1	-- ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ö»ï¿½Ð±ï¿½
BERTH_REPAIR_LIST				= 3	-- ï¿½ï¿½Ö»ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½
BERTH_SALVAGE_LIST			= 4	-- ï¿½ï¿½Ö»ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½
BERTH_SUPPLY_LIST			= 5	-- ï¿½ï¿½Ö»ï¿½ï¿½ï¿½ï¿½ï¿½Ð±ï¿½
BERTH_BOATLEVEL_LIST		= 6	-- ï¿½ï¿½Ö»ï¿½ï¿½ï¿½Ð±ï¿½

--ï¿½ï¿½Ô´ï¿½ï¿½ï¿½ï¿½
RES_WOOD							= 0	--Ä¾ï¿½ï¿½ï¿½ï¿½Ô´
RES_MINE							= 1	--ï¿½ï¿½Ê¯ï¿½ï¿½Ô´

--ï¿½Ô»ï¿½ï¿½ê¶¨ï¿½ï¿½
ROLE_MAXNUM_PAGEITEM	= 9

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢ï¿½ï¿½ï¿½ï¿½
MIS_HELP_DESP				= 0		--ï¿½ï¿½ï¿½Ö°ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢
MIS_HELP_IMAGE				= 1		--Í¼ï¿½Î°ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢
MIS_HELP_SOUND			= 2		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢
MIS_HELP_BICKER			= 3		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢

--Datatypes
DT_CMD = 0
DT_BN = 1 --Byte Number
DT_2BN = 2 --2-Byte Number
DT_4BN = 3 --4-Byte Number
DT_STR = 4

--npcï¿½ï¿½ï¿½ï¿½×´Ì¬ï¿½ï¿½Ï¢ï¿½ï¿½ï¿½ï¿½
QUEST_ACTION_ACCEPT       = 1			--ï¿½Ð·ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ô½Óµï¿½ï¿½ï¿½ï¿½ï¿½
QUEST_ACTION_DELIVERY     = 2			--ï¿½ï¿½ï¿½ï¿½É¿É½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
QUEST_ACTION_PENDING		= 4			--ï¿½ï¿½ï¿½ï¿½É¿É½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
QUEST_ACTION_IGNORE		= 8			--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ô½Óµï¿½ï¿½ï¿½ï¿½ï¿½

PRIZE_SELONE          = 0
PRIZE_SELALL           = 1

QUEST_TYPE_NORMAL   = 0
QUEST_TYPE_RANDOM   = 1
QUEST_TYPE_GLOBAL   = 2

QUEST_EVENT_LOG = 0
QUEST_EVENT_START = 1
QUEST_EVENT_ONGOING = 2
QUEST_EVENT_END = 3

ALWAYS_SHOW		 	= 0
COMPLETE_SHOW   		= 1
ACCEPT_SHOW			= 2

MIS_PREV			= 0
MIS_NEXT			= 1
MIS_PREV_END	= 2
MIS_NEXT_END	= 3
MIS_SEL				= 4
MIS_TALK			= 5
MIS_BTNACCEPT	= 6
MIS_BTNDELIVERY= 7
MIS_BTNPENDING	= 8
MIS_LOG			= 9

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_RAND_KILL		  = 0   	--ï¿½Ý»ï¿½ï¿½ï¿½ï¿½
MIS_RAND_GET		  = 1		--ï¿½ï¿½È¡ï¿½ï¿½Æ·
MIS_RAND_SEND		  = 2		--ï¿½ï¿½ï¿½ï¿½Æ·
MIS_RAND_CONVOY    = 3		--ï¿½ï¿½ï¿½ï¿½NPC
MIS_RAND_EXPLORE	  = 4		--Ì½ï¿½ï¿½ï¿½ï¿½Í¼

--ï¿½ï¿½ï¿½ï¿½NPCï¿½ï¿½ï¿½ï¿½Ä¿ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_CONVOY_NPC	  = 0		--ï¿½ï¿½ï¿½Í½ï¿½ï¿½ï¿½NPC
MIS_CONVOY_MAP	  = 1		--ï¿½ï¿½ï¿½Íµï¿½Ä³ï¿½ï¿½Ö¸ï¿½ï¿½ï¿½Øµï¿½

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_TRIGGER_NOMAL	= 0	 --ï¿½ï¿½Í¨
MIS_TRIGGER_RAND		= 1	 --ï¿½ï¿½ï¿½(ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ð£ï¿½Ê¹ï¿½ï¿½Ê±ï¿½ï¿½Ý´ï¿½ï¿½ÝµÄ²ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ã´ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ï¢)

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Ã¿ï¿½ï¿½ï¿½Î±ï¿½ï¿½ï¿½ï¿½ï¿½Éµï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Å¿ï¿½ï¿½Ô²ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½Æ·
MIS_RAND_MAXCOMPLETE = 1

--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½È¼ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_LEVEL_CHAR 	= 0	    --ï¿½ï¿½É«ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½È¼ï¿½ï¿½ï¿½ï¿½ï¿½
MIS_LEVEL_GANG 	= 1		--ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½È¼ï¿½ï¿½ï¿½ï¿½ï¿½

--

SK_DPSL		=	73					--???????????
SK_DUALSHOT		=	90					--??????????
SK_METEORSHOWER		=	112					--???????????
SK_HOWL		=	107					--???????
SK_SSD		=	114					--???????
SK_CTD		=	115					--????????
SK_DZY		=	117					--?????????
SK_TIGERROAR		=	127					--??????
SK_MAGMABULLET		=	113					--???????
SK_GREATSWORDMASTERY	=	67					--??????????
SK_BERSERK		=	84					--?????
SK_DUALSWORDMASTERY	=	109					--???????????.............
SK_SEALOFELDER	=	104					--??????????
SK_YMSL		=	108					--???????????
--SK_BC		=	84					--???????
SK_CLXZ		=	76					--???????????
SK_TEMPESTBOOST		=	101					--??????????
SK_RANGEMASTERY		=	74					--???????????

SK_HEALINGSPRING		=	122					--?????????
SK_TORNADOSWIRL		=	102					--????????
SK_JFZ		=	64					--????????
SK_QXYJ		=	65					--?????????
SK_SHTZ		=	71					--???????????
SK_ANGELICSHIELD		=	103					--??????'????
SK_TSQY		=	45					--??????'???
SK_CRIPPLE		=	94					--???????
SK_HEAL		=	97					--?????????
SK_SPIRITUALFIRE		=	100					--???????????
SK_CURSEDFIRE		=	119					--???????????
SK_SMYB		=	73					--????????h??
SK_STEALTH		=	123					--????????
--SK_GZ		=	86					--??????
SK_MB		=	88					--???????
SK_POISONDART		=	87					--???????
--SK_JD		=	78					?????
--SK_ZJCM		=	79					?????????ä
--SK_ZHZD		=	80					??????????
SK_ZJFT		=	110					--??????????
--SK_BSHD		=	82					??????????
SK_HXDJ		=	111					--?????????
SK_HXQJ		=	92					--???????????
SK_FIREGUNMASTERY	=	78					--??????????
--SK_JLHY		=	86					--??????????
--SK_RDGJ		=	87					--??????????
SK_HEADSHOT		=	96					--??????
SK_DIVINEGRACE		=	80					--????????
--SK_QY		=	90					???????
SK_ZF		=	91					--???????
SK_RECOVER		=	98					--????????
SK_DHFS		=	118					--?????????
--SK_LYZY		=	94					???????????
--SK_SHZG		=	95					??????????
--SK_SZWZ		=	96					??????????
--SK_XYZG		=	97					???????????

SK_TRUESIGHT		=	116					--??????????
SK_SHADOWINSIGNIA	=	105					--?????????
--SK_CLCY		=	101					?????????
--SK_HYPS		=	102					???????????
SK_SWORDMASTERY		=	62					--???????????
SK_WILLOFSTEEL		=	63					--??????????
SK_STRENGTHEN		=	64					--???????????
SK_DEFTNESS		=	65					--??????????
SK_CONCENTRATION	=	66					--?????????
SK_ILLUSIONSLASH	=	81					--???????
SK_MIGHTYSTRIKE		=	82					--???????
SK_BLOODBULL		=	68					--??????t???
SK_PRIMALRAGE		=	83					--??????????
SK_HYS		=	69					--????????
SK_BLOODFRENZY		=	70					--?????????
SK_SHADOWSLASH		=	86					--????????
SK_TZHF		=	72					--??????????
SK_WINDWALK		=	75					--???????
SK_EAGLESEYE		=	89					--???????
SK_LDC		=	91					--?????????
SK_VIGOR		=	79					--?????????
SK_SPIRITUALBOLT		=	99					--??????????
SK_FROZENARROW		=	93					--??????
SK_LRWZ		=	77					--????????a?
SK_ENFEEBLE		=	95					--???????
SK_SDBZ		=	120					--???????????
SK_ABYSSMIRE		=	121					--???????????
SK_ENERGYSHIELD	=	106					--????????????
SK_REVIVAL		=	124					--???????
ITEM_RELIFE	=	3143						--????????????

SK_TTCB		=	125					--?????????
SK_DYYJ		=	126					--?????????
SK_DILIGENCE		=	210					--???????
SK_CURRENT		=	211					--???????
SK_CONCHARMOR		=	212					--??????????
SK_TORNADO		=	213					--??????
SK_LIGHTNINGBOLT		=	214					--???????
SK_ALGAEENTANGLEMENT		=	215					--??????????
SK_CONCHRAY		=	216					--?????????
SK_TAILWIND		=	217					--???????
SK_WHIRLPOOL		=	218					--????????
SK_FOG		=	219					--????????
SK_LIGHTNINGCURTAIN		=	220					--??????L
SK_BREAKARMOR		=	222					--???????
SK_ROUSING		=	223					--???????
SK_VENOMARROW		=	224					--???????
SK_HARDEN		=	225					--??????????
SK_HPSL		=	226					--???????????
SK_JBJG		=	227					--????????
SK_CFS		=	228					--????????
SK_CTQH		=	229					--??????????
SK_BJCR		=	230					--???????????
SK_BY		=	231					--???????
SK_DL		=	232					--???????
SK_SWCX		=	234					--???????????
SK_XN		=	235					--?????u
SK_NT		=	236					--???????
SK_DIZ		=	237					--???????
SK_XIK		=	238					--???????
SK_BIW		=	239					--???????
SK_Fer		=	240					--???????
SK_BAT		=	241					--??????
SK_TAUNT		=	242					--???????
SK_ROAR		=	243					--????????
SK_FUZ		=	244					--???????
SK_HZTX		=	245					--?????????
SK_SMDJ		=	246					--?????????
SK_WZXF		=	247					--???????????
SK_SYZM		=	248					--????????????h??
SK_KDZB		=	249					--??????????
SK_SHJNY	=	250					--???????1
SK_SHJNE	=	251					--???????2
SK_BOMB  	=	252					--??????

SK_CRYSTALLINEBLESSING		=	255					--????????
SK_INTENSEMAGIC		=	256					--h?????


SK_JSDD		=	257					--??????????
SK_JSMF		=	258					--?????????
SK_HDSMF	=       259					--????h??
SK_HYMF		=       260					--????h??
SK_HYMH		=	261					--????h??-???
SK_HXMF		=	262					--????h??
SK_HXFWMF	=	263					--??????h??
SK_TZJSMagic	=	264					--???h??????
SK_QZMF		=	265					--????h??
SK_XZSB		=	266					--??????????
SK_QX		=	267					--??
SK_SD		=	268					--???
SK_BLGJ		=	269					--?????

SK_JXJBFW	=	270					--???????
SK_CRXSF	=	271					--???????
SK_SXZZZ	=	272					--?????????
SK_XBLBD	=	273					--??????
SK_BHSD		=	274					--????????
SK_HLKJ		=	275					--??????
SK_HLLM		=	276					--????-????
SK_BlackLY	=	277					--????-????
SK_BlackLX	=	278					--????-???
SK_BlackHeal	=	279					--????????
SK_FAIRYBODY	=	280					--??????
SK_SELFDESTRUCT	=	311					--???????
SK_DEFECATE			=	312				--???????1
SK_UNDERGARMENT			=	313				--???????2
SK_GARMENT			=	314				--???????3
SK_COINSHOWER			=	315				--???????4
SK_FOOL			=	316				--???????5
SK_SNOOTY			=	317				--???????6
SK_DUMB			=	318				--???????7
SK_LIAR			=	319				--???????8


SK_KS			=	200					--???????
SK_WK			=	201					--???????
SK_PKQX			=	254					--????????
SK_ZHIZAO		=	338				----------------????
SK_PENGREN		=	339				----------------????
SK_ZHUZAO		=	340				----------------???
SK_FENJIE			=	341				----------------???
SK_ETHEREALSLASH			=	453				----------------????
SK_SUPERCONSCIOUSNESS			=	454				----------------??????
SK_BEASTLEGIONSMASH			=	455				----------------?????
SK_REDTHUNDERCANNON			=	456				----------------??????
SK_DEVILSCURSE			=	457				----------------??h????
SK_HOLYJUDGEMENT			=	458				----------------???????
SK_REBIRTHMYSTICPOWER			=	459				----------------???????
SK_DS			=	461				----------------???????
SK_QLZX			=	467				----------------g????
SK_ZSZB			=	468				----------------??????
SK_NEWBIEMIGHTYSTRIKE			=	475				----------------?????????
SK_WB			=	491				----------------???
SK_YJWB			=	492				----------------?z????


SK_SIT	= 202

--

SKEFF_MAGMABULLET		=	1					--?????
SKEFF_HEALINGSPRING	=	2					--????????
SKEFF_CURSEDFIRE	=	3					--?????????
SKEFF_POISONDART		=	4					--?????
SKEFF_SDBZ	=	5					--?????????
SKEFF_TRUESIGHT	=	6					--????????
SKEFF_ABYSSMIRE	=	7					--?????????
SKEFF_LQ		=	9					--??????
SKEFF_WQ	=	10					--??????
SKEFF_FQ		=	11					--??????
SKEFF_WHIRLPOOL	=	12					--??????
SKEFF_FOG	=	13					--??????
SKEFF_LIGHTNINGCURTAIN		=	14					--????L
SKEFF_TAUNT		=	15				--??????
SKEFF_BOMB		=	16				--?????
SKEFF_PKMNYS		=	17				--????t??

SKEFF_PKZDYS		=	19				--???????
SKEFF_PKKBYS		=	20				--??????
SKEFF_DPSL	=	21					--??????????
SKEFF_TIGERROAR		=	22					--?????
SKEFF_JJSL	=	23					--?????????
SKEFF_BERSERK		=	24					--????
SKEFF_FSZ	=	84					--????????????
SKEFF_NOSKILL	=	26					--????????
SKEFF_ZMYJ	=	27					--??????h??
SKEFF_CLXZ	=	28					--??????????
SKEFF_TEMPESTBOOST	=	29					--?????????
SKEFF_GJSL	=	30					--??????????
SKEFF_HYS	=	31					--???????

SKEFF_TORNADOSWIRL	=	33					--???????
SKEFF_JFZ		=	34					--???????
SKEFF_QXYJ	=	35					--???????
SKEFF_SHTZ	=	36					--?????????
SKEFF_ANGELICSHIELD	=	37					--????'????
SKEFF_TSQY	=	38					--????'???
SKEFF_CRIPPLE		=	39					--?????
SKEFF_SPIRITUALFIRE	=	40					--?????????
SKEFF_PKJSYS	=	41					--????????
SKEFF_PKSFYS	=	42					--???????
SKEFF_STEALTH		=	43					--??????
SKEFF_PKJZYS		=	44					--???????
SKEFF_STUN		=	45					--?????
SKEFF_LIGHTNINGBOLT		=	46					--?????
SKEFF_PKWD		=	47					--?????
SKEFF_SBJYGZ		=	48				--?????????
SKEFF_SBBLGZ		=	49				--?????????
SKEFF_INTENSEMAGIC		=	50				--??h?????
SKEFF_BSHD	=	51					--?????????
SKEFF_BD		=	52					--????

SKEFF_HQSL	=	53					--?????????
SKEFF_JLHY	=	54					--?????????
SKEFF_RDGJ	=	55					--????????
SKEFF_SY		=	56					--??????
SKEFF_QY		=	57					--?????
SKEFF_ZF		=	58					--?????
SKEFF_LYZY	=	59					--?????????
SKEFF_SHZG	=	60					--????????
SKEFF_SZWZ	=	61					--????????
SKEFF_XYZG	=	62					--?????????
SKEFF_PKDYK	=	63					--???????û??
SKEFF_PKLC	=	64					--??????û??
SKEFF_NOATTACK	=	65					--?????????
SKEFF_CLCY	=	66					--????????

SKEFF_ENFEEBLE		=	68					--?????
SKEFF_JSSL	=	69					--??????????
SKEFF_WILLOFSTEEL	=	70					--?????????
SKEFF_QHTZ	=	71					--?????????
SKEFF_LQHB	=	72					--????????
SKEFF_JDZZ	=	73					--????????
SKEFF_MNRX	=	74					--????t???
SKEFF_HYS	=	75					--???????
SKEFF_PXKG	=	76					--???????
SKEFF_TZHF	=	77					--????????
SKEFF_JFB		=	78					--??????
SKEFF_EAGLESEYE		=	79					--?????
SKEFF_JSJC	=	80					--????????
SKEFF_FROZENARROW	=	81					--?????
SKEFF_LRWZ	=	82					--??????a?

SKEFF_ENERGYSHIELD	=	83					--??h????
SKEFF_JY	=	85					--??????
SKEFF_TORNADO		=	86					--????
SKEFF_ALGAEENTANGLEMENT	=	87					--?????????
SKEFF_TAILWIND		=	88					--?????
SKEFF_BREAKARMOR		=	89				--?????
SKEFF_ROUSING		=	90				--??????
SKEFF_VENOMARROW		=	91				--??????
SKEFF_HARDEN		=	92				--????????
SKEFF_XN		=	93				--???u
SKEFF_NT		=	94				--?????
SKEFF_DIZ		=	95				--??????
SKEFF_CRYSTALLINEBLESSING		=	96				--??????
SKEFF_MCK		=	97				--??'??l????
SKEFF_SWCX		=	98				--?????????
SKEFF_BAT		=	99				--?????


SKEFF_YSLLQH		=	102				--?????????
SKEFF_YSMJQH		=	103				--????????
SKEFF_YSLQQH		=	104				--???????
SKEFF_YSTZQH		=	105				--?????'??
SKEFF_YSJSQH		=	106				--????????
SKEFF_JLGLJB		=	107				--????????'??
SKEFF_HCGLJB		=	108				--??????'??
SKEFF_DENGLONG		=	109				--??????
SKEFF_MEIGUI		=	110				--õ????
SKEFF_YPCXHFSM		=	111				--????????????
SKEFF_CFZJiu1		=	112				--???????1
SKEFF_CFZJiu2		=	113				--???????2
SKEFF_JSDD		=	114				--?????
SKEFF_HYMH		=	115				--????h??-???
SKEFF_HLKJ		=	116				--????-???
SKEFF_HLLM		=	117				--????-????
SKEFF_CRXSF		=	118				--?????-??
SKEFF_MarchElf		=	119				--???¾???
SKEFF_YSMspd		=	120				--????????
SKEFF_YSBoatMspd	=	121				--???????????
SKEFF_YSBoatDEF		=	122				--????????????
SKEFF_TTISW		=	123				--????????
SKEFF_PKSBYS		=	124				--???????????????
SKEFF_BlackHX		=	125				--?????
SKEFF_ZDSBJYGZ		=	127				--????????
SKEFF_KUANGZ			=	128				--?????
SKEFF_QUANS			=	129				--??????
SKEFF_QINGZ			=	130				--?????
SKEFF_JLDS			=	131				--??????
SKEFF_FAIRYBODY1			=	132				--??????
SKEFF_CJBBT			=	133				--?????????
SKEFF_JRQKL			=	134				--?????????
SKEFF_WLRSD			=	135				--????????
SKEFF_WLJS			=	136				--???????
SKEFF_YWGJ			=	142				--??????
SKEFF_KLCS			=	138				--???ò???
SKEFF_KLHD			=	139				--???û???
SKEFF_WLCX			=	140				--?????
SKEFF_ZZZX			=	141				--??????
SKEFF_WLDB			=	142				--??????
SKEFF_WLJY			=	143				--???????
SKEFF_WLXW			=	137				--????????
SKEFF_WLNH			=	146				--????u??
SKEFF_JLJSGZ			=	147				--??????????
SKEFF_JLTX1			=	148				--???????1
SKEFF_JLTX2			=	149				--???????2
SKEFF_JLTX3			=	150				--???????3
SKEFF_JLTX4			=	151				--???????4
SKEFF_JLTX5			=	152				--???????5
SKEFF_JLTX6			=	153				--???????6
SKEFF_JLTX7			=	154				--???????7
SKEFF_JLTX8			=	155				--???????8
SKEFF_CZZX			=	156				--???????
SKEFF_KALA			=	157				--?????????
SKEFF_5MBS			=	158				--5?????
SKEFF_ShanGD			=	159				--????
SKEFF_FuShe			=	160				--????
SKEFF_PSQ			=	161				--?????????
SKEFF_PRD			=	162				--?????
SKEFF_CZRSD			=	163				--???????
SKEFF_XUEYU			=	165				--???HP????????
SKEFF_MANTOU			=	166				--????????
SKEFF_NVER			=	167				--U???????
SKEFF_FAIRYBODY2			=	168				--??????
SKEFF_FAIRYBODY3			=	169				--??????
SKEFF_FAIRYBODY4			=	170				--??????
SKEFF_FAIRYBODY5			=	171				--??????
SKEFF_FAIRYBODY6			=	172				--??????
SKEFF_FAIRYBODY7			=	173				--??????
SKEFF_FAIRYBODY8			=	174				--??????
SKEFF_FSZQ                      =       176                             --??????
SKEFF_ZYZZ                      =       177                             --??????????
SKEFF_DZFS                      =       178                             --????????
SKEFF_LD                        =       179                             --????
SKEFF_HYFS                      =       180                             --?????????
SKEFF_CZQX                      =       181                             --??????
SKEFF_LEIDA                     =       182                             --???
SKEFF_FSD                       =       183                             --?????
SKEFF_Slrs                      =       184                             --??????
SKEFF_Myrs                      =       185                             --???????
SKEFF_LST                       =       186                             --??????
SKEFF_HFZQ                      =       187                             --?????
SKEFF_DEVILSCURSE                      =       188                             --??h????
SKEFF_SUPERCONSCIOUSNESS                      =       189                             --???????
SKEFF_JHKML                     =       190                             --????û??
SKEFF_BDH                       =       191                             --??
SKEFF_DHZ		=	194				--??????????
SKEFF_DSZ		=	195				--????????
SKEFF_APPLE			=	196					----------------??????????
SKEFF_ILOVEDAD			=	197					--------------?????????
SKEFF_HPHMHF		=	198				--????????????
SKEFF_SPHMHF		=	199				--????????????
SKEFF_BBRING1		=	 200				--85BBt????
SKEFF_BBRING2		=	 201				--85BB?????
SKEFF_BBRING3		=	 202				--85BB?????
SKEFF_BBRING4		=	 203				--85BB??????
SKEFF_BBRING5		=	 204				--85BB?????
SKEFF_BBRING6		=	 205				--85BB?????
SKEFF_LANTERN		=	206				--??????
SKEFF_RAPIDDRUG		=	207				--?????
SKEFF_WARSIT		=	208				--??????
SKEFF_DARKDRESS		=	209				--???????
SKEFF_DEMON		=	210				--??h???
SKEFF_GREATSH		=	211				--??????
SKEFF_MIRAGE		=	212				--???????
SKEFF_DEVOTE		=	213				--?????
SKEFF_VIGOUR	=	214				--???????
SKEFF_SHIPDRIVE	=	215				--?????????
SKEFF_SHIPRECOVER	=	216				--????????
SKEFF_BKDB	=	217				--????????
SKEFF_XZDLL	=	218				--????????????
SKEFF_XTMFS	=	219				--?????h????        by peter 2008.7.1   ??????
SKEFF_MWMXJ	=	220				--?????I???        by peter 2008.7.1   ???????
SKEFF_FFJGD	=	221				--???L???        by peter 2008.7.1   ???????
SKEFF_YSYS	=	43				--??????????              by peter 2008.7.1  ????30??
SKEFF_CD		=	199				--??????????              by peter 2008.7.1   ???h?????????????
SKEFF_XLTX			=	224				--?????????              by Chaos 2008.7.3
SKEFF_ZSZB			=	225			----------------??????
SKEFF_CJRS		=	226					--?????
SKEFF_JLBYS		=	227					--100%????
SKEFF_HCBYS		=	228					--100%???
SKEFF_zhongshen		=	229					--95BB???
SKEFF_LEIPI	=	230				--???????
SKEFF_XIANRENJIAO = 231			--????????
SKEFF_CUSI = 232				--??????
SKEFF_GANMAO	=	233			--???????ð
SKEFF_ZBMAXHP	= 234			--U???HP????
SKEFF_ZBHP	= 235			--U???HP????
SKEFF_ZBSP	= 236			--SP????
SKEFF_ZBMOVE	= 237			--?z?????
SKEFF_GUOMIN	= 238			--???????
SKEFF_QB	= 239			--???l?
SKEFF_XZK1=240			--????
SKEFF_XZK2=241
SKEFF_XZK3=242
SKEFF_XZK4=243
SKEFF_XZK5=244
SKEFF_XZK6=245
SKEFF_XZK7=246
SKEFF_XZK8=247
SKEFF_XZK9=248
SKEFF_XZK10=249
SKEFF_XZK11=250
SKEFF_XZK12=251					--????
SKEFF_HAIDAOQI=253
SKEFF_NSTX=252				--U????????              by C.Y.DUNG 2009.3.15

--

SK_WARRIORSRAGE = 361
SKEFF_WARRIORSRAGE = 255
SK_WINDRIDERSGRACE = 362
SKEFF_WINDRIDERSGRACE = 256
SK_BLUNTINGBOLT = 355
SK_FLAMINGDART = 354
SK_EYEOFPRECISION = 356
SKEFF_EYEOFPRECISION = 258
SK_DEADEYEBLOOD = 363
SK_EXCRUCIO = 360
SKEFF_EXCRUCIO = 259
SK_PETRIFYINGPUMMEL = 357
SK_CURSEOFWEAKENING = 359
SKEFF_CURSEOFWEAKENING = 260
SK_BANEOFWANING = 358
SKEFF_BANEOFWANING = 261
SK_OMNISIMMUNITY = 353
SKEFF_OMNISIMMUNITY = 257
SK_SOULKEEPER = 352

--

npcs = {}

--TII = Table Item Info
TII_name = 2
TII_icon = 3
TII_dropmodel = 4
TII_lancemodel = 5
TII_carsisemodel = 6
TII_phyllismodel = 7
TII_amimodel = 8
TII_type = 11
TII_maxamount = 21
TII_char_lvl = 25
TII_slots = 29
TII_switchlocations = 30

TII_STRP = 32
TII_AGIP = 33
TII_ACCP = 34
TII_CONP = 35
TII_SPRP = 36
TII_LUKP = 37
TII_ASPDP = 38
TII_CRTRP = 39
TII_ATKP = 40
TII_MATKP = 41
TII_DEFP = 42
TII_MHPP = 43
TII_MSPP = 44
TII_FLEEP = 45
TII_HITP = 46
TII_CRTP = 47
TII_DROPP = 48
TII_HPRECP = 49
TII_SPRECP = 50
TII_MSPDP = 51
TII_EXPP = 52
TII_STR = 53 --min,max
TII_AGI = 54
TII_ACC = 55
TII_CON = 56
TII_SPR = 57
TII_LUK = 58
TII_ASPD = 59
TII_CRTR = 60
TII_ATK = 61
TII_MATK = 62
TII_DEF = 63
TII_MHP = 64
TII_MSP = 65
TII_FLEE = 66
TII_HIT = 67
TII_CRT = 68
TII_DROP = 69
TII_HPREC = 70
TII_SPREC = 71
TII_MSPD = 72
TII_EXP = 73
TII_MDEF = 74

TII_description = 94

ITEMATTR_COUNT_BASE0    = 0;
ITEMATTR_COE_STR        = ITEMATTR_COUNT_BASE0 + 1; -- ����ϵ��ӳɣ�strength coefficient��
ITEMATTR_COE_AGI        = ITEMATTR_COUNT_BASE0 + 2; -- ����ϵ��ӳ�
ITEMATTR_COE_ACC        = ITEMATTR_COUNT_BASE0 + 3; -- רעϵ��ӳ�
ITEMATTR_COE_CON        = ITEMATTR_COUNT_BASE0 + 4; -- ����ϵ��ӳ�
ITEMATTR_COE_SPR        = ITEMATTR_COUNT_BASE0 + 5; -- ����ϵ��ӳ�
ITEMATTR_COE_LUK        = ITEMATTR_COUNT_BASE0 + 6; -- ����ϵ��ӳ�
ITEMATTR_COE_ASPD       = ITEMATTR_COUNT_BASE0 + 7; -- ����Ƶ��ϵ��ӳ�
ITEMATTR_COE_CRTR       = ITEMATTR_COUNT_BASE0 + 8; -- ��������ϵ��ӳ�
ITEMATTR_COE_ATK        = ITEMATTR_COUNT_BASE0 + 9; -- ��С������ϵ��ӳ�
ITEMATTR_COE_MATK       = ITEMATTR_COUNT_BASE0 + 10; -- ��󹥻���ϵ��ӳ�
ITEMATTR_COE_DEF        = ITEMATTR_COUNT_BASE0 + 11; -- ����ϵ��ӳ�
ITEMATTR_COE_MXHP       = ITEMATTR_COUNT_BASE0 + 12; -- ���Hpϵ��ӳ�
ITEMATTR_COE_MXSP       = ITEMATTR_COUNT_BASE0 + 13; -- ���Spϵ��ӳ�
ITEMATTR_COE_FLEE       = ITEMATTR_COUNT_BASE0 + 14; -- ������ϵ��ӳ�
ITEMATTR_COE_HIT        = ITEMATTR_COUNT_BASE0 + 15; -- ������ϵ��ӳ�
ITEMATTR_COE_CRT        = ITEMATTR_COUNT_BASE0 + 16; -- ������ϵ��ӳ�
ITEMATTR_COE_DROP       = ITEMATTR_COUNT_BASE0 + 17; -- Ѱ����ϵ��ӳ�
ITEMATTR_COE_HREC       = ITEMATTR_COUNT_BASE0 + 18; -- hp�ָ��ٶ�ϵ��ӳ�
ITEMATTR_COE_SREC       = ITEMATTR_COUNT_BASE0 + 19; -- sp�ָ��ٶ�ϵ��ӳ�
ITEMATTR_COE_MSPD       = ITEMATTR_COUNT_BASE0 + 20; -- �ƶ��ٶ�ϵ��ӳ�
ITEMATTR_COE_EXP        = ITEMATTR_COUNT_BASE0 + 21; -- ��Դ�ɼ��ٶ�ϵ��ӳ�
ITEMATTR_COE_MDEF       = ITEMATTR_COUNT_BASE0 + 22; -- ����ֿ�ϵ��ӳ�

ITEMATTR_COUNT_BASE1    = 25; --All stats can be added with *ItemFinalAttr commands
ITEMATTR_VAL_STR        = ITEMATTR_COUNT_BASE1 + 1; -- ��������ӳɣ�strength value��
ITEMATTR_VAL_AGI        = ITEMATTR_COUNT_BASE1 + 2; -- ���ݳ���ӳ�
ITEMATTR_VAL_ACC        = ITEMATTR_COUNT_BASE1 + 3; -- רע����ӳ�
ITEMATTR_VAL_CON        = ITEMATTR_COUNT_BASE1 + 4; -- ���ʳ���ӳ�
ITEMATTR_VAL_SPR        = ITEMATTR_COUNT_BASE1 + 5; -- ��������ӳ�
ITEMATTR_VAL_LUK        = ITEMATTR_COUNT_BASE1 + 6; -- ���˳���ӳ�
ITEMATTR_VAL_ASPD       = ITEMATTR_COUNT_BASE1 + 7; -- ����Ƶ�ʳ���ӳ�
ITEMATTR_VAL_CRTR       = ITEMATTR_COUNT_BASE1 + 8; -- �������볣��ӳ�
ITEMATTR_VAL_ATK        = ITEMATTR_COUNT_BASE1 + 9; -- ��С����������ӳ� --EDITABLE.
ITEMATTR_VAL_MATK       = ITEMATTR_COUNT_BASE1 + 10; -- ��󹥻�������ӳ� --EDITABLE.
ITEMATTR_VAL_DEF        = ITEMATTR_COUNT_BASE1 + 11; -- ������ӳ� --EDITABLE.
ITEMATTR_VAL_MXHP       = ITEMATTR_COUNT_BASE1 + 12; -- ���Hp����ӳ�
ITEMATTR_VAL_MXSP       = ITEMATTR_COUNT_BASE1 + 13; -- ���Sp����ӳ�
ITEMATTR_VAL_FLEE       = ITEMATTR_COUNT_BASE1 + 14; -- �����ʳ���ӳ�
ITEMATTR_VAL_HIT        = ITEMATTR_COUNT_BASE1 + 15; -- �����ʳ���ӳ�
ITEMATTR_VAL_CRT        = ITEMATTR_COUNT_BASE1 + 16; -- �����ʳ���ӳ�
ITEMATTR_VAL_DROP       = ITEMATTR_COUNT_BASE1 + 17; -- Ѱ���ʳ���ӳ�
ITEMATTR_VAL_HREC       = ITEMATTR_COUNT_BASE1 + 18; -- hp�ָ��ٶȳ���ӳ�
ITEMATTR_VAL_SREC       = ITEMATTR_COUNT_BASE1 + 19; -- sp�ָ��ٶȳ���ӳ�
ITEMATTR_VAL_MSPD       = ITEMATTR_COUNT_BASE1 + 20; -- �ƶ��ٶȳ���ӳ�
ITEMATTR_VAL_EXP        = ITEMATTR_COUNT_BASE1 + 21; -- ��Դ�ɼ��ٶȳ���ӳ�
ITEMATTR_VAL_MDEF       = ITEMATTR_COUNT_BASE1 + 22; -- ����ֿ�����ӳ� --EDITABLE.

ITEMATTR_COUNT_BASE2    = 49;
ITEMATTR_LHAND_VAL      = ITEMATTR_COUNT_BASE2 + 1; -- �������ּӳ�
ITEMATTR_MAXURE	        = ITEMATTR_COUNT_BASE2 + 2; -- Maximum Durability --EDITABLE.
ITEMATTR_MAXFORGE       = ITEMATTR_COUNT_BASE2 + 3; -- 0, always
ITEMATTR_MAXENERGY      = ITEMATTR_COUNT_BASE2 + 4; -- Quality --EDITABLE.
ITEMATTR_URE            = ITEMATTR_COUNT_BASE2 + 5; -- Durability --EDITABLE.
ITEMATTR_FORGE          = ITEMATTR_COUNT_BASE2 + 6; -- Forge Level (goes after "Cardic" or similar prefix) --EDITABLE.
ITEMATTR_ENERGY         = ITEMATTR_COUNT_BASE2 + 7; -- Quality --EDITABLE.


ITEMATTR_MAX_NUM        = 58;
ITEMATTR_CLIENT_MAX     = ITEMATTR_VAL_MDEF + 1;    -- �ͻ������ڶ�ȡ�����ã���Ϊ��󼸸����Բ���Ҫ��ʾ

ITEMATTR_COUNT_BASE3    = 180;
ITEMATTR_VAL_PARAM1		= ITEMATTR_COUNT_BASE3 + 1;	-- ���߸�����Ϣһ�����ڼ�¼������Ϣ��
ITEMATTR_VAL_PARAM2		= ITEMATTR_COUNT_BASE3 + 2;	-- ���߸�����Ϣ��(�����ۺϺ��¼ԭ���Ե��ߵ�ID��
ITEMATTR_VAL_LEVEL		= ITEMATTR_COUNT_BASE3 + 3; -- ����װ���ȼ���Ϣ�����ڼ�¼�ۺϳ���װ���ĵȼ���
ITEMATTR_VAL_FUSIONID   = ITEMATTR_COUNT_BASE3 + 4; -- ����װ���ۺ���ϢID


--����֤���Զ�Ӧ
-- ITEMATTR_VAL_STR  ����ֵ
-- ITEMATTR_VAL_AGI  ɱ����
-- ITEMATTR_VAL_DEX  ��ɱ��
-- ITEMATTR_VAL_CON  �μӳ���
-- ITEMATTR_VAL_STA  ʤ����
-- ITEMATTR_MAXURE   ��ӹ��׵���

ITEMATTR_VAL_BaoshiLV = ITEMATTR_MAXENERGY --��ʯ�ȼ�
ITEMATTR_VAL_GEMLV = ITEMATTR_MAXENERGY --��ʯ�ȼ�

BAG_TYPE_EQUIP = 1
BAG_TYPE_INV = 2
BAG_TYPE_TEMPINV = 3
BAG_TYPE_BANK = 4

EQUIP_SLOT_HEAD = 0
EQUIP_SLOT_FACE = 1 --unexisting
EQUIP_SLOT_ARMOR = 2
EQUIP_SLOT_HAND = 3
EQUIP_SLOT_BOOTS = 4
EQUIP_SLOT_NECKLACE = 5
EQUIP_SLOT_WEAPON_2 = 6
EQUIP_SLOT_RING_1 = 7
EQUIP_SLOT_RING_2 = 8
EQUIP_SLOT_WEAPON_1 = 9
EQUIP_SLOT_BELT = 10 --dunno exactly
EQUIP_SLOT_BRACELET_1 = 11 --dunno exactly
EQUIP_SLOT_BRACELET_2 = 12 --dunno exactly
EQUIP_SLOT_HANDGUARD = 13 --dunno exactly

ITEM_TYPE_SWORD = 1
ITEM_TYPE_GREATSWORD = 2
ITEM_TYPE_BOW = 3
ITEM_TYPE_FIREGUN = 4
ITEM_TYPE_BLADE = 5
ITEM_TYPE_BOXINGGLOVES = 6
ITEM_TYPE_DAGGER = 7
ITEM_TYPE_GOLDPOUCH = 8
ITEM_TYPE_SHORTSTAFF = 9
ITEM_TYPE_HAMMER = 10
ITEM_TYPE_SHIELD = 11
ITEM_TYPE_ARROW = 12
ITEM_TYPE_AMMUNITION = 13
ITEM_TYPE_HEADGEAR = 14
ITEM_TYPE_AXE = 18
ITEM_TYPE_METALPICKAXE = 19
ITEM_TYPE_HAIR = 20
ITEM_TYPE_FACE = 21
ITEM_TYPE_ARMOR = 22
ITEM_TYPE_GLOVES = 23
ITEM_TYPE_SHOE = 24
ITEM_TYPE_NECKLACE = 25
ITEM_TYPE_RING = 26
ITEM_TYPE_RECOVERY = 31
ITEM_TYPE_ADDITION = 32
ITEM_TYPE_SPECIALEFFECT = 33
ITEM_TYPE_COMMON = 41
ITEM_TYPE_QUEST = 42
ITEM_TYPE_PROW = 51
ITEM_TYPE_HULL = 52
ITEM_TYPE_MOBILITY = 53
ITEM_TYPE_SHIPCANNON = 54
ITEM_TYPE_PROPELLER = 55
ITEM_TYPE_SHIPSYMBOL = 56
ITEM_TYPE_CANNONBASE = 61
ITEM_TYPE_CANNONTOWER = 62
ITEM_TYPE_CANNONTUBE = 63
ITEM_TYPE_CANNONSYMBOL = 64
ITEM_TYPE_MAX = 82

tsv = {}