--TODO: (low) Implement "Random" quests.
--Actually they will be called sub-quests. Customization like normal quests + use of random quest functions/variables.



quest={}
quests = {}
quests_mt = {}

QUEST_STATE_START = 1
QUEST_STATE_END = 2
QUEST_STATE_ONGOING = 3
QUEST_STATE_LOG = 4

QUEST_EXECUTE_CONDITIONALS = 0
QUEST_EXECUTE_ACTIONS = 1

function conditional(state)
	return state
end

function quests_mt.__index (t,key)
	if rawget(t, key) == nil then
		local o = {{}}
		o[1].name = '' --mission title
		o[1].id = key
		o[1].type = QUEST_TYPE_NORMAL
		o[1].show = ALWAYS_SHOW
		o[1].indicators = {}
		o[1].indicators.needs = {}
		o[1].indicators.prizes = {seltype=PRIZE_SELALL} --PRIZE_SELONE is bugged in top1.
		o[1].actions = {}
		o[1].actions[QUEST_STATE_START]={text='',conditions={{conditional,{true}}}, actions={}}
		o[1].actions[QUEST_STATE_END]={text='',conditions={{conditional,{true}}}, actions={}}
		o[1].actions[QUEST_STATE_ONGOING]={text=''}
		o[1].actions[QUEST_STATE_LOG]={text='',conditions={{conditional,{true}}}, actions={}}
		setmetatable(o[1], quest)
		quest.__index = quest --if fail indexing in child, go to main class
		rawset(t, key, o)
		quest_init(key, nil, nil)
	end
	return rawget(t, key)
end

setmetatable(quests, quests_mt)

function quest.init(self, id, player, char)
	print(player.name)
	quests[id].to_player = player or nil
	quests[id][1].to_player = player or nil
	--need a loop right here for more than 1 subquest.
	quest_init(id, player, char)
	return quests[id]
end

function quest.send(self, to_player, event)
	self.to_player = to_player
	event = event or QUEST_EVENT_LOG
	local packet = packet:new()
	local cmd
	local subcmd
	local text

	if event == QUEST_EVENT_LOG then
		cmd = CMD_MC_MISLOGINFO
		text = self.actions[QUEST_STATE_LOG].text
	elseif event == QUEST_EVENT_START then
		cmd = CMD_MC_MISPAGE
		subcmd = MIS_BTNACCEPT
		text = self.actions[QUEST_STATE_START].text
	elseif event == QUEST_EVENT_ONGOING then
		cmd = CMD_MC_MISPAGE
		subcmd = MIS_BTNPENDING
		text = self.actions[QUEST_STATE_ONGOING].text
	elseif event == QUEST_EVENT_END then
		cmd = CMD_MC_MISPAGE
		subcmd = MIS_BTNDELIVERY
		text = self.actions[QUEST_STATE_END].text
		print(text)
	end

	packet:add(DT_CMD, cmd)
	if cmd == CMD_MC_MISPAGE then
		packet:add(DT_BN, subcmd)
		packet:add(DT_4BN, to_player.interacting_with_npc.id) --check how to get npc id here
		print('npc id in quest', to_player.interacting_with_npc.id)
	else
		packet:add(DT_2BN, self.id) --quest id
	end
	packet:add(DT_STR, self.name)
	packet:add(DT_BN, table.getn(self.indicators.needs)) -- on conclusion this is empty (amount of stuff done is otherwise shown)
	for n = 1, table.getn(self.indicators.needs), 1 do
		packet:add(DT_BN, self.indicators.needs[n].type)
		if self.indicators.needs[n].type == MIS_NEED_ITEM then
			packet:add(DT_2BN, self.indicators.needs[n][1]) --id
			packet:add(DT_2BN, self.indicators.needs[n][2]) --amount needed
			--dunno why this is not working, but I have a better alternative but doesn't report item gets -.-
			--might be a missing callback
			--local ret, num = GetNeedItemCount(to_player.role, self.id, self.indicators.needs[n][1])
			--if ret ~= LUA_TRUE then
			--    num = 0
			--end
			packet:add(DT_BN, light_item_wrap(self.indicators.needs[n][1], to_player).report[1].bag.amount) --amount of items needed already collected
		elseif self.indicators.needs[n].type == MIS_NEED_KILL then
			packet:add(DT_2BN, self.indicators.needs[n][1]) --id
			packet:add(DT_2BN, self.indicators.needs[n][2]) --amount needed
			packet:add(DT_BN, 0) --dunno why it's not working
			--packet:add(DT_BN, GetNumFlag(to_player.role, self.id, self.indicators.needs[n][3], self.indicators.needs[n][2])) --amount mobs killed
		elseif self.indicators.needs[n].type == MIS_NEED_DESP then --description
			packet:add(DT_STR, self.indicators.needs[n][1])
		elseif self.indicators.needs[n].type == MIS_NEED_SEND then
			packet:add(DT_2BN, self.indicators.needs[n][1]) --id
			packet:add(DT_2BN, self.indicators.needs[n][2]) --amount needed
			packet:add(DT_BN, self.indicators.needs[n][3]) --amount collected
		elseif self.indicators.needs[n].type == MIS_NEED_CONVOY then
			packet:add(DT_2BN, self.indicators.needs[n][1]) --id
			packet:add(DT_2BN, self.indicators.needs[n][2]) --amount needed
			packet:add(DT_BN, self.indicators.needs[n][3]) --amount collected
		elseif self.indicators.needs[n].type == MIS_NEED_EXPLORE then
			packet:add(DT_2BN, self.indicators.needs[n][1]) --id
			packet:add(DT_2BN, self.indicators.needs[n][2]) --amount needed
			packet:add(DT_BN, self.indicators.needs[n][3]) --amount collected
		else
			return
		end
	end

	--TODO: See what PRIZE_SELONE would do (user could select 1 item out of many?)
	packet:add(DT_BN, self.indicators.prizes.seltype) --either PRIZE_SELONE (used on all quests) or PRIZE_SELALL (used on random quests)
	if event == QUEST_EVENT_END then
		packet:add(DT_BN, table.getn(self.indicators.prizes))
		for i = 1, table.getn(self.indicators.prizes), 1 do
			packet:add(DT_BN, self.indicators.prizes[i].type)
			packet:add(DT_2BN, self.indicators.prizes[i][1])
			packet:add(DT_2BN, self.indicators.prizes[i][2])
		end
	else
		packet:add(DT_BN, 0)
	end
	packet:add(DT_STR, text)
	packet:send(to_player)
end

function quest.execute(self, quest_action, execute_type) --quests need to be added for every player in server (automatically associates with a npc).
	--return true: when conditions are met.
	--local state = GetMissionState(player.role, player.interacting_with_npc.id, char_quest_id)
	local boolean = C_TRUE
	local _table = self.actions[quest_action].conditions
	if execute_type == QUEST_EXECUTE_ACTIONS then
		_table = self.actions[quest_action].actions
	end

	if (quest_action ~= QUEST_STATE_ONGOING) then
		for i, func_set in _table do -- conditionals need to be interpreted as ANDs
			--myprint(func_set)
			local temp_boolean = func_set[1](table.unpack(func_set[2]))
			if ((temp_boolean ~= true) and (temp_boolean ~= C_TRUE)) then
				boolean = C_FALSE
				break
			end
		end
	end
	return boolean --will return false to anything that fails here.
end

--originally published by Momo
function LocalChat(char,player,text)
	local packet = packet:new()
	packet:add(DT_CMD, CMD_MC_TALK)
	packet:add(DT_4BN, GetCharID(char.role))
	packet:add(DT_STR, text)
	packet:send(player.role)
end

--posted by Insider:
-- If i'll say something in Argent then people in Thundoria will heard it. Pretty local. You should use GetChaByRange instead.
-- Also to show names you need to patch Game.exe (pretty simple, just one byte).

--[[
function Say(character,text)
local map_copy = GetChaMapCopy ( character )
local ply_num = GetMapCopyPlayerNum(map_copy)
local ps={}
local i = 1

BeginGetMapCopyPlayerCha ( map_copy )
for i = 1 ,ply_num , 1 do
ps[i]=GetMapCopyNextPlayerCha ( map_copy )
end

for i=1,ply_num,1 do
if(ps[i]~=0 and ps[i]~=nil)then
LocalChat( ps[i], character, text )
end
end
end
--]]

--TODO: Implement TriggerProc

--[[
function TriggerProc( character, id, param1, param2, param3, param4 )
	PRINT( "TriggerProc:2 ID = , Trigger = , p1 = , p2 = , p3 = , p4 = ", id, TriggerList[id], param1, param2, param3, param4 )	
	if id == nil or TriggerList[id] == nil or TriggerList[id].actions == nil or param1 == nil or param2 == nil then
		SystemNotice( character, "TriggerProc:incorrect function notice or trigger does not have action notice!ID = "..id )
		return LUA_ERROR
	end
	
	PRINT( "TriggerProc: conditions proc!" )
	local trigger = TriggerList[id]
	if trigger.tp == MIS_TRIGGER_NOMAL then
		--?????????
		if trigger.conditions ~= nil and trigger.conditions[1] ~= nil then
			local ret = ConditionsTest( character, trigger.conditions, param1, param2 )
			if ret ~= LUA_TRUE then
				PRINT( "TriggerProc: conditions return false, return false" )
				return LUA_FALSE
			end
		end
		
		--???????
		PRINT( "TriggerProc:actions proc" )
		if trigger.actions ~= nil and trigger.actions[1] ~= nil then
			local ret = ActionsProc( character, trigger.actions, nil, nil, 0, 0, param1, param2 )
			if ret ~= LUA_TRUE then
				PRINT( "TriggerProc: actions return false, return false" )
				SystemNotice( character, "TriggerProc: actions return false, return false" )
				return LUA_FALSE
			end
		end
	
	elseif trigger.tp == MIS_TRIGGER_RAND then
		PRINT( "TriggerProc:random quest: param3, param4", param3, param4 )
		if trigger.actions[1].func == AddRMNextFlag then
			if param3 == nil or param4 == nil then
				PRINT( "TriggerProc:random quest:while reseting AddRMNextFlagfunction parameter, param3, param4 cannot be nil" )
				SystemNotice( character, "TriggerProc:random quest:while reseting AddRMNextFlagfunction parameter, param3, param4 cannot be nil" )
				LG( "randmission_error", "TriggerProc:random quest:while reseting AddRMNextFlagfunction parameter, param3, param4 cannot be nil, triggerid = ", id )
				return LUA_FALSE
			end
			
			trigger.actions[1].p2 = param3
			trigger.actions[1].p3 = param4
		end
		
		--?????????
		if trigger.conditions ~= nil and trigger.conditions[1] ~= nil then
			local ret = ConditionsTest( character, trigger.conditions, param1, param2 )
			if ret ~= LUA_TRUE then
				PRINT( "TriggerProc: random quest: conditions return false, return false" )
				return LUA_FALSE
			end
		end
		
		--???????
		PRINT( "TriggerProc:random quest: actions proc" )
		if trigger.actions ~= nil and trigger.actions[1] ~= nil then
			local ret = ActionsProc( character, trigger.actions )
			if ret ~= LUA_TRUE then
				PRINT( "TriggerProc: random quest: actions return false, return false" )
				SystemNotice( character, "TriggerProc: random quest: actions return false, return false" )
				return LUA_FALSE
			end
		end
		--reset rand mission trigger actions's value
		trigger.actions[1].p2 = 0
		trigger.actions[1].p3 = 0
	else
		PRINT( "TriggerProc:incorrect trigger type notice.id = ", id )
		SystemNotice( character, "TriggerProc:incorrect trigger type notice.id = "..id )
		return LUA_FALSE
	end
	
	PRINT( "TriggerProc: return true." )
	return LUA_TRUE
end

function TriggerResult( character, id, param1, param2 )
	PRINT( "TriggerResult" )	
	if id == nil or TriggerList[id] == nil or param1 == nil or param2 == nil then
		SystemNotice( character, "TriggerResult:incorrect function notice!" )
		return LUA_ERROR
	end
	
	PRINT( "TriggerResult: conditions proc!" )
	local trigger = TriggerList[id]
	
	--?????????
	PRINT( "TriggerProc:failures proc" )
	if trigger.failures ~= nil and trigger.failures[1] ~= nil then
		local ret = ActionsProc( character, trigger.failures )
		if ret ~= LUA_TRUE then
			PRINT( "TriggerProc: failures return false, return false" )
		end
	end
	
	PRINT( "TriggerResult: return true" )
	return LUA_TRUE
end
--]]

print('- Loading definition file: quests.lua')
dofile(GetResPath('script/'..mod..'/defs/quests.lua'))