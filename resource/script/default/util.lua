--[[
Concatenate the contents of the parameter list,
separated by the string delimiter (just like in perl)
example: string.join(", ", {"Anna", "Bob", "Charlie", "Dolores"})
--]]
function string.join(delimiter, list)
	local len = table.getn(list)
	if len == 0 then
		return ""
	end
	local string = list[1]
	for i = 2, len do
		string = string .. delimiter .. list[i]
	end
	return string
end

--[[
Split text into a list consisting of the strings in text,
separated by strings matching delimiter (which may be a pattern).
example: string.split(",%s*", "Anna, Bob, Charlie,Dolores")
--]]
function string.split(delimiter, text)
	local list = {}
	local pos = 1
	if string.find("", delimiter, 1) then -- this would result in endless loops
		error("delimiter matches empty string!")
	end
	while 1 do
		local first, last = string.find(text, delimiter, pos)
		if first then -- found?
			table.insert(list, string.sub(text, pos, first-1))
			pos = last+1
		else
			table.insert(list, string.sub(text, pos))
			break
		end
	end
	return list
end

--Fixed size string split
function string.split_fixedsize(text, size)
	local list = {}
	local pos = 1
	while 1 do
		local _string = string.sub(text, pos, pos+size)
		if string.len(_string) <= 0 then
			break
		end
		table.insert(list, _string)
		pos = pos+size+1
	end
	return list
end

--[[
table.copy( t )
returns a exact copy of table t
--]]
function table.copy( t, lookup_table )
	lookup_table = lookup_table or {}
	local tcopy = {}
	if not lookup_table[t] then
		lookup_table[t] = tcopy
	end
	for i,v in pairs( t ) do
		if type( i ) == "table" then
			if lookup_table[i] then
				i = lookup_table[i]
			else
				i = table.copy( i, lookup_table )
			end
		end
		if type( v ) ~= "table" then
			tcopy[i] = v
		else
			if lookup_table[v] then
				tcopy[i] = lookup_table[v]
			else
				tcopy[i] = table.copy( v, lookup_table )
			end
		end
	end
	return tcopy
end

--[[
This function is an extension to the original tostring function, and returns
the string representation of many values including tables.
--]]
mytostring = serpent.line

--[[
This function uses the new tostring function extension.
--]]
function myprint(a)
	print(mytostring(a))
end

--[[
myfile = io.open(GetResPath('iteminfo.bin'), 'rb')
myfile2 = io.open('test.txt', 'wb')
myfile2:write(myfile:read('*all'))
myfile2:close()
myfile:close()
--]]

function math_percent_random(a)
	local z=false
	if (math.random(0,1000000000)<=(a*1000000000)) then
		z=true
	end
	return z
end

function table.inv_func(ot)
	local nt = {}
	for i,v in pairs(ot) do
		nt[v]=i
	end
	return nt
end

-- x bits unsigned to signed conversion (might lose data)
function int_to_signed_int_x(n,x)
	if n < 0 then
		n = n + 2^x
	end
	return n
end

function io.linesw(filename, list) --opposite of io.lines
	file = io.open(filename, 'w')
	for i, v in ipairs(list) do
		file:write(v..'\n')
	end
	file:close()
end

--[[
function tsv_read(file_name)
	local _file_lines = {}
	for line in io.lines(file_name) do
		--print('linesub', string.sub(line,0,2))
		if (string.sub(line,0,2) ~= '//') then
			table.insert(_file_lines, string.split("\t", line))
		end
	end
	return _file_lines
end
--]]


    -- "Sorted by key" table iterator
    -- Extracted from http://www.lua.org/pil/19.3.html
     
function pairsByKeys(t, f)
	local a = {}
	for n in pairs(t) do
		table.insert(a, n)
	end
	table.sort(a, f)
	 
	local i = 0 -- iterator variable
	local iter = function () -- iterator function
		i = i + 1
		if a[i] == nil then
			return nil
		else
			return a[i], t[a[i]]
		end
	end
	
	return iter
end

function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

function tsv_read(file_name)
	local _file_lines = {}
	if file_exists(file_name) then
		for line in io.lines(file_name) do
			if (string.sub(line,0,2) ~= '//') then
				local split_line = string.split("\t", line)
				local index = table.remove(split_line, 1)
				table.insert(_file_lines, tonumber(index), split_line)
			end
		end
	end
	return _file_lines
end

function tsv_write(file_name, file_lines)
	_file = io.open(file_name, 'w')
	for i,line in pairsByKeys(file_lines) do
		table.insert(line, 1, tostring(i))
		if i ~= 1 then
			_file:write('\n'..string.join("\t", line))
		else
			_file:write(string.join("\t", line))
		end
	end
end

function GetResStringInString(old_string)
	if old_string ~= nil then
		return string.gsub(old_string,'#(.-)#', GetResString)
	end
	return ''
end

function range(from, to, step)
	local return_table = {}
	
	if (step == nil) then
		step = 1
	end
	
	for i=from, to, step do
		table.insert(return_table, i)
	end
	return return_table
end

function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

db_write = function (file, data)
	fp = io.open(GetResPath(file), 'w')
	fp:write(mytostring(data))
	fp:close()
end
db_read = function (file)
	return dofile(GetResPath(file))
end

function math.modf(number)
	return math.floor(number),number-math.floor(number)
end

function _te(table,else_part)
	local mt = {__index = function(table,key)
		local v = rawget(table, key)
		if v == nil then
			if else_part == nil then
				v = key
			else
				v = else_part
			end
		end
		return v
	end}
	
	return setmetatable(table,mt)
end

function merge_tables(table_array)
	local result_table = {}
	for i,v in ipairs(table_array) do
		if i == 1 then
			result_table = v()
		else
			local old_length = #result_table
			for j,w in ipairs(v()) do result_table[old_length+j] = w end
		end
	end
	return result_table
end