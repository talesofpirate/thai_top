--TODO: Skills to check player stats and equipment stats.
--TODO: 1 Skill to pick a player by targetting, can also target user (with on and off state):
--1 skill to pick an equipment slot by creating a marker item, double clicking on it will set the slot to that (on/off).
--1 skill to actually get the info of player or the equipment and print it on the system.
--TODO: Use ResetItemFinalAttr and AddItemFinalAttr - with stat modifiers - to remove gems.
--TODO: Check Angela's effect. Might be related to the final attr.
--TODO: If AddItemFinalAttr really adds the stats to the equip permanently, extend the stats to the setting of the normal stats of equips.

--TODO: Implement gunpowder/arrows totally.
--TODO: Implement crit calculations for all active skills.
--TODO: Set max stack for ammo as 9999, for gems as 511 (amount to get lvl 9 starting at lvl 0).
--TODO: For anything that shouldn't be stacked while farming, create another item with 1 stack, which when opened, yields a stackable type.

--IMPORTANT TODO: Re-update skillinfo.

--BTW: If you make character learn the melee skills, you will make it not to be able to do so.

function Relive(role)
	Relive_now(role,10)
end

function Relive_now(role,slvl)
	local _ps = player_wrap(role).stats
	_ps[ATTR_HP] = (_ps[ATTR_MXHP]/(11-slvl))-1
	_ps[ATTR_SP] = (_ps[ATTR_MXSP]/(11-slvl))-1
end

--coral usage (or other equip requirement) in skillinfo: slot in player, amount, type.
--If those aren't matched, skill cannot be activated should be very, very useful in creating classes with many skills.
--that work only for specific glove/boot/ring/neck sets.


--What the game can't do:
---AOE with 1 player main target (can't do conch ray targetting someone without making it non-aoe).
---Weapon based skills are actually hardcoded. Can't be created by adding item type and addding skill.


PLAYER_SKILL = 1
PLAYER_SKILL_EFFECT = 2
ITEM_SKILL = 3 --also automatic use effect


SK_EVENT_PRETARG = 1
SK_EVENT_AOEEFFECT = 2 -- SetRangeState(effect, lvl, time) -- sets effect duration (only works in function)
SK_EVENT_SP = 3 -- return sp_reduce -- sp_reduce = the amount of sp from player to reduce. (only works on function)
SK_EVENT_DURABILITY = 4 -- reduces item durability, see below
SK_EVENT_ENERGY = 5 -- reduces item energy (could either be item on "coral usage" only, or could include other item reqs...
SK_EVENT_AOERANGE = 6 --SetSkillRange ( rangetype , dimensions... ) --sets size of aoe
SK_EVENT_START = 7 --Starts the active effect.
SK_EVENT_END = 8 --Ends the active effect.
SK_EVENT_USE = 9 --Starts the passive effect.
SK_EVENT_UNUSE = 10 --Ends the passive effect.
SK_EVENT_COOLDOWN = 11 --Sets the cooldown.
SK_EVENT_MISS = 12 --Added: Called whenever the skill misses the target.

--

SKEFF_EVENT_TRANS = 1
SKEFF_EVENT_ADD = 2
SKEFF_EVENT_REM = 3

-- Skill constants are in initial.lua now

function switch_or_eq(value, _table)
	for i,v in ipairs(_table) do
		if(value==v) then
			return true
		end
	end
	return false
end

--skills_mt = {}
--
--function skills_mt.__index (t,key)
--	print('asb')
--	if rawget(t, key) == nil then
--		print('asb2')
--		return nil
--	end
--	return rawget(t, key)
--end

skills = {}
--setmetatable(skills, skills_mt)

function skill(type, id, event, lvl, ...)
	local arg = {...}
	--myprint({type,id,event,lvl,...})
	local tskill = {
		p=nil,ps=nil,pD=nil,pDs=nil,
		b=nil,bs=nil,bD=nil,bDs=nil,
		i=nil,is=nil,iD=nil,iDs=nil,
		type=type,
		id=id,
		name="",
		event=event,
		lvl=lvl,
		passive_switch = 0 --for mathematical purposes
	}
	local tsv_from_skill_type = {tsv.skillinfo,tsv.skilleff,tsv.iteminfo}
	tskill.name = tsv_from_skill_type[tskill.type][tskill.id]['name']
	--print(type, id, event, lvl)
	--myprint({type,id,event,lvl,...})
	--btw: event is ignored for items.
	--remember that pDs only exists on SK_EVENT_END for skills, everything else uses ps.

	--print(tskill.type, tskill.id, tskill.event, tskill.lvl)
	
	if ((type == PLAYER_SKILL) or (type == PLAYER_SKILL_EFFECT)) then
		if (#arg >= 0) then
			if (ChaIsBoat(arg[1]) == C_TRUE) then
				tskill.b = player_wrap(arg[1])
				tskill.bs = tskill.b.stats
				tskill.p = player_wrap(GetMainCha(tskill.b.role))
			else
				tskill.p = player_wrap(arg[1])
			end
			tskill.ps = tskill.p.stats

			if (#arg >= 1) then
				if (ChaIsBoat(arg[2]) == C_TRUE) then
					tskill.bD = player_wrap(arg[2])
					tskill.bDs = tskill.bD.stats
					tskill.pD = player_wrap(GetMainCha(tskill.bD.role))
				else
					tskill.pD = player_wrap(arg[2])
				end
				tskill.pDs = tskill.pD.stats
			end
		end
	elseif (type == ITEM_SKILL) then
		if (#arg >= 2) then -- will at least have player and item pointers
			if (ChaIsBoat(arg[1]) == C_TRUE) then
				tskill.b = player_wrap(arg[1])
				tskill.bs = tskill.b.stats
				tskill.p = player_wrap(GetMainCha(tskill.b.role))
			else
				tskill.p = player_wrap(arg[1])
			end
			tskill.ps = tskill.p.stats
			tskill.i = item_wrap(arg[2])
			tskill.is = tskill.i.stats
			if (#arg >= 3) then
				tskill.iD = item_wrap(arg[3])
				tskill.iDs = tskill.iD.stats
			end
		end
	end
	
	if (type == PLAYER_SKILL) then
		if(event==SK_EVENT_USE) then
			tskill.passive_switch = 1
		elseif (event==SK_EVENT_UNUSE) then
			tskill.passive_switch = -1
		end
	end
	if (type == PLAYER_SKILL_EFFECT) then
		if (event==SKEFF_EVENT_ADD) then
			tskill.passive_switch = 1
		elseif (event==SKEFF_EVENT_REM) then
			tskill.passive_switch = -1
		end
	end	

	local tsv_func_from_skill_event = {{
		'func_pre_targetting',
		'func_aoe_effect',
		'func_sp',
		'func_durability',
		'func_energy',
		'func_aoe_range',
		'func_start',
		'func_end',
		'func_use',
		'func_unuse',
		'func_cooldown',
		'func_miss'
	}, {
		'func_transition',
		'func_start',
		'func_end'
	}, {
		'func_use'
	}}

	
	if (((type == PLAYER_SKILL) and (event == SK_EVENT_END)) and (tskill.p.role ~= tskill.pD.role)) then
		local hitting_chance = math.min(math.max(0,tskill.ps[ATTR_HIT] - (tskill.pDs[ATTR_FLEE] - 100)),100)
		if hitting_chance < math.random(0,100) then
			SkillMiss(tskill.pD.role)
			--local return_value = tskill[type][name][SK_EVENT_MISS](tskill)
			local return_value
			if tsv_from_skill_type[tskill.type][tskill.name][tsv_func_from_skill_event[tskill.type]['func_miss']] ~= nil then
				return_value = tsv_from_skill_type[tskill.type][tskill.name][tsv_func_from_skill_event[tskill.type]['func_miss']](tskill)
			end
			if (return_value == C_FALSE) or (return_value == nil) then
				return C_FALSE
			end
		end
	end
	
	--myprint({tskill.type,tskill.name,tskill.id,tskill.event,tskill.lvl})	
	--return skills[tskill.type][tskill.name][tskill.event](tskill)
	
	--myprint({tskill.type,tskill.name,tskill.id,tskill.event,tskill.lvl})
	
	--TODO: Implement workaround of item in an item skill not being removed until this function call returns:
	--Impossible! All game functions detect there's still an item there until function exits...
	--Can only be fixed by changing source code.

	local return_value = tsv_from_skill_type[tskill.type][tskill.name][tsv_func_from_skill_event[tskill.type][tskill.event]](tskill)
	if (type == ITEM_SKILL) then
		--[[ CHANGED: Make item skills return like C function calls:
		true = failed (call UseItemFailed) and return nil
		false or nil = succeeded and return nil
		any other value = return as is
		--]]
		if (return_value == false) or (return_value == nil) then
			return_value = nil
		elseif return_value then --= true
			UseItemFailed(tskill.p.role)
			return_value = nil
		end
	end
	return return_value
end

function RemoveYS( role )
	print('Remove Stealth')
	RemoveState(role,SKEFF_STEALTH)
	return 1 
end

--SetRelive ( ATKER , DEFER ,  sklv , "Player"..ChaName.."\n\n wish to revive you. Accept?" )

print('- Loading definition file: skills.lua')
dofile(GetResPath('script/'..mod..'/defs/skills.lua'))