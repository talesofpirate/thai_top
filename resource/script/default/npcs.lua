--AdjustTradeCess( character, p1, p2 ) -- adjusts tax rate.
--AddPetExp( character, p1, p2 ) -- adds pet exp.
--AddMoney( character, npc, p1 ) -- adds money.
--GiveItem( character, npc, p1, p2, p3 ) -- gives item.
--TODO: (low) Quests that skip own dialogs and that skip quest phases.

--TODO:
---New exp formula.
---Change exp given by mobs.
---Change "pewp" to a skill (no exp skill).
---Make a teleporter hub.
---Remove all quests.
---Remove all useless npc.
---Forging skills in the player, not another npc.
---Skill to bank.
---No need for gem exchangers with my gemming system.
---

NPC_INIT = 0
NPC_UPDATE = 1

npc={char=-1, id=-1, map='', name='', proc=nil, dialog=nil, trade=nil, quests={}}
npcs_cur_id = 1
npcs_max_id = 300

function npc.new(self, scriptid, o)
	o = o or {char=-1, id=-1, map='', name='', proc=nil, dialog=nil, trade=nil, quests={}}
	setmetatable(o, self)
	self.__index = self
	return o
end
function npc.init(self, map, id, name, o)
	for i, v in ipairs(npcs) do
		if v.name == name then
			npcs[i].id = id
            npcs[i].map = map
            break
		end
	end
end

for i=1,npcs_max_id,1 do
	npcs[i] = npc:new(i)
end
function npc_id(name)
	for i,v in ipairs(npcs) do
		if v.name == name then
			return i
		end
	end
	return -1
end

-- npcs are 1 clickable with mouse to have action, and have icons in minimap.
-- Unfortunately, they are only ones with a message proc. That means, no way
-- to have choice without being a npc (that means, can still have dialog box
-- with just text as player).

--order of execution:
--ResetNpcInfo
--NpcFunction (actually, the function found in the npc list)
--GetNpcInfo

-- main npc initializer callback
-- called in order in first run, then if reloaded, it is called in reverse order.
-- called only after scripts are loaded.
function ResetNpcInfo(char_role, name) --called right after map files are called (System is a system npc).
	--print('ResetNpcInfo', name)
	--if name ~= 'System' and name ~= 'guardian' then --needed on top 1.38
		SetNpcScriptID(char_role, npcs_cur_id)
		npcs[npcs_cur_id].char = char_wrap(char_role)
		npcs[npcs_cur_id].name = name
	--end
	npcs_cur_id = npcs_cur_id+1
end

--UPDATE: Seems not to be called anymore
function ModifyNpcInfo(char, name, id) --called when asked to update npcs on client

	--[[ --dunno if this works, but if it works, then I should also put npc_init line here too...
	local scriptid = npc_id(name)
	npc_init(npcs[scriptid], scriptid)
	if table.getn(npc.quests) >= 1 then
	SetNpcHasMission( npc, 1 ) --makes the ! show on top of the npc (if player didn't finish them all)
	else
	SetNpcHasMission( npc, 0 )
	end
	--]]
	print('ModifyNpcInfo')
end

-- first npc initializer callback (only if npc specific function exists)
function GetNpcInfo(char, name)
	--print('GetNpcInfo', name)
	--if name == 'System' then -- works only on top 1.38
	--	npc:init('',1,'System')
	--	return
	--end
	local scriptid = npc_id(name)

	npc_init(npcs[scriptid], scriptid, nil, NPC_INIT)
	if table.getn(npcs[scriptid].quests) >= 1 then --I don't think anyone would want this differently...
		SetNpcHasMission(npcs[scriptid].char.role, 1)
	end
	--    else --at initialization, it's expected to be 0 by default.
	--        SetNpcHasMission(npcs[scriptid].char, 0)
	--    end
end

--UPDATE: Seems not to be called anymore.
function NpcInfoReload( name,  func ) --apparently this is different on v2.
	print('NpcInfoReload')
	--[[
	local ret, char, scriptid = FindNpc( name )
	if ret == C_FALSE or char == nil or scriptid == nil then
	return
	end
	local npc = npc:new(char)
	npcs[table.getn(npcs)]=npc
	--]]
	--[[
	InitPage()
	InitTrade()
	InitNpcMission()
	NpcPointer = npc
	local str = "Initialization NPC ["..name.."] script notice successful!"
	PRINT( str )
	LG( "npcinit", str )
	--]]

	--myprint(func)
	func(npc)

	--[[
	if NpcMissionList.count > 0 then
	PRINT( "mission", "Set NPC bring quest label!" )
	SetNpcHasMission( npc, 1 )
	else
	PRINT( "mission", "set NPC does not carry quest label !" )
	SetNpcHasMission( npc, 0 )
	end
	--]]
end

--main network message callback

--berth means: port (in terms of boat). Port ID (berth ID) is specified when ports are created (check map files).
--Each port has a trading place. Boat has to be in the port for trading to work. Even though port could be in a place,
--the spawn could be somewhere else. Something could be done for this though:
--The ships are going to be called: Ship accounts.
--These ships accounts cannot be taken out of the place, but they can be transfered to other players.
--All items in inventory can be transfered to ship account.
--Impossible, can only "packbag" them, meaning, can only put items in... only way to take them out is with boat shops.
--Those items can be used, but cannot be equipped. Also cannot be hotkey'ed (so ship inv would have to be shown).

function NpcProc(player, char, _packet, scriptid) --scriptid comes from here
	--local npcname = GetCharName( npc )
	--local str = npcname..": Hi! You are looking for me? I am quite busy right now._......."
	--SendPage( character, npc, 0, str, nil, 0 )
	if player == nil or char == nil or _packet == nil then
		return
	end
	player = player_wrap(player)
	char = char_wrap(char)
	
	npc_init(npcs[scriptid], scriptid, player, NPC_UPDATE)

	local packet = packet:new(_packet)
	local cmd = packet:rem(DT_2BN)
	if cmd == CMD_CM_TALKPAGE then
		quests_reset_state(player, npcs[scriptid])
		local event = packet:rem(DT_BN)
		if npcs[scriptid].proc ~= nil then
			npcs[scriptid].proc(npcs[scriptid], scriptid, player, cmd, event, nil)
		end
	elseif cmd == CMD_CM_FUNCITEM then
		quests_reset_state(player, npcs[scriptid])
		local page = packet:rem(DT_BN)
		local item = packet:rem(DT_BN)+1
		if npcs[scriptid].proc ~= nil then
			npcs[scriptid].proc(npcs[scriptid], scriptid, player, cmd, page, item)
		end
	elseif cmd == CMD_CM_TRADEITEM then
		quests_reset_state(player, npcs[scriptid])
		local tradetype = packet:rem(DT_BN)
		if tradetype == ROLE_TRADE_SALE then -- Selling an item
			if ((npcs[scriptid].trade.type ~= TRADE_BUY) and (npcs[scriptid].trade.type ~= TRADE_SALE)) then
				return
			end
			local index = packet:rem(DT_BN)
			local count = packet:rem(DT_2BN)
			if count == 0 then
				return
			end
			--TODO: Customize sales with custom prices/item qualities (lower priority, because prices/qualities are not shown anyways)
			SafeSale(player.role, index, count) --asks if player wants to sell, and then sells it.
		elseif tradetype == ROLE_TRADE_BUY then -- Buying an item
			if ((npcs[scriptid].trade.type ~= TRADE_BUY) and (npcs[scriptid].trade.type ~= TRADE_SALE)) then
				return
			end
			local itemtype = packet:rem(DT_BN)+1
			local index1 = packet:rem(DT_BN)+1
			local index2 = packet:rem(DT_BN)
			local count = packet:rem(DT_2BN)
			if count == 0 then
				return
			end
			--TODO: Customize buying with custom prices (lower priority, because prices are not shown anyways)
			SafeBuy(player.role, npcs[scriptid].trade.sections[itemtype][index1][1], index2, count) --asks if player wants to buy, and then buys it.
			-- no boats so far for the rest
		end
	elseif cmd == CMD_CM_BLACKMARKET_EXCHANGE_REQ then --���жһ�
		quests_reset_state(player, npcs[scriptid])
		local timeNum = packet:rem(DT_2BN)
		local srcID = packet:rem(DT_2BN)
		local srcNum = packet:rem(DT_2BN)
		local tarID = packet:rem(DT_2BN)
		local tarNum = packet:rem(DT_2BN)
		local byIndex = packet:rem(DT_2BN)

		if KitbagLock(player.role,0) ~= LUA_TRUE then
			SystemNotice(player.role, "Shop Item Exchange: Inventory is locked." )
			return
		end

		if HasLeaveBagGrid(player.role,1) ~= LUA_TRUE then
			SystemNotice(player.role, "Shop Item Exchange: Inventory is full.")
			return
		end

		ExchangeReq(player.role, char.role, srcID, srcNum, tarID, tarNum, timeNum) --asks if player wants to exchange, then exchanges it
		--TODO: Make exchange actually take out items from npc's inventory too (by using something else than ExchangeReq).
		--Also, make it update the exchange too, while doing it, so to show how many items are left.
		--Certainly it's better to use dialogs for trading than these built-in dialogs...
	elseif cmd == CMD_CM_MISSION then --������
		local byCmd = packet:rem(DT_BN)
		if byCmd == MIS_SEL then
			local selindex = packet:rem(DT_BN)

			local ret, npc_quest_id, state = GetMissionInfo(player.role, char.id, selindex)
			print(ret, npc_quest_id, state, 'GetMissionInfo', player.role, char.id, selindex)

			if ret ~= LUA_TRUE then
				SystemNotice(player.role, "MissionProc:obtain quest notice failed!")
				return
			end
			if npcs[scriptid].quests[npc_quest_id] == nil then
				SystemNotice(player.role, "MissionProc:Server does not have requested quest notice error!" )
				return
			end

			player.interacting_with_npc = char
			
			local quest = quest:init(npcs[scriptid].quests[npc_quest_id][1].id, player, char)[1]
			--if quest.type == QUEST_TYPE_RANDOM then
			--    ret = HasRandMission( character, missionlist[id].id )
			--    local ret, miscount = GetRandMissionCount(player.role)
			--    print(ret, miscount,'getrandmissioncount')
			--    quest = quest:init(id, player, char)[miscount] -- GetCharMissionLevel(player.role) or GetCharGangLevel(player.role)
			--end

			if quest.type ~= QUEST_TYPE_RANDOM then
				if SetMissionTempInfo(player.role, char.id, npcs[scriptid].quests[npc_quest_id][1].id, state, quest.type) ~= LUA_TRUE then
					SystemNotice(player.role, "MissionProc:set quest temporary data notice failed!")
					return
				end
			end

			--HasLeaveBagGrid(player.role, mission.begin.baggrid) -- leave empty inventory spaces.
			--ret = RefreshMissionState( character, npc ) -- refreshes quest state
			--AddRandMissionNum( character, mission.id ) -- +1 on quest cycle number.
			--GetRandMissionNum( character, mission.id ) -- gets random quest number in cycle.
			--ResetRandMissionNum( character, mission.id ) -- resets random quests cycle.
			--HasRandMissionCount( character, mission.id, mission.loopinfo[loopnum].num ) -- has x amount of random quest cycles passed?

			--if quest.type == QUEST_TYPE_NORMAL then
			if state == QUEST_ACTION_ACCEPT then
				--[[
				if quest.type == QUEST_TYPE_RANDOM then
				print('testadasdspo1')
				local ret = HasRandMission(player.role, quest.id)
				print('testadasdspo2')
				if ret ~= C_TRUE then
				print('testadasdspo3')
				if IsMissionFull(player.role) == LUA_TRUE then
				return BickerNotice(player.role, "Quest slots are full. Couldn't accept quest." )
				end
				print('testadasdspo4')

				--local ret, id, state, tp = GetMissionTempInfo(player.role, char.id)
				--print('testadasdspo5')
				--if ret ~= LUA_TRUE then
				--    SystemNotice(player.role, "AcceptMission:obtain character dialogue temporary quest notice error!" )
				--    return C_FALSE
				--end
				--print('testadasdspo6')

				quest:execute(QUEST_STATE_START,QUEST_EXECUTE_ACTIONS)
				print('testadasdspo7')

				AddRandMission(player.role, id, id, quest.type, 1, 1, 1, 1, 1, 1)

				print('testadasdspo8')

				--for i, _quest in quest:init(id, player, char) do
				--    SetRandMissionData(player.role, id, i, 0, 0, 0, 0, 0, 0)
				--end
				print('testadasdspo9')

				print('before reset state')
				quests_reset_state(player)
				print('after reset state')

				print('testadasdspo10')

				quest:execute(QUEST_STATE_START,QUEST_EXECUTE_ACTIONS) --there are no conditions to start
				--local ret, miscount = GetRandMissionCount(player.role, quest.id) -- detects counting of series
				--local ret, miscount = GetRandMissionNum(player.role, quest.id) --detects number in the series
				print('testadasdspo11')
				AddRandMissionNum(player.role, quest.id)
				print('testadasdspo12')
				return quest:send(player, QUEST_EVENT_ONGOING)
				end
				end
				--]]
				if quest.show == COMPLETE_SHOW then
					SystemNotice(player.role, "Quest: Couldn't select it because conditions were not met for it to show up." )
				else
					if quest:execute(QUEST_STATE_START,QUEST_EXECUTE_CONDITIONALS) == C_TRUE then
						quest:send(player, QUEST_EVENT_START)
					else
						SystemNotice(player.role, "Quest: Couldn't select it because conditions were not met for it to show up." )
					end
				end
				
				return
			elseif ((state ~= QUEST_ACTION_DELIVERY) and (state ~= QUEST_ACTION_PENDING)) then
				SystemNotice(player.role, "SelMissionList:incorrect type of quest status notice!" )
				return
			else
				print('coming here?')
				if quest.show == ACCEPT_SHOW then
					quest:send(player, QUEST_EVENT_ONGOING)
					return
				else
					if quest:execute(QUEST_STATE_END,QUEST_EXECUTE_CONDITIONALS) == C_TRUE then
						--if quest.type == QUEST_TYPE_RANDOM then
						--    AddRandMissionNum(player.role, quest.id)
						--    local ret, miscount = GetRandMissionCount(player.role, quest.id)
						--    if table.getn(quest:init(id, player, char)) > miscount then
						--        CompleteRandMissionCount(player.role, quest.id)
						--    end
						--end
						quest:send(player, QUEST_EVENT_END)
						return
					else
						quest:send(player, QUEST_EVENT_ONGOING)
						return
					end
				end
			end
		elseif byCmd == MIS_BTNACCEPT then
			if IsMissionFull(player.role) == LUA_TRUE then
				return BickerNotice(player.role, "Quest slots are full. Couldn't accept quest." )
			end

			local ret, id, state, tp = GetMissionTempInfo(player.role, char.id)
			if ret ~= LUA_TRUE then
				SystemNotice(player.role, "AcceptMission:obtain character dialogue temporary quest notice error!" )
				return
			end

			local quest = quest:init(id, player, char)[1]
			if quest:execute(QUEST_STATE_START,QUEST_EXECUTE_CONDITIONALS) == C_TRUE then
				quest:execute(QUEST_STATE_START,QUEST_EXECUTE_ACTIONS)
				
				AddMission(player.role, id, id) --one of the actions usually used.
				quests_reset_all_states(player)
			else
				SystemNotice(player.role, "Quest: Couldn't accept it because conditions weren't met at the time." )
				quests_reset_state(player, npcs[scriptid])
			end
		elseif byCmd == MIS_BTNDELIVERY then
			local byParam1 = packet:rem(DT_BN)
			local byParam2 = packet:rem(DT_BN)

			local ret, id, state, tp = GetMissionTempInfo(player.role, char.id)
			if ret ~= LUA_TRUE then
				SystemNotice(player.role, "CompleteMission:obtain character dialogue temporary quest notice error!" )
				return
			end

			local ret = HasMisssionFailure(player.role, id) --supposed to be id from table
			if ret == LUA_TRUE then
				BickerNotice(player.role, "Quest has failed, please select to abandon to clear quest log!" )
				return
			end

			local quest = quest:init(id, player, char)[1]
			if quest:execute(QUEST_STATE_END,QUEST_EXECUTE_CONDITIONALS) == C_TRUE then
				quest:execute(QUEST_STATE_END,QUEST_EXECUTE_ACTIONS)

				ClearMission(player.role, id)
				quests_reset_all_states(player)
			else
				SystemNotice(player.role, "Quest: Couldn't complete it because conditions weren't met at the time." )
				quests_reset_state(player, npcs[scriptid])
			end
			--SetRecord(player.role, id) --leave it commented to test quests
		end
	end
end

--npc����״̬���?��
function NpcState(player_role, char_id, scriptid)
	print(char_id)
	for i = 1, 32, 1 do
		if npcs[scriptid].quests[i] == nil then
			break
		end
		local quest = quest:init(npcs[scriptid].quests[i][1].id,player_wrap(player_role), npcs[scriptid].char)[1]

		--if quest.type == QUEST_TYPE_NORMAL then
		--[[if quest.type == QUEST_TYPE_RANDOM then
		print('def_1')
		ret = HasRandMission(player.role, quest.id)
		print(ret, 'def_2')
		else
		]]
		local ret = HasMission(player.role, quest.id)
		--end
		if ret == C_TRUE then
			--[[if quest.type == QUEST_TYPE_RANDOM then
			print('def_3')
			local ret, miscount = GetRandMissionCount(player.role, quest.id) --GetRandMissionCount needs +1 on its values
			print(miscount, 'def_4')
			quest = quest:init(npcs[scriptid].quests[i][miscount].id,player_wrap(player_role), npcs[scriptid].char)[index]
			print('def_5')
			end
			--]]
			if (quest.show == ALWAYS_SHOW) or (quest.show == COMPLETE_SHOW) then
				print('quest id name:',npcs[scriptid].char.name)
				if quest:execute(QUEST_STATE_END,QUEST_EXECUTE_CONDITIONALS) == C_TRUE then
					print('concluding quest here')
					AddMissionState(player.role, char_id, i, QUEST_ACTION_DELIVERY)
				else
					AddMissionState(player.role, char_id, i, QUEST_ACTION_PENDING)
				end
			else
				--if (quest.show == ALWAYS_SHOW)or (quest.show == ACCEPT_SHOW) then
				AddMissionState(player.role, char_id, i, QUEST_ACTION_PENDING)
			end

			--end
			--local test, state = GetCharMission(player.role, char_id, i)
			--print(state) -- equals 0 when  always show (didnt start), equals 1 when already started.
			--elseif mission.show ~= COMPLETE_SHOW then
			--    AddMissionState(player.role, char_id, i, QUEST_EVENT_ONGOING)
		else
			if (quest.show == ALWAYS_SHOW) or (quest.show == ACCEPT_SHOW) then
				if quest:execute(QUEST_STATE_START,QUEST_EXECUTE_CONDITIONALS) == C_TRUE then
					AddMissionState(player.role, char_id, i, QUEST_ACTION_ACCEPT)
				end
			end
		end
		--end
	end

	return C_TRUE
end

function MissionLog(player_role, id) --fix here
	print('MissionLog', id)
	
	--Can't be implemented properly: Go to npc to check on quest.
	
	local player = player_wrap(player_role)
--	if scriptid == nil or quests[scriptid] == nil then
--		SystemNotice(player.role, 'Quest: Script ID "'..quest_sid..'" doesn\'t exist.')
--		return
--	end
	
	--check if each of

	if quests[id][1].type == QUEST_TYPE_NORMAL or quests[id][1].type == QUEST_TYPE_GLOBAL then
		quests[id][1]:send(player, QUEST_EVENT_LOG)
		--[[
		elseif quests[id][1].type == QUEST_TYPE_RANDOM then
		quests[id][1]:send(player, QUEST_EVENT_LOG)
		--]]
	end

	quests_reset_all_states(player)
end

function CancelMission(player_role, quest_id, npc_id)
	local player = player_wrap(player_role)
	local quest = quest:init(quest_id, player)[1]

	local boolean = quest:execute(QUEST_STATE_LOG, QUEST_EXECUTE_CONDITIONALS)
	if (boolean == C_TRUE) then
		quest:execute(QUEST_STATE_LOG, QUEST_EXECUTE_ACTIONS)
		
		local ret = ClearMission(player_role, quest_id)
		if ret ~= C_TRUE then
			SystemNotice(player_role, 'Quest: Cleared "'.. quests[quest_id][1].name .. '" failed (probably doesn\'t exist already).')
		else
			SystemNotice(player_role, 'Quest: Clearing "'.. quests[quest_id][1].name .. '" successfully.')
		end

		quests_reset_all_states(player)

		return C_TRUE
	end
	return C_FALSE
end

function npc_stddialog_proc(npc, scriptid, player, cmd, p1, p2)
	if cmd == CMD_CM_TALKPAGE then
		if p1 == ROLE_FIRSTPAGE then
			player.interacting_with_npc = npc --to make sure quests are using right char for the player.
			npcs[scriptid].dialog:open(1,player)
		elseif p1 == ROLE_CLOSEPAGE then
			npcs[scriptid].dialog:close()
		end
	elseif cmd == CMD_CM_FUNCITEM then
		--remember to specify the target player on all kinds of network things here.
		npcs[scriptid].dialog.to_player = player
		npcs[scriptid].trade.to_player = player
		npcs[scriptid].dialog.pages[p1].options[p2][2](table.unpack(npcs[scriptid].dialog.pages[p1].options[p2][3]))
	end
end

function npc_tradeonly_proc(npc, scriptid, player, cmd, p1, p2)
	if cmd == CMD_CM_TALKPAGE then
		if p1 == ROLE_FIRSTPAGE then
			npcs[scriptid].trade:open(TRADE_SALE,player)
		elseif p1 == ROLE_CLOSEPAGE then
			npcs[scriptid].trade:close()
		end
	end
end

function npc_custominit_proc(npc, scriptid, player, cmd, p1, p2)
	if cmd == CMD_CM_TALKPAGE then
		if p1 == ROLE_FIRSTPAGE then
			npcs[scriptid].on_init(npc, scriptid, player, cmd, p1, p2)
			--elseif p1 == ROLE_CLOSEPAGE then  --not used
			--    npcs[scriptid].on_end(npc, scriptid, player, cmd, p1, p2)
		end
	end
end

function quests_reset_state(player, npc) --everywhere
	if table.getn(npc.quests) >= 1 then
		ResetMissionState(player.role, npc.char.role)
	end
end

function quests_reset_all_states(player) --everywhere
	for i, npc in npcs do
		if table.getn(npc.quests) >= 1 then
			ResetMissionState(player.role, npc.char.role)
		end
	end
end

function check_item_final_data ( Item )
end

print('- Loading definition file: npcs.lua')
dofile(GetResPath('script/'..mod..'/defs/npcs.lua'))