--[[ Creat_Item
return: tuple of statid,statquality x7 (max is 7, the other 3 are for gems only...)
quality: number between 0 and 32768.
can only change variable types... like energy/dura/+stats...
if ITEMATTR_MAXENERGY then range is 0-9999 (THIS IS REFERRING TO EQUIPS).
0-999 - Gray.
1000-2999 - White.
3000-4999 - Green.
5000-6999 - Purple.
7000-8999 - Red.
9000-9999 - Yellow.

ITEMATTR_MAXENERGY --in terms of equip quality, max 2^15 = 32768.
first right digit, means nothing (as of now).
2 next digits, means type of prefix (check list of prefixes).
1 next digit, means item color quality (as specified above).

ITEMATTR_ENERGY has the max limit = ITEMATTR_MAXENERGY, except it can go -32768,
without making its value wrap.

ITEMATTR_COE_* never work here

Also, recommended not to set changeable stats, so more stats can be changed here:

Also GiveItem comes here, so gm making items/player getting item are same:
print(item,item_type,item_lv,item_event)

Also used by &make (gm command).
--]]

function Creat_Item(item, item_type, item_lv, item_event) --[[��������ʵ��]]--
	print('Item event:'..item_event)
	if ((item_type == 49) or (item_type == 50)) then
		print('test')
		return 1,ITEMATTR_VAL_GEMLV,5
	end

	return
end

--[[ Item Gems

it's very annoying that LUA doesn't have the bitwise operators, like in c,
so game makers had problems in introducing flagged values

also, because all numbers are signed, and some c numbers might be unsigned,
another problem is with negative values being unwanted
because of that, 64 bit signed numbers (lua) need to become 32 bit unsigned numbers (at least I think)

Documented details:
Part 1     range: 0-3. (could be 4 but there are not enough parts for 4 gems).
Part 2,4,6 range: 0-99.
Part 3,5,7 range: 0-9.
--]]

item_gems_mt = {}
function Item_GetGems(p)
	print('def2')
	local num = int_to_signed_int_x(GetItemForgeParam(p,1),32)
	local str = tostring(num)
	if string.len(str) < 10 then
		return {0,0,0,0,0,0,0}
	end
	local values = {}
	local pos = 1
	local size = 1
	while 1 do
		if size == 1 then
			size = 0
		else
			size = 1
		end
		local _string = string.sub(str, pos, pos+size)
		if string.len(_string) <= 0 then
			break
		end
		table.insert(values, tonumber(_string))
		pos = pos+size+1
	end
	print('def')
	return values
end

function Item_SetGems(p, t)
	print('abc')
	local myt = {}
	for i, value in ipairs(t) do
		if (((math.fmod(i,2) ~= 0)) and (string.len(value) == 1)) then
			table.insert(myt, tostring(value))
		else
			if value >= 10 then
				table.insert(myt, tostring(value))
			else
				table.insert(myt, "0"..tostring(value))
			end
		end
	end
	print('abc2')
	print('setgems:'.. tonumber(table.concat(myt,'')))
	return SetItemForgeParam(p,1,tonumber(table.concat(myt,'')))
end

function item_gems_mt.__index (t,key)
	if key == "pointer" or
	key == "gems_t" then
		return rawget(t, key)
	else
		if t.gems_t == nil then
			rawset(t, "gems_t", Item_GetGems(t.pointer))
		end

		if key == "slots" then
			return t.gems_t[1] -- alternative: GetItemHoleNum(t.pointer)
		elseif key == 1 then
			return {t.gems_t[2],t.gems_t[3]}
		elseif key == 2 then
			return {t.gems_t[4],t.gems_t[5]}
		elseif key == 3 then
			return {t.gems_t[6],t.gems_t[7]}
		end
	end
end

function item_gems_mt.__newindex (t, key, value)
	if key == "pointer" or
	key == "gems_t" then
		rawset(t, key, value)
	else
		if t.gems_t == nil then
			rawset(t, "gems_t", Item_GetGems(t.pointer))
		end

		if key == "slots" then
			t.gems_t[1] = value
		elseif key == 1 then
			t.gems_t[2] = value[1]
			t.gems_t[3] = value[2]
		elseif key == 2 then
			t.gems_t[4] = value[1]
			t.gems_t[5] = value[2]
		elseif key == 3 then
			t.gems_t[6] = value[1]
			t.gems_t[7] = value[2]
		end
		Item_SetGems(t.pointer, t.gems_t)
		rawset(t, "gems_t", Item_GetGems(t.pointer))
	end
end

item_stats_mt = {}

function item_stats_mt.__index (t,key)
	if key == "pointer" then
		return rawget(t, key)
	else
		return GetItemAttr(t.pointer,key)
	end
end

function item_stats_mt.__newindex (t, key, value)
	if key == "pointer" then
		rawset(t, key, value)
	else --TODO: put exceptions
		SetItemAttr(t.pointer,key,value)
	end
end

item_mt = {}

function Item_SearchOnPlayers(item)
	function search_onbag(item,player)
		--Step 1: Search bag.
		for j=0, 48-1 do
			--			local cur_item_pointer = GetChaItem(player.role,2,j)
			--			if item.id == GetItemID(cur_item_pointer) then
			--				return player
			--			end
			if (HasItem(player.role, item.id, 1) == C_TRUE) then
				return player
			end
		end
	end

	function search_onequip(item,player)
		--Step 2: Equip bag.
		for j=0, 9 do --max equippable items = 10 (id 1 could be ammo, which is non-existant)
			local cur_item_pointer = GetChaItem(player.role,BAG_TYPE_EQUIP,j)
			if item.id == GetItemID(cur_item_pointer) then
				return player
			end
		end
	end

	function search_onbank(item,player)
		--Step 3: Bank bag.
		for j=0, 9 do --max equippable items = 10 (id 1 could be ammo, which is non-existant)
			if (BankHasItem(player.role, item.id, 1) == C_TRUE) then
				return player
			end
		end
	end

	local players_with_item = {}
	local k = 1
	--print('players')
	for i,player in ipairs(players) do --from "player.lua"
		--print('players2')
		local searching = search_onbag(item,player)
		if (searching == nil) then
			--print('players3')
			searching = search_onequip(item,player)
			if (searching == nil) then
				--print('players4')
				searching = search_onbank(item,player)
			end
		end

		--print('players5')
		if (searching ~= nil) then
			--print('players6')
			players_with_item[k]=player
			k=k+1
		end
	end
	return players_with_item
end

--Item pointers = player item slot pointers.
--That means, items are not detectable outside of player bags/bank.
--can't get ids of items outside of player bags (bank can be detectable but can't get id).

--[[ light_item structure

Used whenever item amount reports need to be done for any amount of players.
Contrasting with item structure, it only needs an item id for the search.
Uses a modified binary search algorithm to do this search (because the game can't return item amounts).

--]]

function light_item_report(l_i)
	local _players = l_i.players
	if (_players == nil) then
		_players = players
	end

	function item_search_binary(player,item_id,low,high,func)
		if (high<low) then
			return 0 --no items
		end
		local mid = low + math.floor((high - low)/2)

		if (func(player.role, item_id, mid) == C_FALSE) then -- means, mid is higher than the real amount (0 == false)
			return item_search_binary(player,item_id,low,mid-1,func)
		else
			if (low==mid) then
				if (func(player.role, item_id, high) == C_TRUE) then
					return high
				else
					return mid
				end
			else
				return item_search_binary(player,item_id,mid,high,func)
			end
		end
	end

	function item_search_bag_slots(player,id)
		local result = {}
		for i=0,48-1 do
			--print('id:'..id..'getitemID:'..GetItemID(GetChaItem(player.role,2,i)))
			if (id == GetItemID(GetChaItem(player.role,BAG_TYPE_INV,i))) then
				result[table.getn(result)+1] = i
			end
		end
		return result
	end

	function item_search_equip(player,id)
		local result = {amount=0,slots={}}
		for i=0, 9 do --max equippable items = 10 (id 1 could be ammo, which is non-existant) TODO: Set to top 2 standards?
			if (id == GetItemID(GetChaItem(player.role,BAG_TYPE_EQUIP,i))) then
				result.amount = result.amount + 1
				result.slots[table.getn(result.slots)+1] = i
			end
		end
		return result
	end

	--GetKbCap(player.role) gets bugged sometimes -.- replaced with 48
	local result = {}
	local max_stack_size = 9999
	for j,player in ipairs(_players) do
		local _bag = {amount=0,slots={}}

		_bag.amount = item_search_binary(player,l_i.id,1,max_stack_size*48,HasItem)
		_bag.slots = item_search_bag_slots(player,l_i.id)

		result[j]={
			bag=_bag,
			bank=item_search_binary(player,l_i.id,1,max_stack_size*48,BankHasItem),
			equip=item_search_equip(player,l_i.id)
		}
	end

	return result
end

--[[ item structure

Stands for an item a player possess.
A pointer to the item needs to be used. Even though there are items with same id in
different slots, the item could still have the same pointer (for non-equips).
Gives greater detail to what the item is (including gems, stats...).
Inherited from light_item and has item_stat and item_gems structures.
item_wrap needs an item pointer. Player (the second parameter) is not really needed
but it's the only way the item can have an owner (no other way to detect otherwise,
and might think it's associated with the floor).

Multiple players might not be wanted (still to develop in this area).

--player based item: player + item (item structure).
--floor based item: item only (item structure).
--generic item search: id only (light_item structure).
--player based item search: id + player (light_item structure).

Item pointer is gotten from the item's ID and player role.

An amount of 0 = an invalid pointer. Do not forget about that!

Basicly light_item refers to tsv.iteminfo directly.

--]]

function item_mt.__index (t,key)
	if rawget(t, key) == nil then
		if	key == "stats" then
			local stats = {}
			setmetatable(stats, item_stats_mt)
			stats.pointer = t.pointer
			rawset(t, key, stats)
		elseif	key == "player" then
			if (rawget(t, key) == nil) then
				print("Item Structure: Single player not set.")
			end
		elseif	key == "players" then
			--returns the id of all players with the item.
			local array = Item_SearchOnPlayers(t)
			rawset(t, key, array)
		elseif 	key == "gems" then
			local gems = {}
			setmetatable(gems, item_gems_mt)
			gems.pointer = t.pointer
			rawset(t, key, gems)
		else
			rawget(rawget(t, "light"), key) --grab info from light_item
		end
		-- "IsItemValid"
		-- "UseItemFailed"
	end
	return rawget(t, key)
end

function item_mt.__newindex (t, key, value)
	if key == "player" then --and also "slot"
		rawset(t, key, value) --if it's a player, then set the value normally. Some items can belong to multiple players.
	elseif key == "gems" then
		t[key].slots = value.slots
		t[key][1] = value[1]
		t[key][2] = value[2]
		t[key][3] = value[3]
	else
		print("Item Structure: Can't set "+key+" attribute")
	end
end

function item_wrap(pointer,...)
	local arg = ...
	local player = nil
	print('asd')
	if (#arg >= 1) then
		player = arg[1]
	end
	item = {}
	print('item_a')
	item.pointer = pointer
	print('item_b')
	item.player = player
	print('item_c')
	item.id = GetItemID(item.pointer)
	print('item_d')
	if (item.id ~= 0) then
		print('item_e')
		print(item.id)
		item.light = light_item_wrap(item.id,{item.player})
		print('item_f')
		setmetatable(item, item_mt)
		print('item_g')
		return item
	else
		print('Item Structure: Invalid pointer.')
		return -1
	end
end



light_item_mt = {}

function light_item_mt.__index(t,key)
	if key == "report" then
		rawset(t, key, light_item_report(t))
	elseif rawget(t, key) == nil then --all constants go here - only need to be initialized once.
		if 	key == "name" or key == "id" then
			rawset(t, key, tsv.iteminfo[id_or_name][key]) --use id_or_name just for these 2, for speed only.
		else
			rawset(t, key, tsv.iteminfo[t.id][key]) -- will use id for the rest.
		end
	end
	return rawget(t, key)
end

function light_item_mt.__newindex(t, key, value)
	if	key == "players" then
		rawset(t, key, array) --this will the player restriction for the report
	else
		print("Light Item Structure: Can't set "+key+" attribute") --editing tsv won't matter to game server.
	end
end

--kept only as compatibility
function light_item_wrap(id_or_name,...)
	print('light_item_a_')
	light_item = {}
	print('light_item_a')
	light_item.players = ...
	print('light_item_b')
	light_item.id_or_name = id_or_name
	print('light_item_c')
	
	setmetatable(light_item, light_item_mt)
	print('light_item_d')
	return light_item
end




--In top 2, C_TRUE should be used to determine true. In top 1, it's C_FALSE.
function CanUseItem(role ,Item)
	return C_TRUE
end

function CanUseSkill(atk ,skillNo,skillLV)
	return C_TRUE
end