--Game functions:
--player_role is a char_role with:
--- inventories (regular, temporary, bank).
--- an interface (anything that affects interface directly, like the skill list for example).

BickerNotice	= function (player_role, scrolling_message) end
--Shows a scrolling_message on player_role's screen.


--Misc:
ToDword	= function (number) end
--Transforms a number or string into a 32bit signed number.

AdjustTradeItemCess	= function (player_role, min_tax, new_tax) end
--Adjusts tax percent of sea commerce. Not used in top 2.x. Can be done purely in lua by adjusting the commerce permit's energy stat, so this is basically useless.

--Tournament:
AddAmphitheaterSeason	= function (tournament_season_id) end
--Adds another tournament season with tournament_season_id id.
AmphitheaterTeamCancel	= function (tournament_team_id) end
--Cancels a tournament team with tournament_team_id.
AmphitheaterTeamSignUP	= function (tournament_team_id, tournament_team_leader_player_role, tournament_player_role_1, tournament_player_role_2) end
--Creates a tournament team with 3 members total with tournament_team_id team id and team_leader_player_role as the team leader.
CaptainConfirmAsk	= function (player_role, tournament_team_id) end
--Shows leader confirmation dialog to a player in the tournament team.
CloseReliveByState	= function () end
--Don't allow ressurection at tournament arena depending on state (which is always 0 - no input allowed). Returns previous state.
CleanMapFlag	= function (tournament_team_id_1, tournament_team_id_2) end
--Clears all map flags associated with tournament_team_id_1 vs tournament_team_id_2. Not used by top.
DisuseAmphitheaterSeason	= function (tournament_season_id, unknown, unknown2) end
--Cancels a tournament season tournament_season_id. 2nd and 3rd parameters are unknown (server uses 1 at unknown, and nil at unknown2).
GetAmphitheaterNoUseTeamID	= function () end
--Returns a new free tournament_team_id. Should be used while creating a tournament.
GetAmphitheaterRound	= function () end
--Returns the current tournament_round_id.
GetAmphitheaterSeason	= function () end
--Returns the current tournament_season_id.
GetAmphitheaterTeamCount	= function () end
--Returns the amount of team left.
IsAmphitheaterLogin	= function (player_role) end
--Can player enter the tournament?
IsMapFull	= function (tournament_map_id) end
--Returns 1 if the tournament_map_id is full (if both teams are there).
OpenAmphitheater	= function () end
--Opens the tournament leaderboard.
UpdateAmphitheaterRound	= function (tournament_season_id, tournament_round_id) end
--Updates the current tournament round of tournament_season_id and tournament_round_id.
UpdateWinnum	= function (tournament_team_id) end
--Adds a win to tournament_team_id. Not used by top.
UpdateMap	= function (tournament_team_id) end
--Updates the tournament map.
UpdateMapAfterEnter	= function (tournament_leader_id, tournament_map_id) end
--Updates the tournament_leader_id on tournament_map_id.
UpdateMapNum	= function (tournament_team_id, tournament_map_id, tournament_map_flags) end
--Updates the tournament_team_id based on tournament_map_id with tournament_map_flags.
GetCaptainByMapId	= function (tournament_map_id) end
--Gets the tournament team leader based on tournament_map_id.
GetMapFlag	= function (tournament_team_id) end
--Returns the flags set to the tournament_team_id.


--Packet:
--Pop: Removes an item from end of array.
--Push: Adds an item to the end of array.
GetPacket	= function () end
--Returns a new packet_pointer.
ReadByte	= function (packet_pointer) end
--Pops a 1 byte number from packet_pointer.
ReadCmd	= function (packet_pointer) end
--Pops a game command from packet_pointer.
ReadDword	= function (packet_pointer) end
--Pops a 4 byte number from packet_pointer.
ReadString	= function (packet_pointer) end
--Pops a string from packet_pointer.
ReadWord	= function (packet_pointer) end
--Pops a 2 byte number from packet_pointer.
WriteByte	= function (packet_pointer, one_byte_number) end
--Pushes a one_byte_number to packet_pointer.
WriteCmd	= function (packet_pointer, game_cmd) end
--Pushes a game_cmd to packet_pointer.
WriteDword	= function (packet_pointer, four_byte_number) end
--Pushes a four_byte_number to packet_pointer.
WriteString	= function (packet_pointer, string) end
--Pushes a string to packet_pointer.
WriteWord	= function (packet_pointer, two_byte_number) end
--Pushes a two_byte_number to packet_pointer.
SendPacket	= function (packet_pointer) end
--Sends packet to client.
SynPacket	= function (packet_pointer) end
--Syncronizes packet with client.

--Spawn points / char movement:
AddBirthPoint	= function (area_city_name, map_short_name, pos_x, pos_y) end
--Adds a char spawn point.
--area_city_name: the name of the area in areaset.txt. It will be used whenever player saves spawn point.
--If there is more than 1 spawn point set to the same city name, the spawn will be randomized between them.
--map_short_name: the name of the map where it is located. The name is located in mapinfo.txt.
--pos_x,pos_x: real in-game coordinates.
ClearAllBirthPoint	= function () end
--Removes all spawn points added up until now.
SetSpawnPos	= function (char_role, area_city_name) end
--Sets the spawn point area_city_name for char_role.

BeatBack	= function (char_role, char_def_role, distance) end
--Pushes back char_def_role distance away from char_role. Negative distance will make enemy to be pushed back and forth.
MoveCity	= function (char_role, area_city_name) end
--Moves char_role one of the spawn points given by area_city_name.
MoveTo	= function (char_role, pos_x, pos_y, map_short_name) end
--Moves char_role to coordinates pos_x,pos_y in map_short_name instantenuously.
ChaMove	= function (char_role, pos_x, pos_y) end
--Moves char_role to coordinates pos_x,pos_y using the character's movement speed.
ChaMoveToSleep	= function (char_role, pos_x, pos_y) end
--Moves char_role to coordinates pos_x,pos_y using the character's movement speed and when arriving,
--make it stand still.

--NPCs:
AddTradeNPC	= function (npc_name) end
--Makes npc into a trading npc. Not used by top.
SetNpcScriptID	= function (npc_char_role, npc_id) end
--Sets a npc_id for the npc_char_role. When server is started, npcs don't have ids associated to them.
--This function does that at ResetNpcInfo callback function. Pretty much has no other use.

---Help:
AddHelpNPC	= function (help_npc_name) end
--Associate the help_npc_name with help info system (see AddHelpInfo)
AddHelpInfo	= function (help_info_key, help_info_value) end
--Adds a new help info item. /"help_info_key" is what a player needs to input into message system for
--npc to ouput "help_info_value" into system message box.
ClearHelpNPC	= function () end
--Removes all help npcs added up until now.

--General:
AddExp	= function (char_role, npc_id, min_exp, max_exp) end
--Makes npc_id give a random amount of experience between min_exp and max_exp to char_role.
AddExpAndType	= function (char_role, npc_id, exp_type, min_exp, max_exp) end
--Makes npc_id give a random amount of experience between min_exp and max_exp of exp_type to char_role.
--exp_type: 1: regular experience (see AddExp), 2: life experience(, 3: rebirth experience?)
AddLifeExp	= function (char_role, npc_id, min_life_exp, max_life_exp) end
--Makes npc_id give a random amount of experience between min_life_exp and max_life_exp to char_role. Not used by top.
AddMoney	= function (char_role, min_money, max_money) end
--Adds between min_money and max_money to char_role.
AddSailExp	= function (char_role, npc_id, min_rebirth_exp, max_rebirth_exp) end
--Makes npc_id give a random amount of rebirth experience between min_rebirth_exp and max_rebirth_exp to char_role.


--Skills:
AddChaSkill	= function (char_role, skill_id, skill_level, skill_points_consumed, appear_on_skill_list) end
--Adds a skill_id with skill_level to a char_role, with an amount of skill_points_consumed from ATTR_TP.
--If appear_on_skill_list is 1, then it will appear on the skill list. This option has no effect on non-players.
AddSkill	= function (char_role, npc_id, skill_id, skill_level) end
--Makes npc_id give skill_id with skill_level to char_role. Not used by top.
AddState	= function (char_role, skill_effect_id, skill_effect_level, time_sec) end
--Assigns skill_effect_id to char_role, with skill_effect_level and time_sec.

--Reputation:
AddCreditX	= function (char_role, reputation_amount) end
--Adds reputation_amount to the char_role specified.
---Master/Disciple:
AddMasterCredit	= function (player_role, reputation_amount) end
--Adds reputation_amount to the master of player_role.
DelCredit	= function (char_role, reputation_amount) end
--Removes reputation_amount from char_role.


--Energy system (Corals): 
AddEquipEnergy	= function (char_role, equipment_slot_id, min_energy_amount, max_energy_amount) end
--Adds a random amount between min_energy_amount and max_energy_amount energy to equipment_slot_id on
--char_role, if the equipment uses energy. If it doesn't, this function does nothing. Returns ?.

--Drop system:
AddHate	= function (defender_char_role, attacker_char_role, hate_amount) end
--Makes attacker_char_role do pseudo damage to defender_char_role. In regular top, pseudo damage will effect
--which char is attacked, and which char gets the drops.

--Item:
BagTempHasItem	= function (player_role, item_id, item_amount) end
--Returns true if player_role's temporary inventory has item_amount or more of item_id.
BankHasItem	= function (player_role, item_id, item_amount) end
--Returns true if player_role's bank inventory has item_amount or more of item_id.

---Item stats:
AddItemAttr	= function (...) end
--Alternative to SetItemAttr - will add to an item stat. Not used by top.
AddItemEffect	= function (char_role, item_pointer, appear) end
--Make the item_pointer's effect appear on char_role. Appear: 1: appear, 0: disappear.

--Item hidden stats (used by gems - not saved to db, so need to be re-added every time player joins):
AddItemFinalAttr	= function (item_pointer, item_stat_id, stat_hidden_amount) end
--Adds stat_hidden_amount of item_stat_id to item_pointer.
GetItemFinalAttr	= function (item_pointer, item_stat_id) end
--Gets the hidden stat value of item_stat_id in item_pointer. Not used by top.
ResetItemFinalAttr	= function (item_pointer) end
--Resets the hidden stats in item_pointer.
SetItemFinalAttr	= function (item_pointer, item_setat_id, stat_hidden_amount) end
--Sets the stat_hidden_amount of item_stat_id to item_pointer. Not used by top.


--Player:
---Inventory space:
AddKbCap	= function (player_role, added_inventory_slots) end
--Increase the player_role's inventory amount of slots by the added_inventory_slots.
GetKbCap	= function (player_role) end
--Get the player_role's current amount of inventory slots.

ChaIsBoat	= function (player_role) end
--Checks if the player_role is a boat. Returns 1 as a yes, and 0 as no.


--Lottery system:
AddLotteryIssue	= function (lottery_instance_id) end
--Adds the lottery_instance_id to db.
CalWinLottery	= function (lottery_instance_id, max_ticket_numbers) end
--Returns a winner for lottery_instance_id with max_ticket_numbers.
--There's a low chance of max_ticket_numbers of ever being used.
DisuseLotteryIssue	= function (lottery_instance_id) end
--Removes a lottery_instance_id from db.
GetLotteryIssue	= function () end
--Returns the current lottery instance.
GetWinLotteryItemno	= function (lottery_instance_id, winning_number_digit) end
--Returns the winning_number_digit from lottery_instance_id.
OpenLottery	= function (player_role, npc_id) end
--Makes npc_id open the lottery dialog box for player_role.

--Quest:
--quest_id vs script_id:
--Each quest has a script. In top, quests can have more than 1 script associated with it.
--For reduced confusion, top-recode makes script_id and quest_id equal, even in scripts - instead of using
--another script id, use an if. Scripts are also used for triggers (don't need to be quest related).

--quest_id vs npc_quest_id:
--npcs have an array of quests_id's associated with it (at most 32) - which are defined on npc creation.
--npc_quest_id is the index of that array.
AddMission	= function (player_role, quest_id, script_id) end
--Makes player_role get associated with quest_id and script_id.
AddMissionState	= function (player_role, npc_id, npc_quest_id, quest_status_id) end
--Makes the npc_quest_id the npc_id have the added quest_status_id towards player_role.
--quest_status_id: uses QUEST_ACTION_* constants.
ClearMission	= function (player_role, quest_id) end
--Removes the quest_id from the player_role's quest list.
GetCharMission	= function (player_role, npc_id, npc_quest_id) end
--Gets the quest_id from the npc_quest_id and if it's started or not.
GetCharMissionLevel	= function (player_role) end
--Gets value of ATTR_LV or player_role.
GetMisScriptID	= function (player_role, quest_id) end
--Gets the script_id from quest_id.
GetMissionInfo	= function (player_role, npc_id, quest_dialog_list_item_id) end
--Gets which mission player sees on quest list dialog box.
GetMissionPage	= function (player_role, quest_id) end
--Returns does page exist, which page comes before, which page comes after, and current state of mission.
GetMissionState	= function (player_role, npc_id, npc_quest_id) end
--Returns the state of the mission for the npc (return is a set of QUEST_ACTION_*).
GetMissionTempInfo	= function (player_role, npc_id) end
--Returns the temporary info for the quest in npc_id - if it was succesful, quest_id, quest_state_id, and quest_type_id.
GetNextMission	= function (player_role, npc_id) end
--Returns the info for the next quest in the small random quest cycle - if it was successful, quest_id, script_id, quest_state_id.
GetNpcHasMission	= function (npc_id) end
--Returns 1 if npc_id has a quest.
GetNumMission	= function (player_role, npc_id, listtype) end
--Returns the current number associated with small random mission.
HasCancelMissionMoney	= function (player_role) end
--Returns 1 if player_role has enough money to cancel a quest.
HasMission	= function (player_role, quest_id) end
--Returns 1 if player_role has the quest in the quest list.
HasMisssionFailure	= function (player_role, quest_id) end
--Returns 1 if player_role has failed the quest_id.
HasRandMission	= function (player_role, random_quest_id) end
--Returns 1 if player_role has the random_quest_id.
HasRandMissionCount	= function (player_role, random_quest_id, small_random_quest_count) end
--Returns 1 if player_role is at that small_random_quest_count in random_quest_id.
HasRandMissionNpc	= function (player_role, random_quest_id, npc_id, area_id) end
--Returns 1 if random_quest_id's npc_id is in the area_id.
HasRandNpcItemFlag	= function (player_role, random_quest_id, npc_id) end
--Returns 1 if npc_id has an item flag. ?
IsMisNeedItem	= function (player_role, item_id) end
--Checks if one of the triggers currently active need that item_id.
IsMissionFull	= function (player_role) end
--Checks if player_role's quest list is full.
IsMissionState	= function (current_quest_state_flags, quest_state_id) end
--Checks if quest_state_id is in current_quest_state_flags. current_quest_state_flags is gotten from GetMissionState.
ResetMissionState	= function (player_role, npc_id) end
--Removes all ! and ? marks on top of the npc_id.
ResetRandMissionCount	= function (player_role, quest_id) end
--Resets small random quest count (meaning: the amount of successes and failures continue the same).
ResetRandMissionNum	= function (player_role, quest_id) end
--Resets small random quest cycle.
SaveMissionData	= function (player_role) end
--Saves quests, quest records and triggers to db (mostly triggers - others are already saved automatically).
SetMissionComplete	= function (player_role, mission_id) end
--Sets the mission state to QUEST_ACTION_DELIVERY directly to player_role.
SetMissionFailure	= function (player_role, mission_id) end
--Sets the mission state to failed directly to player_role.
SetMissionPage	= function (player_role, npc_id, quest_dialog_list_item_id, index_unknown) end
--Sets which mission player sees on quest list dialog box.
SetMissionPending	= function (player_role, mission_id) end
--Sets the mission state to QUEST_ACTION_PENDING directly to player_role.
SetMissionTempInfo	= function (player_role, npc_id, quest_id, quest_state_id, quest_type_id) end
--Sets the data that accept/continue/end quest dialog box will show up when opened.
SetNpcHasMission	= function (npc_char_role, is_there_a_mission_associated_with_npc) end
--If is_there_a_mission_associated_with_npc is set to 1, then ? or ! sign will show up on npc_char_role.
SetRandMissionData	= function (player_role, quest_id, small_random_quest_id, rand_p2, rand_p3, rand_p4, rand_p5, rand_p6, rand_p7) end
--Sets data for small random quest.
TakeCancelMissionMoney	= function (player_role, npc) end
--Removes money when attempting to cancel quest. Probably the amount specified for random missions or hard coded.


---Random quest:
AddRandMission	= function (player_role, quest_id, script_id, rand_p1, rand_p2, rand_p3, rand_p4, rand_p5, rand_p6, rand_p7) end
--Makes player_role get associated with quest_id and script_id arguments with 7 optional parameters.
--In top, the rand_p* is as follows: type, level, exp, money, prizedata, prizetp, numdata
--The first is only used when making the smaller random quests.
AddRandMissionNum	= function (player_role, random_quest_id) end
--Adds another random quest for player_role using the random_quest_id.
GetRandMission	= function (player_role, ramdom_quest_id) end
--Returns all of the rand_p* input to AddRandMission (7 values).
GetRandMissionCount	= function (player_role, random_quest_id) end
--Returns the amount of small random quests.
GetRandMissionData	= function (player_role, random_quest_id, small_ramdom_quest_id) end
--Returns the last 6 rand_p* parameters that only apply to the small random quests.
GetRandMissionNum	= function (player_role, random_quest_id) end
--Returns the current small random quest number.
CompleteRandMissionCount	= function (player_role, random_quest_id) end
--Returns the amount of successful small random quests. 
FailureRandMissionCount	= function (player_role, random_quest_id) end
--Returns the amount of failed small random quests.

--Trigger:
--Triggers have 6 parameters. Uses script_id (just like quests).
AddNpcTrigger	= function (npc_char_role, trigger_p1, trigger_p2, trigger_p3, trigger_p4, trigger_p5, trigger_p6) end
--Adds a trigger to npc_char_role. 
AddTrigger	= function (char_role, script_id, trigger_p1, trigger_p2, trigger_p3, trigger_p4, trigger_p5, trigger_p6) end
--Adds a trigger to char_role's script_id.
ClearTrigger	= function (char_role, script_id) end
--Clears the trigger associated with script_id and char_role.
DeleteTrigger	= function (char_role, script_id) end
--Deletes the trigger - script_id can be reused by AddTrigger.

--Map:
GetChaMapName	= function (char_role) end
--Returns the map name where the char_role is.
GetChaMapOpenScale	= function (player_role) end
--Returns the size scale of map the player_role is on. Most probably it's always going to be 1 (because you can't change map size) so this is useless. Not used by top.
GetChaMapType	= function (char_role) end
--Returns the map_type_id of current map the character is on.
GetCurSubmap	= function () end
--Returns the current map copy the script is on. Call the function (or derived) only from a map script.
GetMapActivePlayer	= function (map_copy_pointer) end
--Returns the amount of alive players in map_copy_pointer at the moment.
GetMapName	= function (char_role) end
--Returns the name of the map the char_role is on. Not used by top.
GetMapPlayer	= function (map_copy_pointer) end
--Returns the amount of players in map_copy_pointer at the moment.
IsInMap	= function (char_role, map_name, x_coord, y_coord, width, height) end
--Returns 1 if char_role is within the rectangle coordinates in map_name.
IsInSameMap	= function (char_role, char_role2) end
--Returns 1 if char_role and char_role2 are in the same map.
IsMapChar	= function (char_role, map_name) end
--Returns 1 if char_role is in map_name. Not used by top properly!
IsMapNpc	= function (npc_role, map_short_name, npc_id) end
--Returns 1 if npc_id is in map_short_name. Unknown use for npc_role, but it has to be a npc.
MapChaLight	= function () end
--Returns 0. Completely useless.
SetCurMap	= function (map_short_name) end
--Sets the current map to map_short_name. All the scripts refering to a map without requiring map pointer will use this map.
SetMap	= function (map_short_name, map_id) end
--Sets the map_id for map_short_name. Set map_id the same as mapinfo id, otherwise it will get very confusing.

---Config section:
MapCanGuild	= function (map_pointer, can_attack_guild_mates) end
--If can_attack_guild_mates is 1, map_pointer will allow players to attack guildmates.
MapCanPK	= function (map_pointer, can_attack_other_players) end
--If can_attack_other_players is 1, map_pointer will allow players to attack other players.
MapCanSavePos	= function (map_pointer, can_save_spawn_point) end
--If can_save_spawn_point is 1, map_pointer will allow players to save respawn points.
MapCanStall	= function (map_pointer, can_place_stalls) end
--If can_place_stalls is 1, map_pointer will allow players to set up stalls.
MapCanTeam	= function (map_pointer, can_party) end
--If can_party is 1, map_pointer will allow players to be in parties.
MapCopyNum	= function (map_pointer, map_copy_amount) end
--Sets the amount of copies of the map to start.
MapCopyStartType	= function (map_pointer, map_copy_start_type) end
--Sets the map_copy_start_type of each map copy started from map_pointer. Unknown what's this used for.
MapType	= function (map_pointer, map_type_id) end
--Sets the map_type_id of the map_pointer.
SetMapGuildWar	= function (map_pointer, can_guild_war) end
--If can_guild_war is 1, map_pointer will allow players to play guild war (it has its set of scripts).
SingleMapCopyPlyNum	= function (map_pointer, amount) end
--Sets the amount of players that can enter each map copy of map_pointer.
RepatriateDie	= function (map_pointer) end
--Unknown. Possibly will make player lose affiliation to pirate/navy side if ded.

---Entrance portal:
CallMapEntry	= function (map_short_name) end
--Starts the map_short_name entrance portal - which will depend on how it's setup. Not used by top.
CloseMapEntry	= function (map_short_name) end
--Closes the map_short_name entrance portal.
FinishSetMapEntryCopy	= function (map_entry_pointer, map_entry_copy_id) end
--Sends map_entry_pointer info to client (including changed spawn points).
GetMapEntryCopyObj	= function (map_copy_entry_pointer, map_entry_copy_id) end
--Returns a map_entry_copy_pointer.
SetMapEntryEntiID	= function (map_entry_pointer, char_id, obj_event_id) end
--Sets the char_id and obj_event_id for the map entrance portal. obj_event_id refers to id in objevent.txt tsv. TODO: Detail objevent.txt properly.
SetMapEntryEventName	= function (map_entry_pointer, map_entry_name) end
--Sets the name shown at the entrance portal of the map.
SetMapEntryMapName	= function (map_pointer, map_short_name) end
--Sets the map_short_name where the entrance portal to map_pointer will be located.
SetMapEntryTime	= function (map_pointer, open_start_date_time, open_frequency_time, close_delta_time, map_close_delta_time) end
--Sets the time when the entrace portal will be first shown, its frequency, its close time and the map close time. Syntax as follows:
--start_date: year/month/day/hour/minute. 
--open_frequency_time,close_delta_time,map_close_delta_time: hour/minute/second.

---Weather:
AddWeatherRegion	= function (skill_effect_id, casting_frequency, duration, x, y, width, height) end
--Adds a weather effect to the current map. Must be called from map lua script.
ClearMapWeather	= function () end
--Removes all weather effects from map. Can only be used in map scripts. Not used by top (pretty much closing the map will do this automatically).

---Map copy:
ClearAllSubMapCha	= function (map_copy_pointer) end
--Removes all players from the map_copy_pointer.
ClearAllSubMapMonster	= function (map_copy_pointer) end
--Removes all mobs from the map_copy_pointer.
CloseMapCopy	= function (map_short_name, map_copy_id) end
--Closes the map_copy_id. If map_copy_id is not specified, closes the last one opened.
GetChaMapCopy	= function (char_role) end
--Returns the map_copy_pointer where the char_role is.
GetChaMapCopyNO	= function (char_role) end
--Returns the map_copy_id where the char_role is. Not used by top.
GetMapCopyID	= function (map_copy_pointer) end
--Gets the id of the map copy from map_copy_pointer. Used by MoveCity.
GetMapCopyID2	= function (map_copy_pointer) end
--Gets the id of the map copy from map_copy_pointer. Used by CloseMapCopy.
GetMapCopyIDByMapID	= function (player_role, map_id, map_player_id) end
--Returns the map copy id. All 3 parameters are needed.
GetMapCopyParam	= function (map_copy_pointer, map_copy_param_id) end
--Returns the param input into SetMapCopyParam.
GetMapCopyParam2	= function (map_copy_pointer, map_copy_param_id) end
--Same as above, but probably another array.
GetMapCopyPlayerNum	= function (map_copy_id) end
--Returns the number of players in the map copy.
IsInSameMapCopy	= function (char_role, char_def_role) end
--Returns true if both chars are in the same map copy.
KillMonsterInMapByName	= function (map_copy_pointer, char_name) end
--Kills the monster with char_name in map_copy_pointer.
MapCopyNotice	= function (map_copy_pointer, message) end
--Sends a notice to all players in tha map_copy_pointer.
MapCopyNotice2	= function (map_id, map_copy_id, message) end
--Sends a notice to all players in the map_copy_id from map_id. If map_copy_id is negative, all map copies players will receive the notice. Not used in top.
SetMapCopyParam	= function (map_copy_pointer, map_copy_param_id, value) end
--Sets the value of the map_copy_param_id param in map_copy_pointer. Domain of map_copy_param_id: 0 <= map_copy_param_id <= 15. value: 0 <= value <= 255, and must be integer.
SetMapCopyParam2	= function (map_copy_pointer, map_copy_param_id, value) end
--Sets the value of the map_copy_param_id param in map_copy_pointer. Domain of map_copy_param_id: 0 <= map_copy_param_id <= 7. value must be an integer.

---Hard-coded map players iterators (can be deprecated by adding/removing players at onjoin/leave map copy):
BeginGetMapCopyPlayerCha	= function (map_copy_id) end
--Initializes the players iterator.
GetMapCopyNextPlayerCha	= function (map_copy_id) end
--Returns the next player name in the iterator.
DealAllActivePlayerInMap	= function (map_copy_pointer, func) end
--iterator that calls func for each player alive in map_copy_pointer. Use GetMapActivePlayer to know the amount of alive players for iteration.
DealAllPlayerInMap	= function (map_copy_pointer, func) end
--iterator that calls func for each player in map_copy_pointer (including dead ones). Use GetMapPlayer to know the amount of players for iteration.

--Boat:
BoatBerth	= function (player_role, boat_spawn_id, x_coord, y_coord, angle) end
--Spawns the player's boat at x_coord and y_coord and angle. Player's ATTR_BOAT_BERTH stat will be set to boat_spawn_id. Not used by top.
BoatBerthList	= function (player_role, npc_id, boat_service_id, boat_spawn_id, x_coord, y_coord, angle) end
--Associates the boat_service_id with the npc_id and boat_spawn_id. x_coord, y_coord, angle are only used
--if specifying the spawn point for the boat when using the boat spawn service.
BoatBuildCheck	= function (player_role, boat_type_id) end
--Returns 1 if the boat amount didn't reach limit.
BoatLuanchOut	= function (player_role, boat_id, boat_spawn_id, x_coord, y_coord, angle) end
--Set sail with selected boat_id at boat_spawn_id with x_coord, y_coord and angle.
BoatTrade	= function (player_role, boat_spawn_id) end
--Shows the boat commerce shop with boat_spawn_id.
CreateBoat	= function (char_role, boat_id, boat_spawn_id) end
--Creates a boat with boat_id on boat_spawn_id.
GetBoatCtrlTick	= function (player_role) end --Return: player_boat_tick.
--Gets the player_boat_tick of player_role.
SetBoatCtrlTick	= function (player_role, player_boat_tick) end --(nothing here means return value is always nil).
--Sets the player_boat_tick of player_role.

--Auction
EndAuction	= function (item_id) end
--Ends an auction with item_id.
ListAuction	= function (char_role, npc_role) end
--Shows the auction dialog box.
StartAuction	= function (item_id, item_name, item_amount, item_price, item_minimum_bid) end
--Starts an auction for item_amount of item_id with item_name starting at item_minimum_bid with base item_price.


--Picking items from floor:
FindItem	= function (x_coord, y_coord, radius) end --(2.4 src has return disabled)
--Returns item_pointer of an item found on the floor at x_coord, y_coord with radius.
PickItem	= function (player_role, item_pointer) end
--Picks the item_pointer that is on the floor and put it in player_role's inventory.



ChaActEyeshot	= function (char_role, is_in_sight) end
--Determines if char_role will check if player is_in_sight or not. (or will be seen or not, maybe)
ChaNotice	= function (player_role, message) end
--Shows the message at the system message box to the player only.
ChaPlayEffect	= function (char_role, skill_effect_id) end
--Shows a skill_effect_id on char_role.
ChaUseSkill	= function (char_role, char_def_role, skill_id) end
--Makes char_role cast skill_id on char_def_role.
ChaUseSkill2	= function (char_role, skill_id, skill_level, x_coord, y_coord) end
--Makes char_role cast skill_id with skill_level on x_coord, y_coord.
ChangeExternValue	= function (char_role, function_name, value) end
--Calls a callback function_name with value for char_role.
ChangeJob	= function (char_role, class_id) end
--Changes char_role's ATTR_JOB stat to class_id.
CheckBagItem	= function (player_role, item_id) end
--Returns the amount of item_id on player_role's inventory.
CheckChaPKState	= function (player_role) end
--Returns 1 if player_role can kill others players.
CheckChaRole	= function (char_role) end
--Returns 1 if char_role is a player.
CheckFusionItem	= function (item_pointer, fused_item_pointer) end
--Returns 1 if item_pointer is fused with fused_item_pointer.
ClearAllConvoyNpc	= function (player_role, quest_id) end
--Removes all npcs following player_role while on quest_id.
ClearConvoyNpc	= function (player_role, quest_id, npc_follower_id) end
--Removes npc_follower_id following player_role while on quest_id.
ClearFightSkill	= function (player_role) end
--Returns all skill points used per level of all weapon skills which this function removes from player_role.
ClearFlag	= function (player_role, quest_id, flag) end
--Removes 1 flag from quest_id for player_role.
ClearHideChaByRange	= function (char_role, rel_x_coord,rel_y_coord,radius,flag) end
--Makes char_role recognize there's an enemy within rel_x_coord,rel_y_coord and radius with flag.
ClearRecord	= function (player_role, quest_id) end
--Removes completion record of quest_id for player_role.
ConvoyNpc	= function (char_role, npc_id, quest_id, char_id, ai_type) end
--Makes npc_id which is of char_id to follow char_role using ai_type as part of quest_id.
CreateCha	= function (char_id, x_coord, y_coord, angle, respawn_time) end
--Summons char_id at x_coord, y_coord, angle which will have respawn_time on current map script.
CreateChaEx	= function (char_id, x_coord, y_coord, angle, respawn_time, map_copy_pointer) end
--Summons char_id at x_coord, y_coord, angle which will have respawn_time on map_copy_pointer.
CreateChaX	= function (char_id, x_coord, y_coord, angle, respawn_time, char_role) end
--Summons char_id at x_coord, y_coord, angle which will have respawn_time and have char_role
--as responsible for spawning it.
CreateEventEntity	= function (resource_type_id, map_copy_pointer, char_name, char_id, x_coord, y_coord, angle) end
--Summons char_id with char_name as a resource_type_id with x_coord, y_coord and angle on map_copy_pointer.
CreateGuild	= function (player_role, is_pirate) end
--Creates a new guild with player_role as leader. is_pirate: 1, it will be a pirate guild. 0: navy.
DebugInfo	= function (data) end
--Does absolutely nothing.
DelBagItem	= function (char_role, item_id, item_amount) end
--Removes item_amount of item_id in char_role's inventory.
DelBagItem2	= function (char_role, inventory_slot, item_amount) end
--Removes item_amount of an item at inventory_slot in char_role. Not used by top.
DelCha	= function (char_role) end
--Removes char_role from server.
EXLG	= function (file_name, message, char_name) end
--Writes to log folder under file_name.txt the message with char_name.
EnableAI	= function (enable) end
--Enables/disables AI scripts from running.
EnableProfile	= function (enable) end
--Enables/disables the profile monitor. Needs program in same folder as GameServer.exe.
EndGuildBid	= function (guild_level) end
--Ends guild bid, needs guild_level.
EndGuildChallenge	= function (guild_id_1, guild_id_2, update) end
--Ends the guild challenge between guild_id_1 and guild_id_2. If update is 1: dialog box gets updated with new data (should be done if challenger wins).
EquipHasItem	= function (player_role, item_id, item_amount) end
--Returns 1 if player_role has item_amount of item_id as part of equips.
ExchangeReq	= function (player_role, npc_role, source_item_id, source_item_amount, target_item_id, target_amount_id, time_delta) end
--Exchanges an item for another - usually done on server side of item exchange dialog box. time_delta is not used, but needs to be a number.
Exit	= function () end
--Closes game server.
FindNpc	= function (npc_name) end --Returns: is_successful, npc_role, npc_id.
--Returns the npc_role and npc_id of npc_name.
FusionItem	= function (apparel_item_pointer, item_pointer) end --Returns: is_successful_or_nil. (is_successful_or_nil returns 1 if it was true, else returns 0)
--Fuses item_pointer to apparel_item_pointer.
GMNotice	= function (message) end
--Shows a GM message to all players.
Garner2GetWiner	= function (player_role, npc_role) end --Returns: is_successful_or_nil.
--Shows chaos ranking dialog box.
Garner2RequestReorder	= function (player_role, npc_role) end --Returns: is_successful_or_nil.
--Adds player_role to chaos ranking listing (if player_role has enough chaos).
GetActName	= function (player_role) end --Returns: player_name.
--Returns the player_name from the player_role.
GetAreaName	= function (areaset_id) end --Returns: areaset_name.
--Returns the name of areaset_id.
GetAreaStateLevel	= function (player_role, skill_effect_id) end --Returns: skill_level.
--Returns the level of the skill_effect_id affecting player_role.
GetBoatID	= function (player_role, boat_id_in_npc) end --Returns: is_successful, boat_id. boat_id_in_npc is between 0 and 2 (player can have at most 3 boats at same time).
--Returns the boat_id for player_role with boat_id_in_npc of it.
GetCatAndPf	= function (char_role) end --Returns: is_successful_or_nil, char_category_id, char_class_id.
--Returns char_category_id and char_class_id from char_role.
GetCategory	= function (char_role) end --Returns: is_successful_or_nil, char_category_id.
--Returns char_category_id from char_role. --Returns: is_successful_or_nil, char_category_id. Not used by top.
GetChaAIType	= function (char_role) end --Returns: ai_type_id
--Returns the ai_type_id.
GetChaAttr	= function (char_role, char_stat_id) end --Returns: char_stat_value.
GetChaAttrI	= function (char_role, char_stat_id) return GetChaAttr(char_role, char_stat_id) end --Same as GetChaAttr.

GetChaBlockCnt	= function (char_role) end --Returns: char_block_amount (0-255).
SetChaBlockCnt	= function (char_role, char_block_amount) end

GetChaBody	= function (char_role) end --Returns: char_body_id (probably something with ship parts).
GetChaByRange	= function (char_role, x_coord, y_coord, range, flags) end --Returns: char_role (output).
--Returns the closest attackable char from char_role (input).
--If char_role is non-nil: x_coord and y_coord are not used, but still need to be a number (bad coding).
--Coordinates and map are taken from current char_role position.
--If char_role is nil: function will need to be in a map script, and x_coord and y_coord are used.
--By default: Will detect any char that can be attacked by char_role.
--If flag is 0: Detects living non-gm players.
--If flag is 1: Detects only non-players.
--If flag is anything else: Default.
GetChaChaseRange	= function (char_role) end --Returns: chase_range_or_0 (0 if char_role is invalid).
GetChaDefaultName	= function (char_role) end --Returns: character_info_name.
GetChaFacePos	= function (char_role) end --Returns: x_coord_of_char_face, y_coord_of_char_face. Useful for big monsters.
GetChaFirstTarget	= function (char_role) end --Returns char_role.
--Returns the most hated alive char_role.
GetChaFreeBagGridNum	= function (char_role) end --Returns: free_inventory_slot_id.
GetChaGuildID	= function (char_role) end --Returns: char_guild_id.
GetChaHarmByNo	= function (char_role, ordinal_place) end --Returns: char_role.
--Returns the most harmed alive char_role by ordinal_place.
GetChaHateByNo	= function (char_role, ordinal_place) end --Returns: char_role.
--Returns the most hated alive char_role by ordinal_place.
GetChaHost	= function (char_role) end --Return char_role.
--Returns the host of char_role (who claimed as pet with pet command).
GetChaID	= function (char_role) end --Returns char_id.
GetChaItem	= function (char_role, inventory_kind_id, slot) end --Returns item_pointer.
--If inventory_kind_id is 1: slot refers to the equipment slot.
--If inventory_kind_id is 2: slot refers to the inventory slot.
GetChaItem2	= function (...) end
GetChaName	= function (char_role) end --Returns: char_name.
GetChaParam	= function (...) end
GetChaPatrolPos	= function (...) end
GetChaPatrolState	= function (...) end
GetChaPlayer	= function (...) end
GetChaPos	= function (...) end
GetChaSetByRange	= function (...) end
GetChaSideID	= function (...) end
GetChaSkillInfo	= function (...) end
GetChaSkillNum	= function (...) end
GetChaSpawnPos	= function (...) end
GetChaStateLv	= function (...) end
GetChaTarget	= function (...) end
GetChaTeamID	= function (...) end
GetChaTypeID	= function (...) end
GetChaVision	= function (...) end
GetChallengeGuildID	= function (...) end
GetCharID	= function (...) end
GetCharName	= function (...) end
GetCredit	= function (...) end
GetCtrlBoat	= function (...) end
GetDisTime	= function (...) end
GetEquipItemP	= function (...) end
GetEudemon	= function (...) end
GetExpState	= function (...) end
GetFirstAtker	= function (...) end
GetGuildName	= function (...) end
GetItemAttr	= function (...) end
GetItemAttrRange	= function (...) end
GetItemForgeParam	= function (...) end
GetItemHoleNum	= function (...) end
GetItemID	= function (...) end
GetItemLv	= function (...) end
GetItemLv2	= function (...) end
GetItemName	= function (...) end
GetItemP	= function (...) end
GetItemType	= function (...) end
GetItemType2	= function (...) end
GetMainCha	= function (...) end
GetMonsterName	= function (...) end
GetNeedItemCount	= function (...) end
GetNumItem	= function (...) end
GetObjDire	= function (...) end
GetPersonBirthday	= function (...) end
GetPetNum	= function (...) end
GetPlayerID	= function (...) end
GetPlayerTeamID	= function (...) end
GetProfession	= function (...) end
GetResPath	= function (...) end
GetResString	= function (...) end
GetRoleID	= function (...) end
GetRolebyID	= function (...) end
GetSItemGrid	= function (...) end
GetSaleGoodsItem	= function (...) end
GetScriptID	= function (...) end
GetSection	= function (...) end
GetSkillLv	= function (...) end
GetSkillPos	= function (...) end
GetStateByTeamid	= function (...) end
GetTeamCha	= function (...) end
GetTickCount	= function (...) end
GetTicketIssue	= function (...) end
GetTicketItemno	= function (...) end
GetTradeItemData	= function (...) end
GetUniqueMaxWinnum	= function (...) end
GetsPremissSkill	= function (...) end
GiveItem	= function (...) end
GiveItemX	= function (...) end
HarmLog	= function (...) end
HasAllBoatInBerth	= function (...) end
HasBoatInBerth	= function (...) end
HasConvoyNpc	= function (...) end
HasDeadBoatInBerth	= function (...) end
HasFame	= function (...) end
HasGuild	= function (...) end
HasGuildLevel	= function (...) end
HasItem	= function (...) end
HasLeaveBagGrid	= function (...) end
HasLeaveBagTempGrid	= function (...) end
HasLuanchOut	= function (...) end
HasMaster	= function (...) end
HasMoney	= function (...) end
HasNavyGuild	= function (...) end
HasPirateGuild	= function (...) end
HasTeammate	= function (...) end
Hide	= function (...) end
HpCheck	= function (...) end
HpEqual	= function (...) end
HpThan	= function (...) end
IsBoatFull	= function (...) end
IsCategory	= function (...) end
IsChaFighting	= function (...) end
IsChaInLand	= function (...) end
IsChaInRegion	= function (...) end
IsChaInTeam	= function (...) end
IsChaLiving	= function (...) end
IsChaSleeping	= function (...) end
IsChaStall	= function (...) end
IsConvoyNpc	= function (...) end
IsEquip	= function (...) end
IsFlag	= function (...) end
IsGarnerWiner	= function (...) end
IsGuildType	= function (...) end
IsInArea	= function (...) end
IsInGymkhana	= function (...) end
IsInPK	= function (...) end
IsInTeam	= function (...) end
IsItemValid	= function (...) end
IsNeedRepair	= function (...) end
IsNeedSupply	= function (...) end
IsPlayer	= function (...) end
IsPosValid	= function (...) end
IsRecord	= function (...) end
IsSpawnPos	= function (...) end
IsTeamLeader	= function (...) end
IsValidFlag	= function (...) end
IsValidRecord	= function (...) end
IsValidRegTeam	= function (...) end
KillCha	= function (...) end
KillMyMonster	= function (...) end
KitbagLock	= function (...) end
LG	= function (...) end
LifeSkillBegin	= function (...) end
ListAllGuild	= function (...) end
ListChallenge	= function (...) end
Lua	= function (...) end
LuaAll	= function (...) end
LuaPrint	= function (...) end
LvCheck	= function (...) end
LvEqual	= function (...) end
LvThan	= function (...) end
MSG_TEST	= function (...) end
MakeItem	= function (...) end
Msg	= function (...) end
NoNavyGuild	= function (...) end
NoPirateGuild	= function (...) end
NoRandNpcItemFlag	= function (...) end
Notice	= function (...) end
OpenBank	= function (...) end
OpenEidolonFusion	= function (...) end
OpenEidolonMetempsychosis	= function (...) end
OpenForge	= function (...) end
OpenFusion	= function (...) end
OpenGMRecv	= function (...) end
OpenGMSend	= function (...) end
OpenGetStone	= function (...) end
OpenHair	= function (...) end
OpenItemEnergy	= function (...) end
OpenItemFix	= function (...) end
OpenItemTiChun	= function (...) end
OpenJewelryUp	= function (...) end
OpenMilling	= function (...) end
OpenRepair	= function (...) end
OpenTiger	= function (...) end
OpenUnite	= function (...) end
OpenUpgrade	= function (...) end
PackBag	= function (...) end
PackBagList	= function (...) end
PfEqual	= function (...) end
QueryChaAttr	= function (...) end
Rand	= function (...) end
ReAll	= function (...) end
ReAllHp	= function (...) end
ReAllSp	= function (...) end
ReHp	= function (...) end
ReSp	= function (...) end
RefreshCha	= function (...) end
ReloadCal	= function (...) end
ReloadNpcInfo	= function (...) end
RemoveChaItem	= function (...) end
RemoveState	= function (...) end
RepairBoat	= function (...) end
SafeBuy	= function (...) end
SafeBuyGoods	= function (...) end
SafeSale	= function (...) end
SafeSaleGoods	= function (...) end
SaveProfile	= function (...) end
ScrollNotice	= function (...) end
SetActive	= function (...) end
SetAttrChangeFlag	= function (...) end
SetChaAIType	= function (...) end
SetChaAttr	= function (...) end
SetChaAttrI	= function (...) end
SetChaAttrMax	= function (...) end
SetChaChaseRange	= function (...) end
SetChaEmotion	= function (...) end
SetChaEquipValid	= function (...) end
SetChaFaceAngle	= function (...) end
SetChaHost	= function (...) end
SetChaKbItemValid	= function (...) end
SetChaKbItemValid2	= function (...) end
SetChaKitbagChange	= function (...) end
SetChaLifeTime	= function (...) end
SetChaMotto	= function (...) end
SetChaParam	= function (...) end
SetChaPatrolPos	= function (...) end
SetChaPatrolState	= function (...) end
SetChaSideID	= function (...) end
SetChaTarget	= function (...) end
SetCopySpecialInter	= function (...) end
SetEntityData	= function (...) end
SetFlag	= function (...) end
SetItemAttr	= function (...) end
SetItemFall	= function (...) end
SetItemForgeParam	= function (...) end
SetItemHost	= function (...) end
SetMatchResult	= function (...) end
SetMatchnoState	= function (...) end
SetMaxBallotTeamRelive	= function (...) end
SetPkState	= function (...) end
SetProfession	= function (...) end
SetRangeState	= function (...) end
SetRecord	= function (...) end
SetRelive	= function (...) end
SetSkillRange	= function (...) end
SetTradeItemLevel	= function (...) end
Show	= function (...) end
SkillCrt	= function (...) end
SkillMiss	= function (...) end
SkillUnable	= function (...) end
SpCheck	= function (...) end
SpEqual	= function (...) end
SpThan	= function (...) end
Stop	= function (...) end
SummonCha	= function (...) end
SummonNpc	= function (...) end
SupplyBoat	= function (...) end
SynChaKitbag	= function (...) end
SynTigerString	= function (...) end
SyncBoat	= function (...) end
SyncChar	= function (...) end
SyncCharLook	= function (...) end
SystemNotice	= function (...) end
TakeAllRandItem	= function (...) end
TakeItem	= function (...) end
TakeItemBagTemp	= function (...) end
TakeMoney	= function (...) end
TakeRandNpcItem	= function (...) end
TestDBLog	= function (...) end
TestTest	= function (...) end
TestTest1	= function (...) end
TradeItemDataCheck	= function (...) end
TradeItemLevelCheck	= function (...) end
UnlockItem	= function (...) end
UpdateAbsentTeamRelive	= function (...) end
UpdateState	= function (...) end
UseAexpItem	= function (...) end
UseItemFailed	= function (...) end
UseItemNoHint	= function (...) end

dofile(GetResPath('script/default/initial.lua'))