#pragma once

#define USE_DSOUND

#include "MPGameApp.h"
#include "script.h"
#include "CRCursorObj.h"
#include "RenderStateMgr.h"
#include "steadyframe.h"

#include "CameraCtrl.h"

#ifdef USE_DSOUND
#include "DSoundManager.h"
#include "DSoundInstance.h"
#endif

#include "AudioSDL.h"

#include "Tables.h"

extern DWORD g_dwCurMusicID;

namespace Ninja {
class Camera;
struct SphereCoord;
template <class T>
class Controller;
} // namespace Ninja

class CSceneObj;

constexpr int SHOWRSIZE = 40;
constexpr int TIP_TEXT_NUM = 128;
constexpr int SHOW_TEXT_TIME = 3000;

//#define TESTDEMO

namespace GUI {
class CTextHint;
class CTextScrollHint; //Add by sunny.sun20080804
};					   // namespace GUI

enum eSceneType {
	enumLoginScene = 0,		// 登录场景
	enumWorldScene = 1,		// 游戏场景
	enumSelectChaScene = 2, // 选择人物
	enumCreateChaScene = 3, // 创建人物
	enumSceneEnd,
};

struct STipText {
	char szText[TIP_TEXT_NUM];
	DWORD dwBeginTime;
	BYTE btAlpha;
};

class CSteadyFrame;
class CGameScene;
class CMPShadeMap;
class CPointTrack;
class CEffectObj;
class CActor;
class CDrawPointList;
class CCameraCtrl;
class CGameScene;
struct stSceneInitParam;

struct SAddSceneObj {
	int nTypeID;
	int nPosX;
	int nPosY;
	int nHeightOff;
	int nAngle;
};

class CGameApp : public MPGameApp {
public:
	CGameApp();
	~CGameApp();

	virtual void End() override;

	virtual void MouseButtonDown(int nButton) override;
	virtual void MouseButtonUp(int nButton) override;
	virtual void MouseButtonDB(int nButton) override;
	virtual void MouseMove(int nOffsetX, int nOffsetY) override;
	virtual void MouseScroll(int nScroll) override;
	virtual void MouseContinue(int nButton) override;
	virtual void HandleKeyDown(DWORD dwKey) override;
	virtual void LG_Config(const LGInfo& info) override;
	virtual void HandleKeyUp() override {}

	bool LoadRes4();

	void SetIsRun(bool v) {
		if (v != _isRun) {
			_isRun = v;
			if (v == 0) {
				::SendMessage(_hWnd, WM_DESTROY, 0, 0);
			}
		}
	}
	bool IsRun() const { return _isRun; }
	void SetIsRenderTipText(bool v) { _IsRenderTipText = v; }
	bool GetIsRenderTipText() { return _IsRenderTipText; }

	/*
	void InitAllTable();	// 读入所有的表格文本
	void ReleaseAllTable(); // 释放所有的表格文本
	*/

	void HandleKeyContinue();
	bool HandleWindowMsg(DWORD dwMsg, DWORD dwParam1, DWORD dwParam2);

	void ChangeVideoStyle(int width, int height, D3DFORMAT format, bool bWindowed); //by billy

	void EnableCameraFollow(bool bEnable) { _bCameraFollow = bEnable; }
	bool IsCameraFollow() const { return _bCameraFollow; }
	void ResetGameCamera(int type = 0);
	void ResetCamera();

	CPointTrack* GetCameraTrack() { return _pCamTrack.get(); }

	void HandleSuperKey();
	void HandleContinueSuperKey();
	void EnableSuperKey(bool bEnable) { _bEnableSuperKey = bEnable; }
	bool IsEnableSuperKey() const;
	void PlayMusic(int nMusicNo);
	void PlaySound(int nSoundNo);

	void SendMessage(DWORD dwTypeID, DWORD dwParam1 = 0, DWORD dwParam2 = 0);

	bool IsInit() { return _IsInit; }

	bool HasLogFile(const char* log_file, bool isOpen = true);

	void Loading(int nFrame = 40);				// 参数为多少帧
	static void Waiting(bool isWaiting = true); // 显示出一个等待对话框,这其间不能操作UI
	static bool IsMouseContinue(int nButton) { return _dwMouseDownTime[nButton] > 12; }

	void AddTipText(const char* pszFormat, ...);
	void SysInfo(const char* pszFormat, ...);
	void ShowNotify(const char* szStr, DWORD dwColor);
	void ShowNotify1(const char* szStr, int setnum, DWORD dwColor); //Add by sunny.sun20080804
	void ShowHint(int x, int y, const char* szStr, DWORD dwColor);

	static void SetMusicSize(float fVol); // 0~1,0静音景,1最大音量
	static float GetMusicSize() { return (float)_nMusicSize / 128.0f; }

	static void MsgBox(const char* pszFormat, ...);

	void ShowBigText(const char* pszFormat, ...);
	void ShowMidText(const char* pszFormat, ...);

	// 切换场景begin
	CGameScene* CreateScene(stSceneInitParam* param);
	void GotoScene(CGameScene* scene, bool isDelCurScene = true, bool IsShowLoading = true); // 直接切换到另一个场景
	int Run();
	static CGameScene* GetCurScene() { return _pCurScene; } // 获得当前场景
	// 切换场景end

	void CreateCharImg();

	void RefreshLoadingProgress();

public: // 脚本创建场景
	void LoadScriptScene(eSceneType eType);
	void LoadScriptScene(const char* script_file);

	bool btest{false}; // ?
	int ihei{0};	   // ?

	std::unique_ptr<Tables> tables{nullptr};

public:
	static DWORD GetCurTick() { return _dwCurTick; }
	void SetTickCount(DWORD dwTick) { _dwCurTick = dwTick; }
	void SetFPSInterval(DWORD v);
	static DWORD GetFrameFPS();

	static CSteadyFrame* GetFrame() { return _pSteady; }

	// 根据文件名初始化当前场景，xuedong 2004.09.06 用于“生成障碍信息文件“
	bool CreateCurrentScene(char* szMapName);

	CursorMgr* GetCursor() { return &_stCursorMgr; }

	CDrawPointList* GetDrawPoints() { return _pDrawPoints.get(); }

	void OnLostDevice();
	void OnResetDevice();

	CCameraCtrl* GetMainCam() { return _pMainCam.get(); }
	static bool IsMusicSystemValid() { return _IsMusicSystemValid; }

	CMPFont* GetFont() { return &g_CFont; }
	RenderStateMgr* GetRenderStateMgr() { return _rsm.get(); }

	void SetCameraPos(D3DXVECTOR3& pos, bool bRestoreCustom = true); // bRestoreCustom参数为true，表示清除用户对镜头的改变

	void SetStartMinimap(int ix, int iy, int destx, int desty);
	CScriptMgr* GetScriptMgr() { return &_stScriptMgr; }
	void ResetCaption();

	void AutoTest();							   // 自动化测试
	void AutoTestInfo(const char* pszFormat, ...); // 用于自动化测试时显示测试内容
	void AutoTestUpdate();

	static bool IsMouseInScene() { return _MouseInScene; }

	//用于记录/读取登陆游戏的Tick时间
	static void SetLoginTime(DWORD _dwLoginTime) { m_dwLoginTime = _dwLoginTime; }
	static DWORD GetLoginTime() { return m_dwLoginTime; }

public:
	std::list<SAddSceneObj*> m_AddSceneObjList;
	DWORD m_dwRenderUITime;
	DWORD m_dwRenderSceneTime;
	DWORD m_dwRenderScneObjTime;
	DWORD m_dwRenderChaTime;
	DWORD m_dwRenderEffectTime;
	DWORD m_dwLoadingObjTime;
	DWORD m_dwTranspObjTime;
	DWORD m_dwRenderMMap;
	DWORD m_dwPathFinding;

#ifdef USE_DSOUND
	std::unique_ptr<SoundManager> mSoundManager{nullptr};
	void PlaySample(std::string SoundName);
#endif

protected:
	virtual bool _Init() override;
	virtual void _PreMouseRun(DWORD dwMouseKey) override;
	virtual void _FrameMove(DWORD dwTimeParam, bool camMove) override; //ViM
	virtual void _Render() override;
	virtual void _End() override;

	bool _PrintScreen();
	bool _CreateAviScreen();
	bool _IsSceneOk() { return _pCurScene != NULL; }
	bool _CreateSmMap(MPTerrain* pTerr);
	void _RenderTipText();
	void _ShowLoading();

protected:
	bool _bCameraFollow{true};
	bool _bEnableSuperKey{false};
	std::unique_ptr<CPointTrack> _pCamTrack{nullptr};
	std::unique_ptr<CDrawPointList> _pDrawPoints{std::make_unique<CDrawPointList>()};

	std::list<STipText*> _TipText;

	std::unique_ptr<CCameraCtrl> _pMainCam{std::make_unique<CCameraCtrl>()};

public:
	std::unique_ptr<RenderStateMgr> _rsm{nullptr};
#if (defined USE_TIMERPERIOD)
	MPITimerPeriod* _TimerPeriod;
#endif
	float xp{SHOWRSIZE / 2}, xp1{0};
	float yp{SHOWRSIZE / 2}, yp1{0};

	float destxp;
	float destyp;

protected:
	void _SceneError(const char* info, CGameScene* p);
	void _HandleMsg(DWORD dwTypeID, DWORD dwParam1, DWORD dwParam2);

	CGameScene* _pStartScene;	  // 第一个场景
	static CGameScene* _pCurScene; // 目前正在运行的场景
	static DWORD _dwCurTick;

	int _nSwitchScene{1}; // 大于零,正在切换场景

	bool _isRun;

	CScriptMgr _stScriptMgr;
	CursorMgr _stCursorMgr;

	bool _IsRenderTipText{false};

private: // 用于脚本
	bool _IsInit{false};
	DWORD _dwGameThreadID;

	static DWORD _dwMouseDownTime[2];

	static int _nMusicSize;	// 背景音量
	bool _IsUserEnabled{true}; // 是否接收用户输入

	static char _szOutBuf[256];

	CMPFont g_CFont;
	CMPFont _MidFont; // 在屏幕中央偏上的一个中等字体
	STipText _stMidFont;

	static bool _IsMusicSystemValid;

private: // 背景音乐的切换,经历三个阶段:1.当前音乐音量变小,2.为零时切换到新音乐,并且音乐变大,3.正常播放新音乐
	enum class BackgroundMusic {
		NoMusic,
		OldMusic,
		NewMusic,
		MusicPlay
	};
	BackgroundMusic _eSwitchMusic{BackgroundMusic::NoMusic};
	int _nCurMusicSize{1};		// 切换音乐时的音量大小
	char _szBkgMusic[256] = {}; // 新音乐

	static CSteadyFrame* _pSteady;
	static bool _MouseInScene;

	std::unique_ptr<CTextHint> _pNotify{nullptr};
	std::unique_ptr<CTextScrollHint> _pNotify1{nullptr};
	DWORD _dwNotifyTime{0};
	DWORD _dwNotifyTime1{0};

	int SetNum;

	static DWORD m_dwLoginTime; //记录登陆游戏的Tick时间

	DWORD _dwLoadingTick; // 开始 Loading 的时间

public:
	Ninja::Camera* GetNinjaCamera() { return _pNinjaCamera; }
	//	Ninja::Camera*		_pNinjaCamera;
	//	Ninja::Controller < D3DXVECTOR3 > *_ctrl;

	// Added by CLP
private:
	Ninja::Camera* _pNinjaCamera;
	Ninja::Controller<D3DXVECTOR3>* _camera_target_ctrl;
	Ninja::Controller<Ninja::SphereCoord>* _camera_eye_ctrl;
};

inline void CGameApp::SendMessage(DWORD dwTypeID, DWORD dwParam1, DWORD dwParam2) {
	int i = 0;
	while (!::PostMessage(GetHWND(), dwTypeID, dwParam1, dwParam2) && i <= 10) {
		Sleep(50);
		i++;
	}
}

inline void CGameApp::SetStartMinimap(int ix, int iy, int destx, int desty) {
	xp = (float)((ix / SHOWRSIZE) * SHOWRSIZE) + (SHOWRSIZE / 2);
	yp = (float)((iy / SHOWRSIZE) * SHOWRSIZE) + (SHOWRSIZE / 2);

	destxp = (float)xp + destx * SHOWRSIZE;
	destyp = (float)yp + desty * SHOWRSIZE;

	xp1 = xp;
	yp1 = yp;
}

inline DWORD CGameApp::GetFrameFPS() {
	return CSteadyFrame::GetFPS();
}

#define TipI(con, t1, t2)               \
	{                                   \
		if (con) {                      \
			g_pGameApp->AddTipText(t1); \
		} else {                        \
			g_pGameApp->AddTipText(t2); \
		}                               \
	}
#define Tip(t)                     \
	{                              \
		g_pGameApp->AddTipText(t); \
	}

extern std::unique_ptr<CGameApp> g_pGameApp;
extern bool volatile g_bLoadRes;
bool RenderHintFrame(const RECT* rc, DWORD color);
