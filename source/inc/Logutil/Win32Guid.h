#pragma once

#include <Rpc.h>
#include <string>

class Win32Guid {
public:
	Win32Guid(void);
	~Win32Guid(void);

	void Generate();

	std::string AsString() const;

private:
	UUID uuid;
};
