#include "gateserver.h"
#include <algorithm>
using namespace std;

ToGameServer::ToGameServer(char const* fname, ThreadPool* proc, ThreadPool* comm)
	: TcpServerApp(this, proc, comm), RPCMGR(this),
	  _game_heap(1, 20) {
	// 开始监听
	IniFile inf(fname);
	IniSection& is = inf["ToGameServer"];
	cChar* ip = is["IP"];
	uShort port = atoi(is["Port"]);

	// 启动 PING 线程

	SetPKParse(0, 2, 64 * 1024, 400);
	BeginWork(atoi(is["EnablePing"]));
	if (OpenListenSocket(port, ip) != 0) {
		THROW_EXCP(excp, "ToGameServer listen failed");
	}
}

ToGameServer::~ToGameServer() { ShutDown(12 * 1000); }

void ToGameServer::_add_game(GameServer* game) {
	game->next = _game_list;
	_game_list = game;
	++_game_num;
}

bool ToGameServer::_exist_game(char const* game) {
	GameServer* curr = _game_list;
	while (curr) {
		if (curr->gamename == game) {
			return true;
		}
		curr = curr->next;
	}
	return false;
}

void ToGameServer::_del_game(GameServer* game) {
	GameServer* curr = _game_list;
	GameServer* prev = nullptr;
	while (curr) {
		if (curr == game) {
			break;
		}
		prev = curr;
		curr = curr->next;
	}

	if (curr) {
		if (prev) {
			prev->next = curr->next;
		} else {
			_game_list = curr->next;
		}
		--_game_num;
	}
}

bool ToGameServer::OnConnect(DataSocket* datasock) // 返回值:true-允许连接,false-不允许连接
{
	datasock->SetPointer(nullptr);
	datasock->SetRecvBuf(64 * 1024);
	datasock->SetSendBuf(64 * 1024);
	LogLine l_line(g_gatelog);
	l_line << newln << "GameServer= [" << datasock->GetPeerIP() << "] come,Socket num= " << GetSockTotal() + 1;
	return true;
}

void ToGameServer::OnDisconnect(DataSocket* datasock, int reason) // reason值:0-本地程序正常退出；-3-网络被对方关闭；-1-Socket错误;-5-包长度超过限制。
{
	LogLine l_line(g_gatelog);
	l_line << newln << "GameServer= [" << datasock->GetPeerIP() << "] gone,Socket num= " << GetSockTotal() + 1 << ",reason= " << GetDisconnectErrText(reason).c_str();
	l_line << endln;

	if (reason == DS_SHUTDOWN || reason == DS_DISCONN) {
		return;
	}

	auto disconnectingGameServer = static_cast<GameServer*>(datasock->GetPointer());
	if (disconnectingGameServer == nullptr) {
		return;
	}

	// 从链表中删除此 GameServer
	// NOTE: Free all data associated with disconnected GameServer?
	try {
		std::lock_guard<std::mutex> lock(_mut_game);

		// NOTE: Function block is commented, why?
		disconnectingGameServer->DeleteGameServer(reason);

		// NOTE: What are we actually deleting in _del_game?
		l_line << newln << " delete [" << disconnectingGameServer->gamename.c_str() << "]" << endln;
		_del_game(disconnectingGameServer);

		for (int i = 0; i < disconnectingGameServer->mapcnt; ++i) {
			l_line << newln << "delete map [" << disconnectingGameServer->maplist[i].c_str() << "]" << endln;
			_map_game.erase(disconnectingGameServer->maplist[i]);
		}
		disconnectingGameServer->mapcnt = 0;
		disconnectingGameServer->Free();
		datasock->SetPointer(nullptr);
	} catch (...) {
		l_line << newln << "Exception raised from OnDisconnect{delete GameServer from list}" << endln;
	}

	try {
		RPacket retpk = g_GateServer->toGroupServer->get_playerlist();
		uShort player_count = retpk.ReverseReadShort();

		std::vector<Player*> disconnectingPlayers;
		disconnectingPlayers.reserve(player_count);

		for (size_t i = 0; i < player_count; ++i) {
			auto ply_addr = static_cast<Player*>(MakePointer(retpk.ReadLong()));
			uLong db_id = retpk.ReadLong();

			if (ply_addr == nullptr) {
				continue;
			}

			if (disconnectingGameServer == ply_addr->game) {
				disconnectingPlayers.push_back(ply_addr);
			} else {
				continue;
			}

			try {
				uLong tmp_id = ply_addr->m_dbid;
				if (tmp_id != db_id) {
					continue;
				}
			} catch (...) {
				l_line << newln << "Exception raised from OnDisconnect{uLong tmp_id = ply_addr->m_dbid}" << endln;
				continue;
			}

			try {
				g_GateServer->cli_conn->post_mapcrash_msg(ply_addr);
			} catch (...) {
				l_line << newln << "Exception raised from OnDisconnect{cli_conn->post_mapcrash_msg(ply_addr)}" << endln;
				continue;
			}

			continue;
		}

		for (auto& player : disconnectingPlayers) {
			try {
				l_line << newln << "because GameServer trouble, disconnect [" << player->m_datasock->GetPeerIP() << "] " << endln;
				g_GateServer->cli_conn->Disconnect(player->m_datasock, 100, -29);
			} catch (...) {
				l_line << newln << "Exception raised from OnDisconnect{Disconnect}" << endln;
			}
		}
	} catch (...) {
		l_line << newln << "Exception raised from OnDisconnect{exit}" << endln;
	}
}

WPacket ToGameServer::OnServeCall(DataSocket* datasock, RPacket& in_para) {
	return nullptr;
}

void ToGameServer::SendAllGameServer(WPacket& in_para) {
	std::vector<GameServer*> informedGameServers;

	for (std::map<std::string, GameServer*>::iterator it = _map_game.begin(); it != _map_game.end(); it++) {
		if (it->second) {
			std::vector<GameServer*>::iterator mapIt = std::find(informedGameServers.begin(), informedGameServers.end(), it->second);

			if (mapIt == informedGameServers.end()) {
				if (it->second->m_datasock) {
					this->SendData(it->second->m_datasock, in_para);
				}

				informedGameServers.push_back(it->second);
			}
		}
	}
}

void ToGameServer::OnProcessData(DataSocket* datasock, RPacket& recvbuf) {
	auto outer_scope_game = static_cast<GameServer*>(datasock->GetPointer());
	const uShort cmd = recvbuf.ReadCmd();
	//LG("ToGameServer", "-->l_cmd = %d\n", l_cmd);
	try {
		switch (cmd) {
		case CMD_MT_LOGIN: {
			MT_LOGIN(datasock, recvbuf);
		} break;
		case CMD_MT_SWITCHMAP: {
			RPacket rpk = recvbuf;
			uShort l_aimnum = rpk.ReverseReadShort(); //l_aimnum永远等于1

			auto player = static_cast<Player*>(MakePointer(rpk.ReverseReadLong()));
			if (player->m_dbid != rpk.ReverseReadLong()) { //chaid
				break;
			}
			const uChar l_return = rpk.ReverseReadChar();
			recvbuf.DiscardLast(sizeof(uChar) + sizeof(uLong) * 2 * l_aimnum + sizeof(uShort));

			const cChar* l_srcmap = rpk.ReadString();
			const Long lSrcMapCopyNO = rpk.ReadLong();
			//...
			const uLong l_srcx = rpk.ReadLong(); //坐标
			const uLong l_srcy = rpk.ReadLong(); //坐标

			const cChar* l_map = rpk.ReadString();
			const Long lMapCopyNO = rpk.ReadLong();
			//...
			const uLong l_x = rpk.ReadLong(); //坐标
			const uLong l_y = rpk.ReadLong(); //坐标

			LogLine l_line(g_gatelog);
			GameServer* inner_scope_game = g_GateServer->gm_conn->find(l_map);
			if (inner_scope_game) {
				player->game->m_plynum--;
				player->game = nullptr;
				player->gm_addr = 0;

				// Add by lark.li 20081210 begin
				player->m_switch = 1;
				// End

				l_line << newln << "clinet: " << player->m_datasock->GetPeerIP() << ":" << player->m_datasock->GetPeerPort()
					   << "	Switch to map,to Gate[" << inner_scope_game->m_datasock->GetPeerIP() << "]send EnterMap command,dbid:" << player->m_dbid
					   << uppercase << hex << ",Gate address:" << MakeULong(player) << dec << nouppercase << endln;

				inner_scope_game->EnterMap(player, player->m_loginID, player->m_dbid,
										   player->m_worldid, l_map, lMapCopyNO,
										   l_x, l_y, 1, player->m_sGarnerWiner); //根据地图查找GameServer，然后请求GameServer以进入这个地图。
				inner_scope_game->m_plynum++;
			} else if (!l_return) //目标地图不可达，重新进入源地图
			{
				WPacket wpk = datasock->GetWPacket();
				wpk.WriteCmd(CMD_MC_SYSINFO);
				wpk.WriteString(dstring("[") << l_map << "] can't reach, please retry later!");
				player->m_datasock->SendData(wpk);

				l_line << newln << "client: " << player->m_datasock->GetPeerIP() << ":" << player->m_datasock->GetPeerPort()
					   << "	Switch back map,to Gate[" << player->game->m_datasock->GetPeerIP() << "]send EnterMap command,dbid:" << player->m_dbid
					   << uppercase << hex << ",Gate address:" << MakeULong(player) << dec << nouppercase << endln;

				player->game->EnterMap(player, player->m_loginID, player->m_dbid,
									   player->m_worldid, l_srcmap, lSrcMapCopyNO,
									   l_srcx, l_srcy, 1, player->m_sGarnerWiner); //根据地图查找GameServer，然后请求GameServer以进入这个地图。
			} else {
				g_GateServer->cli_conn->Disconnect(player->m_datasock, 0, -24);
			}

			break;
		}
		case CMD_MC_ENTERMAP: {
			RPacket rpk = recvbuf;
			uShort l_aimnum = rpk.ReverseReadShort(); //l_aimnum永远等于1

			auto player = static_cast<Player*>(MakePointer(rpk.ReverseReadLong()));
			if (player == nullptr) {
				break;
			}

			uLong l_dbid = rpk.ReverseReadLong();
			LogLine l_line(g_gatelog);
			if (player->m_dbid != l_dbid) //chaid
			{
				l_line << newln << "receive from [" << datasock->GetPeerIP() << "] EnterMap command ,can't match DBID:locale [" << player->m_dbid << "],far[" << l_dbid << "]"
					   << uppercase << hex << ",Gate address:" << MakeULong(player) << endln;
				return;
			}
			const uShort ret_code = rpk.ReadShort();
			if (ret_code == ERR_SUCCESS) {
				player->game = outer_scope_game;
				player->gm_addr = rpk.ReverseReadLong();
				outer_scope_game->m_plynum = rpk.ReverseReadLong();
				const char l_isSwitch = rpk.ReverseReadChar();

				// Add by lark.li 20081210 begin
				player->m_switch = 0;
				// End

				l_line << newln << "client: " << player->m_datasock->GetPeerIP() << ":" << player->m_datasock->GetPeerPort()
					   << "	receive Gate  from [" << datasock->GetPeerIP() << "]success EnterMap command,Game address:"
					   << uppercase << hex << player->gm_addr << ",Gate address:" << MakeULong(player) << dec << nouppercase << endln;

				recvbuf.DiscardLast(sizeof(uShort) + sizeof(uLong) * 2 * l_aimnum + sizeof(uLong) * 2 + sizeof(uChar));
				g_GateServer->cli_conn->SendData(player->m_datasock, recvbuf);
				{
					WPacket wpk = GetWPacket();
					wpk.WriteCmd(CMD_MP_ENTERMAP);
					wpk.WriteChar(l_isSwitch);
					wpk.WriteLong(MakeULong(player));
					wpk.WriteLong(player->gp_addr);
					g_GateServer->toGroupServer->SendData(g_GateServer->toGroupServer->get_datasock(), wpk);
				}
			} else {
				// Add by lark.li 20081210 begin
				player->m_switch = 0;
				// End

				player->m_status = 1;
				outer_scope_game->m_plynum--;

				l_line << newln << "client: " << player->m_datasock->GetPeerIP() << ":" << player->m_datasock->GetPeerPort()
					   << "	Gate receive from [" << datasock->GetPeerIP() << "]failed EnterMap command ,Error:"
					   << ret_code << endln;

				recvbuf.DiscardLast(sizeof(uShort) + sizeof(uLong) * 2 * l_aimnum);
				//g_gtsvr->cli_conn->SendData(player->m_datasock,recvbuf);
				g_GateServer->cli_conn->Disconnect(player->m_datasock, 10, -33);
			}
			break;
		}
		case CMD_MT_KICKUSER: {
			const uShort l_aimnum = recvbuf.ReverseReadShort();
			for (uShort i = 0; i < l_aimnum; i++) {
				auto player = static_cast<Player*>(MakePointer(recvbuf.ReverseReadLong()));
				if (player && player->m_dbid == recvbuf.ReverseReadLong()) {
					g_GateServer->cli_conn->Disconnect(player->m_datasock, 0, -23); //－23错误码表示GameServer要求踢掉某人
				}
			}
			break;
		}
		case CMD_MT_MAPENTRY: {
			// Modify by lark.li 20081225 begin
			//WPacket wpk0 = WPacket( recvbuf ).Duplicate();
			//WPacket wpk = wpk0;
			WPacket wpk = WPacket(recvbuf).Duplicate();
			// ENd

			RPacket rpk = recvbuf;
			const cChar* l_map = rpk.ReadString();
			if (l_map == nullptr) {
				break;
			}

			GameServer* l_game = g_GateServer->gm_conn->find(l_map);
			if (l_game) {
				LG("ToGameServer", "CMD_MT_MAPENTRY = %s\n", l_map);
				wpk.WriteCmd(CMD_TM_MAPENTRY);
				g_GateServer->gm_conn->SendData(l_game->m_datasock, wpk);
			} else {
				wpk.WriteCmd(CMD_TM_MAPENTRY_NOMAP);
				g_GateServer->gm_conn->SendData(datasock, wpk);
			}
		} break;
		case CMD_MT_STATE: {
			RPacket rpk = recvbuf;

			auto player = static_cast<Player*>(MakePointer(rpk.ReverseReadLong()));
			if (player == nullptr) {
				break;
			}

			const uLong l_dbid = rpk.ReverseReadLong();
			if (player->m_dbid == l_dbid) //chaid
			{
				WPacket wpk = datasock->GetWPacket();
				wpk.WriteCmd(CMD_MC_SYSINFO);
				wpk.WriteString(rpk.ReadString());
				player->m_datasock->SendData(wpk);
			}
		} break;
		case CMD_MT_MAP_ADMIN: {
			const uShort l_aimnum = recvbuf.ReverseReadShort(); //l_aimnum永远等于1

			auto player = static_cast<Player*>(MakePointer(recvbuf.ReverseReadLong()));
			if (player == nullptr) {
				break;
			}

			if (player->m_dbid != recvbuf.ReverseReadLong()) //chaid
			{
				LG("ToGameServer", "CMD_MT_MAP_ADMIN error id\n");
				break;
			}

			WPacket wpk = WPacket(recvbuf).Duplicate();

			RPacket rpk = recvbuf;
			const cChar* l_map = rpk.ReadString(); // 操作的地图名字
			if (l_map == nullptr) {
				LG("ToGameServer", "CMD_MT_MAP_ADMIN error map\n");
				break;
			}

			GameServer* l_game = g_GateServer->gm_conn->find(l_map);
			if (l_game) {
				LG("ToGameServer", "CMD_MT_MAP_ADMIN = %s\n", l_map);
				wpk.WriteCmd(CMD_TM_MAP_ADMIN);
				g_GateServer->gm_conn->SendData(l_game->m_datasock, wpk);
			} else {
				WPacket wpk = datasock->GetWPacket();
				wpk.WriteCmd(CMD_MC_SYSINFO);
				wpk.WriteString(dstring("[") << l_map << "] can't find!");
				player->m_datasock->SendData(wpk);
			}
		} break;
		default: // 缺省转发
		{
			if (cmd / 500 == CMD_MC_BASE / 500) {
				RPacket rpk = recvbuf;
				uShort l_aimnum = rpk.ReverseReadShort();
				recvbuf.DiscardLast(sizeof(uLong) * 2 * l_aimnum + sizeof(uShort));
				for (uShort i = 0; i < l_aimnum; i++) {
					auto player = static_cast<Player*>(MakePointer(rpk.ReverseReadLong()));
					if (player->m_dbid == rpk.ReverseReadLong()) {
						g_GateServer->cli_conn->SendData(player->m_datasock, recvbuf);
					}
				}
			} else if (cmd / 500 == CMD_MP_BASE / 500) {
				RPacket rpk = recvbuf;
				uShort l_aimnum = rpk.ReverseReadShort();
				recvbuf.DiscardLast(sizeof(uLong) * 2 * l_aimnum + sizeof(uShort));
				if (l_aimnum > 0) {
					WPacket wpk;
					WPacket wpk0 = WPacket(recvbuf).Duplicate();
					for (uShort i = 0; i < l_aimnum; i++) {
						auto player = static_cast<Player*>(MakePointer(rpk.ReverseReadLong()));
						if (player->m_dbid == rpk.ReverseReadLong()) {
							wpk = wpk0;
							wpk.WriteLong(MakeULong(player));
							wpk.WriteLong(player->gp_addr);
							g_GateServer->toGroupServer->SendData(g_GateServer->toGroupServer->get_datasock(), wpk);
						}
					}
				} else {
					WPacket wpk = WPacket(recvbuf).Duplicate();
					g_GateServer->toGroupServer->SendData(g_GateServer->toGroupServer->get_datasock(), wpk);
				}
			} else if (cmd / 500 == CMD_MM_BASE / 500) {
				for (GameServer* l_game = _game_list; l_game; l_game = l_game->next) {
					g_GateServer->gm_conn->SendData(l_game->m_datasock, recvbuf);
				}
			}
			break;
		}
		}
	} catch (...) {
		LG("ToGameServerError", "l_cmd = %d\n", cmd);
	}
	//LG("ToGameServer", "<--l_cmd = %d\n", l_cmd);
}

void ToGameServer::MT_LOGIN(DataSocket* datasock, RPacket& rpk) {
	std::lock_guard<std::mutex> lock(_mut_game);

	const cChar* gms_name = rpk.ReadString();
	const cChar* map_list = rpk.ReadString();
	//==================================
	GameServer* gms = _game_heap.Get();
	if (gms == nullptr) {
		LG("ToGameServerError", "GameServer == NULL\n");
		WPacket retpk = GetWPacket();
		retpk.WriteCmd(CMD_TM_LOGIN_ACK);
		retpk.WriteShort(ERR_TM_MAPERR);
		datasock->SetPointer(nullptr);
		SendData(datasock, retpk);
		return;
	}
	gms->m_plynum = 0;
	gms->gamename = "";
	gms->ip = "";
	gms->port = 0;
	gms->m_datasock = nullptr;
	gms->next = nullptr;

	gms->maplist = {};
	gms->mapcnt = 0;

	//==================================
	WPacket retpk = GetWPacket();
	bool valid = true;

	retpk.WriteCmd(CMD_TM_LOGIN_ACK);
	int cnt = Util_ResolveTextLine(map_list, gms->maplist.data(), MAX_MAP, ';', 0);
	LogLine l_line(g_gatelog);
	l_line << newln << "receive GameServer [" << gms_name << "] map string [" << map_list << "] total [" << cnt << "]" << endln;
	if (cnt <= 0) { // MAP串语法有错
		l_line << newln << "map string [" << map_list << "] has syntax mistake, please use ';'compart" << endln;
		retpk.WriteShort(ERR_TM_MAPERR);
		datasock->SetPointer(nullptr);
		gms->Free();
	} else {
		gms->gamename = gms_name;
		gms->mapcnt = cnt;

		try {
			do { // 首先检查 GameServer 名字是否已注册
				if (_exist_game(gms_name)) {
					l_line << newln << "the same name GameServer exist: " << gms_name << endln;
					retpk.WriteShort(ERR_TM_OVERNAME);
					datasock->SetPointer(nullptr);
					valid = false;
					break;
				}

				// 其次检查地图名是否会有重复的
				for (int i = 0; i < cnt; ++i) {
					if (this->find(gms->maplist[i].c_str()) != nullptr) {
						l_line << newln << "the same name MAP exist: " << gms->maplist[i].c_str() << endln;
						retpk.WriteShort(ERR_TM_OVERMAP);
						datasock->SetPointer(nullptr);
						valid = false;
						break;
					}
				}
			} while (false);

			if (valid) {		// 合法的 GameServer， 加入到表中
				_add_game(gms); // 添加到链表中
				l_line << newln << "add GameServer [" << gms_name << "] ok" << endln;
				for (int i = 0; i < cnt; ++i) // 添加到 map 中
				{
					l_line << newln << "add [" << gms_name << "]  [" << gms->maplist[i].c_str() << "] map ok" << endln;
					_map_game[gms->maplist[i]] = gms;
				}

				datasock->SetPointer(gms);
				gms->m_datasock = datasock;
				retpk.WriteShort(ERR_SUCCESS);
				retpk.WriteString(g_GateServer->toGroupServer->_myself.c_str());
			} else { // 非法的 GateServer
				gms->Free();
			}
		} catch (...) {
			l_line << newln << "Exception raised from MT_LOGIN{add map}" << endln;
		}

		//if (!valid) Disconnect(datasock);
	}
	SendData(datasock, retpk);
}

GameServer* ToGameServer::find(cChar* mapname) {
	std::map<std::string, GameServer*>::iterator it = _map_game.find(mapname);
	if (it == _map_game.end()) {
		LogLine l_line(g_gatelog);
		l_line << newln << "not found [" << mapname << "] map!!!";
		return nullptr;
	}
	return it->second;
}

void GameServer::Initially() {
	m_plynum = 0;
	gamename = "";
	ip = "";
	port = 0;
	m_datasock = NULL;
	next = NULL;
	mapcnt = 0;
}

void GameServer::Finally() {
	m_plynum = 0;
	gamename = "";
	ip = "";
	port = 0;
	m_datasock = NULL;
	next = NULL;
	mapcnt = 0;
}

void GameServer::EnterMap(Player* ply, uLong actid, uLong dbid, uLong worldid, cChar* map, Long lMapCpyNO, uLong x, uLong y, char entertype, short swiner) {
	WPacket wpk = m_datasock->GetWPacket();
	wpk.WriteCmd(CMD_TM_ENTERMAP);
	wpk.WriteLong(actid);
	wpk.WriteString(ply->m_password);
	wpk.WriteLong(dbid);
	wpk.WriteLong(worldid);
	wpk.WriteString(map);
	wpk.WriteLong(lMapCpyNO);
	wpk.WriteLong(x);
	wpk.WriteLong(y);
	wpk.WriteChar(entertype);
	wpk.WriteLong(MakeULong(ply)); //第一次附加上自己的地址
	wpk.WriteShort(swiner);
	g_GateServer->gm_conn->SendData(m_datasock, wpk);
}

void GameServer::DeleteGameServer(int reason) {
	//WPacket wpk	=m_datasock->GetWPacket();
	//wpk.WriteCmd(CMD_TM_DELETEMAP);
	//wpk.WriteLong(reason);		//异常原因
	//g_gtsvr->gm_conn->SendData(m_datasock,wpk);
}
