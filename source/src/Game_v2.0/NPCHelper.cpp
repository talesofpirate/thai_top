#include "stdafx.h"
#include ".\npchelper.h"
#include <tchar.h>

NPCHelper* NPCHelper::_Instance = NULL;
BOOL NPCHelper::_ReadRawDataInfo(CRawDataInfo* pRawDataInfo, std::vector<std::string>& ParamList) {
	if (ParamList.size() == 0)
		return FALSE;

	NPCData* pInfo = (NPCData*)pRawDataInfo;

	int m = 0, n = 0;
	std::string strList[8];
	std::string strLine;

	// npc显示名称
	strncpy_s(pInfo->szName, pInfo->szDataName, _TRUNCATE);
	pInfo->szName[NPC_MAXSIZE_NAME - 1] = _TEXT('\0');

	strncpy_s(pInfo->szArea, ParamList[m++].c_str(), _TRUNCATE);
	pInfo->szMapName[NPC_MAXSIZE_NAME - 1] = _TEXT('\0');

	// npc位置信息
	Util_ResolveTextLine(ParamList[m++].c_str(), strList, 8, ',');
	pInfo->dwxPos0 = Str2Int(strList[0]);
	pInfo->dwyPos0 = Str2Int(strList[1]);

	// npc所在地图显示名称
	strncpy_s(pInfo->szMapName, ParamList[m++].c_str(), _TRUNCATE);
	pInfo->szMapName[NPC_MAXSIZE_NAME - 1] = _TEXT('\0');

	return TRUE;
}
