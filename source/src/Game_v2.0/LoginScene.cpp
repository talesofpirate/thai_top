#include "stdafx.h"
#include "LoginScene.h"

#include "GameApp.h"
#include "Character.h"
#include "SceneObj.h"
#include "UiFormMgr.h"
#include "UiTextButton.h"
#include "CharacterAction.h"
#include "SceneItem.h"
#include "ItemRecord.h"
#include "PacketCmd.h"
#include "GameConfig.h"

#include "Character.h"
#ifdef CALUA_COMPAT
#include "caLua.h" //CHANGED: Removed CaLua dependency
#include "lualib.h"
#include "lauxlib.h"
#else
#include <lua.hpp>
#endif
#include "UIRender.h"
#include "UIEdit.h"
#include "UILabel.h"
#include "uiformmgr.h"
#include "uitextbutton.h"
#include "uilabel.h"
#include "uiprogressbar.h"
#include "uiscroll.h"
#include "uilist.h"
#include "uicombo.h"
#include "uiimage.h"
#include "UICheckBox.h"
#include "uiimeinput.h"
#include "uigrid.h"
#include "uilistview.h"
#include "uipage.h"
#include "uitreeview.h"
#include "uiimage.h"
#include "UILabel.h"
#include "RenderStateMgr.h"
#include "cameractrl.h"
#include "UIListView.h"

#include "UIMemo.h"
#ifdef CALUA_COMPAT
#include "caLua.h" //CHANGED: Removed CaLua dependency
#endif

#include "Connection.h"
#include "ServerSet.h"
#include "GameAppMsg.h"

#include "UI3DCompent.h"
#include "UIForm.h"
#include "UITemplete.h"
#include "commfunc.h"
#include "uiboxform.h"
#include "bill.h"

#include <shellapi.h>

//#define USE_STATUS		// 此���议��大波���网������������关闭 lh by 2006-3-22

#ifdef USE_STATUS
#include "UdpClient.h"

using namespace client_udp;
#endif

#pragma comment(lib, "shell32.lib")

#include "xmlwriter.h"

using namespace std;

//#ifdef KOP_TOM.
TOM_SERVER g_TomServer;
//#endif
Cooperate g_cooperate; //  add by jampe

CForm* CLoginScene::frmArea = nullptr;
CForm* CLoginScene::frmAccount = nullptr;
CForm* CLoginScene::frmKeyboard = nullptr; // add by Philip.Wu  软键��������  2006-06-05

std::array<CList*, MAX_REGION_LIST> CLoginScene::lstRegion = {};
std::array<CLabelEx*, MAX_REGION_LIST> regionNames = {};

CEdit* CLoginScene::edtID = nullptr;
CEdit* CLoginScene::edtPassword = nullptr;
CEdit* CLoginScene::edtFocus = nullptr;		// add by Philip.Wu  ����激活���编辑��  2006-06-06
CCheckBox* CLoginScene::chkShift = nullptr; // add by Philip.Wu  软键�������� Shift  2006-06-09
CCheckBox* CLoginScene::chkID = nullptr;

CImage* CLoginScene::imgLogo1 = nullptr;
CImage* CLoginScene::imgLogo2 = nullptr;

static void _GoBack(CCompent* pSender, int nMsgType, int x, int y, DWORD dwKey) {
	if (auto pLoginScene = dynamic_cast<CLoginScene*>(CGameApp::GetCurScene()); pLoginScene) {
		g_cooperate.code ? pLoginScene->ShowServerSelection()
						 : pLoginScene->ShowLoginForm();
	}
}

#define MAX_SERVER_NUM 8

CLoginScene::CLoginScene(stSceneInitParam& param) : CGameScene(param) {
	LG("scene memory", "CLoginScene Create\n");
}

CLoginScene::~CLoginScene() {
	LG("scene memory", "CLoginScene Destroy\n");
}

bool CLoginScene::_Init() {
	_IsUseSound = false;

	_eState = eLoginState::Init;

	{
		lwIByteSet* res_bs = g_Render.GetInterfaceMgr()->res_mgr->GetByteSet();
		_loadtex_flag = res_bs->GetValue(OPT_RESMGR_LOADTEXTURE_MT);
		_loadmesh_flag = res_bs->GetValue(OPT_RESMGR_LOADMESH_MT);
		res_bs->SetValue(OPT_RESMGR_LOADTEXTURE_MT, 0);
		res_bs->SetValue(OPT_RESMGR_LOADMESH_MT, 0);
	}

	CGameScene::_Init();

	_bEnableCamDrag = TRUE;
	MPTimer tInit;
	tInit.Begin();

	//static bool IsLoad = false;
	static CGuiPic LoginPic;
	if (!IsLoad) {
		IsLoad = true;
		pObj = AddSceneObj(435); // 435, 512

		if (pObj) {
			pObj->SetCullingFlag(0);
			// position of the scene
			pObj->setPos(0, 0);
			pObj->setYaw(180);

			DWORD num = pObj->GetPrimitiveNum();
			for (DWORD i = 0; i < num; i++) {
				pObj->GetPrimitive(i)->SetState(STATE_TRANSPARENT, 0);
				pObj->GetPrimitive(i)->SetState(STATE_UPDATETRANSPSTATE, 0);
			}
			pObj->PlayDefaultAnimation();
		}
	}

	g_Render.SetClip(g_Config.fnear, g_Config.ffar);

	/*CCameraCtrl* pCam = g_pGameApp->GetMainCam();
	if (pCam) {
		g_pGameApp->EnableCameraFollow(TRUE);
		pCam->m_EyePos.x = 70.00f;
		pCam->m_EyePos.y = 130.00f;
		pCam->m_EyePos.z = 200.00f;

		pCam->m_RefPos.x = 0.034f;
		pCam->m_RefPos.y = -200.00f;
		pCam->m_RefPos.z = 0.868f;
	}
	g_Render.SetWorldViewFOV(Angle2Radian(70.0f));
	g_Render.SetWorldViewAspect(1.33f);
	g_Render.SetClip(1.0f, 2000.0f);

	if (pCam) {
		g_Render.LookAt(pCam->m_EyePos, pCam->m_RefPos);
	}*/
	g_Render.SetCurrentView(MPRender::VIEW_WORLD);
	MPIDeviceObject* dev_obj = g_Render.GetInterfaceMgr()->dev_obj;

	//SetupVertexFog(dev_obj, 0, 0, D3DCOLOR_XRGB(28, 221, 246), D3DFOG_EXP2, 1, 0.0006f);
	//SetupVertexFog(dev_obj, 0, 0, D3DCOLOR_XRGB(86, 209, 246), D3DFOG_EXP2, 1, 0.0006f);

	g_Render.SetRenderStateForced(D3DRS_LIGHTING, 0);
	g_Render.SetRenderState(D3DRS_AMBIENT, 0xffffffff);

	SetFocus(g_pGameApp->GetHWND());

	if (!_InitUI()) {
		LG("login_ini", RES_STRING(CL_LANGUAGE_MATCH_168));
		return false;
	}

	CFormMgr::s_Mgr.SetEnabled(TRUE);

	const char szUpdateFileName[] = "_Update.exe";
	SetFileAttributes(szUpdateFileName, FILE_ATTRIBUTE_NORMAL);
	DeleteFile(szUpdateFileName);

	m_sUsername = "player";
	m_sPassword = "";

	static bool isFirst = true;
	if (g_TomServer.bEnable && isFirst) {
		isFirst = false;
		m_sUsername = g_TomServer.szUser;
		m_sPassword = g_TomServer.szPassword;
		m_sPassport = g_TomServer.szPassport;
		_Connect();
		return true;
	}

	return true;
}

bool CLoginScene::_Clear() {
	CGameScene::_Clear();

	//    g_Render.SetRenderState(D3DRS_FOGENABLE, 0);
	{ // reset loading res mt flag
		if (_loadtex_flag != 9 && _loadmesh_flag != 9) {
			lwIByteSet* res_bs = g_Render.GetInterfaceMgr()->res_mgr->GetByteSet();
			res_bs->SetValue(OPT_RESMGR_LOADTEXTURE_MT, _loadtex_flag);
			res_bs->SetValue(OPT_RESMGR_LOADMESH_MT, _loadmesh_flag);
		}
	}

	g_Render.SetClip(1.F, 1000.0f);

	return true;
}

void CLoginScene::ShowLoginForm() {
	chkID->SetIsChecked(m_bSaveAccount);
	edtID->SetCaption(m_sSaveAccount.c_str());
	edtPassword->SetCaption("");
	frmAccount->Show();
	frmArea->Hide();

	// add by Philip.Wu  2006-07-03  ���示输入��时���时���示软键���
	frmKeyboard->SetIsShow(false); //CHANGED: Made keyboard hide by default
	imgLogo1->SetIsShow(false);
	imgLogo2->SetIsShow(false);

	if (m_sSaveAccount == "") {
		edtID->SetActive(edtID);
	} else {
		edtPassword->SetActive(edtPassword);
	}
}

void CLoginScene::_FrameMove(DWORD dwTimeParam) {
	int x = g_pGameApp->GetMouseX();
	int y = g_pGameApp->GetMouseY();
	GetRender().ScreenConvert(x, y);

	if (frmArea->GetIsShow()) {
		for (auto& list : lstRegion) {
			if (!list->InRect(x, y)) {
				list->GetItems()->GetSelect()->SetNoSelect();
			}
		}
	}

	if (_eState == eLoginState::Connect) {
		switch (g_NetIF->GetConnStat()) {
		case Connection::Connection_Status::CONNECTING: {
		} break;
		case Connection::Connection_Status::INVALID:
		case Connection::Connection_Status::FAILURE: {
			if (g_TomServer.bEnable) {
				MessageBox(0, RES_STRING(CL_LANGUAGE_MATCH_169), "Info", 0);
				g_pGameApp->SetIsRun(false);
				return;
			}

			// ·µ»��µ½Ñ¡Ôñ·þ��ñ��÷³¡¾°
			_eState = eLoginState::Init;
			CGameApp::Waiting(false);

			frmArea->SetIsShow(false);
			frmAccount->SetIsShow(false);
			g_stUIBox.ShowMsgBox(_GoBack, RES_STRING(CL_LANGUAGE_MATCH_169));
		} break;
		case Connection::Connection_Status::CONNECTED: {
			CGameApp::Waiting(false);

			//�����������,����������������
			_Login();
		} break;
		case Connection::Connection_Status::TIMEOUT:
			_eState = eLoginState::Init;
			g_pGameApp->SendMessage(APP_NET_DISCONNECT, 1000);
			return;
		}
		return;
	}
}

void CLoginScene::_Render() {
	/*static bool IsLoad = false;
	static CGuiPic LoginPic;
	if (!IsLoad) {
		LoginPic.LoadImage("texture/ui/new_login.gif", 1024, 768, 0, 0, 0, 1.F, 1.F);
		IsLoad = true;
	}
	LoginPic.SetScale(0, GetRender().GetScreenWidth(), GetRender().GetScreenHeight());
	LoginPic.Render(0, 0);*/

	MPTimer mpt;
    mpt.Begin();
    //CGameScene::_Render();
    MPIDeviceObject* dev_obj = g_Render.GetInterfaceMgr()->dev_obj;
    RenderStateMgr* rsm = g_pGameApp->GetRenderStateMgr();
	DWORD dwOldState;

	rsm->BeginScene();

    D3DLIGHTX env_light;
    D3DLIGHTX env_light_old;
    memset(&env_light, 0, sizeof(env_light));
    env_light.Type = D3DLIGHT_DIRECTIONAL;

    env_light.Direction.x = -1.0f;
    env_light.Direction.y = -1.0f;
    env_light.Direction.z = -0.5f;
    D3DXVec3Normalize((D3DXVECTOR3*)&env_light.Direction, (D3DXVECTOR3*)&env_light.Direction);

    MPDwordByte4 c;
    c.b[3] = 0xff;
    c.b[2] = 185;
    c.b[1] = 36;
    c.b[0] = 54;
    env_light.Diffuse.r = (float)(c.b[2] / 255.0f);
    env_light.Diffuse.g = (float)(c.b[1] / 255.0f);
    env_light.Diffuse.b = (float)(c.b[0] / 255.0f);

    g_Render.GetLight(0, &env_light_old);
    g_Render.SetLight(0, &env_light);
	dev_obj->SetRenderState(D3DRS_LIGHTING, dwOldState);
    g_Render.SetLight(0, &env_light_old);

    rsm->BeginSceneObject();
	if (pObj)
		pObj->FrameMove(0);

	// shade islands
    dev_obj->GetRenderState(D3DRS_LIGHTING, &dwOldState);
    dev_obj->SetRenderState(D3DRS_LIGHTING, FALSE);

	if (pObj)
		pObj->Render();
    rsm->EndSceneObject();

    rsm->BeginTranspObject();
    lwUpdateSceneTransparentObject();
    rsm->EndTranspObject();
    rsm->EndScene();
}

void CLoginScene::LoadingCall() {
	g_pGameApp->PlayMusic(1);
}

//-----------------
// ��������关Routines
//-----------------
void CLoginScene::CallbackUIEvent_LoginScene(CCompent* pSender, int state, int x, int y, DWORD key) {
	auto pSceneLogin = dynamic_cast<CLoginScene*>(CGameApp::GetCurScene());
	if (!pSceneLogin) {
		return;
	}

	if (_stricmp("frmAccount", pSender->GetForm()->GetName()) == 0) {
		const std::string strName = pSender->GetName();
		if (strName == "btnYes") {
			pSceneLogin->LoginFlow();
			return;
		} else if (strName == "btnNo") {
			if (g_NetIF->IsConnected()) {
				CS_Disconnect(DS_DISCONN);
			}
			pSender->GetForm()->Hide();
			return;
		}
	}
}

void CLoginScene::_evtRegionFrm(CCompent* pSender, int state, int x, int y, DWORD key) {
	auto pLoginScene = dynamic_cast<CLoginScene*>(CGameApp::GetCurScene());
	if (!pLoginScene) {
		return;
	}

	if (const std::string name = pSender->GetName(); name == "btnNo") {
		//关闭������������表表���,���示����表���
		pSender->GetForm()->SetIsShow(false);
		g_pGameApp->SetIsRun(false);
		return;
	}
}

void CLoginScene::_evtLoginFrm(CCompent* pSender, int state, int x, int y, DWORD key) {
	auto pLoginScene = dynamic_cast<CLoginScene*>(CGameApp::GetCurScene());
	if (!pLoginScene) {
		return;
	}

	if (const std::string strName = pSender->GetName(); strName == "btnYes") {
		// ��关闭软键���
		if (frmKeyboard->GetIsShow()) {
			frmKeyboard->SetIsShow(false);

			imgLogo1->SetIsShow(true);
			imgLogo2->SetIsShow(true);
		}

		// ��������������
		pLoginScene->LoginFlow();
	} else if (strName == "btnNo") {
		// ��关闭软键���
		if (frmKeyboard->GetIsShow()) {
			frmKeyboard->SetIsShow(false);

			imgLogo1->SetIsShow(true);
			imgLogo2->SetIsShow(true);
		}

		//关闭�����
		if (g_NetIF->IsConnected()) {
			CS_Disconnect(DS_DISCONN);
		}
		//关闭������表���,���示��一次���示���������������表表���
		pSender->GetForm()->SetIsShow(false);
		pLoginScene->ShowServerSelection();
	} else if (strName == "btnKeyboard") {
		// 软键������示��关闭  add by Philip  2006-06-20
		// ��������开软键����������������示 LOGO ��������LOGO ���由两张����������������
		// �����������������������导���软键������ LOGO ��������无法������
		const bool bShow = frmKeyboard->GetIsShow();

		ShowKeyboard(!bShow);
		//imgLogo1->SetIsShow(bShow);
		//imgLogo2->SetIsShow(bShow);
		//frmKeyboard->SetIsShow(! bShow);
	}
}

void CLoginScene::_evtRegionLDBDown(CGuiData* pSender, int x, int y, DWORD key) {
	auto pkScene = dynamic_cast<CLoginScene*>(CGameApp::GetCurScene());
	if (!pkScene) {
		return;
	}

	auto activeRegion = dynamic_cast<CList*>(pSender);
	if (!activeRegion) {
		return;
	}

	for (auto region : lstRegion) {
		if (region != activeRegion) {
			continue;
		};
		const char* selectedServerName = activeRegion->GetItems()->GetSelect()->GetItem()->GetBegin()->GetString();
		const CServerGroupInfo* info = GetServerGroupInfo(selectedServerName);
		if (!info) {
			return;
		}

		int RegionIndex = GetReginIndex(info->szRegion);
		pkScene->SetCurSelRegionIndex(RegionIndex);

		int ServerGroupCnt = GetCurServerGroupCnt(RegionIndex);
		if (ServerGroupCnt == 0) {
			break;
		}

		int ServerIndex = activeRegion->GetItems()->GetSelect()->GetIndex();
		pkScene->SetCurSelServerIndex(ServerIndex);

		pkScene->ShowLoginForm();
		break;
	}
}

void CLoginScene::_evtEnter(CGuiData* pSender) {
	auto pScene = dynamic_cast<CLoginScene*>(CGameApp::GetCurScene());
	if (!pScene) {
		return;
	}
	pScene->LoginFlow();
}

void CLoginScene::InitRegionServerLists() {
	for (auto& list : lstRegion) {
		list->GetItems()->Clear();
	}

	ReginListMap& ListMap = GetReginListMap();

	// For each region,
	for (int Index = 1; Index <= MAX_REGION_LIST; ++Index) {
		auto iter = ListMap.find(Index);
		if (iter != ListMap.end()) {
			ReginList& regionList = iter->second;
			// Initialize every server names in the list
			for (auto it : regionList) {
				regionNames[Index - 1]->SetCaption(it.c_str());
			}
		}
	}

	// Init server names
	for (int RegionIndex = 0; RegionIndex < GetRegionCnt(); ++RegionIndex) {
		const int ServerCount = GetCurServerGroupCnt(RegionIndex);
		for (int ServerIndex = 0; ServerIndex < ServerCount; ++ServerIndex) {
			const char* ServerName = GetCurServerGroupName(RegionIndex, ServerIndex);
			lstRegion[RegionIndex]->Add(ServerName);
		}
	}

	// Hide region header when region is empty
	for (int i = 0; i < MAX_REGION_LIST; ++i) {
		if (lstRegion[i]->GetItems()->GetCount() < 1) {
			regionNames[i]->SetCaption("");
		}
	}

	//�������认���第一个��项
	SetCurSelRegionIndex(0);
	CListItems* items = lstRegion[0]->GetItems();
	if (!items) {
		return;
	}
	items->Select(GetCurSelRegionIndex());
}

BOOL CLoginScene::_InitUI() {
	// Initialize Server Selection UI
	{
		frmArea = CFormMgr::s_Mgr.Find("frmArea");
		if (!frmArea) {
			return false;
		}
		frmArea->evtEntrustMouseEvent = _evtRegionFrm;

		const char* luaRegionNames[] = {
			"region0Name",
			"region1Name",
			"region2Name",
		};
		const char* luaRegionLists[] = {
			"lstRegion0",
			"lstRegion1",
			"lstRegion2",
		};

		for (int index = 0; index < MAX_REGION_LIST; ++index) {
			regionNames[index] = static_cast<CLabelEx*>(frmArea->Find(luaRegionNames[index]));
			if (!regionNames[index]) {
				return false;
			}
		}

		for (int index = 0; index < MAX_REGION_LIST; index++) {
			lstRegion[index] = static_cast<CList*>(frmArea->Find(luaRegionLists[index]));
			if (lstRegion[index]) {
				lstRegion[index]->evtListMouseDown = _evtRegionLDBDown;
			}
		}

		InitRegionServerLists();
		if (!g_TomServer.bEnable) {
			frmArea->SetIsShow(true);
		}
	}

	{ // Initialize login UI
		frmAccount = CFormMgr::s_Mgr.Find("frmAccount");
		if (!frmAccount) {
			return false;
		}
		frmAccount->evtEntrustMouseEvent = _evtLoginFrm;

		chkID = static_cast<CCheckBox*>(frmAccount->Find("chkID"));
		if (!chkID) {
			return false;
		}
		m_bSaveAccount = false;
		//检����次时����中��账���
		char szChkID[128] = {0};
		std::string strChkID;
		ifstream inCheck("user\\checkid.txt");
		if (inCheck.is_open()) {
			while (!inCheck.eof()) {
				inCheck.getline(szChkID, 128);
				strChkID = szChkID;
				int nCheck = Str2Int(strChkID);
				m_bSaveAccount = (nCheck == 1) ? true : false;
				chkID->SetIsChecked(m_bSaveAccount);
			}
		} else {
			m_bSaveAccount = true;
			chkID->SetIsChecked(m_bSaveAccount);
		}

		edtID = dynamic_cast<CEdit*>(frmAccount->Find("edtID"));

		if (!edtID)
			return false;
		m_sSaveAccount = "";
		//读入数���, �����次chkID�����时�� �������������入�����username.txt��件�� ������读入该�����
		char szName[128] = {0};
		ifstream in("user\\username.txt");

		_bAutoInputAct = FALSE;
		if (in.is_open()) {
			while (!in.eof()) {
				in.getline(szName, 128);
			}
			_bAutoInputAct = TRUE;
		}
		m_sSaveAccount = std::string(szName);
		edtID->SetCaption(m_sSaveAccount.c_str());

		if (edtID) {
			edtID->evtEnter = _evtEnter;
			edtID->SetIsWrap(true);
		}

		edtPassword = dynamic_cast<CEdit*>(frmAccount->Find("edtPassword"));
		if (edtPassword) {
			edtPassword->SetCaption("");
			edtPassword->SetIsPassWord(true);
			edtPassword->SetIsWrap(true);
			edtPassword->evtEnter = _evtEnter;
		}

		// 添���软键��������
		frmKeyboard = CFormMgr::s_Mgr.Find("frmKeyboard");
		if (!frmKeyboard) {
			return false;
		}

		chkShift = (CCheckBox*)frmKeyboard->Find("chkShift");
		if (!chkShift) {
			return false;
		}

		// 设置软键��������鼠����������件�����
		frmKeyboard->evtEntrustMouseEvent = _evtKeyboardFromMouseEvent;

		if (edtID) {
			edtID->evtActive = _evtAccountFocus;
		}

		if (edtPassword) {
			edtPassword->evtActive = _evtAccountFocus;
		}
	}

	{ // Logo images seen in the background
		imgLogo1 = static_cast<CImage*>(frmAccount->Find("imgLogo1"));
		if (!imgLogo1) {
			return false;
		}

		imgLogo2 = static_cast<CImage*>(frmAccount->Find("imgLogo2"));
		if (!imgLogo2) {
			return false;
		}
	}

	return TRUE;
}

bool CLoginScene::IsValidCheckChaName(const char* name) {
	if (!::IsValidName(name, (unsigned short)strlen(name))) {
		g_pGameApp->MsgBox(RES_STRING(CL_LANGUAGE_MATCH_51));
		return false;
	}
	return true;

	const char* s = name;
	int len = (int)strlen(s);
	bool bOk = true;

	for (int i = 0; i < len; i++) {
		if (s[i] & 0x80) {
			if (!(s[i] == -93)) //用�������������������字������字��
			{
				i++;
			} else {
				bOk = false;
				i++;
				break;
			}
		} else {
			if (!(isdigit(s[i]) || isalpha(s[i]))) {
				bOk = false;
				break;
			}
		}
	}

	if (!bOk)
		g_pGameApp->MsgBox(RES_STRING(CL_LANGUAGE_MATCH_52));

	return bOk;
}

bool CLoginScene::_CheckAccount() {
	// ������检���用������������
	if (strlen(edtID->GetCaption()) == 0) {
		g_pGameApp->MsgBox(RES_STRING(CL_LANGUAGE_MATCH_174));
		return false;
	}
	if (!IsValidCheckChaName(edtID->GetCaption()))
		return false;

	if (strlen(edtPassword->GetCaption()) <= 4) {
		g_pGameApp->MsgBox(RES_STRING(CL_LANGUAGE_MATCH_175));
		return false;
	}

	// ����用������
	SaveUserName(*chkID, *edtID);

	m_sUsername = edtID->GetCaption();
	m_sPassword = edtPassword->GetCaption();

	return true;
}

//bool CLoginScene::_Bill() {
//	//收费����
//
//	if (g_Config.m_IsBill) {
//		static CPAI cpai;
//		int ret = cpai.Login(edtID->GetCaption(), edtPassword->GetCaption());
//		if (10000 == ret) {
//			m_sPassport = cpai.GetPassport();
//			return true;
//		} else if (10004 == ret) {
//			CBoxMgr::ShowMsgBox(NULL, RES_STRING(CL_LANGUAGE_MATCH_176), true);
//		} else {
//			std::string ret = cpai.LastError();
//			if (ret == "[Player_Missing]") {
//				CBoxMgr::ShowMsgBox(NULL, RES_STRING(CL_LANGUAGE_MATCH_177), true);
//			} else if (ret == "[Player_Failure]") {
//				CBoxMgr::ShowMsgBox(NULL, RES_STRING(CL_LANGUAGE_MATCH_178), true);
//			} else {
//				CBoxMgr::ShowMsgBox(NULL, "PAI: Unknown Error. !", true);
//			}
//		}
//		return false;
//	}
//	m_sPassport = "nobill";
//	return true;
//}

void CLoginScene::_Connect() {
	CGameApp::Waiting(true);

	_eState = eLoginState::Connect;

	if (g_TomServer.bEnable) {
		CS_Connect(g_TomServer.szServerIP.c_str(), g_TomServer.nPort, g_Config.m_nConnectTimeOut);
		return;
	}

	LG("connect", RES_STRING(CL_LANGUAGE_MATCH_179), m_iCurSelRegionIndex, m_iCurSelServerIndex);

	//�����没���得��������������Gate���Ip
	const char* selectGateIP = SelectGroupIP(m_iCurSelRegionIndex, m_iCurSelServerIndex);
	if (!selectGateIP) {
		LG("connect", RES_STRING(CL_LANGUAGE_MATCH_180), m_iCurSelRegionIndex, m_iCurSelServerIndex);
	} else {
		CS_Connect(selectGateIP, 1973, g_Config.m_nConnectTimeOut);
	}
	//#endif
}

void CLoginScene::LoginFlow() {
	////////////////////////////////////////
	//
	//  By Jampe
	//  �������用���������������
	//  2006/5/19
	//
	switch (g_cooperate.code) {
	case COP_OURGAME:
	case COP_SINA:
	case COP_CGA: {
		m_sUsername = g_cooperate.uid;
		m_sPassword = g_cooperate.pwd;
	} break;
	case 0:
	default: {
		if (!_CheckAccount())
			return;
	} break;
	}
	//  end
	//if (!_Bill())
	//	return;
	_Connect();
}

void CLoginScene::_Login() {
	_eState = eLoginState::Account;

	if (!m_sUsername.empty() && !m_sPassword.empty()) {
		//��������������

		//确���������������
		CS_Login(m_sUsername.c_str(), m_sPassword.c_str(), m_sPassport.c_str());

		CGameApp::Waiting();
	}
}

void CLoginScene::SaveUserName(CCheckBox& chkID, CEdit& edtID) {
	//���建新��件夹
	if (!CreateDirectory("user", NULL)) {
	}

	m_bSaveAccount = chkID.GetIsChecked();
	m_sSaveAccount = std::string(edtID.GetCaption());

	//�����件
	FILE* fchk;
	fopen_s(&fchk, "user\\checkid.txt", "wb");
	if (fchk) {
		fwrite(m_bSaveAccount ? "1" : "0", strlen("1"), 1, fchk);
		fclose(fchk);
	}

	FILE* fp;
	fopen_s(&fp, "user\\username.txt", "wb");
	if (fp) {
		if (m_bSaveAccount)
			fwrite(m_sSaveAccount.c_str(), m_sSaveAccount.size(), 1, fp);
		else
			fwrite("", 0, 1, fp);

		fclose(fp);
	}
}

void CLoginScene::_evtVerErrorFrm(CCompent* pSender, int nMsgType, int x, int y, DWORD key) {
	g_pGameApp->SetIsRun(false);

	if (nMsgType != CForm::mrYes) {
		// 弹开一个网页
		if (strlen(g_Config.m_szVerErrorHTTP) == 0) {
			return;
		}

		/*	2008-10-15	close!
		::ShellExecute( NULL, "open", 
			g_Config.m_szVerErrorHTTP,
			NULL, NULL, SW_SHOW);
		*/
		return;
	}

	// ���������新
	extern bool g_IsAutoUpdate;
	g_IsAutoUpdate = true;
}

void CLoginScene::Error(int error_no, const char* error_info) {
	CGameApp::Waiting(false);
	LG("error", "%s Error, Code:%d, Info: %s", error_info, error_no, g_GetServerError(error_no));

	if (ERR_MC_VER_ERROR == error_no && !g_TomServer.bEnable) {
		CBoxMgr::ShowSelectBox(_evtVerErrorFrm, RES_STRING(CL_LANGUAGE_MATCH_181), true);
		return;
	}

	g_pGameApp->MsgBox("%s", g_GetServerError(error_no));
}

void CLoginScene::ShowServerSelection() {
	CS_Disconnect(DS_DISCONN);

	if (frmKeyboard) {
		ShowKeyboard(false);
	}

	if (frmAccount) {
		frmAccount->SetIsShow(false);
	}

	InitRegionServerLists();
	if (frmArea) {
		frmArea->SetIsShow(true);
	}
}

// add by Philip.Wu  2006-06-05
// 软键������鼠�����������钮��件
void CLoginScene::_evtKeyboardFromMouseEvent(CCompent* pSender, int nMsgType, int x, int y, DWORD dwKey) {
	if (!edtFocus) {
		return;
	}

	CLoginScene* pLoginScene = dynamic_cast<CLoginScene*>(CGameApp::GetCurScene());
	if (!pLoginScene) {
		return;
	}

	std::string strText = edtFocus->GetCaption();
	std::string strName = pSender->GetName();
	if (strName.empty()) {
		return;
	}

	// ���键�������������
	if (strName == "btnClose" || strName == "btnYes") // 关闭软键���
	{
		if (frmKeyboard->GetIsShow()) {
			ShowKeyboard(false);
		}
	} else if (strName == "btnDel") // ������������一个字符
	{
		if (strText.size() > 0) {
			strText.resize(strText.size() - 1);
			edtFocus->SetCaption(strText.c_str());
		}
	} else if (strName == "chkShift") // 大��转���
	{
	} else if (strName == "btnOther101") // 第一���������字符键
	{
		strText += '~';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther102") {
		strText += '!';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther103") {
		strText += '@';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther104") {
		strText += '#';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther105") {
		strText += '$';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther106") {
		strText += '%';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther107") {
		strText += '^';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther108") {
		strText += '&';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther109") {
		strText += '*';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther110") {
		strText += '(';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther111") {
		strText += ')';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther112") {
		strText += '_';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther113") {
		strText += '+';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther114") {
		strText += '|';
		edtFocus->SetCaption(strText.c_str());
	}

	else if (strName == "btnOther201") // 第�����������字符键
	{
		strText += '`';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther202") {
		strText += '-';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther203") {
		strText += '=';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther204") {
		strText += '\\';
		edtFocus->SetCaption(strText.c_str());
	}

	else if (strName == "btnOther301") // 第�����������字符键
	{
		strText += '{';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther302") {
		strText += '}';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther303") {
		strText += '[';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther304") {
		strText += ']';
		edtFocus->SetCaption(strText.c_str());
	}

	else if (strName == "btnOther401") // 第������������字符键
	{
		strText += ':';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther402") {
		strText += '\"';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther403") {
		strText += ';';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther404") {
		strText += '\'';
		edtFocus->SetCaption(strText.c_str());
	}

	else if (strName == "btnOther501") // 第五���������字符键
	{
		strText += '<';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther502") {
		strText += '>';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther503") {
		strText += '?';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther504") {
		strText += ',';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther505") {
		strText += '.';
		edtFocus->SetCaption(strText.c_str());
	} else if (strName == "btnOther506") {
		strText += '/';
		edtFocus->SetCaption(strText.c_str());
	}

	else // ������键全�������������������������输入字符键�����
	{
		char cAdd = strName.at(strName.size() - 1);

		// ���断���������数字���字�������时������方���������
		if (('0' <= cAdd && cAdd <= '9')) {
			strText += cAdd;
			edtFocus->SetCaption(strText.c_str());
		} else if ('A' <= cAdd && cAdd <= 'Z') {
			if (chkShift->GetIsChecked()) {
				// 大���
				strText += cAdd;
			} else {
				// �����
				strText += cAdd + 32;
			}
			edtFocus->SetCaption(strText.c_str());
		}
	}
}

// add by Philip.Wu  2006-06-07
// 编辑��激活��件��������激活���编辑����
void CLoginScene::_evtAccountFocus(CGuiData* pSender) {
	if (auto edtTemp = dynamic_cast<CEdit*>(pSender); edtTemp) {
		edtFocus = edtTemp;
	}
}

void CLoginScene::ShowKeyboard(bool bShow) {
	frmKeyboard->SetIsShow(bShow);

	//imgLogo1->SetIsShow(! bShow);
	//imgLogo2->SetIsShow(! bShow);
}
