skin_name = "default_new"
skin_folder = 'scripts/lua/'..mod..'/skin/' .. skin_name ..'/'

LG("lua", "Hello World!\n")

function _lg(x)
	LG('lua', x)
end

--for i,v in pairs(_G) do
--	_lg(i..'\n')
--end

dofile('scripts/lua/'..mod..'/fov.lua')
dofile('scripts/lua/'..mod..'/res.lua')
dofile('scripts/lua/'..mod..'/mission/mission.lua')
dofile('scripts/lua/'..mod..'/mission/missioninfo.lua')

dofile('scripts/lua/'..mod..'/table/scripts.lua')
dofile('scripts/lua/'..mod..'/table/stonehint.lua')
dofile('scripts/lua/'..mod..'/table/stonehint_raw.lua')

dofile('scripts/lua/'..mod..'/font.lua')
EXTRA_LoadScreen()
dofile('scripts/lua/'..mod..'/gui.lua')
if EXTRA_IsEditor() then dofile('scripts/lua/'..mod..'/editor.lua') end
dofile(skin_folder..'/init.lua')
dofile('scripts/lua/'..mod..'/filter.lua')

function CreateChaScene_Callback()
-- ´´½¨µÇÂ¼³¡¾°
	SCENE_CREATECHASCENE_CLU_000001 = GetResString("SCENE_CREATECHASCENE_CLU_000001")
	nCreateScene = SN_CreateScene( enumCreateChaScene, SCENE_CREATECHASCENE_CLU_000001, "", FORM_CREATE_CHA, 300, 200, 100, 100 )	
	SN_SetIsShowMinimap( FALSE )
	SN_SetIsShow3DCursor( FALSE )

	-- ³¡¾°UI
	GP_GotoScene( nCreateScene )	-- GotoScene·ÅÔÚ×îºó,ÒòÎªÉæ¼°ÇÐ»»Ä£°åÊ±µÄ±íµ¥³õÊ¼»¯
end

function LoginScene_Callback()
	-- ´´½¨µÇÂ¼³¡¾°
	-- ´´½¨µÇÂ¼³¡¾°
	SCENE_LOGINSCENE_CLU_000001 = GetResString("SCENE_LOGINSCENE_CLU_000001")
	nLoginScene = SN_CreateScene( enumLoginScene, SCENE_LOGINSCENE_CLU_000001, "", FORM_LOGIN, 300, 200, 100, 100 )	
	SN_SetIsShowMinimap( FALSE )
	SN_SetIsShow3DCursor( FALSE )

	-- ³¡¾°UI
	UI_ShowForm( frmLOGO, TRUE )

	GP_GotoScene( nLoginScene )	-- GotoScene·ÅÔÚ×îºó,ÒòÎªÉæ¼°ÇÐ»»Ä£°åÊ±µÄ±íµ¥³õÊ¼»¯
end

function MainScene_Callback()
	SCENE_MAINSCENE_CLU_000001 = GetResString("SCENE_MAINSCENE_CLU_000001")
	local nMainScene = SN_CreateScene( enumWorldScene, SCENE_MAINSCENE_CLU_000001, "garner", FORM_MAIN, 300, 400, 400, 600)
	GP_GotoScene( nMainScene )
end

function SelectChaScene_Callback()
	_lg(tostring(1))
	SCENE_SELECTCHASCENE_CLU_000001 = GetResString("SCENE_SELECTCHASCENE_CLU_000001")
	_lg(tostring(2))
	nScene = SN_CreateScene( enumSelectChaScene, SCENE_SELECTCHASCENE_CLU_000001, "", FORM_SELECT, 100, 10, 2000, 300 )	

	_lg(tostring(5))
	GP_GotoScene( nScene )
	_lg(tostring(3))
	SN_SetIsShowMinimap( FALSE )
	_lg(tostring(4))
	SN_SetIsShow3DCursor( FALSE )
	_lg(tostring(6))
end