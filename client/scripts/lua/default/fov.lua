--TODO: Make all symbols available to next files on list.
C_NORMAL    = 0  --正常模式   
C_NEAR      = 1  --偏近模式  距离比正常模式偏近
C_HIGHSPEED = 2  --高速模式  固定镜头, 不可旋转
C_SHIP      = 3  --海上模式  在船上, 距离很远

--重新读取镜头参数配置文件
function LoadCameraConfig()
  
end
--test = IsRes1024()

if EXTRA_IsRes1024() then
	CameraRangeXY(C_NORMAL, 38, 39.5)
	CameraRangeZ(C_NORMAL, 10.5, 36)
	CameraRangeFOV(C_NORMAL, 17, 20)
	CameraEnableRotate(C_NORMAL, 1)
	CameraShowSize(C_NORMAL, 63, 63) --CameraShowSize1024 used to do same as CameraShowSize

	CameraRangeXY(C_NEAR, 28, 39.5)
	CameraRangeZ(C_NEAR, 10.5, 36)
	CameraRangeFOV(C_NEAR, 17, 20)
	CameraEnableRotate(C_NEAR, 0)
	CameraShowSize(C_NEAR, 51, 51)

	CameraRangeXY(C_HIGHSPEED, 40, 45)
	CameraRangeZ(C_HIGHSPEED, 25, 43)
	CameraRangeFOV(C_HIGHSPEED, 6, 26)
	CameraEnableRotate(C_HIGHSPEED, 0)
	CameraShowSize(C_HIGHSPEED, 63, 63)

	CameraRangeXY(C_SHIP, 40, 45)
	CameraRangeZ(C_SHIP, 25, 43)
	CameraRangeFOV(C_SHIP, 6, 26)
	CameraEnableRotate(C_SHIP, 1)
	CameraShowSize(C_SHIP, 81, 81)
else
	CameraRangeXY(C_NORMAL, 75, 30) 
	CameraRangeZ(C_NORMAL, 20, 60) 
	CameraRangeFOV(C_NORMAL, 25, 30) 
	CameraEnableRotate(C_NORMAL, 1) 
	CameraShowSize(C_NORMAL, 60, 55)

	CameraRangeXY(C_NEAR, 28, 39.5)
	CameraRangeZ(C_NEAR, 10.5, 36)
	CameraRangeFOV(C_NEAR, 17, 20)
	CameraEnableRotate(C_NEAR, 0)
	CameraShowSize(C_NEAR, 34, 34)

	CameraRangeXY(C_HIGHSPEED, 40, 45)
	CameraRangeZ(C_HIGHSPEED, 25, 43)
	CameraRangeFOV(C_HIGHSPEED, 6, 26)
	CameraEnableRotate(C_HIGHSPEED, 0)
	CameraShowSize(C_HIGHSPEED, 42, 42) 

	CameraRangeXY(C_SHIP, 75, 30)
	CameraRangeZ(C_SHIP, 20, 60)
	CameraRangeFOV(C_SHIP, 25, 30)
	CameraEnableRotate(C_SHIP, 1)
	CameraShowSize(C_SHIP, 60, 55)
end